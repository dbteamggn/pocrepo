trigger SetEMInterestedToContact on Activity_Product_Relationship__c (after insert, after update) {
	
    /*
    list<Contact> interestedContacts = new list<Contact>();
    
    
    if(trigger.isInsert && Trigger.isAfter){
        
        for (Activity_Product_Relationship__c newActProdRel : trigger.new){
            
            String newProdName = [select Name from Product2 where Id = :newActProdRel.Fund__c limit 1].Name;
            system.debug('newProdName +++ ' + newProdName);
            if(newProdName != null && newProdName.contains('Emerging Markets')){
                Event eventRecord = [select Id, StartDateTime from Event where Strategies_Funds__c = :newActProdRel.Activity_alias__c limit 1];
                
                system.debug('eventId +++ ' + eventRecord.Id);
                system.debug('eventRecord.Start_Date__c +++ ' + eventRecord.StartDateTime);
                
                if(eventRecord.StartDateTime >= Date.Today().addDays(-90)){
                	list<EventRelation> eventRelations = [select Id, RelationId, EventId from EventRelation where EventId = :eventRecord.Id and IsParent = true and IsWhat = false limit 10000];
                    system.debug('eventRelations +++ ' + eventRelations);
                    if(eventRelations != null && eventRelations.size() > 0 ){
                        for(EventRelation evRlt : eventRelations){
                            Contact con = new Contact();
                            con.Id = evRlt.RelationId;
                            con.Interested_In_EM_Debt_Products__c = true;
                            interestedContacts.add(con);
                            
                        }
                        system.debug('interestedContacts +++ ' + interestedContacts);
                    }    
                }
                
            }
            
            
        }
        
    } else  if (trigger.isUpdate && Trigger.isAfter){
        
        for (Activity_Product_Relationship__c newActProdRel : trigger.new){
			
			Activity_Product_Relationship__c oldActProdRel = Trigger.oldMap.get(newActProdRel.Id);            
            String oldProdName = oldActProdRel.Fund__r.Name;
            String newProdName = newActProdRel.Fund__r.Name;
            
            if(newProdName != null && newProdName.contains('Emerging Markets') && !oldProdName.equalsIgnoreCase(newProdName)){
                
                list<EventRelation> eventRelations = [select Id, RelationId, EventId from EventRelation where EventId = :newActProdRel.Activity_alias__r.Activity_ID__c and IsParent = true and IsWhat = false limit 10000];
                system.debug('eventRelations +++ ' + eventRelations);
                if(eventRelations != null && eventRelations.size() > 0 ){
                    for(EventRelation evRlt : eventRelations){
                        Contact con = new Contact();
                        con.Id = evRlt.RelationId;
                        con.Interested_In_EM_Debt_Products__c = true;
                        interestedContacts.add(con);
                    }
                    system.debug('interestedContacts +++ ' + interestedContacts);
                }
            }
        }
        
    }
    
    update interestedContacts;
    */
}