/***************************************************************************
 Name            :   AccountContactRelation_Trigger
 Description     :   Trigger for AccountContactRelation
 Created By      :   Deloitte
 Release    	 : 	 R2
 Phase  	     :	 P1
 Created On      :   02 Dec 2016
 ---------------------------------------------------------------------------
 Modification Log
 ----------------
 Name            Date            Comments
 -----------     ------------    -------------------------------------------
 Yash             02-12-2016      Initial Release
 ---------------------------------------------------------------------------
 Review Log
 -----------
 Name            Date            Comments
 -----------     ------------    -------------------------------------------

****************************************************************************/
trigger AccountContactRelation_Trigger on AccountContactRelation (before insert, after insert, before update, after update, before delete, after delete, after undelete) {
    new AccountContactRelationTriggerHandler().run();
}