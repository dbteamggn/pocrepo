trigger ContactTrigger on Contact (before insert, before update, before delete,
                                   after insert, after update, after delete, after undelete)
{
    ContactTriggerHandler handler = ContactTriggerHandler.getInstance();

    if (Trigger.isBefore) {

        handler.onBeforeBulk(Trigger.new);

        if (Trigger.isInsert) {

            handler.OnBeforeInsert(Trigger.new);

        } else if (Trigger.isUpdate) {

            handler.OnBeforeUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);

        } else if (Trigger.isDelete) {

            handler.OnBeforeDelete(Trigger.old, Trigger.oldMap);
        }

    } else if (Trigger.isAfter) {

        handler.onAfterBulk(Trigger.new);

        if (Trigger.isInsert) {

            handler.OnAfterInsert(Trigger.new, Trigger.newMap);

        } else if (Trigger.isUpdate) {

            handler.OnAfterUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);

        } else if (Trigger.isDelete) {

            handler.OnAfterDelete(Trigger.old, Trigger.oldMap);

        } else if (Trigger.isUndelete) {

            handler.OnAfterUndelete(Trigger.new);
        }
    }
}