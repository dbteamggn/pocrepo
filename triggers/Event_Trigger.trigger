trigger Event_Trigger on Event (before insert, after insert, before update, after update, before delete, after delete, after undelete) {
    new EventTriggerHandler().run();
}