trigger Account_Trigger on Account (before insert, before update, before delete,
                                   after insert, after update, after delete, after undelete){
    new AccountTriggerHandler().run();
    if(trigger.isInsert && trigger.isBefore){
         Account_Utility.checkDuplicateAccout(trigger.new);
    }
}