/***************************************************************************
 Name            :   Department_Client_Relationship_Trigger
 Description     :   Trigger for Department_Client_Relationship__c
 Created By      :   Deloitte
 Created On      :   17 Sep 2016
 ---------------------------------------------------------------------------
 Modification Log
 ----------------
 Name            Date            Comments
 -----------     ------------    -------------------------------------------
 Hemangini       09-17-2016      Initial Release
 ---------------------------------------------------------------------------
 Review Log
 -----------
 Name            Date            Comments
 -----------     ------------    -------------------------------------------

****************************************************************************/
trigger Department_Client_Relationship_Trigger on Department_Client_Relationship__c (before insert, after insert, before update, after update, before delete, after delete, after undelete) {
    new DeptClientRelationshipTriggerHandler().run();
}