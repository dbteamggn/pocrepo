trigger updateStartAndEndDate on Goals__c (before insert,before update) {
    Organization org=[Select FiscalYearStartMonth From Organization];
    for( Goals__c g:trigger.new){
        if(trigger.isinsert||(trigger.isupdate&&((g.Period__c<>trigger.oldmap.get(g.id).Period__c)||(g.Goal_Period_Starts__c<>trigger.oldmap.get(g.id).Goal_Period_Starts__c)))){
        //update start and end date when period is weekly
            if(g.Period__c=='Weekly'){
                if(g.Goal_Period_Starts__c=='This Week')
                     g.Goal_Start_Date__c=system.today().toStartofWeek()+1;
                else
                    g.Goal_Start_Date__c=system.today().toStartofWeek()+8;
                    
                g.Goal_End_Date__c=g.Goal_Start_Date__c+6;          
                
            }
            //update start and end date when period is monthly
            if(g.Period__c=='Monthly'){
                if(g.Goal_Period_Starts__c=='This Month')
                     g.Goal_Start_Date__c=system.today().toStartOfMonth();
                     
                else
                    g.Goal_Start_Date__c=system.today().toStartOfMonth().addMonths(1);
                   
                g.Goal_End_Date__c=g.Goal_Start_Date__c.addMonths(1)-1;    
            }
             //update start and end date when period is yearly
            if(g.Period__c=='Yearly'){
                if(g.Goal_Period_Starts__c=='Current Year')
                     g.Goal_Start_Date__c=date.newinstance(system.today().year(), 1, 1);
                     
                else
                    g.Goal_Start_Date__c=date.newinstance(system.today().year(), 1, 1).addYears(1);
                   
                g.Goal_End_Date__c=g.Goal_Start_Date__c.addyears(1)-1;    
            }
            //update start and end date when period is quarterly
             if(g.Period__c=='Quarterly'){
                if(g.Goal_Period_Starts__c=='Q1')
                     g.Goal_Start_Date__c=date.newinstance(system.today().year(),org.FiscalYearStartMonth,1);
                else if(g.Goal_Period_Starts__c=='Q2')
                     g.Goal_Start_Date__c=date.newinstance(system.today().year(),org.FiscalYearStartMonth,1).addMonths(3);
                else if(g.Goal_Period_Starts__c=='Q3')
                     g.Goal_Start_Date__c=date.newinstance(system.today().year(),org.FiscalYearStartMonth,1).addMonths(6);
                else
                     g.Goal_Start_Date__c=date.newinstance(system.today().year(),org.FiscalYearStartMonth,1).addMonths(9);
                                   
                     
                g.Goal_End_Date__c=g.Goal_Start_Date__c.addMonths(3)-1;     
                   
            }
        }
    }

}