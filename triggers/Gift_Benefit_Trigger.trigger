trigger Gift_Benefit_Trigger on Gift_Benefit__c (before insert, after insert, before update, after update, before delete, after delete, after undelete) {
    new Gift_Benefit_TriggerHandler().run();
}