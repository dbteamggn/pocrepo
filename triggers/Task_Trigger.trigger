/*----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            Task_Trigger
   Description:     Trigger for Tasks
                    
   Date             Author                             Summary of Changes                  
   -----------      -----------------                  -------------------------------------------------------------------------------------
    08 Jul 2016      Sridhar Gurram                     Initial Release 
    31 Aug 2016      Hemangini                          Updated as per new architecture
------------------------------------------------------------------------------------------------------------------------------------------------------------*/

trigger Task_Trigger on Task (before insert, after insert, before update, after update, before delete, after delete, after undelete) {

    new TaskTriggerHandler().run();
    
}