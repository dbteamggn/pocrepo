trigger OpportunityTeamMember_Trigger on OpportunityTeamMember(before insert, before update, before delete){
        
    OpportunityTeamMember_Utility util = OpportunityTeamMember_Utility.getInstance();
    
    if(trigger.isBefore){
        //before insert 
        if(trigger.isInsert){           
            util.beforeInsert(trigger.new);
        }
        //before update
        else if(trigger.isUpdate){
            util.beforeUpdate(trigger.new, trigger.oldMap);
        }
        //before delete
        else if(trigger.isDelete){
            util.beforeDelete(trigger.old);
        }
    }
}