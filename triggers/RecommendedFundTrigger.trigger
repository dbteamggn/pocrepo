trigger RecommendedFundTrigger on Client_Fund__c (after insert, after update, after delete) {
    new RecommendedFundTriggerHandler().run();

}