trigger Client_Territory_Trigger on Client_Territory__c (before insert, after insert, before delete,after delete,before update, after update) {
    new Client_Territory_TriggerHandler().run();
}