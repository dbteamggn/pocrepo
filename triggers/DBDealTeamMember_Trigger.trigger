trigger DBDealTeamMember_Trigger on DB_Deal_Team_Member__c
(before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    
    DBDealTeamMemberTriggerHandler handler = DBDealTeamMemberTriggerHandler.getInstance();
    
    if(trigger.isInsert && trigger.isBefore) {
        handler.onBeforeInsert(Trigger.new);
    }
    
    if(trigger.isUpdate && trigger.isAfter) {
        handler.onAfterUpdate(Trigger.new);
    }
    
    if(trigger.isDelete && trigger.isBefore) {
        handler.onAfterDelete(Trigger.old);
    }
}