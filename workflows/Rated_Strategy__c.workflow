<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Default_Date_Added</fullName>
        <description>Default Date Added to Today&apos;s Date</description>
        <field>Date_Added__c</field>
        <formula>Today()</formula>
        <name>Default Date Added</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Date Added on Rated Strategy</fullName>
        <actions>
            <name>Default_Date_Added</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When inserting a new Rated Strategy or updating the Current Rating on a Rated Strategy, update Date Added to today&apos;s date</description>
        <formula>OR(ISNEW(), ISCHANGED( Current_Rating__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
