<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Address_Last_Modified_on_Account</fullName>
        <field>Address_Last_Modified_Date__c</field>
        <formula>NOW()</formula>
        <name>Address Last Modified on Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Info_Last_Modified_on_Account</fullName>
        <field>Contact_Info_Last_Modified_Date__c</field>
        <formula>NOW()</formula>
        <name>Contact Info Last Modified on Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Parent_ID_Last_Modified_Date_on_Account</fullName>
        <field>Parent_Last_Modified_Data__c</field>
        <formula>NOW()</formula>
        <name>Parent ID Last Modified Date on Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Phone_Last_Modified_on_Account</fullName>
        <field>Phone_Last_Modified_Date__c</field>
        <formula>NOW()</formula>
        <name>Phone Last Modified on Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Address Last Modified Date field update on Account</fullName>
        <actions>
            <name>Address_Last_Modified_on_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update the Address Last Modified Date on Account. Have to use workflow rather than process builder as PB fails with an error if a duplicate rule is ignored by the user doing the update.</description>
        <formula>OR( ISNEW(), ISCHANGED(Billing_Street_Line_1__c ), ISCHANGED(Billing_Street_Line_2__c ), ISCHANGED(Billing_Street_Line_3__c ), ISCHANGED(Billing_State__c ), ISCHANGED(Billing_PostalCode__c ), ISCHANGED(Billing_County__c), ISCHANGED(Billing_Country__c ), ISCHANGED(Billing_City__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact Info Last Modified Date field update on Account</fullName>
        <actions>
            <name>Contact_Info_Last_Modified_on_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update the Contact Info Last Modified Date on Account. Have to use workflow rather than process builder as PB fails with an error if a duplicate rule is ignored by the user doing the update.</description>
        <formula>OR( ISNEW(), ISCHANGED(Website)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Parent ID Last Modified Date field update on Account</fullName>
        <actions>
            <name>Parent_ID_Last_Modified_Date_on_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update the Parent ID Last Modified Date on Account. Have to use workflow rather than process builder as PB fails with an error if a duplicate rule is ignored by the user doing the update.</description>
        <formula>OR( ISNEW(), ISCHANGED(ParentId)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Phone Last Modified Date field update on Account</fullName>
        <actions>
            <name>Phone_Last_Modified_on_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update the Phone Last Modified Date on Account. Have to use workflow rather than process builder as PB fails with an error if a duplicate rule is ignored by the user doing the update.</description>
        <formula>OR( ISNEW(), ISCHANGED(Fax), ISCHANGED(Phone)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
