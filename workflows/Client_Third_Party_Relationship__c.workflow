<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Custom_Last_Modified_Date_on_CTP</fullName>
        <field>Relation_Last_Modified_Date__c</field>
        <formula>NOW()</formula>
        <name>Update Custom Last Modified Date on CTP</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Last Modified Date on Client Third Party Rel</fullName>
        <actions>
            <name>Update_Custom_Last_Modified_Date_on_CTP</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update external Id field on change of relationship</description>
        <formula>ISCHANGED(External_ID__c) || ISCHANGED(  Client_Name__c ) || ISCHANGED( Third_Party_Name__c ) || ISNEW()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
