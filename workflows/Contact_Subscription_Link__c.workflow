<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Subscription_to_Inactive</fullName>
        <description>Set Contact Subscription Status to Inactive</description>
        <field>Status__c</field>
        <literalValue>Inactive</literalValue>
        <name>Set Subscription to Inactive</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Subscription to Inactive</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Contact_Subscription_Link__c.End_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_Subscription_Link__c.Status__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <description>Set Contact Subscription status to inactive on end date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_Subscription_to_Inactive</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Contact_Subscription_Link__c.End_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
