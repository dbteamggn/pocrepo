<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Opportunity_Stage_Change_Won_n_f_and_finals</fullName>
        <description>Opportunity Stage Change Won n-f and finals</description>
        <protected>false</protected>
        <recipients>
            <recipient>Administrator</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Associate</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Client Service</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Consultant Relations</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Customer Relationship Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>IPO UK</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Investment Team</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Pre-Sales Consultant</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Product Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales Team Member</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Senior Sales</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Stage_Won_NF_Finals</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Stage_change</fullName>
        <description>Opportunity Stage change</description>
        <protected>false</protected>
        <recipients>
            <recipient>Administrator</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Associate</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Client Service</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Consultant Relations</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Customer Relationship Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>IPO UK</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Investment Team</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Pre-Sales Consultant</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Product Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales Team Member</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Senior Sales</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Stage_Change</template>
    </alerts>
    <fieldUpdates>
        <fullName>DB_Change_Record_Type_To_Closed</fullName>
        <field>RecordTypeId</field>
        <lookupValue>DB_Closed_Stage_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>DB Change Record Type To Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DB_Change_Record_Type_To_Closed_Lost</fullName>
        <field>RecordTypeId</field>
        <lookupValue>DB_Closed_Lost_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>DB Change Record Type To Closed Lost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DB_Change_Record_Type_To_Identified</fullName>
        <field>RecordTypeId</field>
        <lookupValue>DB_Identified_Stage_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>DB Change Record Type To Identified</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DB_Change_Record_Type_To_Lead</fullName>
        <field>RecordTypeId</field>
        <lookupValue>DB_Lead_Stage_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>DB Change Record Type To Lead</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DB_Change_Record_Type_To_Mandated</fullName>
        <field>RecordTypeId</field>
        <lookupValue>DB_Mandated_Stage_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>DB Change Record Type To Mandated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DB_Change_Record_Type_To_Pitch</fullName>
        <field>RecordTypeId</field>
        <lookupValue>DB_Pitch_Stage_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>DB Change Record Type To Pitch</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DB_Change_Record_Type_To_Qualified</fullName>
        <field>RecordTypeId</field>
        <lookupValue>DB_Qualified_Stage_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>DB Change Record Type To Qualified</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DB_Set_Stage_to_Identified</fullName>
        <field>StageName</field>
        <literalValue>Identified</literalValue>
        <name>DB Set Stage to Identified</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DB_Set_Stage_to_Lead</fullName>
        <field>StageName</field>
        <literalValue>Lead</literalValue>
        <name>DB Set Stage to Lead</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DB_Set_Standard_Prob_To_Custom_Value</fullName>
        <field>Probability</field>
        <formula>DB_Probability__c</formula>
        <name>DB Set Standard Prob To Custom Value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>DB Deal Record Type is Identified</fullName>
        <actions>
            <name>DB_Set_Stage_to_Identified</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Identified Stage Record Type</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>DB Deal Record Type is Lead</fullName>
        <actions>
            <name>DB_Set_Stage_to_Lead</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Lead Stage Record Type</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>DB Deal Stage is Closed and not Lost</fullName>
        <actions>
            <name>DB_Change_Record_Type_To_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.DB_Deal_Closure_Status__c</field>
            <operation>notEqual</operation>
            <value>Lost</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DB Deal Stage is Identified</fullName>
        <actions>
            <name>DB_Change_Record_Type_To_Identified</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Identified</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DB Deal Stage is Lead</fullName>
        <actions>
            <name>DB_Change_Record_Type_To_Lead</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Lead</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DB Deal Stage is Mandated</fullName>
        <actions>
            <name>DB_Change_Record_Type_To_Mandated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Mandated</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DB Deal Stage is Pitch</fullName>
        <actions>
            <name>DB_Change_Record_Type_To_Pitch</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Pitch</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DB Deal Stage is Qualified</fullName>
        <actions>
            <name>DB_Change_Record_Type_To_Qualified</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Qualified</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DB Probability Is Changed</fullName>
        <actions>
            <name>DB_Set_Standard_Prob_To_Custom_Value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Probability) || ISCHANGED(DB_Probability__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>DB Stage is Closed and Lost</fullName>
        <actions>
            <name>DB_Change_Record_Type_To_Closed_Lost</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.DB_Deal_Closure_Status__c</field>
            <operation>equals</operation>
            <value>Lost</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
