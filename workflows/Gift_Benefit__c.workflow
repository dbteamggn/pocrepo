<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Gift_and_Benefits_Limit_Notifications</fullName>
        <description>Gift and Benefits Limit Notifications</description>
        <protected>false</protected>
        <recipients>
            <recipient>avchoubey@deloitte.com.assetapp</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Gifts_and_Benefits_Limits</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_G_B_Current_Year</fullName>
        <field>Current_Year__c</field>
        <literalValue>1</literalValue>
        <name>Update G&amp;B Current Year</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Update G%26B Current Year</fullName>
        <actions>
            <name>Update_G_B_Current_Year</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>YEAR( Date__c ) = YEAR(TODAY())</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
