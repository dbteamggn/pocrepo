<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Inactive_Date_Change_Field</fullName>
        <description>Updates Inactive_Date_Change__c</description>
        <field>Inactive_Date_Change__c</field>
        <formula>Today()</formula>
        <name>Update Inactive Date Change Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Subscription End Date</fullName>
        <actions>
            <name>Update_Inactive_Date_Change_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Subscription__c.Status__c</field>
            <operation>equals</operation>
            <value>Inactive</value>
        </criteriaItems>
        <description>Set Subscription End Date on Status Change to Inactive</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
