<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Event_Subject</fullName>
        <field>Subject</field>
        <formula>TEXT( Sub_Type__c )  &amp; &apos;_&apos; &amp; TEXT( Type__c ) &amp; &apos;_&apos; &amp; TEXT(  ActivityDate )</formula>
        <name>Update Event Subject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Event_Type</fullName>
        <field>Type__c</field>
        <literalValue>Meeting</literalValue>
        <name>Update Event Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Post default for Event Subject</fullName>
        <actions>
            <name>Update_Event_Subject</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Event.Subject</field>
            <operation>contains</operation>
            <value>Please enter Subject</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Event Type for Outlook Events</fullName>
        <actions>
            <name>Update_Event_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Event.Outlook_Type__c</field>
            <operation>contains</operation>
            <value>Salesforce</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
