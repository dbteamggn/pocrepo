<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>DB_Update_List_With_Client_Participants</fullName>
        <field>Name</field>
        <formula>IF(NOT(ISBLANK(DB_Client_Participant__c)), 
DB_Client_Participant__r.FirstName + &quot; &quot; + 
DB_Client_Participant__r.LastName, &quot;&quot;) + 
IF(NOT(ISBLANK(DB_Client_Participant__c)) &amp;&amp; 
NOT(ISBLANK(DB_Client_Participant_2__c)),&quot;, &quot;,&quot;&quot;) + 
IF(NOT(ISBLANK(DB_Client_Participant_2__c)), 
DB_Client_Participant_2__r.FirstName + &quot; &quot; + 
DB_Client_Participant_2__r.LastName, &quot;&quot;) + 
IF((NOT(ISBLANK(DB_Client_Participant__c)) || 
NOT(ISBLANK(DB_Client_Participant_2__c))) &amp;&amp; 
NOT(ISBLANK(DB_Client_Participant_3__c)),&quot;, &quot;,&quot;&quot;) + 
IF(NOT(ISBLANK(DB_Client_Participant_3__c)), 
DB_Client_Participant_3__r.FirstName + &quot; &quot; + 
DB_Client_Participant_3__r.LastName, &quot;&quot;)</formula>
        <name>DB Update List With Client Participants</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>DB Client Participants Selected</fullName>
        <actions>
            <name>DB_Update_List_With_Client_Participants</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>1=1</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
