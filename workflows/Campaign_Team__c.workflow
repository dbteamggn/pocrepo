<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Campaign_Notification_for_Chatter_User</fullName>
        <description>Campaign Notification for Chatter User</description>
        <protected>false</protected>
        <recipients>
            <field>Chatter_User_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Campaign_Notification_for_Chatter_User</template>
    </alerts>
</Workflow>
