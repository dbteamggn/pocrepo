<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Notification_of_new_task</fullName>
        <description>Email Notification of new task</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/New_Task_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_SubType</fullName>
        <field>Sub_Type__c</field>
        <literalValue>Invitation</literalValue>
        <name>Set Sub Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Sub_Type</fullName>
        <field>Sub_Type__c</field>
        <literalValue>Invitation</literalValue>
        <name>Set Sub Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Type</fullName>
        <field>Type__c</field>
        <literalValue>Email</literalValue>
        <name>Set Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Subject</fullName>
        <description>Update Subject to &quot;Inbound Call&quot;</description>
        <field>Subject</field>
        <formula>&apos;Inbound Call&apos;</formula>
        <name>Update Subject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Task_Type</fullName>
        <field>Type__c</field>
        <literalValue>Email</literalValue>
        <name>Update Task Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>defaultcloseddate</fullName>
        <description>Default the closed date if the task is completed</description>
        <field>Closed_Date__c</field>
        <formula>TODAY()</formula>
        <name>Default Closed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Change subject CTI Call</fullName>
        <actions>
            <name>Update_Subject</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>equals</operation>
            <value>Demo Call Log</value>
        </criteriaItems>
        <description>To change the CTI call task&apos;s subject</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Closed Date on Completion</fullName>
        <actions>
            <name>defaultcloseddate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Default the closed date to today when the task is Completed &amp; the date wasn&apos;t manually entered</description>
        <formula>AND( OR(ISCHANGED(IsClosed),ISNEW()),IsClosed)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Type and Sub Type</fullName>
        <actions>
            <name>Set_SubType</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Set Type and Sub Type at the time of inserting record</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Task Type for Outlook Tasks</fullName>
        <actions>
            <name>Update_Task_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Outlook_Type__c</field>
            <operation>contains</operation>
            <value>Salesforce</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
