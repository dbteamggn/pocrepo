<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>DB_Update_List_With_DB_Participants</fullName>
        <field>Name</field>
        <formula>DB_Participant__r.FirstName + &quot; &quot; +
DB_Participant__r.LastName</formula>
        <name>DB Update List With DB Participants</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>DB Participants Selected</fullName>
        <actions>
            <name>DB_Update_List_With_DB_Participants</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>1=1</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
