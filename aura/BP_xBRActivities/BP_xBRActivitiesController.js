({
	doInit : function(component, event, helper) {
    	var action = component.get("c.getActionName");
        action.setParams({
            "actionId": component.get("v.actionId")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS"){
                component.set("v.actionName", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
        
        var actionBPC = component.get("c.getUsers");
        actionBPC.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS"){
                var opts = [];
                var vals = response.getReturnValue();
                for(var each in vals){
                    var oneOption = {class: "optionClass", label: vals[each].name, value: vals[each].id};
                    opts.push(oneOption);
                }
                component.find("ownerId").set("v.options", opts);
            }
        });
        $A.enqueueAction(actionBPC);
    },
    prev : function(component, event, helper){
        component.set("v.stepNum", component.get("v.stepNum") - 1);
    }, 
    next : function(component, event, helper){
        helper.createActivity(component, 0);
    },
    addAction : function(component, event, helper){
        //Update this with step number for Action component
        helper.createActivity(component, 1);
    },
    addActivity : function(component, event, helper){
        helper.createActivity(component, 2);
    },
    showContactList: function(component, event) {     
        var classValue = event.getSource().get("v.class"); 
        var queryParam;
        if(classValue.indexOf('participant')>0){
            queryParam = "participant";
        } else if(classValue.indexOf('attendee')>0){
            queryParam = "attendee";
        }else{
            queryParam = "all";
        }
        var selectedContact = component.get('v.selectedContact'); 
        var myEvent = $A.get("e.c:NameOnFormClick");
        myEvent.setParams({
            "compNameToHide": "form",
            "compNameToShow": "contactList",
            "selectedContact": selectedContact,
            "queryParam" : queryParam
        });
        myEvent.fire();
    },
    showFundList: function(component, event) {     
        var selectedFund = component.get('v.selectedFund');       
        var myEvent = $A.get("e.c:FundOnFormClick");
        myEvent.setParams({
            "compNameToHide": "form",
            "compNameToShow": "fundList",
            "selectedFunds": selectedFund
        });
        myEvent.fire();
    },
    passData: function(component, event) {
        var selectedContact = event.getParam("selectedContact");
        var allNames='';
        for(var items in selectedContact){
            allNames=allNames+selectedContact[items].name+' ; ';
        }
        component.find("name").set('v.value',allNames);
        component.set('v.selectedContact',selectedContact);
    },
    passFunds: function(component, event) {
        var selectedFunds = event.getParam("selectedFunds");
        var allNames='';
        for(var items in selectedFunds){
            allNames=allNames+selectedFunds[items].name+' ; ';
        }
        component.find("funds").set('v.value',allNames);
        component.set('v.selectedFund',selectedFunds);
    },
    NameOnFormClick : function(component, event) {  
        var compNameToHide = event.getParam("compNameToHide");
        var compNameToShow = event.getParam("compNameToShow");
        var selectedContact = event.getParam("selectedContact");
        var queryParam = event.getParam("queryParam");
        var hideComp = component.find(compNameToHide);
        $A.util.addClass(hideComp, "hide");
        var showComp = component.find(compNameToShow);
        $A.util.removeClass(showComp, "hide");
        var myEvent = $A.get("e.c:passData");   
        myEvent.setParams({
            "selectedContact": selectedContact,
            "queryParam" : queryParam
        });
        myEvent.fire();       
    },
    FundOnFormClick : function(component, event){
        var compNameToHide = event.getParam("compNameToHide");
        var compNameToShow = event.getParam("compNameToShow");
        var selectedFunds = event.getParam("selectedFunds");
        var hideComp = component.find(compNameToHide);
        $A.util.addClass(hideComp, "hide");
        var showComp = component.find(compNameToShow);
        $A.util.removeClass(showComp, "hide");
        var myEvent = $A.get("e.c:passFunds");
        myEvent.setParams({
            "selectedFunds": selectedFunds
        });
        myEvent.fire();
    }
})