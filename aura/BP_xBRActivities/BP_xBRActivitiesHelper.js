({
	createActivity : function(component, nextCase) {
    	var newActivity = new Object();
        newActivity.subject = component.find("subject").get("v.value");
        newActivity.startDateTime = component.find("startDateTimeValue").get("v.value"); 
        newActivity.endDateTime = component.find("endDateTimeValue").get("v.value");
		newActivity.ownerId = component.find("ownerId").get("v.value");
		newActivity.selectedContact = component.get('v.selectedContact');
        newActivity.selectedFund = component.get('v.selectedFund');
		newActivity.whatId = component.get("v.actionId");
        newActivity.description = component.find("description").get("v.value");
        newActivity.gnb = false;
        newActivity.allDayEvent = false;
        console.log(newActivity);
        
        var isError=false;
        
        if( typeof newActivity.subject === 'undefined' || newActivity.subject === null || newActivity.subject == ''){
            component.find("subject").set("v.errors", [{message:"Please enter " + component.find("subject").get("v.label") }]);
            isError=true;
        }else{
            component.find("subject").set("v.errors", null );
        }
        if( typeof newActivity.startDateTime === 'undefined' || newActivity.startDateTime === null ){
            component.find("startDateTimeValue").set("v.errors", [{message:"Please enter " + component.find("startDateTimeValue").get("v.label") }]);
            isError=true;
        }else{
            component.find("startDateTimeValue").set("v.errors", null );
        }
        if( typeof newActivity.endDateTime === 'undefined' || newActivity.endDateTime === null ){
            component.find("endDateTimeValue").set("v.errors", [{message:"Please enter " + component.find("endDateTimeValue").get("v.label") }]);
            isError=true;
        }else{
            component.find("endDateTimeValue").set("v.errors", null );
        }
		if( typeof newActivity.selectedFund === 'undefined' || newActivity.selectedFund === null || newActivity.selectedFund.length==0){
            component.find("funds").set("v.errors", [{message:"Please select Funds "}]);
            isError=true;
        }else{
            component.find("funds").set("v.errors", null );
        }
        if( typeof newActivity.selectedContact === 'undefined' || newActivity.selectedContact === null || newActivity.selectedContact.length==0){
            component.find("name").set("v.errors", [{message:"Please select Contacts "}]);
            isError=true;
        }else{
            component.find("name").set("v.errors", null );
        }
        if(!isError){
            if(newActivity.startDateTime > newActivity.endDateTime){
                component.find("endDateTimeValue").set("v.errors", [{message:"End Date Time should be after Start Date Time"}]);
            	isError=true;
            } else{
                component.find("endDateTimeValue").set("v.errors", null );
            }
        }
        if(!isError){
            var action = component.get("c.createEvent");
            var jsonString = JSON.stringify(newActivity);
            action.setParams({
                "stringInst": jsonString
            });
			action.setCallback(this, function(a){
                if(nextCase == 0){
                    component.set("v.stepNum", component.get("v.stepNum") + 1);
                } else if(nextCase == 1){
                    component.set("v.stepNum", component.get("v.stepNum") - 1);
                } else if(nextCase == 2){
                    helper.resetValues(component);
                }
            });
            $A.enqueueAction(action);
        }
    },
    resetValues : function(component) {
        component.find("subject").set("v.value", null);
        component.find("startDateTimeValue").set("v.value", null); 
        component.find("endDateTimeValue").set("v.value", null);
		component.set('v.selectedContact', []);
        component.set('v.selectedFund', []);
        component.find("description").set("v.value", null);
        component.find("name").set("v.value", "");
        component.find("funds").set("v.value", "");
    }
})