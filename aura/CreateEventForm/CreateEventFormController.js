({
    passData: function(component, event) {
        console.log('inContactForm');
        var selectedContact = event.getParam("selectedContact");
        var allNames='';
        for(var items in selectedContact){
            allNames=allNames+selectedContact[items].name+' ; ';
        }
        console.log(allNames);
        component.find("name").set('v.value',allNames);
        component.set('v.selectedContact',selectedContact);
    },
    passFunds: function(component, event) {
        console.log('inFundForm');
        var selectedFunds = event.getParam("selectedFunds");
        console.log('selectedFunds',selectedFunds);
        var allNames='';
        for(var items in selectedFunds){
            allNames=allNames+selectedFunds[items].name+' ; ';
        }
        console.log(allNames);
        component.find("funds").set('v.value',allNames);
        component.set('v.selectedFund',selectedFunds);
    },
    doInit: function(component, event){
        var startDate=new Date().toISOString();
        var endDate=new Date();
		endDate.setHours(endDate.getHours()+1);
        endDate=endDate.toISOString();
        component.find("startDateTimeValue").set("v.value",startDate);
        component.find("endDateTimeValue").set("v.value",endDate);
    },
    allDayEvent: function(component, event){
        var allDayEvent = component.find("allDayEvent").get("v.value");
        if(allDayEvent){
            $A.util.addClass(component.find("startDateTime"), "hide");
            $A.util.addClass(component.find("endDateTime"), "hide");
            $A.util.removeClass(component.find("startDate"), "hide");
            $A.util.removeClass(component.find("endDate"), "hide");
        }else{
            $A.util.addClass(component.find("startDate"), "hide");
            $A.util.addClass(component.find("endDate"), "hide");
            $A.util.removeClass(component.find("startDateTime"), "hide");
            $A.util.removeClass(component.find("endDateTime"), "hide");
        }
    },
    showContactList: function(component, event) {     
        var classValue = event.getSource().get("v.class"); 
        var queryParam;
        console.log('contactList----First');
        if(classValue.indexOf('participant')>0){
            queryParam = "participant";
        }else if(classValue.indexOf('attendee')>0){
            queryParam = "attendee";
        }else{
            queryParam = "all";
        }
        var selectedContact = component.get('v.selectedContact'); 
        var myEvent = $A.get("e.c:NameOnFormClick");
        myEvent.setParams({
            "compNameToHide": "form",
            "compNameToShow": "contactList",
            "selectedContact": selectedContact,
            "queryParam" : queryParam
        });
        myEvent.fire();
    },
    showFundList: function(component, event) {     
        var selectedFund = component.get('v.selectedFund'); 
        console.log('selectedFund',selectedFund);
        console.log('FundList----First');        
        var myEvent = $A.get("e.c:FundOnFormClick");
        myEvent.setParams({
            "compNameToHide": "form",
            "compNameToShow": "fundList",
            "selectedFunds": selectedFund
        });
        myEvent.fire();
    },
    AddContactClick : function(component, event) {
        console.log('This is in Create Event Form');
        var selectedContact = event.getParam("selectedContact"); 
        console.log('This is in Create Event Form',selectedContact);        
        var myEvent = $A.get("e.c:NameOnFormClick");
        myEvent.setParams({
            "compNameToHide": "contactList",
            "compNameToShow": "form",
            "selectedContact": selectedContact
        });
        console.log(myEvent);
        myEvent.fire();     
    },
    logMeeting: function(component, event){
        console.log('hello');
        var whatId = component.get('v.recordId');
        console.log(whatId);
        var isError=false;
        var addMeeting = new Object();
        addMeeting.subject=component.find("subject").get("v.value");
        if(component.find("allDayEvent").get("v.value")){
            addMeeting.startDateTime=component.find("startDateValue").get("v.value");   
            addMeeting.endDateTime=component.find("endDateValue").get("v.value"); 
        }else{
            addMeeting.startDateTime=component.find("startDateTimeValue").get("v.value"); 
            addMeeting.endDateTime=component.find("endDateTimeValue").get("v.value");
        }      
        addMeeting.allDayEvent=component.find("allDayEvent").get("v.value");
        addMeeting.selectedContact=component.get('v.selectedContact');
        addMeeting.selectedFund=component.get('v.selectedFund');
        addMeeting.location=component.find("location").get("v.value");
        addMeeting.whatId=whatId;
        
        if( typeof addMeeting.subject === 'undefined' || addMeeting.subject === null || addMeeting.subject == ''){
            component.find("subject").set("v.errors", [{message:"Please enter " + component.find("subject").get("v.label") }]);
            isError=true;
        }else{
            component.find("subject").set("v.errors", null );
        }
        if( typeof addMeeting.startDateTime === 'undefined' || addMeeting.startDateTime === null ){
            component.find("startDateValue").set("v.errors", [{message:"Please enter " + component.find("startDateValue").get("v.label") }]);
            isError=true;        
        }else{
            component.find("startDateValue").set("v.errors", null );
        }
        if( typeof addMeeting.endDateTime === 'undefined' || addMeeting.endDateTime === null ){
            component.find("endDateValue").set("v.errors", [{message:"Please enter " + component.find("endDateValue").get("v.label") }]);
            isError=true;            
        }else{
            component.find("endDateValue").set("v.errors", null );
        }
        if( typeof addMeeting.selectedFund === 'undefined' || addMeeting.selectedFund === null || addMeeting.selectedFund.length==0){
            component.find("funds").set("v.errors", [{message:"Please select Funds "}]);
            isError=true;            
        }else{
            component.find("funds").set("v.errors", null );
        }
        if( typeof addMeeting.selectedContact === 'undefined' || addMeeting.selectedContact === null || addMeeting.selectedContact.length==0){
            component.find("name").set("v.errors", [{message:"Please select Contacts "}]);
            isError=true;            
        }else{
            component.find("name").set("v.errors", null );
        }
        if( typeof addMeeting.location === 'undefined' || addMeeting.location === null || addMeeting.location == ''){
            component.find("location").set("v.errors", [{message:"Please enter " + component.find("location").get("v.label") }]);
            isError=true;         
        }else{
            component.find("location").set("v.errors", null );
        }
        console.log(addMeeting);
        if(!isError){
            var action = component.get("c.createEvent");
            console.log(action);
            var newMeetingData = JSON.stringify(addMeeting);
            console.log(newMeetingData);
            action.setParams({
                "param": newMeetingData
            });
            action.setCallback(this, function(a) {
                console.log('recordId',a.getReturnValue());
                var recordId = a.getReturnValue();
                if(typeof sforce !== "undefined" && sforce !== null) {
                    sforce.one.navigateToURL('/' + recordId);
                }else{
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": recordId,
                        "slideDevName": "related"
                    });
                    navEvt.fire();
                }
            });
            $A.enqueueAction(action);
        }
    },
    handleError: function(component, event){
        /* do any custom error handling
         * logic desired here */
        // get v.errors, which is an Object[]
    },
    
    handleClearError: function(component, event) {
        /* do any custom error handling
         * logic desired here */
    }
})