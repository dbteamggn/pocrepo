({
    searchKeyChange: function(component, event, helper) {
        var searchKey=component.find("inputSearchStr").get("v.value");
        console.log("#insideSearchKeyComponent");
        if(searchKey!=null && searchKey!=''){
            var myEvent = $A.get("e.c:SearchKeyChange");
            myEvent.setParams({"searchKey": searchKey});
            myEvent.fire();
        }
    }
})