({
    doInit: function(component, event, helper) {      
        //Fetch the Type picklist values from the Apex controller   
        var fieldName = "Status";
        helper.getPicklist(component,fieldName);
        fieldName = "Type__c";
        helper.getPicklist(component,fieldName);
        var dueDate=new Date().toISOString();
        component.set("v.newTask.ActivityDate",dueDate);
    },
    RefreshSubTypeVals: function(component, event, helper){
        //Reset the sub type picklist to have only None before other values are retrieved
        var lst = new Array();
        //Fetch the Sub-Type picklist values from the Apex controller   
        helper.SubTypeVals(component);
    },
    passData: function(component, event) {
        var selectedContact = event.getParam("selectedContact");
        var allNames='';
        for(var items in selectedContact){
            allNames=allNames+selectedContact[items].name+' ; ';
        }
        console.log(allNames);
        component.find("name").set('v.value',allNames);
        component.set('v.selectedContact',selectedContact);
    },
    showContactList: function(component, event) {     
        var classValue = event.getSource().get("v.class"); 
        var queryParam;
        if(classValue.indexOf('participant')>0){
            queryParam = "participant";
        }else if(classValue.indexOf('attendee')>0){
            queryParam = "attendee";
        }else{
            queryParam = "all";
        }
        var selectedContact = component.get('v.selectedContact');        
        var myEvent = $A.get("e.c:NameOnFormClick");
        myEvent.setParams({
            "compNameToHide": "form",
            "compNameToShow": "contactList",
            "selectedContact": selectedContact,
            "queryParam" : queryParam
        });
        myEvent.fire();
    },
    AddContactClick : function(component, event) {
        console.log('This is in Create Task Form');
        var selectedContact = event.getParam("selectedContact"); 
        console.log('This is in Create Task Form',selectedContact);        
        var myEvent = $A.get("e.c:NameOnFormClick");
        myEvent.setParams({
            "compNameToHide": "contactList",
            "compNameToShow": "form",
            "selectedContact": selectedContact
        });
        console.log(myEvent);
        myEvent.fire();     
    },
    createAddnTask : function(component, event, helper){
        component.set("v.addntask", true);
        component.createtask();
    },
    createTask : function(component, event, helper){
        var newTask = component.get('v.newTask');
        var selectedContact = component.get("v.selectedContact");
        var isError=false;
        if( typeof newTask.Status === 'undefined' || newTask.Status === null || newTask.Status == ''){
            component.find("status").set("v.errors", [{message:"Please enter " + component.find("status").get("v.label") }]);
            isError=true;
            component.set("v.addntask", false);
        }else{
            component.find("status").set("v.errors", null );
        }
        if( typeof newTask.ActivityDate === 'undefined' || newTask.ActivityDate === null ){
            component.find("duedate").set("v.errors", [{message:"Please enter " + component.find("duedate").get("v.label") }]);
            isError=true;    
            component.set("v.addntask", false);
        }else{
            component.find("duedate").set("v.errors", null );
        }
        if( typeof newTask.Type__c === 'undefined' || newTask.Type__c === null || newTask.Type__c === ''){
            component.find("type").set("v.errors", [{message:"Please enter " + component.find("type").get("v.label") }]);
            isError=true;         
            component.set("v.addntask", false);
        }else{
            component.find("type").set("v.errors", null );
        }
        /*if( typeof newTask.Sub_Type__c === 'undefined' || newTask.Sub_Type__c === null || newTask.Sub_Type__c === ''){
            component.find("subtype").set("v.errors", [{message:"Please enter " + component.find("subtype").get("v.label") }]);
            isError=true; 
            component.set("v.addntask", false);
        }else{
            component.find("subtype").set("v.errors", null );
        }*/
        if( typeof selectedContact === 'undefined' || selectedContact === null || selectedContact.length==0){
            component.find("name").set("v.errors", [{message:"Please select Contacts "}]);
            isError=true;   
            component.set("v.addntask", false);
        }else{
            component.find("name").set("v.errors", null );
        }
        console.log(newTask);
        if(!isError){
            var action = component.get("c.TaskCreation");
            console.log(action);
            console.log("FinalSeldContacts",selectedContact);
            var contactIds =[];
            for(var i=0;i< selectedContact.length;i++){
                contactIds[i] = selectedContact[i].id;
            }
            console.log(contactIds);
            action.setParams({
                "newTask": newTask,
                "contactIds": contactIds,
                "recordId": component.get('v.recordId')
            });
            action.setCallback(this, function(a) {
                if(component.get("v.addntask") === false){
                    console.log('recordId',a.getReturnValue());
                    var recordId = a.getReturnValue();
                    if(typeof sforce !== "undefined" && sforce !== null) {
                        console.log("I am in sforce section");
                        sforce.one.navigateToURL('/' + recordId);
                    }else{
                        var navEvt = $A.get("e.force:navigateToSObject");
                        console.log("I am in eforce section",recordId);
                        navEvt.setParams({
                            "recordId": recordId,
                            "slideDevName": "detail"
                        });
                        navEvt.fire();
                    }
                }
                else{
                    /*  if(typeof sforce !== "undefined" && sforce !== null) {
                    console.log("I am in sforce section");
                    sforce.one.navigateToURL('/apex/CreateTaskPage?recordId='+component.get("v.recordId"));
                }else{
                    var navEvt = $A.get("e.force:navigateToURL");
                    console.log("I am in eforce section",recordId);
                    navEvt.setParams({
                        "url": "apex/CreateTaskPage?recordId" + component.get("v.recordId")
                    });
                    navEvt.fire();
                } */ 
                 component.find("status").set("v.value", null );
                 component.find("Subject").set("v.value",null);
                 component.find("duedate").set("v.value", null );                 
                 component.find("type").set("v.value", null );                 
                 component.find("subtype").set("v.value", null );
                 component.find("notes").set('v.value',null);
                 component.find("name").set("v.value", null );
                 
             }
            });
            $A.enqueueAction(action);
        }
    },
    
    
})