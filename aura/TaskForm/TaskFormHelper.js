({
	getPicklist : function(component, fieldName) {
	var action = component.get("c.GetPckLstValues");
	action.setParams({
      "ObjectApi_name": "Task",
      "picklistField": fieldName
    });	
    //Set up the callback
    var self = this;
    action.setCallback(this, function(response) {
        var state = response.getState();
        var lst = response.getReturnValue();
        if (state === "SUCCESS" && lst !== null) {
			var newlst = new Array();
            newlst.push('--None--');
            for(var i=0;i<lst.length;i++){
                if(lst[i] !== '--None--')
                	newlst.push(lst[i]);
            }
           if(fieldName === "Type__c")
            component.set('v.typelist', newlst);	
           else if(fieldName === "Status")
             component.set('v.statuslist', newlst);  
        }
    });
    $A.enqueueAction(action);
  },
    SubTypeVals : function(component) {
        var action = component.get("c.GetDpndntPckLstValues");
        action.setParams({
            "ObjectName": "Task",
            "CtrlField": "Type__c",
            "DependField": "Sub_Type__c",
            "CtrlFieldValue": component.get("v.newTask.Type__c")
        });
     //Set up the callback
    var self = this;
    action.setCallback(this, function(response) {
        var state = response.getState();
        var lst = response.getReturnValue();
        if (state === "SUCCESS" && lst !== null) {
            var newlst = new Array();
            newlst.push('--None--');
            for(var i=0;i<lst.length;i++){
                newlst.push(lst[i]);
            }
            component.set('v.subtypelist', newlst);   
        }
    });
    $A.enqueueAction(action);
  },
})