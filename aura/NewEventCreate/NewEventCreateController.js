({    
    NameOnFormClick : function(component, event) {  
        var compNameToHide = event.getParam("compNameToHide");
        var compNameToShow = event.getParam("compNameToShow");
        var selectedContact = event.getParam("selectedContact");
        var queryParam = event.getParam("queryParam");
        var hideComp = component.find(compNameToHide);
        $A.util.addClass(hideComp, "hide");
        var showComp = component.find(compNameToShow);
        $A.util.removeClass(showComp, "hide");
        var myEvent = $A.get("e.c:passData");
        console.log('contactList----Second');       
        myEvent.setParams({
            "selectedContact": selectedContact,
            "queryParam" : queryParam
        });
        myEvent.fire();       
    },
    FundOnFormClick : function(component, event){
        var compNameToHide = event.getParam("compNameToHide");
        var compNameToShow = event.getParam("compNameToShow");
        var selectedFunds = event.getParam("selectedFunds");
        var hideComp = component.find(compNameToHide);
        $A.util.addClass(hideComp, "hide");
        var showComp = component.find(compNameToShow);
        $A.util.removeClass(showComp, "hide");
        console.log('selectedFundselectedFund',selectedFunds);
        var myEvent = $A.get("e.c:passFunds");
        myEvent.setParams({
            "selectedFunds": selectedFunds
        });
        console.log('fundList----Second');       
        console.log('selectedFundselectedFund',selectedFunds);
        myEvent.fire();
    }
})