({
    searchKeyChange: function(component, event) {
        var searchKey = event.getParam("searchKey");
        var action = component.get("c.findByName");
        action.setParams({
            "searchKey": searchKey
        });
        action.setCallback(this, function(a) {            
            var newResultDataList = a.getReturnValue();
            console.log("#searchKeyChange#this came here");
            
            var resultDataList = component.get("v.tempUsers");
            for(var items in resultDataList){
                newResultDataList.push(resultDataList[items]);
            }
            component.set("v.users", newResultDataList);
            component.find("parentCheckbox").set("v.value",false);
        });
        $A.enqueueAction(action);
    },
    passUsers: function(component, event) {
        var selectedUser = event.getParam("selectedUser");
		console.log("#passUsers#ThisCameHere");
        if(selectedUser.length>0){
            var ids=[];
            for(var items in selectedUser){
                ids.push(selectedUser[items].id);
            }
            var action = component.get("c.findByListId");
            action.setParams({
                "userIds": ids
            });
            action.setCallback(this, function(a) {
                component.set("v.users", a.getReturnValue());
                component.set("v.tempUsers",a.getReturnValue());
            });
            $A.enqueueAction(action);
        }else{
            var action = component.get("c.findAll");
            action.setCallback(this, function(a) {
                component.set("v.users", a.getReturnValue());
            });
            $A.enqueueAction(action);
        }
    },
    onChangeParentCheckbox: function(component, event) {
        console.log("#insideOnChangeParentCheckbox");
        var checkCmp = component.find("parentCheckbox").get("v.value");
        var resultCmp = component.find("childCheckbox");
        for (var i = 0; i < resultCmp.length; i++){
            resultCmp[i].set("v.value",checkCmp);
        }
        var resultDataList=[];
        var resultData=new Object(); 
        for (var i = 0; i < resultCmp.length; i++){
            if(resultCmp[i].get("v.value")){
                resultData = new Object();
                resultData.userId = resultCmp[i].get("v.text");
                resultData.userName = resultCmp[i].get("v.name");
                resultData.userTitle = resultCmp[i].get("v.labelClass");
                resultData.value = true;
                resultDataList.push(resultData);
            }
            component.set("v.tempUsers",resultDataList);
        }
    },
    onChangeChildCheckbox: function(component, event) {
        console.log("#insideOnChangeChildCheckbox");
        var resultCmp = component.find("childCheckbox");
        var resultDataList=[];
        var resultData=new Object(); 
        for (var i = 0; i < resultCmp.length; i++){
            if(resultCmp[i].get("v.value")){
                resultData = new Object();
                resultData.userId = resultCmp[i].get("v.text");
                resultData.userName = resultCmp[i].get("v.name");
                resultData.userTitle = resultCmp[i].get("v.labelClass");
                resultData.value = true;
                resultDataList.push(resultData);
            }
            component.set("v.tempUsers",resultDataList);
        }
    },
    showSelected: function(component, event) {
        console.log("#showSelected#ThisCameHere");
        var resultCmp = component.find("childCheckbox");
        var resultDataList=[];
        var resultData=new Object();
        if(typeof resultCmp !== 'undefined' && resultCmp !== null ){
            for (var i = 0; i < resultCmp.length; i++){
                if(resultCmp[i].get("v.value")){
                    resultData = new Object(); 
                    resultData.id = resultCmp[i].get("v.text");
                    resultData.name = resultCmp[i].get("v.name");
                    resultDataList.push(resultData);
                    resultCmp[i].set("v.value",false);
                }
            }
        }
        console.log(resultDataList);
        component.find("parentCheckbox").set("v.value",false);           
        var selectedUser = resultDataList;        
        var myEvent = $A.get("e.c:NameOnFormClick");
        myEvent.setParams({
            "compNameToHide": "userList",
            "compNameToShow": "form",
            "selectedContact": selectedUser,
            "queryParam" : ''
        });
        myEvent.fire();
    },
})