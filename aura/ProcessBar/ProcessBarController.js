({
	doInit : function(component, event, helper) {
		var inactCnt = component.get("v.inactiveProcesses");
        var actCnt = component.get("v.activeProcesses");
        if(actCnt.length != 0 || inactCnt.length != 0){
            component.set("v.widthVal", (100 * (actCnt.length - 1))/(actCnt.length + inactCnt.length - 1));
        }
	}
})