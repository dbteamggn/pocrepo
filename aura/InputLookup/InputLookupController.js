({
    doInit :function(component, event, helper) {
        helper.doInit(component);
    },
    Search : function(component, event, helper) {
        helper.doSearch(component);
    },
    refreshComponent : function(component, event, helper){
        //This is only for GBE scenario and shouldn't be used in other input lookups
        if(component.get('v.type') == "User" && event.getParam("searchString") !== null && event.getParam("searchString") !== ""){
            component.set("v.value", event.getParam("searchString"));
            var actionV = component.get("c.getCurrentValue");
            var self = this;
            actionV.setParams({
                'type' : component.get('v.type'),
                'value' : event.getParam("searchString"),
            });
            
            actionV.setCallback(this, function(a) {
                if(a.error && a.error.length){
                    return $A.error('Unexpected error: '+a.error[0].message);
                }
                var result = a.getReturnValue();
                component.set('v.searchString',result || '');
            });
            $A.enqueueAction(actionV);
        }
    },
    AddMatch :function(component, event, helper) {
        component.set("v.value", event.currentTarget.id);
        
        var matches = component.get("v.matches");
        for(var i=0; i<matches.length; i++) {
            if (matches[i].id ==event.currentTarget.id){
                component.set("v.searchString", matches[i].chosenName);
                continue;
            }
        }

        // Hide the Lookup List
        var lookupList = component.find("lookupList");
        $A.util.addClass(lookupList, 'slds-hide');
    },
    ClearSearch :function(component, event, helper) {
        var lookupList = component.find('lookupList');
        var clearPill = component.find('clear-pill');
        var searchPill = component.find('search-pill');
        
        // Hide the lookuplist
        $A.util.addClass(lookupList, 'slds-hide');
        $A.util.addClass(clearPill, 'slds-hide');        
        $A.util.removeClass(searchPill, 'slds-hide');
        component.set('v.searchString', null);
        component.set('v.value', null);

    }
})