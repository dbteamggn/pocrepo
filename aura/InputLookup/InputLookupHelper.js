({
	/**
     * Perform the SObject search via an Apex Controller
     */
    doSearch : function(component) {       
        
        // Get the search string, input element and the selection container
        var searchString = component.get('v.searchString');
        console.log('searchString-->',searchString);
        var lookupList = component.find('lookupList');
        var clearPill = component.find('clear-pill');
        var searchPill = component.find('search-pill');

        // We need at least 2 characters for an effective search
        if (typeof searchString === 'undefined' || searchString.length < 2)
        {
            // Hide the lookuplist
            $A.util.addClass(lookupList, 'slds-hide');
            $A.util.addClass(clearPill, 'slds-hide');        
        	$A.util.removeClass(searchPill, 'slds-hide');
            return;
        }
 
        // Show the lookuplist
        $A.util.removeClass(lookupList, 'slds-hide');        
        $A.util.removeClass(clearPill, 'slds-hide');        
        $A.util.addClass(searchPill, 'slds-hide');

        // Create an Apex action
        var action = component.get('c.getResults');
 
        // Mark the action as abortable, this is to prevent multiple events from the keyup executing
        action.setAbortable();
        
        // Set the parameters
        action.setParams({
            'objectName' : component.get('v.type'),
            'idFieldName' : component.get('v.idFieldName'),
            'searchFields' : component.get('v.searchFields'),
            'sortFields' : component.get('v.sortFields'),
            'searchValue' : searchString,
            'additionalWhereClause' : component.get('v.additionalWhereClause'),
            'limitValue' : component.get('v.limitValue'),
            'returnFormatStringDisplay' : component.get('v.returnFormatStringDisplay'),
            'fieldStartToken' : component.get('v.fieldStartToken'),            
            'fieldEndToken' : component.get('v.fieldEndToken'),
            'returnFormatStringMatches' : component.get('v.returnFormatStringMatches'),
            'excludeIdFieldName' : component.get('v.excludeIdFieldName'),
            'excludeIds' : component.get('v.excludeIds')
        });
                           
        // Define the callback
        action.setCallback(this, function(response) {
            var state = response.getState();
            // Callback succeeded
            if (component.isValid() && state === "SUCCESS")
            {
                // Get the search matches
                var result = response.getReturnValue();
 
                // an array that will be populated with substring matches
                var matches = [];
                
                var matches = JSON.parse(result);
                
                component.set('v.matches', matches);
                
            }
            else if (state === "ERROR") // Handle any error by reporting it
            {
                var errors = response.getError();
                 
                if (errors) 
                {
                    if (errors[0] && errors[0].message) 
                    {
                        alert(errors[0].message);
                    }
                }
                else
                {
                    Alert('Unknown error.');
                }
            }
        });
         
        // Enqueue the action                  
        $A.enqueueAction(action);                
    },
    doInit : function(component) {
        
        if (component.get("v.ObjectLabel") == null){
            // Create an Apex action
            var action = component.get('c.getObjectSearchLabel');
            action.setParams({
                'objectName' : component.get('v.type')
            });
                               
            // Define the callback
            action.setCallback(this, function(response) {
                var state = response.getState();
                // Callback succeeded
                console.log('Response from doInit in input lookup ---> ',response.getReturnValue());
                if (component.isValid() && state === "SUCCESS")
                {
                    // Get the label of the object
                    var result = response.getReturnValue();
     
                    component.set('v.ObjectLabel', result);
                    
                }
                else if (state === "ERROR") // Handle any error by reporting it
                {
                    var errors = response.getError();
                     
                    if (errors) 
                    {
                        if (errors[0] && errors[0].message) 
                        {
                            alert(errors[0].message);
                        }
                    }
                    else
                    {
                        Alert('Unknown error.');
                    }
                }
            });
             
            // Enqueue the action                  
            $A.enqueueAction(action); 
        }
        
        var actionV = component.get("c.getCurrentValue");
        var self = this;
        actionV.setParams({
            'type' : component.get('v.type'),
            'value' : component.get('v.value'),
        });
        
        actionV.setCallback(this, function(a) {
            if(a.error && a.error.length){
                return $A.error('Unexpected error: '+a.error[0].message);
            }
            var result = a.getReturnValue();
            component.set('v.searchString',result || '');
        });
        $A.enqueueAction(actionV);
    }
})