({    
    NameOnFormClick : function(component, event) {  
        var compNameToHide = event.getParam("compNameToHide");
        var compNameToShow = event.getParam("compNameToShow");
        var selectedContact = event.getParam("selectedContact");
        var queryParam = event.getParam("queryParam");
        var hideComp = component.find(compNameToHide);
        $A.util.addClass(hideComp, "hide");
        var showComp = component.find(compNameToShow);
        $A.util.removeClass(showComp, "hide");
        var myEvent = $A.get("e.c:passData");     
        myEvent.setParams({
            "selectedContact": selectedContact,
            "queryParam" : queryParam
        });
        myEvent.fire();       
    },
	doInit: function(component){
        var action = component.get("c.getLoggedInUserId");
    	action.setCallback(this, function(response){
            var state = response.getState();
            component.set("v.ownerId", response.getReturnValue());
            var myEvent = $A.get("e.c:InputLookupEvt");
            myEvent.setParams({
                "searchString": response.getReturnValue()
            });
            myEvent.fire();
       });
       $A.enqueueAction(action);
    },
})