({
	doInit: function(component, event, helper) {
		var account = component.get('v.account');
        console.log('account',account);
		var childAccounts = account.childAccounts || [];
	
		$A.createComponent(
			'c:AccountHierarchyMenu',
			{
				accounts: childAccounts,
				configuration: component.get('v.configuration')
			},
			function(menu){
				component.set('v.menu', menu);
			}
		);
	},

	toggleChildAccounts: function(component, event, helper){
		var expanded = component.get('v.expanded');
		var childAccountsLoaded = component.get('v.childAccountsLoaded') || false;

		if(!childAccountsLoaded){
			helper.loadChildAccounts(component);
		} else {
			component.set('v.expanded', !expanded);
		}
	},

	expandAll: function(component, event, helper){
		var childAccountsLoaded = component.get('v.childAccountsLoaded') || false;

		if(!childAccountsLoaded){
			helper.loadChildAccounts(component);
		} else {
			component.set('v.expanded', true);
			component.get('v.menu').expandAll();
		}
	},

	collapseAll: function(component, event, helper){
		component.set('v.expanded', false);
		component.get('v.menu').collapseAll();
	},

	navigateToAccount: function(component, event, helper){
		//var account = component.get('v.account');
		//var accountId = account.account.Id;
        var accountId = event.target.getAttribute('data-index');
        console.log('accountID',accountId);
		var visualforceContext = component.get('v.configuration.visualforceContext');
		var linkBehavior = component.get('v.configuration.linkBehavior') || 'none';

		if(linkBehavior == 'navigate'){
            if(typeof sforce !== "undefined" && sforce !== null){
                
            }else{
                if(visualforceContext){
                    window.top.location = '/' + accountId;
    
                } else {
                    var navEvt = $A.get("e.force:navigateToSObject");
                    console.log(navEvt);
                    navEvt.setParams({
                      "recordId": accountId
                    });
                    navEvt.fire();
                }
            }
		} else if(linkBehavior == 'toggle'){
			component.loadChildAccounts();
		}

	    return false;
	}

})