({
	loadChildAccounts: function(component) {
		var account = component.get('v.account');
        var value1 = component.get('v.configuration.thirdParty');
		var accountId = account.account.Id;
		var self = this;
		var action = component.get('c.getAccounts');
		component.set('v.showLoadingChildAccounts', true);

		action.setParams({
			parentAccountId: accountId,
            value: value1
		});
		action.setCallback(this, function(response){
			if(component.isValid()){
				$A.createComponent(
					'c:AccountHierarchyMenu',
					{
						accounts: response.getReturnValue(),
						configuration: component.get('v.configuration'),
						childAccountsLoaded: false
					},
					function(newMenu){
						if (component.isValid()) {
							component.set('v.menu', newMenu);
							component.set('v.expanded', true);
							component.set('v.childAccountsLoaded', true);
							component.set('v.showLoadingChildAccounts', false);
						}
					}
				);
			}
		});
		$A.enqueueAction(action);
	}
})