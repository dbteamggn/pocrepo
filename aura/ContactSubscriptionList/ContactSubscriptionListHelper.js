({
    getSubscriptions : function(component,event, page) {
        page= page||1;
        var searchKey =component.get('v.searchText');
        var action = component.get("c.getSubscriptionsByName");
        
        var recordId = component.get("v.recordId");
        var typeParam = component.get("v.type");
        var addList = component.get('v.addSubscriptions');
        var delList = component.get('v.delSubscriptions');
        action.setParams({
            "searchKey": searchKey,
            "recordId" : recordId,
            "pageNumber":page
        });
        action.setCallback(this, function(a) {            
            var newResultDataList=a.getReturnValue();
            var length = newResultDataList.subscriptionResultList.length;
            var subscriptionList = newResultDataList.subscriptionResultList;
            console.log('length ---> ', length);
            if(! component.get("v.isUpdateFailed")){
            for(var j=0; j<subscriptionList.length; j++ ){
                if(addList.indexOf(subscriptionList[j].subscriptionId)>-1){
                    subscriptionList[j].value = true;
                }else if(delList.indexOf(subscriptionList[j].subscriptionId)>-1){
                    subscriptionList[j].value = false;
                }
            }
                }
            var total = newResultDataList.total == 0 ? 1:newResultDataList.total;
            component.set("v.subscriptions", subscriptionList);
            component.set("v.page", newResultDataList.total == 1  ? 1:newResultDataList.page);
            component.set("v.total", newResultDataList.total);
            component.set("v.pages", (Math.ceil(total/newResultDataList.pageSize)));
        });
        $A.enqueueAction(action);
    },
    getAllSubscriptions: function(component,event,page){
        page= page||1;      
        var action = component.get("c.getAllSubscriptions");
        var recordId = component.get("v.recordId");
        var typeParam = component.get("v.type");
        var addList = component.get('v.addSubscriptions');
        var delList = component.get('v.delSubscriptions');
        action.setParams({            
            "recordId" : recordId,
            "pageNumber":page
        });
        action.setCallback(this, function(a) {            
            var newResultDataList = a.getReturnValue();
            var length = newResultDataList.subscriptionResultList.length;
            var subscriptionList = newResultDataList.subscriptionResultList;
            var total =newResultDataList.total == 0 ? 1: newResultDataList.total;
            console.log('response is ===> ', subscriptionList);
            if(length > 0){
                
            for(var j=0; j<subscriptionList.length; j++ ){
                if(addList.indexOf(subscriptionList[j].subscriptionId)>-1){
                    subscriptionList[j].value = true;
                }else if(delList.indexOf(subscriptionList[j].subscriptionId)>-1){
                    subscriptionList[j].value = false;
                }
            
                    }
            }
            
            component.set("v.subscriptions", subscriptionList);
            component.set("v.page", total == 0 ? 1:newResultDataList.page);
            component.set("v.total", total);
            component.set("v.pages", (Math.ceil(total/newResultDataList.pageSize)));            
        });
        $A.enqueueAction(action);
        
    },
    updateSubscription: function(component, event){        
        var RecordId=component.get('v.recordId');
        var action = component.get('c.updateContactSubscription');
        var subscritions = component.get('v.subscriptions');
        var addList = component.get('v.addSubscriptions');
        action.setParams({
            "toAdd": addList,
            "toDelete": component.get('v.delSubscriptions'),
            "recordId" : RecordId
        });
        action.setCallback(this,function(response){
            var subscribed = response.getReturnValue();
            var message = component.find('errorMessageDiv');
            var warningMessage = component.find('warningMessageDiv');
            if(subscribed == 'true'){
                $A.get('e.force:refreshView').fire();
                $A.util.removeClass(warningMessage, 'warningMessageClass');
                $A.util.addClass( warningMessage,'invisible');
                component.set("v.isUpdateFailed", false);
            }else if (subscribed == 'false'){
                for(var i= 0; i< subscritions.length; i++){
           		$A.util.removeClass(document.getElementById(subscritions[i].subscriptionId),"warningText");
                }                         
                $A.util.removeClass(warningMessage, 'warningMessageClass');
                $A.util.addClass( warningMessage,'invisible');
                
                $A.util.removeClass(message, 'invisible');
                $A.util.addClass(message, 'errorMessageClass'); 
                component.set("v.isUpdateFailed", true);
               	  
            }
        });
        $A.enqueueAction(action);
    },
    verifyCountries : function(component, event){
        var action = component.get('c.verifyCountries');
        var subscriptionId = event.getSource().get('v.text');
        var rowId = document.getElementById(subscriptionId);
        var subRecord = component.find("subscriptionRecord");
        console.log('sub record ---->',subRecord);

        var subscriptionCountries = event.getSource().get('v.name');
        console.log('selected row Id value is ----> ',rowId);
        action.setParams({
            "subscriptionId":subscriptionId,
            "recordId":component.get('v.recordId'),
            "limitedCountries":subscriptionCountries
        });
        action.setCallback(this, function(response){
            var countries = response.getReturnValue();
            var message = component.find('warningMessageDiv');
            if(countries.countriesMatched =='false'){
                $A.util.addClass(message, 'warningMessageClass');
                $A.util.addClass(rowId,'warningText');
            }  
        });
        $A.enqueueAction(action);
    },
})