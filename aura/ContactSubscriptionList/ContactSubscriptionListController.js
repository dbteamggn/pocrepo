/* The controller performs below actions,
     * getSubscriptionDetails
     * getSubscriptionDetailsByName
     * updateSubscription 
*/

({    
    doInit: function(component, event, helper){
       helper.getAllSubscriptions(component);
       component.set('v.textSearched', false);
    },
    searchKeyChange: function(component, event, helper) {
        component.set('v.searchText',  event.getParam("searchKey"));
        component.set('v.textSearched', true);
       // component.set('v.contact','');
        $A.util.removeClass(component.find('errorMessageDiv'), 'errorMessageClass');
        $A.util.addClass(component.find('errorMessageDiv'), 'invisible');
        $A.util.removeClass(component.find('warningMessageDiv'), 'warningMessageClass');
        helper.getSubscriptions(component);
    },
    onChangeParentCheckbox: function(component, event) {        
    },
    onChangeChildCheckbox: function(component, event,helper) {
        
        var ListToAdd=component.get('v.addSubscriptions');
        var ListToDelete=component.get('v.delSubscriptions');
        var subscriptionId = event.getSource().get('v.text');
        
        var rowId = document.getElementById(subscriptionId);
        if(event.getSource().get('v.value')){
            var index = ListToDelete.indexOf(subscriptionId);
            if(index > -1){
                ListToDelete.splice(index,1);
                $A.util.addClass(component.find('warningMessageDiv'), 'invisible');
                
            }
            ListToAdd.push(event.getSource().get('v.text'));
            helper.verifyCountries(component, event);
        }else{
            var index = ListToAdd.indexOf(event.getSource().get('v.text'));
            if(index > -1){
                ListToAdd.splice(index,1);
            }
            ListToDelete.push(event.getSource().get('v.text'));
            
        }
        component.set('v.addSubscriptions',ListToAdd);
        component.set('v.delSubscriptions',ListToDelete);
        console.log('ListToAdd',ListToAdd);
        console.log('ListToDelete',ListToDelete);
    },
    updateSubscription: function(component, event, helper) {
        var resultCmp = component.find("childCheckbox");
        var queryParam = component.get("v.queryParam");      
        var resultDataList=[];
        var resultData=new Object();   
        helper.updateSubscription(component,event);
        
    },
    getSubscriptionDetailsByName : function (component, event, helper){
        var page = component.get("v.page") || 1;
        var direction = event.getParam("direction");
        page = direction === "previous" ? (page - 1) : (page + 1);
        helper.getSubscriptions(component, event,page);
    },
    getSubscriptionDetails : function (component, event, helper){
        var page = component.get("v.page") || 1;
        var direction = event.getParam("direction");
        page = direction === "previous" ? (page - 1) : (page + 1);
        helper.getAllSubscriptions(component, event,page);
    },
    navigateToSubscription: function(component, event, helper){
        var subscriptionId = event.target.getAttribute('data-index');
        console.log('subscriptionId ',subscriptionId);
        var visualforceContext = component.get('v.configuration.visualforceContext');
        var linkBehavior = component.get('v.configuration.linkBehavior') || 'none';
        
        if(typeof sforce !== "undefined" && sforce !== null){
        }else{
            if(visualforceContext){
                window.top.location = '/' + subscriptionId;
                
            } else {
                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": subscriptionId
                });
                navEvt.fire();
            }
        }
        
        return false;
    }
})