({
	setup : function(component, event, helper) {
        console.log('=======');
		new Highcharts.Chart({
            chart: {
                renderTo: component.find("chart").getElement(),
                type: 'line'
            },
            title: {
                text: 'AUM'
            },
            xAxis: {
                categories: ['January', 'February', 'March','April','May','June','July']},
            yAxis: {
                title: {
                    text: 'AUM By Month'
                }
            },
            
            series: [{
                name: 'AUM',
                data: [500000,300000,700000,850000,600000,800000,900000]
            }]
        });
         console.log('%%%%%%');
    }
})