({
    passData: function(component, event) {
        console.log('inContactForm');
        var selectedContact = event.getParam("selectedContact");
        var allNames='';
        for(var items in selectedContact){
            allNames=allNames+selectedContact[items].name+' ; ';
        }
        console.log(allNames);
        component.find("name").set('v.value',allNames);
        component.set('v.selectedContact',selectedContact);
    },
    doInit: function(component, event, helper){
        var startDate=new Date().toISOString();
        var endDate=new Date();
		endDate.setHours(endDate.getHours()+1);
        endDate=endDate.toISOString();
        component.find("startDateTimeValue").set("v.value",startDate);
        component.find("endDateTimeValue").set("v.value",endDate);
        helper.getPicklistValues(component);
    },
    showContactList: function(component, event) {     
        var classValue = event.getSource().get("v.class"); 
        var queryParam;
        console.log('contactList----First');
        if(classValue.indexOf('participant')>0){
            queryParam = "participant";
        }else if(classValue.indexOf('attendee')>0){
            queryParam = "attendee";
        }else{
            queryParam = "all";
        }
        var selectedContact = component.get('v.selectedContact'); 
        var myEvent = $A.get("e.c:NameOnFormClick");
        myEvent.setParams({
            "compNameToHide": "form",
            "compNameToShow": "contactList",
            "selectedContact": selectedContact,
            "queryParam" : queryParam
        });
        myEvent.fire();
    },
    AddContactClick : function(component, event) {
        console.log('This is in Create Event Form');
        var selectedContact = event.getParam("selectedContact"); 
        console.log('This is in Create Event Form',selectedContact);        
        var myEvent = $A.get("e.c:NameOnFormClick");
        myEvent.setParams({
            "compNameToHide": "contactList",
            "compNameToShow": "form",
            "selectedContact": selectedContact
        });
        console.log(myEvent);
        myEvent.fire();     
    },
    logMeeting: function(component, event){
        console.log('hello');
        //alert(component.find("subject").get("v.value"));
        var whoId = component.get('v.recordId');
        var ownerId = component.get('v.ownerId');
        console.log(whoId);
        console.log(ownerId);
        var isError=false;
        var addMeeting = new Object();
        addMeeting.subject=component.find("subject").get("v.value");
        addMeeting.startDateTime=component.find("startDateTimeValue").get("v.value"); 
        addMeeting.endDateTime=component.find("endDateTimeValue").get("v.value");
        addMeeting.selectedContact=component.get('v.selectedContact');
        addMeeting.whoId = whoId;
        addMeeting.OwnerId = ownerId;
        addMeeting.type=component.find("type").get("v.value");
        addMeeting.subType=component.find("subtype").get("v.value");
        addMeeting.amt=component.find("amount").get("v.value");
        addMeeting.currencyType=component.find("currency").get("v.value");
        addMeeting.givenReceived=component.find("givenreceived").get("v.value");
        addMeeting.description=component.find("description").get("v.value");
        addMeeting.gnb=true;
        addMeeting.costType = 'Per Head';
        
        if( typeof addMeeting.subject === 'undefined' || addMeeting.subject === null || addMeeting.subject == ''){
            component.find("subject").set("v.errors", [{message:"Please enter " + component.find("subject").get("v.label") }]);
            isError=true;
        }else{
            component.find("subject").set("v.errors", null );
        }
        if( typeof addMeeting.startDateTime === 'undefined' || addMeeting.startDateTime === null ){
            component.find("startDateValue").set("v.errors", [{message:"Please enter " + component.find("startDateValue").get("v.label") }]);
            isError=true;        
        }else{
            component.find("startDateValue").set("v.errors", null );
        }
        if( typeof addMeeting.endDateTime === 'undefined' || addMeeting.endDateTime === null){
            component.find("endDateValue").set("v.errors", [{message:"Please enter " + component.find("endDateValue").get("v.label") }]);
            isError=true;            
        }else{
            component.find("endDateValue").set("v.errors", null );
        }
        if( typeof addMeeting.amt === 'undefined' || addMeeting.amt === null || isNaN(addMeeting.amt)){
            component.find("amount").set("v.errors", [{message:"Please enter a valid number for " + component.find("amount").get("v.label")}]);
			isError = true;
        } else{
            component.find("amount").set("v.errors", null);
        }
        if( typeof addMeeting.selectedContact === 'undefined' || addMeeting.selectedContact === null || addMeeting.selectedContact.length==0){
            component.find("name").set("v.errors", [{message:"Please select Contacts "}]);
            isError=true;            
        }else{
            component.find("name").set("v.errors", null );
        }
        if( !component.find("iAgree").get("v.value")){
            component.find("iAgree").set("v.errors", [{message:"Please confirm that you agree with the previous statement before proceeding"}]);
            isError=true;            
        }else{
            component.find("iAgree").set("v.errors", null );
        }
        console.log(addMeeting);
        if(!isError){
            var action = component.get("c.createEvent");
            console.log(action);
            var newMeetingData = JSON.stringify(addMeeting);
            console.log(newMeetingData);
            action.setParams({
                "param": newMeetingData
            });
            action.setCallback(this, function(a) {
                console.log('recordId',a.getReturnValue());
                var recordId = a.getReturnValue();
                if(typeof sforce !== "undefined" && sforce !== null) {
                    sforce.one.navigateToURL('/' + recordId);
                }else{
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": recordId,
                        "slideDevName": "related"
                    });
                    navEvt.fire();
                }
            });
            $A.enqueueAction(action);
        }
    },
    handleError: function(component, event){
        /* do any custom error handling
         * logic desired here */
        // get v.errors, which is an Object[]
    },
    
    handleClearError: function(component, event) {
        /* do any custom error handling
         * logic desired here */
    }
})