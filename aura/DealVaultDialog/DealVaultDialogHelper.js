({
    navigateTo: function(component, recId) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": recId
        });
        navEvt.fire();
    },
    
    updateDealWithVaultId: function(component, dealId, vaultId) {
        var action = component.get("c.updateDealWithVaultId");
        action.setParams({
            "dealId": dealId,
            "vaultId": vaultId
        });
        action.setCallback(this, function(response){
        });
        $A.enqueueAction(action);
    }
})