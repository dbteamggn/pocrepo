({
    doInit : function(component, event, helper) {
        component.find("dealRecordCreator").getNewRecord(
            "Opportunity",
            "0120v0000009yVKAAY",
            false,
            $A.getCallback(function() {               
                component.set("v.newDeal.StageName", "Lead");               
                var rec = component.get("v.newDealRecord");
                var error = component.get("v.newDealError");
                if (error || (rec === null)) {
                    console.log("Error initializing record template: " + error);
                    return;
                }
                else {
                    console.log("Record template initialized: " + rec.sobjectType);
                }
            })
        );
        
        component.find("vaultRecordCreator").getNewRecord(
            "Vault__c",
            null,
            false,
            $A.getCallback(function() {                           
                var rec = component.get("v.newVaultRecord");
                var error = component.get("v.newVaultError");
                if (error || (rec === null)) {
                    console.log("Error initializing record template: " + error);
                    return;
                }
                else {
                    console.log("Record template initialized: " + rec.sobjectType);
                }
            })
        );
    },
    
    cancelDialog: function(component, event, helper) {
        var homeEvt = $A.get("e.force:navigateToObjectHome");
        homeEvt.setParams({
            "scope": "Opportunity"
        });
        homeEvt.fire();
    },
    
    saveRecords : function(component, event, helper) {
        var vaultRecId;
        component.find("vaultRecordCreator").saveRecord(function(result) {
            console.log(result.state);
            var resultsToast = $A.get("e.force:showToast");
            if (result.state === "SUCCESS") {
                vaultRecId = result.recordId;
                console.log(vaultRecId);
            } else if (result.state === "ERROR") {
                console.log('Error: ' + JSON.stringify(result.error));
            } else {
                console.log('Unknown problem, state: ' + result.state + ', error: ' + JSON.stringify(result.error));
            }
        });
        
        component.find("dealRecordCreator").saveRecord(function(result) {
            console.log(result.state);
            var resultsToast = $A.get("e.force:showToast");
            if (result.state === "SUCCESS") {
                resultsToast.setParams({
                    title: "Saved",
                    message: "The record was saved.",
                    type: "Success"
                });
                resultsToast.fire();
                var dealRecId = result.recordId;
                helper.updateDealWithVaultId(component, dealRecId, vaultRecId);
                helper.navigateTo(component, dealRecId);
            } else if (result.state === "ERROR") {
                console.log('Error: ' + JSON.stringify(result.error));
                resultsToast.setParams({
                    "title": "Error",
                    "message": "There was an error saving the record: " + JSON.stringify(result.error)
                });
                resultsToast.fire();
            } else {
                console.log('Unknown problem, state: ' + result.state + ', error: ' + JSON.stringify(result.error));
            }
        });
    }
})