({
    
	gotoList : function (component, event, helper) {
        var action = component.get("c.getListViews");
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('clicked');
            if (state === "SUCCESS") {
                var listviews = response.getReturnValue();
                console.log(listviews.Id);
                var navEvent = $A.get("e.force:navigateToList");
                navEvent.setParams({
                    "listViewId": listviews.Id,
                    "listViewName": listviews.Name,
                    "scope": "Contact"
                });
                navEvent.fire();
            }
        });
        $A.enqueueAction(action);
	}
    
})