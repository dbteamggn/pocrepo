({
	doInit : function(component, event, helper) {
        helper.setUIValues(component);
	},
    updateBPRec : function(component, event, helper) {
        var recordId = component.get("v.recordId");
        var action = component.get("c.updateBP");
        action.setParams({
            "recordId": recordId,
            "overview": component.find("strategicOverview").get("v.value"),
            "duration": component.find("duration").get("v.value")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS"){
                recordId = response.getReturnValue();
                component.set("v.recordId", recordId);
                component.set("v.stepNum", 2);
            }
        });
        $A.enqueueAction(action);
    },
    onRegionChange : function(component, event, helper){
        helper.updateUIValues(component);
    }
})