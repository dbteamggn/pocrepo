({
    setUIValues: function(component){
        var bpRec;
        var durVal;
        var firstRegion;
        var recordId = component.get("v.recordId");
        var ret = component.get("v.isRetail");
        
        var action = component.get("c.getBPRecord");
        action.setParams({
            "region": "",
            "recId": recordId,
            "isRetail": ret
        });

        action.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS"){
                bpRec = response.getReturnValue();

                if(typeof bpRec !== "undefined" && bpRec !== null){
                    component.set("v.isFIG", bpRec.is_FIG__c);
                    if(!bpRec.Is_Open__c){
                    	component.set("v.isOpen", "false");
                    }
                    component.set("v.hasPlans", "true");
                    console.log(component.get("v.hasPlans"));
                    component.find("strategicOverview").set("v.value", bpRec.Business_Plan_Summary__c);
                    component.set("v.recordId", bpRec.Id);
                    if(ukR && !bpRec.is_FIG__c){
                        var actionR = component.get("c.getRegions");
                        actionR.setCallback(this, function(response){
                            var state = response.getState();
                            if(component.isValid() && state == "SUCCESS"){
                                var opts = [];
                                var vals = response.getReturnValue();
                                for(var each in vals){
                                    var oneOption = {class: "optionClass", label: vals[each], value: vals[each]};
                                    opts.push(oneOption);
                                }
                                if(opts.length > 1){
                                    component.set("v.hasMultipleRegions", "true");
                                    component.find("regionD").set("v.options", opts);
                                    component.find("regionD").set("v.value", bpRec.BP_Territory__r.Name);
                                } else{
                                    component.find("region").set("v.value", bpRec.BP_Territory__r.Name);
                                }
                            }
                        });
                        $A.enqueueAction(actionR);
                    }
                    if(bpRec.Is_Open__c){
                        var actionP = component.get("c.getPicklistValues");
                        actionP.setParams({
                            "objName": "business_plan__c",
                            "fieldName": "business_plan_duration__c"
                        });
                        actionP.setCallback(this, function(response){
                            var state = response.getState();
                            if(component.isValid() && state == "SUCCESS"){
                                var opts = [];
                                var vals = response.getReturnValue();
                                for(var each in vals){
                                    var oneOption = {class: "optionClass", label: vals[each], value: vals[each]};
                                    opts.push(oneOption);
                                }
                                component.find("duration").set("v.options", opts);
                                component.find("duration").set("v.value", bpRec.Business_Plan_Duration__c);
                            }
                        });
                        $A.enqueueAction(actionP);
                    } else{
                        component.find("duration").set("v.value", bpRec.Business_Plan_Duration__c);
                    }
                }
            }
        });
        $A.enqueueAction(action);
    },
    updateUIValues: function(component){
        var action = component.get("c.getBPRecord");
        action.setParams({
            "region": component.find("regionD").get("v.value"),
            "recId": '',
            "isRetail": "true"
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS"){
                var bpRec = response.getReturnValue();
                component.find("strategicOverview").set("v.value", bpRec.Business_Plan_Summary__c);
                component.set("v.recordId", bpRec.Id);
                component.find("duration").set("v.value", bpRec.Business_Plan_Duration__c);
                component.find("regionD").set("v.value", bpRec.BP_Territory__r.Name);
            }
        });
        $A.enqueueAction(action);
    }
})