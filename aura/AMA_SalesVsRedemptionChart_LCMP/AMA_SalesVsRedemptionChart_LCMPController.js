({
	setup : function(component, event, helper) {
      
		new Highcharts.Chart({
            chart: {
                renderTo: component.find("chart").getElement(),
                type: 'line'
            },
            title: {
                text: 'Sales vs Redemptions'
            },
            xAxis: {
                categories: ['January', 'February', 'March','April','May','June','July']},
            yAxis: {
                title: {
                    text: ''
                }
            },
            
            series: [{
                name: 'Sales',
                data: [500000,300000,700000,850000,600000,800000,900000]
            },
                    {
                name: 'Redemption',
                data: [100000,200000,300000,100000,300000,200000,400000]
            }
                    ]
        });
         console.log('%%%%AViiiiiiiiii%%');
    }
})