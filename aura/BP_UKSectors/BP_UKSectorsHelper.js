({
	updateValuesHelper : function(component, updateFlag) {
		var action = component.get("c.updatePredictions");
        var records = component.get("v.records");
        for(var each in records){
            console.log(records[each].Justification__c);
            console.log(records[each].Amended_IP_ms__c);
            console.log(records[each].Amended_Allocation__c);
        }
        action.setParams({
            "uplift": component.find("sectorPerc").get("v.value"),
            "jsonStr": JSON.stringify(component.get("v.records")),
            "updateFlag": updateFlag
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS"){
                component.set("v.records", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
	},
    resetVals : function(component){
        var allPredictions = component.get("v.records");
        for(var each in allPredictions){
            allPredictions[each].Amended_IP_Sales__c = allPredictions[each].IP_Sales__c;
            allPredictions[each].Amended_IP_ms__c = allPredictions[each].IP_m_s__c;
            allPredictions[each].Amended_Region_Sales__c = allPredictions[each].Total_Region_Sales__c;
            allPredictions[each].Amended_Allocation__c = allPredictions[each].Region_Allocation__c;
        }
        component.set("v.records", allPredictions);
    }
})