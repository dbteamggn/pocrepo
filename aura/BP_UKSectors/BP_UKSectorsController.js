({
    doInit : function(component, event, helper) {
		var action = component.get("c.getPredictions");
        action.setParams({
            "busPlanId": component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS"){
                component.set("v.records", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
        
        var actionB = component.get("c.getCurrentBatchStatus");
        actionB.setParams({
            "bpId": component.get("v.recordId")
        });
        actionB.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS"){
                var batchMsgDiv = component.find('batchMsg');
                if(response.getReturnValue() == true){
                    $A.util.removeClass(batchMsgDiv, 'slds-hide');
                } else{
                    $A.util.addClass(batchMsgDiv, 'slds-hide');
                }
            }
        });
        $A.enqueueAction(actionB);
	},
    reset : function(component, event, helper){
        helper.resetVals(component);
    },
    prev : function(component, event, helper){
        component.set("v.stepNum", component.get("v.stepNum") - 1);
    }, 
    save : function(component, event, helper){
        var records = component.get("v.records");
        var allValid = true;
        var each;
        var errorDiv = component.find('errorDivSectors');
        for(each in records){
            if(records[each].IP_Sector__c !== "Total"){
                if(isNaN(records[each].Amended_Allocation__c) || isNaN(records[each].Amended_IP_ms__c)
                   || records[each].Amended_Allocation__c < 0 || records[each].Amended_IP_ms__c < 0){
                    allValid = false;
                    break;
                }
            }
        }
        if(allValid == false){
            component.set("v.errorMessageSector", "Amended Allocation and Amended IP m/s must be positive numbers");
            $A.util.removeClass(errorDiv, 'slds-hide');
        } else{
            $A.util.addClass(errorDiv, 'slds-hide');
            component.set("v.errorMessageSector", "");
            helper.updateValuesHelper(component, false);
            var totalRec;
            for(each in records){
                if(records[each].IP_Sector__c === "Total"){
                    totalRec = records[each];
                }
            }
            if(totalRec.Amended_Allocation__c != 100){
                component.set("v.errorMessageSector", "Please ensure the Total Amended Sector Allocation is 100%");
                $A.util.removeClass(errorDiv, 'slds-hide');
            } else{
                helper.updateValuesHelper(component, true);
                var batchMsgDiv = component.find('batchMsg');
            	$A.util.removeClass(batchMsgDiv, 'slds-hide');
            }
        }
    },
    refreshView : function(component, event, helper){
    	var actionB = component.get("c.getCurrentBatchStatus");
        actionB.setParams({
            "bpId": component.get("v.recordId")
        });
        actionB.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS"){
                var batchMsgDiv = component.find('batchMsg');
                if(response.getReturnValue() == true){
                    $A.util.removeClass(batchMsgDiv, 'slds-hide');
                } else{
                    $A.util.addClass(batchMsgDiv, 'slds-hide');
                }
            }
        });
        $A.enqueueAction(actionB);
    },
    next : function(component, event, helper){
        var records = component.get("v.records");
        var allValid = true;
        var each;
        var errorDiv = component.find('errorDivSectors');
        for(each in records){
            if(records[each].IP_Sector__c !== "Total"){
                if(isNaN(records[each].Amended_Allocation__c) || isNaN(records[each].Amended_IP_ms__c) 
                   || records[each].Amended_Allocation__c < 0 || records[each].Amended_IP_ms__c < 0){
                    allValid = false;
                    break;
                }
            }
        }
        if(allValid == false){
            component.set("v.errorMessageSector", "Amended Allocation and Amended IP m/s must be positive numbers");
            $A.util.removeClass(errorDiv, 'slds-hide');
        } else{
            $A.util.addClass(errorDiv, 'slds-hide');
            component.set("v.errorMessageSector", "");
            var isOpen = component.get("v.isOpen");
            if(isOpen == true){
                helper.updateValuesHelper(component, false);
                var totalRec;
                for(each in records){
                    if(records[each].IP_Sector__c === "Total"){
                        totalRec = records[each];
                    }
                }
                if(totalRec.Amended_Allocation__c != 100){
                    component.set("v.errorMessageSector", "Please ensure the Total Amended Sector Allocation is 100%");
                    $A.util.removeClass(errorDiv, 'slds-hide');
                } else{
                    helper.updateValuesHelper(component, true);
                    component.set("v.stepNum", component.get("v.stepNum") + 1);
                }
            } else{
                component.set("v.stepNum", component.get("v.stepNum") + 1);
            }
        }
    },
    updateValues : function(component, event, helper){
        helper.updateValuesHelper(component, false);
    }
})