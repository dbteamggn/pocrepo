({
	setup : function(component, event, helper) {
		console.log('=======');
        
        var getChartData = component.get("c.prepareChartData");
        getChartData.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var chartWrapper = response.getReturnValue();
                console.log('===='+JSON.stringify(chartWrapper.series));
                console.log(chartWrapper.series);
                new Highcharts.Chart({
                    chart: {
                        renderTo: component.find("chart").getElement(),
                        type: 'pie'
                    },
                    
                    title: {
                        text: chartWrapper.pieChartDetails.chartTitle
                    },
                    
                    tooltip: {
                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                   
                    plotOptions :{
                        pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                dataLabels: {
                                    enabled: false           
                                },
                                showInLegend: true
                            }
                    },
                    series: chartWrapper.series
                        });
            }
        });
        console.log('====Testing');
        $A.enqueueAction(getChartData);
	}
})