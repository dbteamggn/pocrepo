({
    searchKeyChange: function(component, event) {
        var searchKey = event.getParam("searchKey");
        var action = component.get("c.findByName");
        var queryParam = component.get("v.queryParam");
        var recordId = component.get("v.recordId");
        var typeParam = component.get("v.type");
        action.setParams({
            "searchKey": searchKey,
            "queryParam" : queryParam,
            "recordId" : recordId
        });
        action.setCallback(this, function(a) {            
            var newResultDataList=a.getReturnValue();
            console.log('result',newResultDataList);
            var resultDataList=component.get("v.tempContacts");
            console.log('saved',resultDataList);
            for(var items in resultDataList){
                console.log(items);
                console.log(resultDataList[items]);
                newResultDataList.push(resultDataList[items]);
            }
            console.log('newList',newResultDataList);
            component.set("v.contacts", newResultDataList);
            component.find("parentCheckbox").set("v.value",false);
        });
        $A.enqueueAction(action);
    },
    passData: function(component, event) {
        var selectedContact = event.getParam("selectedContact");
        var queryParam = event.getParam("queryParam");
        var recordId = component.get("v.recordId");
        console.log('queryParam',queryParam);
        component.set("v.queryParam",queryParam);
        console.log('passDataContactList',selectedContact);
        if(selectedContact.length>0){
            console.log('if');
            var ids=[];
            for(var items in selectedContact){
                ids.push(selectedContact[items].id);
            }
            console.log(ids);
            var action = component.get("c.findByListId");
            console.log(recordId);
            action.setParams({
                "contactIds": ids,
                "queryParam" : queryParam,
                "recordId" : recordId
            });
            action.setCallback(this, function(a) {
                console.log('data',a.getReturnValue());
                component.set("v.contacts", a.getReturnValue());
                component.set("v.tempContacts",a.getReturnValue());
            });
            $A.enqueueAction(action);
        }else{
            var action = component.get("c.findAll");
            console.log('else');
            action.setParams({
                "queryParam" : queryParam,
                "recordId" : recordId
            });
            action.setCallback(this, function(a) {
                console.log('data',a.getReturnValue());
                component.set("v.contacts", a.getReturnValue());
                //component.set("v.tempContacts",resultDataList);
            });
            $A.enqueueAction(action);
        }
    },
    onChangeParentCheckbox: function(component, event) {
        var checkCmp = component.find("parentCheckbox").get("v.value");
        console.log(checkCmp);
        var resultCmp = component.find("childCheckbox");
        console.log(resultCmp);
        console.log(resultCmp.length);
        for (var i = 0; i < resultCmp.length; i++){
            resultCmp[i].set("v.value",checkCmp);
        }
        var resultDataList=[];
        var resultData=new Object(); 
        for (var i = 0; i < resultCmp.length; i++){
            if(resultCmp[i].get("v.value")){
                resultData=new Object();
                resultData.contactId=resultCmp[i].get("v.text");
                resultData.contactName=resultCmp[i].get("v.name");
                resultData.accountName=resultCmp[i].get("v.labelClass");
                resultData.value=true;
                resultDataList.push(resultData);
            }
            component.set("v.tempContacts",resultDataList);
        }
    },
    onChangeChildCheckbox: function(component, event) {
        var resultCmp = component.find("childCheckbox");
        console.log(resultCmp);
        console.log(resultCmp.length);
        var resultDataList=[];
        var resultData=new Object(); 
        for (var i = 0; i < resultCmp.length; i++){
            if(resultCmp[i].get("v.value")){
                resultData=new Object();
                resultData.contactId=resultCmp[i].get("v.text");
                resultData.contactName=resultCmp[i].get("v.name");
                resultData.accountName=resultCmp[i].get("v.labelClass");
                resultData.value=true;
                resultDataList.push(resultData);
            }
            component.set("v.tempContacts",resultDataList);
        }
        console.log(resultDataList);
        console.log(component.get("v.tempContacts"));
    },
    showSelected: function(component, event) {
        var resultCmp = component.find("childCheckbox");
        var queryParam = component.get("v.queryParam");      
        var resultDataList=[];
        var resultData=new Object();
        console.log(resultCmp);
        if(typeof resultCmp !== 'undefined' && resultCmp !== null ){
            console.log('in loop');
            for (var i = 0; i < resultCmp.length; i++){
                if(resultCmp[i].get("v.value")){
                    resultData=new Object(); 
                    resultData.id=resultCmp[i].get("v.text");
                    resultData.name=resultCmp[i].get("v.name");
                    resultDataList.push(resultData);
                    resultCmp[i].set("v.value",false);
                }
            }
        }
        component.find("parentCheckbox").set("v.value",false);          
        var selectedContact = resultDataList;        
        if(component.get("v.type")=='create'){
            var myEvent = $A.get("e.c:NameOnFormClick");
            myEvent.setParams({
                "compNameToHide": "contactList",
                "compNameToShow": "form",
                "selectedContact": selectedContact,
                "queryParam" : queryParam
            });
            myEvent.fire();
        }else{
            var myEvent = $A.get("e.c:NameOnFormClickEdit");
            myEvent.setParams({
                "compNameToHide": "contactList",
                "compNameToShow": "form",
                "selectedContact": selectedContact,
                "queryParam" : queryParam
            });
            myEvent.fire();
        }
    },
})