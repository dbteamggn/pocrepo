({
    passData: function(component, event) {
        console.log('inContactForm');
        var selectedContact = event.getParam("selectedContact");
        var allNames='';
        for(var items in selectedContact){
            allNames=allNames+selectedContact[items].name+' ; ';
        }
        console.log(allNames);
        component.find("name").set('v.value',allNames);
        component.set('v.selectedContact',selectedContact);
    },
    passFunds: function(component, event) {
        console.log('inFundForm');
        var selectedFunds = event.getParam("selectedFunds");
        console.log('selectedFunds',selectedFunds);
        var allNames='';
        for(var items in selectedFunds){
            allNames=allNames+selectedFunds[items].name+' ; ';
        }
        console.log(allNames);
        component.find("invStrat").set('v.value',allNames);
        component.set('v.selectedFund',selectedFunds);
    },
    doInit: function(component, event,helper){
        helper.getPicklist(component, event , "Asset_Class__c");
        helper.SubTypeVals(component, event);
    },
    showContactList: function(component, event) {     
        var classValue = event.getSource().get("v.class"); 
        var queryParam;
        console.log('contactList----First');
        if(classValue.indexOf('participant')>0){
            queryParam = "participant";
        }else if(classValue.indexOf('attendee')>0){
            queryParam = "attendee";
        }else{
            queryParam = "all";
        }
        var selectedContact = component.get('v.selectedContact'); 
        var myEvent = $A.get("e.c:NameOnFormClick");
        myEvent.setParams({
            "compNameToHide": "form",
            "compNameToShow": "contactList",
            "selectedContact": selectedContact,
            "queryParam" : queryParam
        });
        myEvent.fire();
    },
    logMeeting: function(component, event){
        var isError=false;
        var addMeeting = new Object();
        addMeeting.contactId=component.get('v.recordId');
        addMeeting.subType=component.find("subType").get("v.value");   
        //addMeeting.assetClass=component.find("assetClass").get("v.value");
        addMeeting.selectedContact=component.get('v.selectedContact');
        addMeeting.selectedFund=component.get("v.selectedFund");
        console.log(addMeeting.selectedFund);
        //addMeeting.notes=component.find("notes").get("v.value");
        console.log(addMeeting.contactId);
        
        if( typeof addMeeting.subType === 'undefined' || addMeeting.subType === null || addMeeting.subType == ''){
            component.find("subType").set("v.errors", [{message:"Please enter " + component.find("subType").get("v.label") }]);
            isError=true;
        }else{
            component.find("subType").set("v.errors", null );
        }
        console.log(addMeeting.contactId);
        /*if( typeof addMeeting.assetClass === 'undefined' || addMeeting.assetClass === null ){
            component.find("assetClass").set("v.errors", [{message:"Please enter " + component.find("assetClass").get("v.label") }]);
            isError=true;        
        }else{
            component.find("assetClass").set("v.errors", null );
        }*/
        console.log(addMeeting.contactId);
        if( typeof addMeeting.selectedContact === 'undefined' || addMeeting.selectedContact === null || addMeeting.selectedContact.length==0){
            component.find("name").set("v.errors", [{message:"Please select Contacts "}]);
            isError=true;            
        }else{
            component.find("name").set("v.errors", null );
        }
        console.log(addMeeting.contactId);
        if( typeof addMeeting.selectedFund === 'undefined' || addMeeting.selectedFund === null || addMeeting.selectedFund.length == 0) {
            component.find("invStrat").set("v.errors", [{message:"Please enter " + component.find("invStrat").get("v.label") }]);
            isError=true;         
        }else{
            component.find("invStrat").set("v.errors", null );
        }
        console.log(addMeeting.contactId);
        /*if( typeof addMeeting.notes === 'undefined' || addMeeting.notes === null || addMeeting.notes == ''){
            component.find("notes").set("v.errors", [{message:"Please enter " + component.find("notes").get("v.label") }]);
            isError=true;         
        }else{
            component.find("notes").set("v.errors", null );
        }*/
        console.log(addMeeting);
        if(!isError){
            var action = component.get("c.createMeeting");
            console.log(action);
            var newMeetingData = JSON.stringify(addMeeting);
            console.log(newMeetingData);
            action.setParams({
                "param": newMeetingData
            });
            action.setCallback(this, function(a) {
                console.log('recordId',a.getReturnValue());
                var recordId = a.getReturnValue();
                if(typeof sforce !== "undefined" && sforce !== null) {
                    sforce.one.navigateToURL('/' + recordId);
                }else{
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": recordId,
                        "slideDevName": "related"
                    });
                    navEvt.fire();
                }
            });
            $A.enqueueAction(action);
        }
    },
    showFundList: function(component, event) {     
        var selectedFund = component.get('v.selectedFund'); 
        console.log('selectedFund',selectedFund);
        console.log('FundList----First');        
        var myEvent = $A.get("e.c:FundOnFormClick");
        myEvent.setParams({
            "compNameToHide": "form",
            "compNameToShow": "fundList",
            "selectedFunds": selectedFund
        });
        myEvent.fire();
    },
    handleError: function(component, event){
        /* do any custom error handling
         * logic desired here */
        // get v.errors, which is an Object[]
    },
    
    handleClearError: function(component, event) {
        /* do any custom error handling
         * logic desired here */
    }
})