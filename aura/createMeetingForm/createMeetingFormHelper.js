({
    getPicklist : function(component, event ,fieldName) {
        var action = component.get("c.GetPckLstValues");
        action.setParams({
            "ObjectApi_name": "Task",
            "picklistField": fieldName
        });	
        //Set up the callback
        var self = this;
        action.setCallback(this, function(response) {
            var state = response.getState();
            var lst = response.getReturnValue();
            if (state === "SUCCESS" && lst !== null) {
                var newlst = new Array();
                newlst.push('--None--');
                for(var i=0;i<lst.length;i++){
                    newlst.push(lst[i]);
                }
                component.set('v.assestlist', newlst);	 
            }
        });
        $A.enqueueAction(action);
    },
    SubTypeVals : function(component,event) {
        var action = component.get("c.GetDpndntPckLstValues");
        action.setParams({
            "ObjectName": "Event",
            "CtrlField": "Type__c",
            "DependField": "Sub_Type__c",
            "CtrlFieldValue": "Meeting"
        });
        //Set up the callback
        var self = this;
        action.setCallback(this, function(response) {
            var state = response.getState();
            var lst = response.getReturnValue();
            if (state === "SUCCESS" && lst !== null) {
                var newlst = new Array();
                newlst.push('--None--');
                for(var i=0;i<lst.length;i++){
                    newlst.push(lst[i]);
                }
                component.set('v.typelist', newlst);   
            }
        });
        $A.enqueueAction(action);
    },
})