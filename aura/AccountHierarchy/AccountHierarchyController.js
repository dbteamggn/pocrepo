({
	doInit: function(component, event, helper) {
		var accountId = component.get('v.recordId');
        component.set('v.currentRecordId',accountId);
        var selectedTreeType = component.find("treeType").get("v.value");
		helper.loadContext(component, accountId , selectedTreeType);
	},

	expandAll: function(component){
		component.get('v.menu')[0].expandAll();
	},

	collapseAll: function(component){
		component.get('v.menu')[0].collapseAll();
	},
    
    showFullTree: function(component,event,helper){       
        var el = event.srcElement;
        var value = el.dataset.value;
        var selectedTreeType = component.find("treeType").get("v.value");
        if(value =='View Current Level Hierarchy'){
            component.set('v.ultimateParent','View Full Hierarchy');
            var accountId = component.get('v.recordId');
            component.set('v.currentRecordId',accountId);           
            helper.loadnewContext(component,accountId,selectedTreeType);
        }else{
            component.set('v.ultimateParent','View Current Level Hierarchy');   
            var accountId=el.dataset.id;
            component.set('v.currentRecordId',accountId);           
            helper.loadnewContext(component,accountId,selectedTreeType);
        }
    },
    /*showThirdparty: function(component,event,helper){       
        var el = event.srcElement;
  		var value = el.dataset.value;
        var dataText = el.dataset.text;
        var accountId = component.get('v.recordId');
        console.log('data',component.get('v.ultimateParent'));
        if(dataText =='Show Third Parties'){
            component.set('v.thirdParty',true);  
            component.set('v.thirdPartyText','Hide Third Parties');
            //var accountId = component.get('v.recordId');   
            var accountId = component.get('v.currentRecordId'); 
            helper.loadThirdPartyContext(component,accountId,true)
        }else{
            component.set('v.thirdParty',false);  
         	component.set('v.thirdPartyText','Show Third Parties');   
            //var accountId = component.get('v.recordId');
            var accountId = component.get('v.currentRecordId');
            helper.loadThirdPartyContext(component,accountId,false)
        }
    },*/
    onSelectChange :  function (component,event,helper){
        var selectedTreeType = component.find("treeType").get("v.value");
        var accountId = component.get('v.currentRecordId');
		helper.loadAdditionalTreeContent(component,accountId,selectedTreeType);
    }
})