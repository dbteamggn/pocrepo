({
	loadContext: function(component, accountId,treeType){
		var self = this;
		var action = component.get('c.getAccountInContext');
		action.setParams({
			accountId: accountId,
            value: treeType
		});
        var action1 = component.get('c.getAccountUltimateParent');
        action1.setParams({
            accountId: accountId
        });
        action1.setCallback(this, function(response){
            if(component.isValid()){
                component.set('v.ultimateParentId', response.getReturnValue());
            }
        });
		action.setCallback(this, function(response){
			if(component.isValid()){
				component.set('v.loaded', true);
				console.log('all',response.getReturnValue());
				var configuration = {
					displayRecordTypes: component.get('v.displayRecordTypes'),
					linkBehavior: component.get('v.linkBehavior'),
					visualforceContext: component.get('v.visualforceContext')
				};
				$A.createComponent(
					'c:AccountHierarchyMenu',
					{
						accounts: [ response.getReturnValue() ],
						configuration: configuration,
						childAccountsLoaded: true
					},
					function(newMenu){
						if (component.isValid()) {
							// Auto-expand first account, if required
							var expanded = component.get('v.expanded');

							if(expanded){
								newMenu.expandFirst();
							}

							// Add the root menu to the UI
							var body = component.get('v.menu') || [];
							body.push(newMenu);
							component.set('v.menu', body);
						}
					}
				);
				
			}
		});
		$A.enqueueAction(action);
        $A.enqueueAction(action1);
	},
    loadnewContext: function(component, accountId,value1){
    	var self = this;
		var action = component.get('c.getAccountInContext');
		action.setParams({
			accountId: accountId,
            value: value1
		});
        action.setCallback(this, function(response){
			if(component.isValid()){
				component.set('v.loaded', true);
				
				var configuration = {
					displayRecordTypes: component.get('v.displayRecordTypes'),
					linkBehavior: component.get('v.linkBehavior'),
					visualforceContext: component.get('v.visualforceContext'),                    
                    thirdParty: value1
				};
				$A.createComponent(
					'c:AccountHierarchyMenu',
					{
						accounts: [ response.getReturnValue() ],
						configuration: configuration,
						childAccountsLoaded: true
					},
					function(newMenu){
						if (component.isValid()) {
							// Auto-expand first account, if required
							var expanded = component.get('v.expanded');

							if(expanded){
								newMenu.expandFirst();
							}
						
							// Add the root menu to the UI
							var body = [];
							body.push(newMenu);
							component.set('v.menu', body);
						}
					}
				);
				
			}
		});
		$A.enqueueAction(action);
    },
    loadThirdPartyContext: function(component, accountId,value1){
    	var self = this;
		var action = component.get('c.getAccountInContext');
        console.log('value1loadThirdPartyContext',value1);

		action.setParams({
			accountId: accountId,
            value: value1
		});
        action.setCallback(this, function(response){
			if(component.isValid()){
				component.set('v.loaded', true);
				var configuration = {
					displayRecordTypes: component.get('v.displayRecordTypes'),
					linkBehavior: component.get('v.linkBehavior'),
					visualforceContext: component.get('v.visualforceContext'),
                    thirdParty: value1
				};
				$A.createComponent(
					'c:AccountHierarchyMenu',
					{
						accounts: [ response.getReturnValue() ],
						configuration: configuration,
						childAccountsLoaded: true
					},
					function(newMenu){
						if (component.isValid()) {
							// Auto-expand first account, if required
							var expanded = component.get('v.expanded');

							if(expanded){
								newMenu.expandFirst();
							}
						
							// Add the root menu to the UI
							var body = [];
							body.push(newMenu);
							component.set('v.menu', body);
						}
					}
				);
				
			}
		});
		$A.enqueueAction(action);
    },
    loadAdditionalTreeContent : function(component, accountId, treeType){
    	var self = this;
		var action = component.get('c.getAccountInContext');
        console.log('treeType',treeType);

		action.setParams({
			accountId: accountId,
            value: treeType
		})
        action.setCallback(this, function(response){
			if(component.isValid()){
				component.set('v.loaded', true);
				var configuration = {
					displayRecordTypes: component.get('v.displayRecordTypes'),
					linkBehavior: component.get('v.linkBehavior'),
					visualforceContext: component.get('v.visualforceContext')
				};
				$A.createComponent(
					'c:AccountHierarchyMenu',
					{
						accounts: [ response.getReturnValue() ],
						configuration: configuration,
						childAccountsLoaded: true
					},
					function(newMenu){
						if (component.isValid()) {
							// Auto-expand first account, if required
							var expanded = component.get('v.expanded');

							if(expanded){
								newMenu.expandFirst();
							}
						
							// Add the root menu to the UI
							var body = [];
							body.push(newMenu);
							component.set('v.menu', body);
						}
					}
				);
				
			}
		});
		$A.enqueueAction(action);
    },
})