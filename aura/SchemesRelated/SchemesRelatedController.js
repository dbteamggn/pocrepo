({
    /*
     * Name : doInit
     * Description : Gets the schemes related to the third party.
     * Returns : List of Schemes
     */
    doInit : function(component, event, helper) {
        
        /* Hide the schemes related list on page load */
        $A.util.addClass(component.find("schemesRelatedCardId"),'slds-hide');
        
        var recordId = component.get('v.recordId');
        var action = component.get('c.getRelatedSchemes');
        action.setParams({'recordId': recordId});
        action.setCallback(this,function(a){
            
            var response = a.getReturnValue();
            component.set('v.isThirdParty', response.isThirdParty);
            component.set('v.schemeCount', response.schemeCount);
            if(response.isThirdParty){
                if(response.schemesRelated != null && response.schemesRelated != '' ){
                    component.set('v.schemesRelated', response.schemesRelated);
                    $A.util.removeClass(component.find("schemesRelatedCardId"),'slds-hide');
                }
            }
        });
        $A.enqueueAction(action);
        
    },
    /*
     *	Name		:  navigateToSubscription
     * 	Description	:  This method is used to create custom hyperlink fields
     */
    navigateToSubscription: function(component, event, helper){
        var schemeId = event.target.getAttribute('data-index');   
        var visualforceContext = component.get('v.configuration.visualforceContext');
        var linkBehavior = component.get('v.configuration.linkBehavior') || 'none';
        
        if(typeof sforce !== "undefined" && sforce !== null){
        }else{
            if(visualforceContext){
                window.top.location = '/' + schemeId;
		} else {
                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": schemeId
                });
                navEvt.fire();
            }
        }
        return false;
    }
})