({
	setup : function(component, event, helper) {
        console.log('=======');
        
        var getChartData = component.get("c.prepareChartData");
        getChartData.setParams({
			"accId":component.get("v.accId")
		});
        getChartData.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var chartWrapper = response.getReturnValue();
                console.log('bar chart');
                console.log(response.getReturnValue());
                new Highcharts.Chart({
                    chart: {
                        renderTo: component.find("chart").getElement(),
                        type: 'bar'
                    },
                   title: {
                        text: chartWrapper.barChartDetails.chartTitle
                    },
                    xAxis: {
                        categories: chartWrapper.barChartDetails.categories
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: chartWrapper.barChartDetails.yaxisTitle
                        }
                    },
                    series: chartWrapper.series
                });
            } else if (state === "ERROR") {
                alert('Error : ' + JSON.stringify(errors));
            }
        });
        $A.enqueueAction(getChartData);
        
        
		
         console.log('%%%%%%');
    }
})