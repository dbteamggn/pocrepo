({
	doInit : function(component, event, helper) {
		/*var action = component.get("c.GetThirdParties");
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var thirdParties = response.getReturnValue();
                var opts = [];
                for(var each in thirdParties){
                    var oneOption = {class: "optionClass", label: thirdParties[each].Name, value: thirdParties[each].Id};
                    opts.push(oneOption);
                }
                component.find("thirdParties").set("v.options", opts);
            }
        });
	 	$A.enqueueAction(action);*/
        var actionS = component.get("c.GetStrategies");
        actionS.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var strategies = response.getReturnValue();
                var opts = [];
                for(var each in strategies){
                    var oneOption = {class: "optionClass", label: strategies[each].Name, value: strategies[each].Id};
                    opts.push(oneOption);
                }
                component.find("strategies").set("v.options", opts);
            }
        });
	 	$A.enqueueAction(actionS);
	},
    getActivities : function(component, event, helper){
        var action = component.get("c.getEvents");
        action.setParams({
            //"thirdPartyId": component.find("thirdParties").get("v.value"),
            "strategyId": component.find("strategies").get("v.value")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.records", response.getReturnValue());
                if(response.getReturnValue().length > 0){
                    component.set("v.hasRecords", "true");
                } else if(component.get("v.hasRecords") == "true"){
                    component.set("v.hasRecords", "false");
                }
            }
        });
        $A.enqueueAction(action);
    },
    back : function(component, event, helper) {
        var evt = $A.get("e.c:ConsultantRatingsTableEvent");
        evt.fire();
    }
})