({
	setUIValues : function(component) {
		var action = component.get("c.getClientRecs");
        action.setParams({
            "bpRecId": component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS"){
                component.set("v.records", response.getReturnValue());
                console.log(response.getReturnValue());
                var actionP = component.get("c.getPicklistValues");
                actionP.setParams({
                    "objName": "business_plan_client_relation__c",
                    "fieldName": "resource_needs__c"
                });
                actionP.setCallback(this, function(responseVal){
                    var allCmps = [];
                    console.log(component.find("resrcNeeds"));
                    if(typeof component.find("resrcNeeds") !== "undefined" && component.find("resrcNeeds") !== null){
                        if(component.find("resrcNeeds").length > 1){
                        	allCmps = component.find("resrcNeeds");
                        } else{
                    	    allCmps.push(component.find("resrcNeeds"));
                        }
                    } 
                    console.log(allCmps);
                    var allRecords = response.getReturnValue();
                    var opts = [];
                    var vals = responseVal.getReturnValue();
                    for(var each in vals){
                        var oneOption = {class: "optionClass", label: vals[each], value: vals[each]};
                        opts.push(oneOption);
                    }
                    console.log(opts);
                    for(var each in allCmps){
                        allCmps[each].set("v.options", opts);
                        allCmps[each].set("v.value", allRecords[each].bpcRec.Resource_Needs__c);
                    }
                });
                $A.enqueueAction(actionP);
            }
        });
        $A.enqueueAction(action);
	},
    showModalDialog : function(component, event){
        var selectedId = event.srcElement.id;
        component.set("v.selectedRowId", selectedId);
        var records = component.get("v.records");
        for(var each in records){
            if(records[each].bpcRec.Id == selectedId){
                component.set("v.selectedIds", records[each].selectedIds);
                break;
            }
        }
        var fieldsList = ["Name", "Offerring__c", "Asset_Style__c"];
        var labelsList = ["Name", "Offerring", "Asset Style"];
        $A.createComponent(
            "c:LightningTables",
            {
                "object": "Investment_Strategy__c",
                "showCheckBoxes": "true",
                "limitValue": "25",
                "fields": fieldsList,
                "labels": labelsList,
                "addSearch": "true",
                "searchFields": fieldsList,
                "selectedIds": component.getReference("v.selectedIds"),
                "appendWithSearch": "true"
            },
            function(newTable, status, errorMessage){
                if(status === "SUCCESS"){
                    component.set("v.body", newTable);
                }
            }
        );

		document.getElementById("Strategies").style.display = "block";
		document.getElementById("background").style.display = "block";
    },
    hideModalDialog : function(component){
        document.getElementById("Strategies").style.display = "none";
		document.getElementById("background").style.display = "none";
    },
    setStrategies : function(component){
        var selectedRowId = component.get("v.selectedRowId");
        var selectedIds = component.get("v.selectedIds");
        var action = component.get("c.getStrategyNames");
        action.setParams({
            "selectedIds": selectedIds
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS"){
                document.getElementById(selectedRowId).value = response.getReturnValue();
                var records = component.get("v.records");
                for(var each in records){
                    if(records[each].bpcRec.Id == selectedRowId){
                        records[each].selectedIds = selectedIds;
                        records[each].selectedNames = response.getReturnValue();
                        break;
                    }
                }
            }
        });
        $A.enqueueAction(action);
    },
    saveClientRecs : function(component){
        var action = component.get("c.updateBPC");
        var jsonInputStr = JSON.stringify(component.get("v.records"));
        var allCmps = [];
        if(typeof component.find("resrcNeeds") !== "undefined" && component.find("resrcNeeds") !== null){
            if(component.find("resrcNeeds").length > 1){
                allCmps = component.find("resrcNeeds");
            } else{
                allCmps.push(component.find("resrcNeeds"));
            }
        }
        var allNotes = [];
        if(typeof component.find("notes") !== "undefined" && component.find("notes") !== null){
            if(component.find("notes").length > 1){
                allNotes = component.find("notes");
            } else{
                allNotes.push(component.find("notes"));
            }
        }
        var allMeetings = [];
        if(typeof component.find("expMeetings") !== "undefined" && component.find("expMeetings") !== null){
            if(component.find("expMeetings").length > 1){
                allMeetings = component.find("expMeetings");
            } else{
                allMeetings.push(component.find("expMeetings"));
            }
        }
        var resrcVals = [];
        var meetingVals = [];
        var noteVals = [];
        for(var each in allCmps){
        	resrcVals.push(allCmps[each].get("v.value"));
            meetingVals.push(allMeetings[each].get("v.value"));
            noteVals.push(allNotes[each].get("v.value"));
        }
        action.setParams({
            "recordVals": jsonInputStr,
            "recordId": component.get("v.recordId"),
            "resourceVals": resrcVals,
            "meetingVals": meetingVals,
            "noteVals": noteVals
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS"){
        		component.set("v.stepNum", component.get("v.stepNum") + 1);
            }
        });
        $A.enqueueAction(action);
    }
})