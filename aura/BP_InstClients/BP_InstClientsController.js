({
	prev : function(component, event, helper){
        component.set("v.stepNum", component.get("v.stepNum") - 1);
    }, 
    next : function(component, event, helper){
        helper.saveClientRecs(component);
    },
    doInit : function(component, event, helper){
        helper.setUIValues(component);
    },
	showModal : function(component, event, helper) {
        helper.showModalDialog(component, event);
	},
	showModalBox : function(component, event, helper) {
        helper.hideModalDialog(component);
	},
    passStrategies : function(component, event, helper) {
        helper.setStrategies(component);
        helper.hideModalDialog(component);
	}
})