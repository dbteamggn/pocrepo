({
    doInit : function(component, event, helper) {
        var businessPlanId = component.get('v.recordId');
        var addCampaigns = component.get('v.addCampaigns');        
        var action = component.get('c.getCampaigns'); 
        var isUKRetail = component.get('v.isUKRetail');
        
        
        action.setParams({"businessPlanId":businessPlanId});
        
        action.setCallback(this, function(response){        
            var relatedCampaignList = response.getReturnValue();                    
            if(relatedCampaignList != null){                                     
                for(var j=0; j < relatedCampaignList.length; j++){
                    if(!addCampaigns.indexOf(relatedCampaignList[j].campaignId) > -1){
                        addCampaigns.push(relatedCampaignList[j].campaignId);
                    }    
                }                                                      
                component.set("v.campaigns",relatedCampaignList);
            } 
        }) ;
        $A.enqueueAction(action);      
        
        var actionCmp = component.get('c.getCampaignText');
        actionCmp.setParams({"businessPlanId":businessPlanId});
        actionCmp.setCallback(this, function(response){
            component.find("campaignText").set("v.value", response.getReturnValue());
        });
        $A.enqueueAction(actionCmp);
    },
    addCampaign : function(component, event, helper) {
        var campaignId = component.get('v.campaign');
        var addCampaigns = component.get('v.addCampaigns');
        
        var action = component.get('c.getCampaign');
        var campaigns = component.get('v.campaigns');
        
        action.setParams({"campaignIds":campaignId});
        
        action.setCallback(this, function(response){        
            var relatedCampaignList = response.getReturnValue();        
            var campaignMatch = true;
            if(relatedCampaignList != null){                                     
                for(var j=0; j < relatedCampaignList.length; j++){
                    if(addCampaigns.indexOf(relatedCampaignList[j].campaignId) <= -1){
                        addCampaigns.push(relatedCampaignList[j].campaignId);
                        campaigns.push(relatedCampaignList[j]);
                    }    
                }                                                                           
            } 
            component.set('v.addCampaigns',addCampaigns);
            component.set('v.campaigns', campaigns);
            
        }) ;
        $A.enqueueAction(action); 
    },
    next : function(cmp, evt, helper){
        var campaigns = cmp.get("v.addCampaigns");  
        var businessPlan = cmp.get('v.recordId');
        var delCampaigns = cmp.get('v.delCampaigns');
        var action = cmp.get('c.createBusinessPlanCampaigns');
        var campaignText = '';
        campaignText = cmp.find("campaignText").get("v.value");
        
        action.setParams({
            "businessPlan":businessPlan,
            "campaigns": campaigns,
            "delCampaigns":delCampaigns,
            "campaignText": campaignText
        });
        action.setCallback(this, function(response){
            var isCreated = response.getReturnValue();
            if(!isCreated){
                $A.util.toggleClass(cmp.find('errorMessageDiv'), 'invisible');
            }else{                
                sforce.one.navigateToSObject(businessPlan);
            }
        });
        $A.enqueueAction(action);                                  
    },
    previous : function(cmp, evt, helper){
        cmp.set("v.stepNum", cmp.get('v.stepNum')-1);
        cmp.set('v.recordId', cmp.get('v.recordId'));
    },
    onChangeChildCheckbox: function(component, event,helper) {
        
        var ListToAdd=component.get('v.addCampaigns');
        var ListToDelete=component.get('v.delCampaigns');
        var campaignId = event.getSource().get('v.text');        
        var rowId = document.getElementById(campaignId);
        if(event.getSource().get('v.value')){
            var index = ListToDelete.indexOf(campaignId);
            if(index > -1){
                ListToDelete.splice(index,1);                             
            }
            ListToAdd.push(event.getSource().get('v.text'));            
        }else{
            var index = ListToAdd.indexOf(event.getSource().get('v.text'));
            if(index > -1){
                ListToAdd.splice(index,1);
            }
            ListToDelete.push(event.getSource().get('v.text'));
            
        }
        component.set('v.addCampaigns',ListToAdd);
        component.set('v.delCampaigns',ListToDelete);
        
    }
})