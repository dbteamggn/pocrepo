({    
    NameOnFormClick : function(component, event) {  
        var compNameToHide = event.getParam("compNameToHide");
        var compNameToShow = event.getParam("compNameToShow");
        var selectedContact = event.getParam("selectedContact");
        var queryParam = event.getParam("queryParam");
        console.log('queryParam',queryParam);
        var hideComp = component.find(compNameToHide);
        $A.util.addClass(hideComp, "hide");
        var showComp = component.find(compNameToShow);
        $A.util.removeClass(showComp, "hide");
        var myEvent = $A.get("e.c:passData");
        myEvent.setParams({
            "selectedContact": selectedContact,
            "queryParam" : queryParam
        });
        myEvent.fire();       
    }
})