({
    searchKeyChange: function(component, event) {
        var searchKey = event.getParam("searchKey");
        var action = component.get("c.findByName");
        action.setParams({
            "searchKey": searchKey
        });
        action.setCallback(this, function(a) {            
            var newResultDataList=a.getReturnValue();
            console.log('result',newResultDataList);
            var resultDataList=component.get("v.tempfundAndStrat");
            console.log('saved',resultDataList);
            for(var items in resultDataList){
                console.log(items);
                console.log(resultDataList[items]);
                newResultDataList.push(resultDataList[items]);
            }
            console.log('newList',newResultDataList);
            component.set("v.fundAndStrat", newResultDataList);
            component.find("parentCheckbox").set("v.value",false);
        });
        $A.enqueueAction(action);
    },
    passFunds: function(component, event) {
        console.log('InFundList');
        var selectedFunds = event.getParam("selectedFunds");
        console.log('fundList----Third');
        console.log('selectedFunds',selectedFunds);
        if(selectedFunds.length>0){
            console.log('if');
            var ids=[];
            for(var items in selectedFunds){
                ids.push(selectedFunds[items].id);
            }
            console.log(ids);
            var action = component.get("c.findByListId");
            action.setParams({
                "fundStratIds": ids
            });
            action.setCallback(this, function(a) {
                console.log('data',a.getReturnValue());
                component.set("v.fundAndStrat", a.getReturnValue());
                component.set("v.tempfundAndStrat",a.getReturnValue());
            });
            $A.enqueueAction(action);
        }else{
            var action = component.get("c.findAll");
            action.setCallback(this, function(a) {
                console.log('data',a.getReturnValue());
                component.set("v.fundAndStrat", a.getReturnValue());
            });
            $A.enqueueAction(action);
        }
    },
    onChangeParentCheckbox: function(component, event) {
        var checkCmp = component.find("parentCheckbox").get("v.value");
        console.log(checkCmp);
        var resultCmp = component.find("childCheckbox");
        console.log(resultCmp);
        console.log(resultCmp.length);
        for (var i = 0; i < resultCmp.length; i++){
            resultCmp[i].set("v.value",checkCmp);
        }
        var resultDataList=[];
        var resultData=new Object(); 
        for (var i = 0; i < resultCmp.length; i++){
            if(resultCmp[i].get("v.value")){
                resultData=new Object();
                resultData.id=resultCmp[i].get("v.text");
                resultData.name=resultCmp[i].get("v.name");
                resultData.type=resultCmp[i].get("v.labelClass");
                resultData.value=true;
                resultDataList.push(resultData);
            }
            component.set("v.tempfundAndStrat",resultDataList);
        }
    },
    onChangeChildCheckbox: function(component, event) {
        var resultCmp = component.find("childCheckbox");
        console.log(resultCmp.length);
        var resultDataList=[];
        var resultData=new Object(); 
        for (var i = 0; i < resultCmp.length; i++){
            if(resultCmp[i].get("v.value")){
                resultData=new Object();
                resultData.id=resultCmp[i].get("v.text");
                resultData.name=resultCmp[i].get("v.name");
                resultData.type=resultCmp[i].get("v.labelClass");
                resultData.value=true;
                resultDataList.push(resultData);
            }
            component.set("v.tempfundAndStrat",resultDataList);
        }
        console.log(resultDataList);
        console.log(component.get("v.tempfundAndStrat"));
    },
    showSelected: function(component, event) {
        console.log('Innn show Selector');
        var resultCmp = component.find("childCheckbox");
        var resultDataList=[];
        var resultData=new Object();    
        if(typeof resultCmp !== 'undefined' && resultCmp !== null ){
            for (var i = 0; i < resultCmp.length; i++){
                if(resultCmp[i].get("v.value")){
                    resultData=new Object(); 
                    resultData.id=resultCmp[i].get("v.text");
                    resultData.name=resultCmp[i].get("v.name");
                    resultData.type=resultCmp[i].get("v.labelClass");
                    resultDataList.push(resultData);
                    resultCmp[i].set("v.value",false);
                }
            }
        }
        component.find("parentCheckbox").set("v.value",false);
        console.log('This is in Create Event');              
        var selectedFunds = resultDataList;  
        console.log("#selectedFunds"+selectedFunds);
        //console.log("#event"+event);
        if(component.get("v.type")=='create'){
            console.log('create');
            var myEvent = $A.get("e.c:FundOnFormClick");
            console.log("#insideShowSelected"+myEvent);
            myEvent.setParams({
                "compNameToHide": "fundList",
                "compNameToShow": "form",
                "selectedFunds": selectedFunds
            });
            myEvent.fire();
        }else{
            console.log('edit');
            var myEvent = $A.get("e.c:FundOnFormClickEdit");
            console.log("#insideEdit"+myEvent);
            myEvent.setParams({
                "compNameToHide": "fundList",
                "compNameToShow": "form",
                "selectedFunds": selectedFunds,
            });
            myEvent.fire();
        }
    },
})