({
	doInit: function(component, event, helper) {      
        //Fetch the expense list from the Apex controller   
        if(component.get("v.addSearch")){
            if(component.get("v.appendWithSearch")){
                if(component.get("v.selectedIds") !== null && component.get("v.selectedIds").length > 0){
                	helper.fillTable(component);
                } else{
                    component.set("v.hasRecords", "false");
                }
            } else{
            	component.set("v.hasRecords", "false");
            }
        } else{
     	   helper.getAccountList(component);
    	}
    },
    refreshResults: function(component, event, helper){
        if(component.get("v.addSearch")){
           helper.getRecordList(component);
        } else{
     	   helper.getAccountList(component);
    	}
    },
    setValues: function(component, event, helper){
        var selectedIds = [];
        var allRecords = component.get("v.records");
        for(var each in allRecords){
            if(allRecords[each].isSelected){
                selectedIds.push(allRecords[each].objId);
            }
        }
        component.set("v.selectedIds", selectedIds);
    },
    searchRecords: function(component, event, helper){
        helper.getRecordList(component);
    }
})