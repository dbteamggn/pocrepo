({
    getRecordList: function(component) {
        var searchStr = component.find("searchText").get("v.value");
        if(typeof searchStr !== 'undefined' && searchStr !== null && searchStr.length >= 2){
            var action = component.get("c.getRecords");
            var objectName = component.get("v.object"); //'Account';
            var whereClause = component.get("v.whereClause");
            var orderBy = component.get("v.orderBy");
            var limitValue = component.get("v.limitValue");
            var fieldsList = JSON.stringify(component.get("v.fields"));
            var excludeIds = component.get("v.excludeIds");
            
            if(component.get("v.appendWithSearch")){
            	if(component.get("v.selectedIds") !== null && component.get("v.selectedIds").length > 0){
                    var selectedIds = component.get("v.selectedIds");
                    if(excludeIds !== null){
                        for(var eachId in selectedIds){
                            excludeIds.push(selectedIds[eachId]);
                        }
                    } else{
                        excludeIds = component.get("v.selectedIds");
                    }
                }
            }
            action.setParams({
                "objectName": objectName,
                "fieldNames": fieldsList,
                "whereClause": whereClause,
                "orderBy": orderBy,
                "limitValue": limitValue,
                "searchStr": searchStr,
                "searchFieldNames" : JSON.stringify(component.get("v.searchFields")),
                "excludeIds": excludeIds,
                "excludeIdFieldName": component.get("v.excludeIdFieldName"),
                "idFieldName": component.get("v.idFieldName"),
                "selectedIds": []
            });
            action.setCallback(this, function(actionResult) {
                var objRecs = actionResult.getReturnValue();
                if(component.get("v.appendWithSearch")){
                    var existingRecs = component.get("v.records");
                    for(var eachRec in existingRecs){
                        if(existingRecs[eachRec].isSelected){
                            objRecs.push(existingRecs[eachRec]);
                        }
                    }
                }
                component.set("v.records", objRecs);  
                var hasResVal = false;
                if(objRecs !== null && objRecs.length > 0){
                    hasResVal = true;
                }
                if(component.get("v.hasRecords") != hasResVal){
                    component.set("v.hasRecords", hasResVal);
                }
            });
            $A.enqueueAction(action);
        } else{
            if(!component.get("v.appendWithSearch")){
            	component.set("v.records", []);
                if(component.get("v.hasRecords")){
                    component.set("v.hasRecords", false);
                }
            }
        }
    },
	getAccountList: function(component) {
        var action = component.get("c.getRecords");
        var objectName = component.get("v.object"); //'Account';
        var whereClause = component.get("v.whereClause");
        var orderBy = component.get("v.orderBy");
        var limitValue = component.get("v.limitValue");
        var fieldsList = JSON.stringify(component.get("v.fields"));//'["Id", "Name", "NumberOfEmployees", "Industry", "Type"]';
          action.setParams({
              "objectName": objectName,
              "fieldNames": fieldsList,
              "whereClause": whereClause,
              "orderBy": orderBy,
              "limitValue": limitValue,
              "searchStr": '',
              "searchFieldNames": '',
              "excludeIds": [],
              "excludeIdFieldName": component.get("v.excludeIdFieldName"),
              "idFieldName": component.get("v.idFieldName"),
              "selectedIds": []
          });
        action.setCallback(this, function(actionResult) {
            var objRecs = actionResult.getReturnValue();
            component.set("v.records", objRecs);  
            var each;
            var allIds = [];
            for(each in objRecs){
                allIds.push(objRecs[each].objId)
            }
            component.set("v.allIds", allIds);
        });
        $A.enqueueAction(action);
  	},
    fillTable: function(component){
        var action = component.get("c.getRecords");
		var fieldsList = JSON.stringify(component.get("v.fields"));
        action.setParams({
            "objectName": component.get("v.object"),
            "fieldNames": fieldsList,
            "whereClause": '',
            "orderBy": '',
            "limitValue": '',
            "searchStr": '',
            "searchFieldNames" : '',
            "excludeIds": [],
            "excludeIdFieldName": '',
            "idFieldName": component.get("v.idFieldName"),
            "selectedIds": component.get("v.selectedIds")
        });
        action.setCallback(this, function(actionResult){
            component.set("v.records", actionResult.getReturnValue());
        });
        $A.enqueueAction(action);
    }
})