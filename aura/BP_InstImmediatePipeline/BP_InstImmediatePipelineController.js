({
	doInit : function(component, event, helper) {
        helper.displayOpportunities(component);
	},
    prev : function(component, event, helper) {
        component.set("v.stepNum", component.get("v.stepNum") - 1);
    },
    next : function(component, event, helper) {
        component.set("v.stepNum", component.get("v.stepNum") + 1);
    },    
    insertBPO : function(component, event, helper) {
    	helper.insertBusinessPlanOpp(component);
        helper.updateBPRec(component);
        component.set("v.stepNum", component.get("v.stepNum") + 1);
    },
    navigateToOpportunity : function(component, event, helper){
        var oppId = event.target.getAttribute('data-index');        
        sforce.one.navigateToSObject(oppId);        
        return false;
    }
})