({
	doInit: function(component, event, helper) {
        var eventId = component.get('v.recordId');
        console.log(eventId);
        var action = component.get("c.getEventDetailsFundsAndStrategies");
        var data;
        action.setParams({
            "eventid": eventId
        });
        action.setCallback(this, function(a) {
            data = a.getReturnValue();
            console.log('1212121212121212',data);
            var myEvent = $A.get("e.c:passFunds");
            myEvent.setParams({
                "selectedFunds" : data,
            });
            myEvent.fire();
        });
        $A.enqueueAction(action);       
    },
    FundOnFormClickEdit: function(component, event, helper) {
        var selectedFunds = event.getParam("selectedFunds");
        console.log(selectedFunds);
        var ids=[];
        if(selectedFunds.length>0){
            console.log('if');          
            for(var items in selectedFunds){
                ids.push(selectedFunds[items].id);
            }
        }
        console.log(ids);
        var action = component.get("c.updateEventFundsAndStrategies");
        action.setParams({
            "FundStratIds": ids,
            "eventId" : component.get('v.recordId')
        });
        action.setCallback(this, function(a) {
            console.log('recordId',a.getReturnValue());
            var recordId = a.getReturnValue();
            if(typeof sforce !== "undefined" && sforce !== null) {
                sforce.one.navigateToURL('/'+recordId);
            }else{
                var navEvt = $A.get("e.force:refreshView");
                /*navEvt.setParams({
                        "recordId": recordId,
                        "slideDevName": "related"
                });*/
                navEvt.fire();
            }
        });
        $A.enqueueAction(action);     
    }
})