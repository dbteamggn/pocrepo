({
    prev : function(component, event, helper){
        component.set("v.stepNum", component.get("v.stepNum") - 1);
    }, 
    next : function(component, event, helper){
        component.set("v.stepNum", component.get("v.stepNum") + 1);
    },
    // Check for the territory/region/etc for this business plan.
    // Load existingClientIds with the list of clients currently assigned to the BP.
    doInit : function(component, event, helper) {
            var action = component.get("c.getBPSettings");
            action.setParams({
                "bpId": component.get("v.recordId")
            });
            action.setCallback(this, function(response){
                var state = response.getState();
                var returnedBp = response.getReturnValue();
            });
            $A.enqueueAction(action);
            // Store the client Ids returned by the table in an atribute
            helper.reloadClientIds(component);
    },
                
    // Add the selected client to the list of key clients for this business plan
    addClientPushed : function(component, event, helper) {
        var action = component.get("c.addClient");
        console.log('client',component.get("v.clientId"));
         console.log('recordId',component.get("v.recordId"));
        action.setParams({
            "bpId": component.get("v.recordId"),
            "clientId": component.get("v.clientId")
        });
                                
        action.setCallback(this, function(response){
            var state = response.getState();
            var newClientIdList = component.get("v.existingClientIds");
            var clientIdsList = component.get("v.clientId");
            for(var each in clientIdsList){
            	newClientIdList.push(clientIdsList[each]);
            }
            component.set("v.existingClientIds", newClientIdList);
            var evt = $A.get("e.c:RefreshLightningComponentEvent");
            evt.fire();
        });
        $A.enqueueAction(action);
    },
    
    // Remove the selected clients from the list of key clients for this business plan
    removeClientsPushed : function(component, event, helper) {

        var action = component.get("c.removeClients");
        action.setParams({
           "bpcrIds": component.get("v.selectedIds")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            helper.reloadClientIds(component);
            var evt = $A.get("e.c:RefreshLightningComponentEvent");
            evt.fire();
        });
        $A.enqueueAction(action);
    },
})