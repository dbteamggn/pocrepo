({
	doInit: function(component, event, helper) {
        var crId = component.get('v.recordId');
        console.log(crId);
        var action = component.get("c.getCallReportClientParticipantDetails");
        var data;
        action.setParams({
            "crId": crId
        });
        action.setCallback(this, function(a) {
            data = a.getReturnValue();
            var queryParam = component.get('v.queryParam');
            var myCallReport = $A.get("e.c:passData");
            myCallReport.setParams({
                "selectedContact" : data,
                "queryParam" : queryParam
            });
            myCallReport.fire();
        });
        $A.enqueueAction(action);       
    },
    NameOnFormClickEdit: function(component, event, helper) {
        var selectedContact = event.getParam("selectedContact");
        console.log(selectedContact);
        if(selectedContact.length>0){
            console.log('if');
            var ids=[];
            for(var items in selectedContact){
                ids.push(selectedContact[items].id);
            }
            console.log(ids);
            var action = component.get("c.updateClientParticipants");
            action.setParams({
                "contactIds": ids,
                "crId" : component.get('v.recordId')
            });
            action.setCallback(this, function(a) {
                console.log('recordId-Ram',a.getReturnValue());
                var recordId = a.getReturnValue();
                if(typeof sforce !== "undefined" && sforce !== null) {
                    sforce.one.navigateToURL('/'+recordId);
                }else{
                    var navEvt = $A.get("e.force:refreshView");
                    /*navEvt.setParams({
                        "recordId": recordId,
                        "slideDevName": "related"
                    });*/
                    navEvt.fire();
                }
            });
            $A.enqueueAction(action);     
        }
    }
})