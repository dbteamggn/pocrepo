({
	drawTable : function(component) {
        var ratedStrategyMap = component.get("v.ratedStrategyMap");
        var thirdParties = component.get("v.thirdParties");
        var strategies = component.get("v.strategies");
        var strategyPage = component.get("v.strategyPage");
        var strategyPerPage = component.get("v.strategyPerPage");
        
        var colPercentage = 100/(strategyPerPage+1); 
        
        var tile = '';
        // If both third parties and strategies have not been set up
        if(thirdParties.length == 0 && strategies.length == 0){
            // Display a message & exit
            tile = 'No Third Parties or Investment Strategies are currently configured for this Client.';
            component.set("v.strategyFinalPage", 1);
            document.getElementById("consultantRatingsDetail").innerHTML = tile;
            return;
        }
        
        tile = '<table class="slds-table slds-table--bordered slds-table--cell-buffer slds-table--fixed-layout" style="width: 100%;">';
        tile += '<tr><td style="width:' + colPercentage + '%; min-width:150px; word-wrap: break-word;">Consultant</td>';
               
                
        for (var i = (strategyPage - 1) * strategyPerPage; i < strategyPage * strategyPerPage && i < strategies.length; i++) { 
            tile += '<td class="slds-cell-wrap" style="width:' + colPercentage + '%; min-width:150px; word-wrap: break-word;">' + strategies[i].Name + '</td>';
            console.log(strategies[i].Name);
           /* if (i==(strategies.length -1)){
                for (var j = i; j<(strategyPage * strategyPerPage) -1; j++){
                    tile += '<td style="width:' + colPercentage + '%; min-width:150px; word-wrap: break-word;"></td>';
                }
            }*/
        }
        tile += '</tr>';
        for (var i = 0; i < thirdParties.length; i++) { 
            tile += '<tr><td class="slds-cell-wrap" style="width:' + colPercentage + '%; min-width:150px; word-wrap: break-word;">' + thirdParties[i].Name + '</td>';
            
            for (var j = (strategyPage - 1) * strategyPerPage; j < strategyPage * strategyPerPage && j < strategies.length; j++) { 
                var ratedStrategy = ratedStrategyMap[thirdParties[i].Id + ':' + strategies[j].Id];
                if (ratedStrategy === undefined){
                    tile += '<td></td>';
                } else {
                    tile += '<td';
                    if (ratedStrategy.substring(0,4) == 'Sell'){
                        tile += ' style="background:red; color:white; width:' + colPercentage + '%; min-width:150px; word-wrap: break-word;"';
                    } else if (ratedStrategy.substring(0,3) == 'Buy'){
                        tile += ' style="background:green; color:white; width:' + colPercentage + '%; min-width:150px; word-wrap: break-word;"';
                    } else if (ratedStrategy == 'DU'){
                        tile += ' style="background:grey; width:' + colPercentage + '%; min-width:150px; word-wrap: break-word;"';
                    } else {
                        tile += ' style="background:yellow; width:' + colPercentage + '%; min-width:150px; word-wrap: break-word;"';
                    }
                    tile += '>' + ratedStrategy + '</td>';
                }
            }
            
            tile += '</tr>';
            
        }
        tile += '</table>';
        
        document.getElementById("consultantRatingsDetail").innerHTML = tile;
	}
})