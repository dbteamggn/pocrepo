({
	doInit : function(component, event, helper) {
		
        var desiredPixelSizePerColumn = component.get("v.desiredPixelSizePerColumn");
        
        var strategyPerPage = Math.floor(screen.width/desiredPixelSizePerColumn);  
        if(strategyPerPage==0){
            strategyPerPage = 1;
        }
        component.set("v.strategyPerPage", strategyPerPage);
        console.log('component.g');
        console.log(component.get("v.accId"));
		var action2 = component.get("c.getConRatingData");
        action2.setParams({
			"accId":component.get("v.accId")
		});
        action2.setCallback(this, function(response){
            var state = response.getState();
            console.log('state');
            console.log(state);
            if (state === "SUCCESS") {
                console.log('getReturnValue');
               var  result = response.getReturnValue();
                component.set("v.thirdParties",result.thirdParties);
                component.set("v.strategyFinalPage", Math.ceil(result.strategies.length / strategyPerPage));
                component.set("v.strategies", result.strategies);
                component.set("v.ratedStrategyMap", result.RatedStrategyMap);
                helper.drawTable(component);
            }
        });
	 	$A.enqueueAction(action2);
        /*
        var action3 = component.get("c.GetStrategies");
        action3.setParams({
			"accId":component.get("v.accId")
		});
        action3.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var strategies = response.getReturnValue();
                component.set("v.strategyFinalPage", Math.ceil(strategies.length / strategyPerPage));
                component.set("v.strategies", response.getReturnValue());
            }
        });
	 	$A.enqueueAction(action3);
            
        var action4 = component.get("c.GetStrategyRating");
        
        action4.setCallback(this, function(response){
            var state = response.getState();
            console.log('getReturnValue');
            console.log(state);
            if (state === "SUCCESS") {
                
                component.set("v.ratedStrategyMap", response.getReturnValue());
                console.log(response.getReturnValue());
                helper.drawTable(component);
            }
        });
	 	$A.enqueueAction(action4);*/
	},
    
    next : function(component, event, helper) {
        component.set("v.strategyPage", component.get("v.strategyPage") + 1);
        helper.drawTable(component);
    },
    prev : function(component, event, helper) {
        component.set("v.strategyPage", component.get("v.strategyPage") - 1);
        if (component.get("v.strategyPage") == 1){
            var previousButton = component.find("previousButton");
        	$A.util.toggleClass(previousButton, "toggle");
        }
        helper.drawTable(component);
    }
})