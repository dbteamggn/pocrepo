({
	doInit: function(component, event, helper) {
        var crId = component.get('v.recordId');
        console.log(crId);
        var action = component.get("c.getCallReportDBParticipantDetails");
        var data;
        action.setParams({
            "crId": crId
        });
        action.setCallback(this, function(a) {
            data = a.getReturnValue();
            console.log("This comes here");
            console.log(data);
            var queryParam = component.get('v.queryParam');
            var myCallReport = $A.get("e.c:passUsers");
           
            myCallReport.setParams({
                "selectedUser" : data,
                "queryParam" : queryParam
            });
            myCallReport.fire();
        });
        $A.enqueueAction(action);       
    },
    NameOnFormClickEdit: function(component, event, helper) {
        var selectedUser = event.getParam("selectedUser");
        console.log(selectedUser);
        if(selectedUser.length>0){
            console.log('if');
            var ids=[];
            for(var items in selectedUser){
                ids.push(selectedUser[items].id);
            }
            
            console.log(ids);
            var action = component.get("c.updateDBParticipants");
           
            action.setParams({
                "userIds": ids,
                "crId" : component.get('v.recordId')
            });
            action.setCallback(this, function(a) {
                console.log('recordId',a.getReturnValue());
                var recordId = a.getReturnValue();
                if(typeof sforce !== "undefined" && sforce !== null) {
                    sforce.one.navigateToURL('/'+recordId);
                }else{
                    var navEvt = $A.get("e.force:refreshView");
                    /*navEvt.setParams({
                        "recordId": recordId,
                        "slideDevName": "related"
                    });*/
                    navEvt.fire();
                }
            });
            $A.enqueueAction(action);     
        }
    }
})