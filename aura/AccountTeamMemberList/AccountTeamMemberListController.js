({
	doInit: function(component, event){
        component.set("v.Msg", "Success");
		var action = component.get("c.findAll");
        var recordId = component.get('v.recordId');
        action.setParams({
                "recordId" : recordId
            });
            action.setCallback(this, function(a) {
                console.log('data',a.getReturnValue());
                var TMlist = a.getReturnValue();
                for(var item in TMlist){
                    if(TMlist[item].value==true){
                        component.set("v.selectedPrimary",TMlist[item].UserId);
                    }
                }
                component.set("v.AccountTMs", a.getReturnValue());
            });
            $A.enqueueAction(action);
    },	
    setPrimary: function(component, event){
        var TMlist = component.get("v.AccountTMs");
        var selPrimary = component.get("v.selectedPrimary");
        console.log('selPrimary',selPrimary);
        for(var item in TMlist){
            if(TMlist[item].UserId==selPrimary && TMlist[item].value==true){
                TMlist[item].value = false;
            }
            else if(TMlist[item].UserId!=selPrimary && TMlist[item].value==true){
                component.set("v.selectedPrimary",TMlist[item].UserId);
            }
            else if(TMlist[item].UserId==selPrimary && TMlist[item].value==false){
                component.set("v.selectedPrimary","");
            }
        }
        component.set("v.AccountTMs",TMlist);
    },
    updatePrimaryTM: function(component,event){
        var action = component.get("c.updatePrimary");
        var selPrimary = component.get("v.selectedPrimary");
        var recordId = component.get('v.recordId');
        console.log("FinalSeldPrimary",selPrimary);
        action.setParams({
                "accountId": recordId,
                "primaryId": selPrimary,
            	"errMsg": ''
            });
            action.setCallback(this, function(a) {
              console.log('stat',a.getReturnValue());
              var stat = a.getReturnValue();
              component.set("v.Msg", stat);
              if (stat == "Success")  {
                if(typeof sforce !== "undefined" && sforce !== null) {
                    console.log("I am in sforce section");
                    sforce.one.navigateToURL('/' + recordId);
                }else{
                    var navEvt = $A.get("e.force:navigateToSObject");
                    console.log("I am in eforce section",recordId);
                    navEvt.setParams({
                        "recordId": recordId,
                        "slideDevName": "detail"
                    });
                    navEvt.fire();
                }
              }      
            });
        $A.enqueueAction(action);
    },
    CancelOp: function(component,event){
      	var recordId = component.get('v.recordId');
        if(typeof sforce !== "undefined" && sforce !== null) {
                    console.log("I am in sforce section");
                    sforce.one.navigateToURL('/' + recordId);
        }else{
           var navEvt = $A.get("e.force:navigateToSObject");
           console.log("I am in eforce section",recordId);
           navEvt.setParams({
               "recordId": recordId,
               "slideDevName": "detail"
           });
           navEvt.fire();
         }  
    },
})