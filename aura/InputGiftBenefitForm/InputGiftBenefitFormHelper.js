({
	getPicklistValues : function(component) {
        var action1 = component.get("c.getPicklistValues");
        action1.setParams({
            "fieldName": "given_received__c"
        });
        action1.setCallback(this, function(response){
            var state = response.getState();
            console.log(state);
            if(component.isValid() && state == "SUCCESS"){
                var opts = [];
                var vals = response.getReturnValue();
                for(var each in vals){
                    var oneOption = {class: "optionClass", label: vals[each], value: vals[each]};
                    opts.push(oneOption);
                }
                component.find("givenreceived").set("v.options", opts);
            }
        });
        $A.enqueueAction(action1);
        var action2 = component.get("c.getPicklistValues");
        action2.setParams({
            "fieldName": "gbe_type__c"
        });
        action2.setCallback(this, function(response){
            var state = response.getState();
            console.log(state);
            if(component.isValid() && state == "SUCCESS"){
                var opts = [];
                var vals = response.getReturnValue();
                for(var each in vals){
                    var oneOption = {class: "optionClass", label: vals[each], value: vals[each]};
                    opts.push(oneOption);
                }
            	component.find("type").set("v.options", opts);
            }
        });
        $A.enqueueAction(action2);
        var action3 = component.get("c.getPicklistValues");
        action3.setParams({
            "fieldName": "gbe_sub_type__c"
        });
        action3.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS"){
                var opts = [];
                var vals = response.getReturnValue();
                for(var each in vals){
                    var oneOption = {class: "optionClass", label: vals[each], value: vals[each]};
                    opts.push(oneOption);
                }
            	component.find("subtype").set("v.options", opts);
            }
        });
        $A.enqueueAction(action3);
        var action4 = component.get("c.getPicklistValues");
        action4.setParams({
            "fieldName": "currencyisocode"
        });
        action4.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS"){
            	var opts = [];
                var vals = response.getReturnValue();
                for(var each in vals){
                    var oneOption = {class: "optionClass", label: vals[each], value: vals[each]};
                    opts.push(oneOption);
                }
                component.find("currency").set("v.options", opts);
            }
        });
        $A.enqueueAction(action4);
	},
})