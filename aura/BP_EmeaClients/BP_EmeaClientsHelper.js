({
    reloadClientIds : function(component, event, helper) {
    	var action = component.get("c.getClientIdList");
        action.setParams({
            "bpId": component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            component.set("v.existingClientIds", response.getReturnValue());
        });
        $A.enqueueAction(action);
	}
})