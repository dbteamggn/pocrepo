({
    // Check for the territory/region/etc for this business plan.
    // Load existingClientIds with the list of clients currently assigned to the BP.
    doInit : function(component, event, helper) {
        if (!component.get("v.isRetail")){
            var action = component.get("c.getBPSettings");
            action.setParams({
                "bpId": component.get("v.recordId")
            });
            action.setCallback(this, function(response){
                var state = response.getState();
                var returnedBp = response.getReturnValue();
                if(returnedBp != null && returnedBp.Country__c != null &&  returnedBp.Country__c != ''){
                    component.set("v.whereCountry", "Billing_Country__c='" + returnedBp.Country__c + "' AND ParentId = NULL");
                    component.set("v.whereTerritory", '');
                } else if(returnedBp != null && returnedBp.BP_Territory__c != null &&  returnedBp.BP_Territory__c != ''){
                    component.set("v.whereTerritory", "Territory__c='" + returnedBp.BP_Territory__c + "' AND Client_Third_Party__r.ParentId = NULL");
                    component.set("v.whereCountry", '');
                } else {
                    component.set("v.whereTerritory", '');
                    component.set("v.whereCountry", '');
                }
            });
            $A.enqueueAction(action);
        }
        // Store the client Ids returned by the table in an atribute
        helper.reloadClientIds(component);
    },
                
    // Add the selected client to the list of key clients for this business plan
    addClientPushed : function(component, event, helper) {
        var action = component.get("c.addClient");
        action.setParams({
            "bpId": component.get("v.recordId"),
            "clientId": component.get("v.clientId")
        });
                                
        action.setCallback(this, function(response){
            var state = response.getState();
            var newClientIdList = component.get("v.existingClientIds");
            var clientIdsList = component.get("v.clientId");
            for(var each in clientIdsList){
            	newClientIdList.push(clientIdsList[each]);
            }
            component.set("v.existingClientIds", newClientIdList);
            var evt = $A.get("e.c:RefreshLightningComponentEvent");
            evt.fire();
        });
        $A.enqueueAction(action);
    },
    // Remove the selected clients from the list of key clients for this business plan
    removeClientsPushed : function(component, event, helper) {

        var action = component.get("c.removeClients");
        action.setParams({
           "bpcrIds": component.get("v.selectedIds")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            helper.reloadClientIds(component);
            var evt = $A.get("e.c:RefreshLightningComponentEvent");
            evt.fire();
        });
        $A.enqueueAction(action);
    },
    
    // Check the Key Client flag on the business plan client relation for added client
    addKeyClientPushed : function(component, event, helper) {
        var action = component.get("c.setKeyClient");
        action.setParams({
            "bpId": component.get("v.recordId"),
            "clientId": component.get("v.clientId")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            var newClientIdList = component.get("v.existingClientIds");
            var clientIdsList = component.get("v.clientId");
            for(var each in clientIdsList){
            	newClientIdList.push(clientIdsList[each]);
            }
            //newClientIdList.push(component.get("v.clientId"));
            component.set("v.existingClientIds", newClientIdList);
            var evt = $A.get("e.c:RefreshLightningComponentEvent");
            evt.fire();
        });
        $A.enqueueAction(action);
    },
    
    // Uncheck Key Client flag on the business plan client relation for selected clients
    removeKeyClientsPushed : function(component, event, helper) {
        var action = component.get("c.removeKeyClient");
        action.setParams({
            "bpcrIds": component.get("v.selectedIds")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            helper.reloadClientIds(component);
            var evt = $A.get("e.c:RefreshLightningComponentEvent");
            evt.fire();
        });
        $A.enqueueAction(action);
    },
    prev : function(component, event, helper){
        component.set("v.stepNum", component.get("v.stepNum") - 1);
    }, 
    next : function(component, event, helper){
        component.set("v.stepNum", component.get("v.stepNum") + 1);
    }
})