({
	doInit : function(component, event, helper) {
        
        var recordId = component.get("v.recordId");
        console.log('recordId : ' + recordId);
        
        var action = component.get('c.assignToEMDebtCampaign');
        action.setParams({
			eventId: recordId
        });
        
        action.setCallback(this, function(response){
            var status = response.getState();
            console.log("status" + status);
            
            if(status === "SUCCESS"){
                //component.set("v.accountName", response.getReturnValue());
                
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "type": "success",
                    "message": "Campaign Members have been added successfully."
                });
	            toastEvent.fire();
                
                var urlCampaign = $A.get("e.force:navigateToURL");
                urlCampaign.setParams({
                  "url": "/one/one.app#/sObject/7010v0000001Y56AAE/view"
                });
	            urlCampaign.fire();
                
            } else {
                
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "type": "error",
                    "message": "There is some internal error, please try again."
                });
	            toastEvent.fire();
                
                var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                  "url": "/one/one.app#/sObject/"+recordId+"/view"
                });
	            urlEvent.fire();
                
            }
        });
        
		$A.enqueueAction(action);
    }
})