({
    getoptyData : function(component, event, helper) {
	var getTableData = component.get("c.getOptyTableData");
        getTableData.setParams({
			"accId":component.get("v.accId")
		});
        getTableData.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                 component.set("v.lstOpp", response.getReturnValue());
            }
            });
        
         $A.enqueueAction(getTableData);
    
        
},
    getoppFunnelData : function(component, event, helper) {
		console.log('=======Funnel');
        
        var getChartData = component.get("c.prepareFunnelData");
        getChartData.setParams({
			"accId":component.get("v.accId")
		});
        getChartData.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var chartWrapper = response.getReturnValue();
                console.log('===funnel='+JSON.stringify(chartWrapper.series));
                var cdata = [] ;
                 for(var i=0; i< chartWrapper.series[0].data.length;i++)
                {
                    var obj = chartWrapper.series[0].data[i]; 
                    var arrEle = [];
                    arrEle.push(obj.name);
                    arrEle.push(obj.sumAmt);
                    cdata.push(arrEle);
                }
                
               console.log('cdata');
                console.log(cdata);
                
                console.log(chartWrapper.series);
                new Highcharts.Chart({
                    chart: {
                        renderTo: component.find("chart").getElement(),
                        type: 'funnel'
                    },
                    
                    title: {
                        text: chartWrapper.funnelChartDetails.chartTitle
                    },
                    
                    tooltip: {
                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                     plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b> ({point.y:,.0f})',
                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                softConnector: true
            },
            center: ['40%', '50%'],
            neckWidth: '30%',
            neckHeight: '25%',
            width: '80%'
        }
    },
    legend: {
        enabled: false
    },
   
                    series: [{
        name: chartWrapper.series[0].name,
        data: cdata
                    }]
                   
                        });
            }
        });
        console.log('====Testing');
        $A.enqueueAction(getChartData);
	}
})