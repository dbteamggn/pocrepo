({
	setUIValues : function(component) {
		var actionP = component.get("c.getPicklistValues");
        actionP.setParams({
            "objName": "Business_Plan_Actions__c",
            "fieldName": "Focus__c"
        });
        actionP.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS"){
                var opts = [];
                var vals = response.getReturnValue();
                for(var each in vals){
                    var oneOption = {class: "optionClass", label: vals[each], value: vals[each]};
                    opts.push(oneOption);
                }
                component.find("focus").set("v.options", opts);
            }
        });
        $A.enqueueAction(actionP);
        
        var actionBPC = component.get("c.getLookupValues");
        actionBPC.setParams({
            "bpRecId": component.get("v.recordId")
        });
        actionBPC.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS"){
                var opts = [];
                var vals = response.getReturnValue();
                for(var each in vals){
                    var oneOption = {class: "optionClass", label: vals[each].name, value: vals[each].id};
                    opts.push(oneOption);
                }
                component.find("clientId").set("v.options", opts);
            }
        });
        $A.enqueueAction(actionBPC);
        
        var whereClause = "Business_Plan__c='" + component.get("v.recordId") + "'";
        component.set("v.whereClause", whereClause);
	},
    createEvent : function(component){
        var actionInst = new Object();
        actionInst.Name = component.find("name").get("v.value");
        actionInst.Business_Plan_Client__c = component.find("clientId").get("v.value");
        actionInst.Business_Plan__c = component.get("v.recordId");
        actionInst.Detail__c = component.find("detail").get("v.value");
        actionInst.Field_Coverage__c = component.find("fieldCoverage").get("v.value");
        actionInst.Focus__c = component.find("focus").get("v.value");
        actionInst.Home_Office_Value_Add__c = component.find("valueAdd").get("v.value");
        
        var ownerNames = '';
        var allUsers = component.get("v.selectedUsers");
        for(var each in allUsers){
            ownerNames += allUsers[each].name + ';';
        }
        actionInst.Owners__c = ownerNames;
        
        var isError=false;
        if( typeof actionInst.Name === 'undefined' || actionInst.Name === null || actionInst.Name == ''){
            component.find("name").set("v.errors", [{message:"Please enter " + component.find("name").get("v.label") }]);
            isError=true;
        }else{
            component.find("name").set("v.errors", null );
        }
        if( typeof actionInst.Business_Plan_Client__c === 'undefined' || actionInst.Business_Plan_Client__c === null || actionInst.Business_Plan_Client__c == ''){
            component.find("clientId").set("v.errors", [{message:"Please select a " + component.find("clientId").get("v.label") }]);
            isError=true;
        } else{
            component.find("clientId").set("v.errors", null );
        }
        if(!isError){
            var jsonString = JSON.stringify(actionInst);
            var action = component.get("c.createBPAction");
            var selectedProducts = component.get("v.selectedFund");
            action.setParams({
                "jsonStr": jsonString,
                "fundStr": JSON.stringify(selectedProducts)
            });
            action.setCallback(this, function(response){
                component.set("v.actionId", response.getReturnValue());
                component.set("v.stepNum", component.get("v.stepNum") + 1);
            });
            $A.enqueueAction(action);
        }
    }
})