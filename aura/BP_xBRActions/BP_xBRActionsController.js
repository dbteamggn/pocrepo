({
	doInit : function(component, event, helper) {
        helper.setUIValues(component);
	},
    prev : function(component, event, helper){
        component.set("v.stepNum", component.get("v.stepNum") - 1);
    }, 
    next : function(component, event, helper){
        helper.createEvent(component);
    },
    skipActions : function(component, event, helper){
        component.set("v.stepNum", component.get("v.stepNum") + 2);
    },
    showFundList: function(component, event) {     
        var selectedFund = component.get('v.selectedFund');       
        var myEvent = $A.get("e.c:FundOnFormClick");
        myEvent.setParams({
            "compNameToHide": "form",
            "compNameToShow": "fundList",
            "selectedFunds": selectedFund
        });
        myEvent.fire();
    },
    passFunds: function(component, event) {
        var selectedFunds = event.getParam("selectedFunds");
        var allNames='';
        for(var items in selectedFunds){
            allNames=allNames+selectedFunds[items].name+' ; ';
        }
        component.find("funds").set('v.value',allNames);
        component.set('v.selectedFund',selectedFunds);
    },
    FundOnFormClick : function(component, event){
        var compNameToHide = event.getParam("compNameToHide");
        var compNameToShow = event.getParam("compNameToShow");
        var selectedFunds = event.getParam("selectedFunds");
        var hideComp = component.find(compNameToHide);
        $A.util.addClass(hideComp, "hide");
        var showComp = component.find(compNameToShow);
        $A.util.removeClass(showComp, "hide");
        var myEvent = $A.get("e.c:passFunds");
        myEvent.setParams({
            "selectedFunds": selectedFunds
        });
        myEvent.fire();
    },
    showUserList: function(component, event) {
        var selectedUser = component.get('v.selectedUsers'); 
        var myEvent = $A.get("e.c:NameOnFormClick");
        myEvent.setParams({
            "compNameToHide": "form",
            "compNameToShow": "userList",
            "selectedContact": selectedUser,
            "queryParam" : ""
        });
        myEvent.fire();
    },
    passUsers: function(component, event) {
        var selectedUser = event.getParam("selectedUser");
        var allNames='';
        for(var items in selectedUser){
            allNames=allNames+selectedUser[items].name+' ; ';
        }
        component.find("users").set('v.value',allNames);
        component.set('v.selectedUsers',selectedUser);
    },
    NameOnFormClick : function(component, event) {  
        var compNameToHide = event.getParam("compNameToHide");
        var compNameToShow = event.getParam("compNameToShow");
        var selectedUser = event.getParam("selectedContact");
        var queryParam = event.getParam("queryParam");
        var hideComp = component.find(compNameToHide);
        $A.util.addClass(hideComp, "hide");
        var showComp = component.find(compNameToShow);
        $A.util.removeClass(showComp, "hide");
        var myEvent = $A.get("e.c:passUsers");   
        myEvent.setParams({
            "selectedUser": selectedUser
        });
        myEvent.fire();       
    }
})