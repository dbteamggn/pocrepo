({
	navigateToRec: function(component, event, helper){
        var selectedVals = component.get("v.selectedIds");
        if(typeof selectedVals !== "undefined" && selectedVals !== null){
            var errorDiv = component.find('errorDiv');
            if(selectedVals.length > 1){
                $A.util.removeClass(errorDiv, 'slds-hide');
                component.set("v.errorMessage", "Choose only one record");
            } else if(selectedVals.length < 1){
                $A.util.removeClass(errorDiv, 'slds-hide');
                component.set("v.errorMessage", "Choose a record to proceed");
            } else if(selectedVals.length == 1){
                $A.util.addClass(errorDiv, 'slds-hide');
                component.set("v.recordId", selectedVals.pop());
                component.set("v.stepNum", component.get("v.stepNum") + 1);
            }
        }
    },
    createNew: function(component, event, helper){        
        component.set("v.newBPMode", true);
        
        var errorDiv = component.find('errorDiv');
        $A.util.addClass(errorDiv, 'slds-hide');
        var actionProfile = component.get("c.createNewInstiPlan");
        actionProfile.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS"){
                component.set("v.recordId", response.getReturnValue());
                component.set("v.stepNum", component.get("v.stepNum") + 1);
            }
        });
        $A.enqueueAction(actionProfile);
    }
})