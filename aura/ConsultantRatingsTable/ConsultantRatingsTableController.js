({
	doInit : function(component, event, helper) {
		
        var desiredPixelSizePerColumn = component.get("v.desiredPixelSizePerColumn");
        
        var strategyPerPage = Math.floor(screen.width/desiredPixelSizePerColumn);  
        if(strategyPerPage==0){
            strategyPerPage = 1;
        }
        component.set("v.strategyPerPage", strategyPerPage);
        
		var action2 = component.get("c.GetThirdParties");
        action2.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.thirdParties", response.getReturnValue());
            }
        });
	 	$A.enqueueAction(action2);
        
        var action3 = component.get("c.GetStrategies");
        action3.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var strategies = response.getReturnValue();
                component.set("v.strategyFinalPage", Math.ceil(strategies.length / strategyPerPage));
                component.set("v.strategies", response.getReturnValue());
            }
        });
	 	$A.enqueueAction(action3);
            
        var action4 = component.get("c.GetStrategyRating");
        action4.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                
                component.set("v.ratedStrategyMap", response.getReturnValue());
                
                helper.drawTable(component);
            }
        });
	 	$A.enqueueAction(action4);
	},
    addThirdParty : function(component, event, helper) {
        var evt = $A.get("e.c:ConsultantRatingsConfigEvent");
        evt.fire();
    },
    viewActivites : function(component, event, helper) {
        var evt = $A.get("e.c:ActivitiesEvent");
        evt.fire();
    },
    next : function(component, event, helper) {
        component.set("v.strategyPage", component.get("v.strategyPage") + 1);
        helper.drawTable(component);
    },
    prev : function(component, event, helper) {
        component.set("v.strategyPage", component.get("v.strategyPage") - 1);
        if (component.get("v.strategyPage") == 1){
            var previousButton = component.find("previousButton");
        	$A.util.toggleClass(previousButton, "toggle");
        }
        helper.drawTable(component);
    }
})