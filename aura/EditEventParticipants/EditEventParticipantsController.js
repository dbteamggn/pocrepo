({
	doInit: function(component, event, helper) {
        var eventId = component.get('v.recordId');
        console.log(eventId);
        var action = component.get("c.getEventDetails");
        var data;
        action.setParams({
            "eventid": eventId
        });
        action.setCallback(this, function(a) {
            data = a.getReturnValue();
            var queryParam = component.get('v.queryParam');
            var myEvent = $A.get("e.c:passData");
            myEvent.setParams({
                "selectedContact" : data,
                "queryParam" : queryParam
            });
            myEvent.fire();
        });
        $A.enqueueAction(action);       
    },
    NameOnFormClickEdit: function(component, event, helper) {
        var selectedContact = event.getParam("selectedContact");
        console.log(selectedContact);
        if(selectedContact.length>0){
            console.log('if');
            var ids=[];
            for(var items in selectedContact){
                ids.push(selectedContact[items].id);
            }
            console.log(ids);
            var action = component.get("c.updateEvent");
            action.setParams({
                "contactIds": ids,
                "eventId" : component.get('v.recordId')
            });
            action.setCallback(this, function(a) {
                console.log('recordId',a.getReturnValue());
                var recordId = a.getReturnValue();
                if(typeof sforce !== "undefined" && sforce !== null) {
                    sforce.one.navigateToURL('/'+recordId);
                }else{
                    var navEvt = $A.get("e.force:refreshView");
                    /*navEvt.setParams({
                        "recordId": recordId,
                        "slideDevName": "related"
                    });*/
                    navEvt.fire();
                }
            });
            $A.enqueueAction(action);     
        }
    }
})