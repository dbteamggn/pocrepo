({
	/**
     * Perform the SObject search via an Apex Controller
     */
    doSearchThirdParties : function(cmp) {
        // Get the search string, input element and the selection container
        
        var searchString = cmp.get('v.thirdPartySearchString');
        var lookupList = cmp.find('thirdPartyLookupList');
        
        // We need at least 2 characters for an effective search
        if (typeof searchString === 'undefined' || searchString.length < 2)
        {
            // Hide the lookuplist
            $A.util.addClass(lookupList, 'slds-hide');
            return;
        }
 
        // Show the lookuplist
        $A.util.removeClass(lookupList, 'slds-hide');
 
        // Create an Apex action
        var action = cmp.get('c.GetThirdPartySearchResults');
 
        // Mark the action as abortable, this is to prevent multiple events from the keyup executing
        action.setAbortable();
 
        // Set the parameters
        action.setParams({ "ThirdPartySearchName" : searchString});
                           
        // Define the callback
        action.setCallback(this, function(response) {
            var state = response.getState();
            // Callback succeeded
            if (cmp.isValid() && state === "SUCCESS")
            {
                // Get the search matches
                var matches = response.getReturnValue();
 
                // If we have no matches, return nothing
                if (matches.length == 0)
                {
                    cmp.set('v.thirdPartyMatches', null);
                    return;
                }
                 
                // Store the results
                cmp.set('v.thirdPartyMatches', matches);
            }
            else if (state === "ERROR") // Handle any error by reporting it
            {
                var errors = response.getError();
                 
                if (errors) 
                {
                    if (errors[0] && errors[0].message) 
                    {
                        this.displayToast('Error', errors[0].message);
                    }
                }
                else
                {
                    this.displayToast('Error', 'Unknown error.');
                }
            }
        });
         
        // Enqueue the action                  
        $A.enqueueAction(action);                
    },
    
    doSearchStrategies : function(cmp) {
        // Get the search string, input element and the selection container
        var searchString = cmp.get('v.strategySearchString');
        var lookupList = cmp.find('strategyLookupList');
        
        // We need at least 2 characters for an effective search
        if (typeof searchString === 'undefined' || searchString.length < 2)
        {
            // Hide the lookuplist
            $A.util.addClass(lookupList, 'slds-hide');
            return;
        }
 
        // Show the lookuplist
        $A.util.removeClass(lookupList, 'slds-hide');
 
        // Create an Apex action
        var action = cmp.get('c.GetStrategySearchResults');
 
        // Mark the action as abortable, this is to prevent multiple events from the keyup executing
        action.setAbortable();
 
        // Set the parameters
        action.setParams({ "StrategySearchName" : searchString});
                           
        // Define the callback
        action.setCallback(this, function(response) {
            var state = response.getState();
 
            // Callback succeeded
            if (cmp.isValid() && state === "SUCCESS")
            {
                // Get the search matches
                var matches = response.getReturnValue();
 
                // If we have no matches, return nothing
                if (matches.length == 0)
                {
                    cmp.set('v.strategyMatches', null);
                    return;
                }
                 
                // Store the results
                cmp.set('v.strategyMatches', matches);
            }
            else if (state === "ERROR") // Handle any error by reporting it
            {
                var errors = response.getError();
                 
                if (errors) 
                {
                    if (errors[0] && errors[0].message) 
                    {
                        this.displayToast('Error', errors[0].message);
                    }
                }
                else
                {
                    this.displayToast('Error', 'Unknown error.');
                }
            }
        });
         
        // Enqueue the action                  
        $A.enqueueAction(action);                
    }
})