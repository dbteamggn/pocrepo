({
    doInit : function(component, event, helper) {
        var action = component.get("c.GetThirdPartyWrappers");
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.thirdParties", response.getReturnValue());
            }
        });
	 	$A.enqueueAction(action);
        var action2 = component.get("c.GetStrategyWrappers");
        action2.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.strategies", response.getReturnValue());
            }
        });
	 	$A.enqueueAction(action2);
    },
    
    NavigateToTable : function(component, event, helper) {
        var evt = $A.get("e.c:ConsultantRatingsTableEvent");
        evt.fire();
    },
    
    OpenStrategiesTab : function(component, event, helper) {
        var strategyTabLi = document.getElementById("strategyTabLi");
        strategyTabLi.setAttribute('class', 'slds-tabs--scoped__item slds-text-heading--label slds-active');
        var thirdPartyTabLi = document.getElementById("thirdPartyTabLi");
        thirdPartyTabLi.setAttribute('class', 'slds-tabs--scoped__item slds-text-heading--label');
        component.set("v.openTab", 'Strategies');
    },
    
    OpenThirdPartiesTab : function(component, event, helper) {
        var strategyTabLi = document.getElementById("strategyTabLi");
        strategyTabLi.setAttribute('class', 'slds-tabs--scoped__item slds-text-heading--label');
        var thirdPartyTabLi = document.getElementById("thirdPartyTabLi");
        thirdPartyTabLi.setAttribute('class', 'slds-tabs--scoped__item slds-text-heading--label slds-active');
        component.set("v.openTab", 'ThirdParties');
    },
    
    SearchThirdParties :function(component, event, helper) {
        helper.doSearchThirdParties(component);
    },
    
    SearchStrategies :function(component, event, helper) {
        helper.doSearchStrategies(component);
    },
    
    AddThirdParty : function(component, event, helper) { 
        
        // Resolve the Object Id from the events Element Id (this will be the <a> tag)
        var objectId = event.currentTarget.id;

        var action = component.get("c.AddThirdPartyRef");
        
        action.setParams({
            "thirdPartyId" : objectId
        	
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.thirdParties", response.getReturnValue());  
            }
        });
	 	$A.enqueueAction(action);       
        
        // Hide the Lookup List
        var lookupList = component.find("thirdPartyLookupList");
        $A.util.addClass(lookupList, 'slds-hide');
     
    },
    
    AddStrategy : function(component, event, helper) { 
        // Resolve the Object Id from the events Element Id (this will be the <a> tag)
        var objectId = event.currentTarget.id;

        var action = component.get("c.AddStrategyRef");
        
        action.setParams({
            "strategyId" : objectId
        	
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.strategies", response.getReturnValue());  
            }
        });
	 	$A.enqueueAction(action);       
        
        // Hide the Lookup List
        var lookupList = component.find("strategyLookupList");
        $A.util.addClass(lookupList, 'slds-hide');
     
    },
    
    SaveThirdParties : function(component, event, helper) { 
        
        // Delete any of the user's references to Third Parties that they have unchecked
        // Note - Lightning doesn't yet correctly handle the passing of inner classes from the javascript
        // controller to the apex controller.  To get around this I'm passing the IDs of the Third Parties/Strategies 
        // that need to be deleted as a tokenised String.
        var action = component.get("c.DeleteThirdPartyRefs");
        var thirdParties = component.get("v.thirdParties");

        var thirdPartyRefsToDelete = "";
        for (var i=0; i<thirdParties.length; i++){
            var thirdParty = thirdParties[i];
            
            if (thirdParty.checked == false){
                thirdPartyRefsToDelete += (thirdPartyRefsToDelete.length==0)?thirdParty.acc.Id:":" + thirdParty.acc.Id;            }
        }
        action.setParams({
            "thirdPartyRefsToDelete" : thirdPartyRefsToDelete
        	
        });

        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.thirdParties", response.getReturnValue());  
            }
        });
	 	$A.enqueueAction(action);
        
    },
    
    SaveStrategies : function(component, event, helper) { 
        
        // Now do Strategies.
        var action2 = component.get("c.DeleteStrategyRefs");
        var strategies = component.get("v.strategies");

        var strategyRefsToDelete = "";
        for (var i=0; i<strategies.length; i++){
            var strategy = strategies[i];
            
            if (strategy.checked == false){
                strategyRefsToDelete += (strategyRefsToDelete.length==0)?strategy.investmentStrategy.Id:":" + strategy.investmentStrategy.Id;            }
        }
        action2.setParams({
            "strategyRefsToDelete" : strategyRefsToDelete
        	
        });

        action2.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.strategies", response.getReturnValue());  
            }
        });
	 	$A.enqueueAction(action2);
        
    }
})