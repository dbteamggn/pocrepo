({
    getSalesProjections : function(cmp,eve) {
                
        var action = cmp.get('c.getProjections');
        action.setParams({
            "businessPlanId":cmp.get('v.recordId')
        });        
        action.setCallback(this, function(response){           
            var projections = response.getReturnValue();            
            cmp.set('v.SalesProjections', projections);            
        });
        $A.enqueueAction(action);
    },
    updateBusinessPlan : function(cmp, eve){
        var action = cmp.get('c.updateBusinessPlan');        
        var SalesProjections = cmp.get('v.SalesProjections'); 
        var inputProjects = cmp.find('inputProjections').get('v.value');
        console.log('input-----> ', inputProjects);
        var businessPlan =cmp.get('v.recordId');
        SalesProjections.FY17SalesProjection = inputProjects;
        console.log('JSON ------> ',JSON.stringify(SalesProjections));
        action.setParams({
            "businessPlan": cmp.get('v.recordId'),
            "projections": JSON.stringify(SalesProjections)
        });                
        action.setCallback(this, function(response){            
            var isSuccess = response.getReturnValue();            
            if(isSuccess){                               
                sforce.one.navigateToSObject(businessPlan);
            }
        });
        $A.enqueueAction(action);
    }
})