({
	doInit : function(component, event, helper) {
        
		helper.getSalesProjections(component,event);
	},
    previous : function(component, event, helper){
        component.set("v.stepNum", component.get('v.stepNum')-1);
    },
    next : function(cmp, eve, helper){
        helper.updateBusinessPlan(cmp,eve);
    }  
})