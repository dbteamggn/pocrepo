({
    setUIValues: function(component){
        var bpRec;
        var durVal;
        var recordId = component.get("v.recordId");
        
        var action = component.get("c.getBPRecord");
        action.setParams({
            "recId": recordId
        });

        action.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS"){
                bpRec = response.getReturnValue();
                if(typeof bpRec !== "undefined" && bpRec !== null){                    
                    component.find("strategicOverview").set("v.value", bpRec.Business_Plan_Summary__c);
                    console.log('***',bpRec.Duration_Other__c);
                    
                    console.log(bpRec);
                    component.set("v.recordId", bpRec.Id);
                    component.find("startYear").set("v.value", bpRec.Starting_Fiscal_Year__c);
                    var actionP = component.get("c.getPicklistValues");
                    actionP.setParams({
                        "objName": "business_plan__c",
                        "fieldName": "business_plan_duration__c"
                    });
                    actionP.setCallback(this, function(response){
                        var state = response.getState();
                        if(component.isValid() && state == "SUCCESS"){
                            var opts = [];
                            var vals = response.getReturnValue();
                            for(var each in vals){
                                var oneOption = {class: "optionClass", label: vals[each], value: vals[each]};
                                opts.push(oneOption);
                            }
                            component.find("duration").set("v.options", opts);
                            component.find("duration").set("v.value", bpRec.Business_Plan_Duration__c);
                            if(bpRec.Business_Plan_Duration__c=="Other"){
                                component.set("v.ifOtherDuration",true);
                    			component.find("durationOther").set("v.value", bpRec.Duration_Other__c);
                            }
                        }
                    });
                    $A.enqueueAction(actionP);
                }
            }
        });
        $A.enqueueAction(action);
    },
    updateRecord : function(component){
        var recordId = component.get("v.recordId");
        var action = component.get("c.updateBP");
        var durationOther='';
        if(typeof component.find("durationOther") != 'undefined'){
            durationOther=component.find("durationOther").get("v.value");
        }
        action.setParams({
            "recordId": recordId,
            "overview": component.find("strategicOverview").get("v.value"),
            "duration": component.find("duration").get("v.value"),
            "durationOther": durationOther,
            "yearVal": component.find("startYear").get("v.value")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS"){
                recordId = response.getReturnValue();
                component.set("v.stepNum", component.get("v.stepNum") + 1);
            }
        });
        $A.enqueueAction(action);
    }
})