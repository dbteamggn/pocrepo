({
	doInit : function(component, event, helper) {
        helper.setUIValues(component);
	},
    updateBPRec : function(component, event, helper) {
        helper.updateRecord(component);
    },
    prev : function(component, event, helper) {
        component.set("v.stepNum", component.get("v.stepNum") - 1);
    },
    durationChange : function(component, event, helper){
        if(component.find("duration").get("v.value")!="Other")
        	component.set("v.ifOtherDuration",false);
        else
            component.set("v.ifOtherDuration",true);
    }
})