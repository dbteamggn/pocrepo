({
	doInit: function(component, event, helper) {
        var crId = component.get('v.recordId');
        console.log(crId);
        var action = component.get("c.getCallReportProductDetails");
        var data;
        action.setParams({
            "crId": crId
        });
        action.setCallback(this, function(a) {
            data = a.getReturnValue();
            var myCallReport = $A.get("e.c:passFunds");
            myCallReport.setParams({
                "selectedFunds" : data,
            });
            myCallReport.fire();
        });
        $A.enqueueAction(action);   
    },
    FundOnFormClickEdit: function(component, event, helper) {
        var selectedFunds = event.getParam("selectedFunds");
        console.log("#insideFundOnFormClickEdit"+selectedFunds);
        if(selectedFunds.length>0){
            console.log('if');
            var ids=[];
            for(var items in selectedFunds){
                ids.push(selectedFunds[items].id);
            }
            console.log(ids);
            var action = component.get("c.updateProducts");
            action.setParams({
                "productIds": ids,
                "crId" : component.get('v.recordId')
            });
            action.setCallback(this, function(a) {
                console.log('recordId',a.getReturnValue());
                var recordId = a.getReturnValue();
                if(typeof sforce !== "undefined" && sforce !== null) {
                    sforce.one.navigateToURL('/'+recordId);
                }else{
                    var navEvt = $A.get("e.force:refreshView");
                    /*navEvt.setParams({
                        "recordId": recordId,
                        "slideDevName": "related"
                    });*/
                    navEvt.fire();
                }
            });
            $A.enqueueAction(action);     
        }
    }
})