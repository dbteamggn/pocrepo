({
    
	doInit : function(component, event, helper) {
        // Retrieve contacts during component initialization
        helper.loadContacts(component);
    },
    
    navigateToRecord : function(component, event, helper) {
        var idx = event.target.getAttribute('data-index');
        console.log(idx);
        alert(id);
        var navEvent = $A.get("e.force:navigateToSObject");
        if(navEvent){
            navEvent.setParams({
                  recordId: idx,
                  slideDevName: "detail"
            });
            navEvent.fire(); 
        }
        else{
            window.location.href = '/one/one.app#/sObject/'+idx+'/view'
        }
    },
    
    checkAllCheckboxes:function(component,event,helper){
        alert('call');
        var checkboxes = component.find("inputId");
        var maincheck=component.find("checkbox").get("v.value");
        var ln=checkboxes.length;
        if(maincheck==true ){
            if(ln == 1){
                checkboxes.set("v.value",true);
            }else{
                for (var i = 0; i<ln; i++){
                    checkboxes[i].set("v.value",true);
                }
            }
        }
        else if(maincheck==false){
            for (var i = 0; i <ln; i++){
                checkboxes[i].set("v.value",false);
            }
        }
    },
    
    createRecord : function (component, event, helper) {
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "Contact",
            "recordTypeId": "01246000000Q8X6AAK"
        });
        createRecordEvent.fire();
	}

    
})