({
	displayOpportunities: function(component){
        var alreadyInsertedBPOpp;
        var bpOpportunities;
        var nonBPOpportunities;
        var recordId = component.get("v.recordId");
        
        //get the value of the flag, if true - display both Business Plan Opps and non Business Plan Opps
        var action = component.get("c.hasBusinessPlanOpp");
        action.setParams({
            "recId": recordId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS"){
                alreadyInsertedBPOpp = response.getReturnValue();
                console.log('alreadyInsertedBPOpp-->',alreadyInsertedBPOpp);

                if(alreadyInsertedBPOpp){
                    console.log('alreadyInsertedBPOpp flag true, get BPOs and non BPOs-->');
                    component.set("v.alreadyInsertedBPOpp", alreadyInsertedBPOpp);
                    
                    //get Business Plan Opportunities to be displayed
                    var actionBPO = component.get("c.getBusinessPlanOpps");
                    actionBPO.setParams({
                        "recId": recordId
                    });
                    actionBPO.setCallback(this, function(response){
                        var state = response.getState();
                        if(component.isValid() && state == "SUCCESS"){
                            bpOpportunities = response.getReturnValue();
                            component.set("v.bpOpportunities", bpOpportunities);
                        }
                    });
                    $A.enqueueAction(actionBPO);
                    
                    //get non Business Plan Opportunities to be displayed in another section
                    var actionNonBPO = component.get("c.getNonBusinessPlanOpps");
                    actionNonBPO.setParams({
                        "recId": recordId,
                        "alreadyInsertedBPOpp": alreadyInsertedBPOpp
                    });
                    actionNonBPO.setCallback(this, function(response){
                        var state = response.getState();
                        if(component.isValid() && state == "SUCCESS"){
                            nonBPOpportunities = response.getReturnValue();
                            component.set("v.nonBPOpportunities", nonBPOpportunities);
                        }
                    });
                    $A.enqueueAction(actionNonBPO);
                }
                else{
                    console.log('alreadyInsertedBPOpp flag false, get non BPOs-->');
                    //get non Business Plan Opportunities to be displayed in another section
                    var actionNonBPO = component.get("c.getNonBusinessPlanOpps");
                    actionNonBPO.setParams({
                        "recId": recordId,
                        "alreadyInsertedBPOpp": alreadyInsertedBPOpp
                    });
                    actionNonBPO.setCallback(this, function(response){
                        var state = response.getState();
                        if(component.isValid() && state == "SUCCESS"){
                            nonBPOpportunities = response.getReturnValue();
                            component.set("v.nonBPOpportunities", nonBPOpportunities);
                        }
                    });
                    $A.enqueueAction(actionNonBPO);
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    insertBusinessPlanOpp : function(component){
        console.log('helper: inside insertBusinessPlanOpp-->');
        //var alreadyInsertedBPOpp = component.get("v.alreadyInsertedBPOpp");
        var nonBPOpportunities = component.get("v.nonBPOpportunities");
        var recordId = component.get("v.recordId");
        
        //insert Business Plan Opportunities
        var actionInsertBPO = component.get("c.insertBusinessPlanOpportunities");
        actionInsertBPO.setParams({
            "recId": recordId,
            "nonBPOpportunitiesJSONStr": JSON.stringify(nonBPOpportunities)
        });
        actionInsertBPO.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS"){
                console.log('insert BPO success:');
            }
        });
        $A.enqueueAction(actionInsertBPO);
    },
    
    updateBPRec : function(component){
        console.log('helper: inside updateBPRec-->');        
        var recordId = component.get("v.recordId");
        
        //update Business Plan, set the flag to true
        var actionUpdateBP = component.get("c.updateBusinessPlan");
        actionUpdateBP.setParams({
            "recId": recordId
        });
        actionUpdateBP.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS"){
                console.log('update BP success:');
            }
        });
        $A.enqueueAction(actionUpdateBP);
    }
})