({
    doInit : function(component, event, helper) {
    $A.createComponent(
        "c:ConsultantRatingsTable",
        {
         
        },
        function(newCmp){
            if (component.isValid()) {
            	component.set("v.body", newCmp);
            }
        }
    	);
    },
    NavigateToActivities : function(component, event, helper){
        $A.createComponent(
            "c:ConsultantRatingsActivities",
            {
                
            },
            function(newCmp){
                if (component.isValid()) {
                    component.set("v.body", newCmp);
                }
            }
    	);
    },
    NavigateToConfig : function(component,event,helper) {
        $A.createComponent(
        "c:ConsultantRatingsConfig",
        {
        	
        },
        function(newCmp){
            if (component.isValid()) {
            	component.set("v.body", newCmp);
            }
        }
    	);
    },
    NavigateToTable : function(component,event,helper) {
        $A.createComponent(
        "c:ConsultantRatingsTable",
        {
        	
        },
        function(newCmp){
            if (component.isValid()) {
            	component.set("v.body", newCmp);
            }
        }
    	);
    }
})