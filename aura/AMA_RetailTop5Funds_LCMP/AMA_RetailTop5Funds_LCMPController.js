({
	setup : function(component, event, helper) {
        console.log('=====AVOOOIIIII==');
		new Highcharts.Chart({
            chart: {
                renderTo: component.find("chart").getElement(),
                type: 'column'
            },
           title: {
        text: 'Historic World Population by Region'
    },
    subtitle: {
        text: 'Source: <a href="https://en.wikipedia.org/wiki/World_population">Wikipedia.org</a>'
    },
    xAxis: {
        categories: ['Sterling Bond Fund', 'Global Equities', 'Fixed Income', 'Targetted Return', 'Property Trust'],
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Population (millions)',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ' millions'
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
        shadow: true
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'Sales',
        data: [700000,500000,400000,300000,250000]
    }]
});
         console.log('%%%%AViiiiiiiiii%%');
    }
})