/* The controller performs below actions,
     * getContactDetails
     * getContactDetailsByName
     * updateSubscription 
*/

({
       
    doInit: function(component, event, helper){
        helper.getAllContacts(component);
        component.set('v.textSearched', false);
    },
    searchKeyChange: function(component, event, helper) {
        component.set('v.searchText',  event.getParam("searchKey"));
        component.set('v.textSearched', true);
//        component.set('v.contact','');       
         $A.util.addClass(component.find('errorMessageDiv'), 'invisible');
         $A.util.removeClass(component.find('warningMessageDiv'), 'warningMessageClass');
        helper.getContacts(component);
    },
    onChangeParentCheckbox: function(component, event) {        
    },
    onChangeChildCheckbox: function(component, event,helper) {
       
        var ListToAdd=component.get('v.addContact');
        var ListToDelete=component.get('v.delContact');
        var contactId = event.getSource().get('v.text');
        var rowId = document.getElementById(contactId);
        if(event.getSource().get('v.value')){
            var index = ListToDelete.indexOf(contactId);
            if(index > -1){
                ListToDelete.splice(index,1);
                 $A.util.addClass(component.find('warningMessageDiv'), 'invisible');
             
             }
            ListToAdd.push(event.getSource().get('v.text'));
            
        }else{
            var index = ListToAdd.indexOf(event.getSource().get('v.text'));
            if(index > -1){
                ListToAdd.splice(index,1);
            
            }
            ListToDelete.push(event.getSource().get('v.text'));
             $A.util.addClass(component.find('warningMessageDiv'), 'invisible');
             
        }
        component.set('v.addContact',ListToAdd);
        component.set('v.delContact',ListToDelete);
        console.log('ListToAdd',ListToAdd);
        console.log('ListToDelete',ListToDelete);
    },
    updateSubscription: function(component, event, helper) {
        var resultCmp = component.find("childCheckbox");
        var queryParam = component.get("v.queryParam");      
        var resultDataList=[];
        var resultData=new Object();   
        helper.updateSubscription(component,event);
        
    },
    getContactDetailsByName : function (component, event, helper){
        var page = component.get("v.page") || 1;
        var direction = event.getParam("direction");
        page = direction === "previous" ? (page - 1) : (page + 1);
        helper.getContacts(component, event,page);
    },
    getContactDetails : function (component, event, helper){
        var page = component.get("v.page") || 1;
        var direction = event.getParam("direction");
        page = direction === "previous" ? (page - 1) : (page + 1);
        helper.getAllContacts(component, event,page);
    }
})