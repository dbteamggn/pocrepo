({
    getContacts : function(component,event, page) {
        page= page||1;
        var searchKey =component.get('v.searchText');
        var action = component.get("c.getContactsByName");
        
        var recordId = component.get("v.recordId");
        var typeParam = component.get("v.type");
        var addList = component.get('v.addContact');
        var delList = component.get('v.delContact');
        action.setParams({
            "searchKey": searchKey,
            "recordId" : recordId,
            "pageNumber":page
        });
        action.setCallback(this, function(a) {            
            var newResultDataList=a.getReturnValue();
            var length = newResultDataList.contactResultList.length;
            var contactList = newResultDataList.contactResultList;
            
            
            for(var j=0; j<contactList.length; j++ ){
                if(addList.indexOf(contactList[j].contactId)>-1){
                    contactList[j].value = true;
                }else if(delList.indexOf(contactList[j].contactId)>-1){
                    contactList[j].value = false;
                }
            }
            var total = newResultDataList.total ==0 ? 1: newResultDataList.total;
            component.set("v.contacts", contactList);
            component.set("v.page", newResultDataList.total == 0 ? 1:newResultDataList.page);
            component.set("v.total", newResultDataList.total);
            component.set("v.pages", (Math.ceil(total/newResultDataList.pageSize)));
        });
        $A.enqueueAction(action);
    },
    getAllContacts: function(component,event,page){
        page= page||1;      
        var action = component.get("c.getAllContacts");
        var recordId = component.get("v.recordId");
        var typeParam = component.get("v.type");
        var addList = component.get('v.addContact');
        var delList = component.get('v.delContact');
        action.setParams({            
            "recordId" : recordId,
            "pageNumber":page
        });
        action.setCallback(this, function(a) {            
            var newResultDataList = a.getReturnValue();
            var length = newResultDataList.contactResultList.length;
            var contactList = newResultDataList.contactResultList;
            if(length > 0){
            for(var j=0; j<contactList.length; j++ ){
                if(addList.indexOf(contactList[j].contactId)>-1){
                    contactList[j].value = true;
                }else if(delList.indexOf(contactList[j].contactId)>-1){
                    contactList[j].value = false;
                }
            }
            }
            var total = newResultDataList.total ==0 ? 1: newResultDataList.total;
            component.set("v.contacts", contactList);
            component.set("v.page", newResultDataList.total == 0 ? 0:newResultDataList.page);
            component.set("v.total", newResultDataList.total);
            component.set("v.pages", (Math.ceil(total/newResultDataList.pageSize)));
            
        });
        $A.enqueueAction(action);
        
    },
    updateSubscription: function(component, event){
        
        var RecordId=component.get('v.recordId');
        var action = component.get('c.updateContactSubscription');
        action.setParams({
            "toAdd": component.get('v.addContact'),
            "toDelete": component.get('v.delContact'),
            "recordId" : RecordId
        });
        action.setCallback(this,function(response){
            var subscribed = response.getReturnValue();
            console.log('value', subscribed);
            var message = component.find('errorMessage');
            
            if(subscribed == 'true'){
                $A.get('e.force:refreshView').fire();
            }else if (subscribed == 'false'){
                $A.util.removeClass(message, 'invisible');
                $A.util.addClass(message, 'errorMessageClass');
            }
        });
        $A.enqueueAction(action);
    },
})