({
    doInit : function(component, event, helper) {        
        var action = component.get('c.getRelatedOpportunities');
        var addOpportunities = component.get('v.addOpportunities');
        action.setParams({"businessPlan": component.get('v.recordId')});
        action.setCallback(this, function(response){
            var opportunitiesRelated = response.getReturnValue();            
            if (opportunitiesRelated.length >0){
                component.set('v.opportunities', opportunitiesRelated);
            }
            for(var j=0; j < opportunitiesRelated.length; j++){
                        if(!addOpportunities.indexOf(opportunitiesRelated[j].opportunityId) > -1){
                            addOpportunities.push(opportunitiesRelated[j].opportunityId);                             
                        }  
                    } 
        });
        $A.enqueueAction(action);
    },
    addOpportunity : function(component, event, helper) {
        var opportunityId = component.get('v.opportunity');
        var action = component.get('c.getOpportunity');
        var opportunities = component.get('v.opportunities');
        var addOpportunities = component.get('v.addOpportunities');
        action.setParams({"opportunityId":opportunityId});
        console.log('opportunity value is ===> ', opportunityId);
        action.setCallback(this, function(response){
            console.log('response.getReturnValue()  ', response.getReturnValue());
            var relatedOpportunity = response.getReturnValue();
            
            if(relatedOpportunity != null){                      
                 for(var j=0; j < relatedOpportunity.length; j++){
                     console.log('Length ---> ',addOpportunities.indexOf(relatedOpportunity[j].opportunityId)> -1 );
                        if(addOpportunities.indexOf(relatedOpportunity[j].opportunityId) <= -1){
                            addOpportunities.push(relatedOpportunity[j].opportunityId);                             
                            opportunities.push(relatedOpportunity[j]);                            
                        }  
                    }  
                component.set('v.addOpportunities', addOpportunities);
                component.set('v.opportunities', opportunities);
                
            }
        }) ;
        $A.enqueueAction(action);
    },
    next : function(cmp, evt, helper){
        var opportunities = cmp.get("v.addOpportunities");  
        var delOpportunities = cmp.get('v.delOpportunities');
        var businessPlan = cmp.get('v.recordId');
        var action = cmp.get('c.createBusinessPlanOpportunities');
        
        action.setParams({
            "businessPlan":businessPlan,
            "opportunities": opportunities,
            "delOpportunities":delOpportunities
        });
        action.setCallback(this, function(response){
            var isCreated = response.getReturnValue();            
            if(!isCreated){
                $A.util.toggleClass(cmp.find('errorMessageDiv'), 'invisible');
            }else{
				cmp.set("v.stepNum", cmp.get("v.stepNum") + 1);                
                cmp.set("v.recordId", businessPlan);
            }
        });
        $A.enqueueAction(action);
    },
    previous : function(cmp, evt, helper){
        cmp.set("v.stepNum", cmp.get('v.stepNum')-1);
    },
    onChangeChildCheckbox: function(component, event,helper) {
        
        var ListToAdd=component.get('v.addOpportunities');
        var ListToDelete=component.get('v.delOpportunities');
        var opportunityId = event.getSource().get('v.text');        
        var rowId = document.getElementById(opportunityId);
        if(event.getSource().get('v.value')){
            var index = ListToDelete.indexOf(opportunityId);
            if(index > -1){
                ListToDelete.splice(index,1);                             
            }
            ListToAdd.push(event.getSource().get('v.text'));            
        }else{
            var index = ListToAdd.indexOf(event.getSource().get('v.text'));
            if(index > -1){
                ListToAdd.splice(index,1);
            }
            ListToDelete.push(event.getSource().get('v.text'));
            
        }
        component.set('v.addOpportunities',ListToAdd);
        component.set('v.delOpportunities',ListToDelete);
        
    },
    onChangeParentCheckbox : function(cmp, evt, helper){
        var opportunities = cmp.get('v.opportunities');
        var checked = cmp.find('parentCheckbox').get('value');
        console.log('value is --> ', checked);
        for(var i =0; i< opportunities.length; i++){
            opportunities[i].value = true;
        }
        cmp.set('v.opportunities', opportunities);
    }
})