({
	toggleChildMenus: function(component, expand){
		var items = component.get('v.items') || [];

		for(var i=0; i < items.length; i++){
			var item = items[i];
			if(expand){
				item.expandAll();
			} else {
				item.collapseAll();
			}
		}
	},
})