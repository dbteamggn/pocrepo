({
	doInit: function(component, event, helper){
		var accounts = component.get('v.accounts');
		
		for(var i=0; i < accounts.length; i++){
			var account = accounts[i];
			console.log(account.hierachyLevel);
			$A.createComponent(
				'c:AccountHierarchyMenuItem',
				{
					account: account,
					childAccountsLoaded: component.get('v.childAccountsLoaded'),
					configuration: component.get('v.configuration'),
                    hierachyLevel: account.hierachyLevel
				},
				function(menuItem){
					var items = component.get('v.items');
					items.push(menuItem);
					component.set('v.items', items);
				}
			);
		}
	},

	expandFirst: function(component, event, helper){
		var items = component.get('v.items') || [];
		if(items.length){
			items[0].set('v.expanded', true);
		}
	},

	expandAll: function(component, event, helper){
		helper.toggleChildMenus(component, true);
	},

	collapseAll: function(component, event, helper){
		helper.toggleChildMenus(component, false);
	}
})