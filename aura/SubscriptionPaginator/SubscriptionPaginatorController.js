({
	previousPage : function(component, event, helper) {
        console.log('previous');
        if(component.get('v.textSearched')){
            var myEvent = $A.get("e.c:SubscriptionPageChange");
            myEvent.setParams({ "direction": "previous"});
            myEvent.fire();
        }else{
            var myEvent = $A.get("e.c:SubscriptionPageChangeBeforeSearch");
            myEvent.setParams({ "direction": "previous"});
            myEvent.fire();
        }
	},
	nextPage : function(component, event, helper) {
        console.log('next');
        if(component.get('v.textSearched')){
            var myEvent = $A.get("e.c:SubscriptionPageChange");
            myEvent.setParams({ "direction": "next"});
            myEvent.fire();
        }else{
            var myEvent = $A.get("e.c:SubscriptionPageChangeBeforeSearch");
            myEvent.setParams({ "direction": "next"});
            myEvent.fire();
        }
	}
})