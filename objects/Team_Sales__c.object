<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Advisor__c</fullName>
        <description>Field used to store the Id of the contact record</description>
        <externalId>false</externalId>
        <inlineHelpText>Field used to store the Id of the contact record</inlineHelpText>
        <label>Contact</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Team Sales</relationshipLabel>
        <relationshipName>Team_Sales2</relationshipName>
        <relationshipOrder>1</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>CAD_CSTAR_Advisor__c</fullName>
        <description>Field used to store the Id of the CSTAR record</description>
        <externalId>false</externalId>
        <inlineHelpText>Field used to store the Id of the CSTAR record</inlineHelpText>
        <label>CAD_CSTAR Advisor</label>
        <referenceTo>CAD_CSTAR_Advisor__c</referenceTo>
        <relationshipLabel>Team Sales</relationshipLabel>
        <relationshipName>Team_Sales1</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>CSTAR_PersonId__c</fullName>
        <description>Formula field to retrieve person id of CSTAR record</description>
        <externalId>false</externalId>
        <formula>CAD_CSTAR_Advisor__r.PERSON_ID__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Formula field to retrieve person id of CSTAR record</inlineHelpText>
        <label>CSTAR_PersonId</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CSTAR_UID__c</fullName>
        <description>Formula field to retrieve uid of CSTAR record</description>
        <externalId>false</externalId>
        <formula>CAD_CSTAR_Advisor__r.UID__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Formula field to retrieve uid of CSTAR record</inlineHelpText>
        <label>CSTAR_UID</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>For_Batch_Process__c</fullName>
        <description>Formula field to retrieve the value on for batch process field on CSTAR record</description>
        <externalId>false</externalId>
        <formula>CAD_CSTAR_Advisor__r.For_Batch_Process__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Formula field to retrieve the value on for batch process field on CSTAR record</inlineHelpText>
        <label>For Batch Process</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <description>Stores the Status value of a Team sales record</description>
        <externalId>false</externalId>
        <inlineHelpText>Stores the Status value of a Team sales record</inlineHelpText>
        <label>Status</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Active</fullName>
                    <default>true</default>
                    <label>Active</label>
                </value>
                <value>
                    <fullName>Inactive</fullName>
                    <default>false</default>
                    <label>Inactive</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Unique_Team_Sales__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>Stores the Unique Team Sales value</description>
        <externalId>false</externalId>
        <inlineHelpText>Stores the Unique Team Sales value</inlineHelpText>
        <label>Unique Team Sales</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <label>Team Sales</label>
    <listViews>
        <fullName>All</fullName>
        <columns>Advisor__c</columns>
        <columns>Status__c</columns>
        <columns>Unique_Team_Sales__c</columns>
        <columns>NAME</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>{000000000}</displayFormat>
        <label>ID</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Team Sales</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
