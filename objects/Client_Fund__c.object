<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Recommended Funds associated with a client</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Active__c</fullName>
        <description>Reflects the Fund(Product) Active field</description>
        <externalId>false</externalId>
        <formula>Product__r.IsActive</formula>
        <label>Active</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Asset_Manager_Name__c</fullName>
        <description>Derive the asset manager name from the Product object</description>
        <externalId>false</externalId>
        <formula>Text(Product__r.Asset_Manager_Name__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Asset Manager Name</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Client__c</fullName>
        <description>The client with which the Fund is associated</description>
        <externalId>false</externalId>
        <label>Client</label>
        <referenceTo>Account</referenceTo>
        <relationshipName>Client_Funds</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Fund_Description__c</fullName>
        <description>Derived from Product Description</description>
        <externalId>false</externalId>
        <formula>Product__r.Description</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Fund Description</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Fund_Manager_Name__c</fullName>
        <description>Derive the fund manager name from the Product object</description>
        <externalId>false</externalId>
        <formula>Product__r.Fund_Manager_Name__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Fund Manager Name</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Fund_Sector__c</fullName>
        <description>Derived from the Fund Sector picklist on the Product object</description>
        <externalId>false</externalId>
        <formula>Text(Product__r.Fund_Sector__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Fund Sector</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Fund_Status_at_Company__c</fullName>
        <externalId>false</externalId>
        <label>Fund Status at Company</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Off Panel Regular Purchase</fullName>
                    <default>false</default>
                    <label>Off Panel Regular Purchase</label>
                </value>
                <value>
                    <fullName>On Buy List</fullName>
                    <default>false</default>
                    <label>On Buy List</label>
                </value>
                <value>
                    <fullName>Pending Review</fullName>
                    <default>false</default>
                    <label>Pending Review</label>
                </value>
                <value>
                    <fullName>Sell/Hold</fullName>
                    <default>false</default>
                    <label>Sell/Hold</label>
                </value>
                <value>
                    <fullName>Watch List</fullName>
                    <default>false</default>
                    <label>Watch List</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Is_Inherited_From_Hierarchy__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Is Inherited From Hierarchy</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Logged_in_User__c</fullName>
        <externalId>false</externalId>
        <formula>$User.Id</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Logged in User</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Manager_and_Fund_Name_Sort__c</fullName>
        <externalId>false</externalId>
        <formula>IF(TEXT(Product__r.Asset_Manager_Name__c) = &quot;Deloitte&quot;, &quot;1 - &quot; &amp; Product__r.Name, &quot;2 - &quot; &amp; Product__r.Name)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Manager and Fund Name Sort</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Our_Product__c</fullName>
        <externalId>false</externalId>
        <formula>Product__r.Our_Product__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Our Product</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Product__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>The Fund Product that is being associated with the Client</description>
        <externalId>false</externalId>
        <label>Fund</label>
        <lookupFilter>
            <active>false</active>
            <filterItems>
                <field>Product2.Our_Product__c</field>
                <operation>equals</operation>
                <value>True</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Product2</referenceTo>
        <relationshipName>Client_Funds</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Recommended Fund</label>
    <nameField>
        <displayFormat>{0}</displayFormat>
        <label>Fund Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Recommended Fund List</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>Our_Funds_Only</fullName>
        <active>false</active>
        <description>Only allow firm&apos;s funds to be selected</description>
        <errorConditionFormula>Product__r.Our_Product__c = false</errorConditionFormula>
        <errorMessage>Only firm&apos;s funds can be selected</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
