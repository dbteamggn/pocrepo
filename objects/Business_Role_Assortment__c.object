<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Association object to relate products available during a product audit and order for the business role</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Assortment__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>The product assortment to which this business role assortment relates to</description>
        <externalId>false</externalId>
        <inlineHelpText>The product assortment to which this business role assortment relates to</inlineHelpText>
        <label>Assortment</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Assortment__c.RecordType.DeveloperName</field>
                <operation>equals</operation>
                <value>Business_Role_Assortment</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Assortment__c</referenceTo>
        <relationshipLabel>Business Role Assortments</relationshipLabel>
        <relationshipName>Business_Role_Assortments</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Business_Role__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>The Business Role to which this assortment is assigned to</description>
        <externalId>false</externalId>
        <inlineHelpText>The Business Role to which this assortment is assigned to</inlineHelpText>
        <label>Business Role</label>
        <referenceTo>Business_Role__c</referenceTo>
        <relationshipLabel>Business Role Assortments</relationshipLabel>
        <relationshipName>Business_Role_Assortments</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Created_Date__c</fullName>
        <description>Returns the date value of the created date . Used in time-bound workflow ( Status needs to get activated overnight</description>
        <externalId>false</externalId>
        <formula>DATEVALUE(CreatedDate)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Created Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Deactivation_Date__c</fullName>
        <description>Hidden field to store the date when the &apos;Deactivate&apos; button is clicked . This will be used in the time bound workflow trigger</description>
        <externalId>false</externalId>
        <label>Deactivation Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>GUID__c</fullName>
        <caseSensitive>false</caseSensitive>
        <externalId>true</externalId>
        <label>GUID Id</label>
        <length>200</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <externalId>false</externalId>
        <label>Status</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Waiting Assignment</fullName>
                    <default>true</default>
                    <label>Waiting Assignment</label>
                </value>
                <value>
                    <fullName>Active</fullName>
                    <default>false</default>
                    <label>Active</label>
                </value>
                <value>
                    <fullName>Waiting Removal</fullName>
                    <default>false</default>
                    <label>Waiting Removal</label>
                </value>
                <value>
                    <fullName>Inactive</fullName>
                    <default>false</default>
                    <label>Inactive</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Business Role Assortment</label>
    <nameField>
        <displayFormat>BRA-{000000}</displayFormat>
        <label>Ref No.</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Business Role Assortments</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
