public class campaignMailMergeController {
    public Campaign camp{get;set;}
    public string docName{get;set;}
    public String businessLine {get; set;}
    String delimiter = ', ';
    List<String> allBLs = new List<String>();
    
    public campaignMailMergeController(ApexPages.StandardController controller) {
        Campaign CampRec = (Campaign)controller.getRecord();
        businessLine = '';
        camp = [select id,Name,Briefing_Date__c,Owner.Name,Background__c,Description,StartDate,EndDate,Campaign_Business_Units__c,
                            Campaign_reference_number__c,Target_Audience_Insight__c, Strategic_Context_Rationale__c 
                            from Campaign where id = : CampRec.id];
        datetime currentTime = System.now(); 
        docName = camp.Name +' - '+ currentTime.year() + currentTime.month() + currentTime.day() + currentTime.hour() + currentTime.minute() + currentTime.second();
        businessLine = '';
    }

}