@isTest
public class Event_Utility_Test{
    /*
     * 
     * Description : to validate the roll up of cost feilds on to cmpaign
     *               from associated event
     * Param : 
     * Returns :  
    */
    static testMethod void TestCostRollupToCampaign(){
        User user = CommonTestUtils.createTestUser();
        // create campaign record
        //insert o_campaign;
        
        // perform the test 
        Test.startTest();
        System.runAs(user){ 
            Campaign o_campaign = CommonTestUtils.CreateTestCampaign('Test');
            // create a eventand associate it to campaign
            Event o_event = CommonTestUtils.CreateTestEvent('Test Sub', system.now().addHours(2),system.now().addHours(3), null, null, null);
            o_event.whatId = o_campaign.id;
            o_event.Budgeted_Cost__c = 10.1;
            o_event.Actual_Cost__c = 20.2;
            insert o_event;
            
            // validate roll up of cost field 
            o_campaign = [SELECT Id, Budgeted_Activities_Cost__c, Actual_Activities_Cost__c FROM Campaign WHERE Id =:o_campaign.id];
            system.assertEquals(10.1, o_campaign.Budgeted_Activities_Cost__c );
            system.assertEquals(20.2, o_campaign.Actual_Activities_Cost__c );
            
            // add multiple event to cmapaign
            o_event = CommonTestUtils.CreateTestEvent('Test Sub', system.now().addHours(2),system.now().addHours(3), null, null, null);
            o_event.whatId = o_campaign.id;
            o_event.Budgeted_Cost__c = 5;
            o_event.Actual_Cost__c = 10;
            insert o_event;
            
            o_campaign = [SELECT Id, Budgeted_Activities_Cost__c, Actual_Activities_Cost__c FROM Campaign WHERE Id =:o_campaign.id];
            system.assertEquals(15.1, o_campaign.Budgeted_Activities_Cost__c );
            system.assertEquals(30.2, o_campaign.Actual_Activities_Cost__c );
            
            // update the cost field of one of the campaign
            o_event.Budgeted_Cost__c = 2;
            o_event.Actual_Cost__c = 1;
            update o_event;
            
            o_campaign = [SELECT Id, Budgeted_Activities_Cost__c, Actual_Activities_Cost__c FROM Campaign WHERE Id =:o_campaign.id];
            system.assertEquals(12.1, o_campaign.Budgeted_Activities_Cost__c );
            system.assertEquals(21.2, o_campaign.Actual_Activities_Cost__c );
            
            // delete the event
            delete o_event;
            o_campaign = [SELECT Id, Budgeted_Activities_Cost__c, Actual_Activities_Cost__c FROM Campaign WHERE Id =:o_campaign.id];
            system.assertEquals(10.1, o_campaign.Budgeted_Activities_Cost__c );
            system.assertEquals(20.2, o_campaign.Actual_Activities_Cost__c );
            
            //undelete the delete event
            undelete o_event;
            // validate result after undeletion 
            o_campaign = [SELECT Id, Budgeted_Activities_Cost__c, Actual_Activities_Cost__c FROM Campaign WHERE Id =:o_campaign.id];
            system.assertEquals(12.1, o_campaign.Budgeted_Activities_Cost__c );
            system.assertEquals(21.2, o_campaign.Actual_Activities_Cost__c );
            
             
        }
        Test.stopTest();
    }
    
    //Test case for Gifts and benefits - insert and update
    static testMethod void testGnB(){
        User user = CommonTestUtils.createTestUser();
        Test.startTest();
        Account acc = CommonTestUtils.CreateTestThirdParty('Test Account');
        insert acc;
        Contact cnt = CommonTestUtils.CreateTestContact('Test Contact');
        cnt.AccountId = acc.Id;
        insert cnt;
        
        Contact cnt2 = CommonTestUtils.CreateTestContact('Test Attendee 1');
        cnt2.AccountId = acc.Id;
        insert cnt2;
        
        Contact cnt3 = CommonTestUtils.CreateTestContact('Test Attendee 2');
        cnt3.AccountId = acc.Id;
        insert cnt3;
        
        Contact cnt4 = CommonTestUtils.CreateTestContact('Test Attendee 3');
        cnt4.AccountId = acc.Id;
        insert cnt4;
        
        System.runAs(user){ 
            Event testEvent = CommonTestUtils.CreateTestEvent('Test Sub', system.now().addHours(2),system.now().addHours(3), cnt.Id, null, null);
            testEvent.Gift__c = true;
            testEvent.Total_Gift_Amount__c = 100;
            testEvent.CurrencyISOCode = 'USD';
            testEvent.Given_Received__c = 'Given';
            testEvent.Type__c = 'Gift';
            testEvent.Sub_Type__c = 'Meal';
            testEvent.WhoId = cnt.Id;
            insert testEvent;
            
            List<EventRelation> eRelations = new List<EventRelation>{new EventRelation(RelationId = cnt2.Id, EventId = testEvent.Id, isParent = true),
                                                                    new EventRelation(RelationId = cnt3.Id, EventId = testEvent.Id, isParent = true),
                                          new EventRelation(RelationId = cnt4.Id, EventId = testEvent.Id, isParent = true)};
            insert eRelations;
            testEvent.Total_Gift_Amount__c = 200;
            update testEvent;
            
            EventRelation er = [select Id from EventRelation where EventId = :testEvent.Id AND RelationId = :cnt4.Id LIMIT 1];
            delete er;
            testEvent.Total_Gift_Amount__c = 300;
            testEvent.Cost_Type__c = 'Total';
            update testEvent;
            
            Database.DeleteResult dr = Database.delete(testEvent, false);
            for(Database.Error each: dr.getErrors()){
                System.assertEquals(System.Label.CantDeleteGift, each.getMessage());
            }
        }
        Test.stopTest();
    }
}