@isTest
public class CampaignTeamTriggerTest {
    
    static testMethod void testCampaignTeamTrigger(){
        Campaign campaign = CommonTestUtils.CreateTestCampaign('Test Campaign');
        Contact  contact = CommonTestUtils.CreateTestContact('Jhon Test');
        Test.startTest();
        
        //Add a contact to campaign Team.
        Campaign_Team__c campaignTeam = CommonTestUtils.createTestCampaignTeam(campaign, contact);
        System.assert(contact.email == campaignTeam.Chatter_User_Email__c);
        //Linking a user to the contact. 
        Profile p = [select id from profile where name ='Marketing User'];
        List<User> users = CommonTestUtils.createMultipleUsers(p.id,1);
        insert users;
        for(User user : users)
            contact.User__c= user.id;
        contact.email='test@deloitte.com';
        contact.email_type__c='Personal';
        upsert contact;
        
        // Checking the notification to true so that user receives the feed updates.
        campaignTeam.Notification__c = true;
        campaignTeam.Team_Member__c = contact.id;
        update campaignTeam;
        System.assert(campaignTeam.Chatter_User_Email__c==null);
        
        Contact  contact1 = CommonTestUtils.CreateTestContact('Test Jhon');
        Campaign_Team__c campaignTeam1 = CommonTestUtils.createTestCampaignTeam(campaign, contact1);
        //Linking a chatter free user to the contact
        //Also inserting multiple chatter free users with same email id 
        User u = createTestUser(true,'user11@deloitte.com','user11@deloitte.test.com');
        User u1 = createTestUser(true,'user11@deloitte.com','user12@deloitte.test.com');
        contact1.email = u.email;
        contact1.email_type__c='Personal';
        upsert contact1;
        campaignTeam1.Team_Member__c = contact1.id;
        campaignTeam1.Notification__c = true;
        update campaignTeam1;
        
        //System.assertEquals(contact.Email  ,campaignTeam1.Chatter_User_Email__c);
        
         Contact  contact2 = CommonTestUtils.CreateTestContact('Test Jhon2');
         Campaign_Team__c campaignTeam2 = CommonTestUtils.createTestCampaignTeam(campaign, contact2);
          upsert contact2;
        campaignTeam2.Team_Member__c = contact.id;
        campaignTeam2.Notification__c = true;
        update campaignTeam2;
        
        Test.stopTest();
    }
    public static User createTestUser(boolean createUser, String email, String username) {
        id userId = UserInfo.getUserId() ;
        Profile p = [select id from profile where name = 'Chatter Free User'];
        user uss=[select LanguageLocaleKey,ProfileId,TimeZoneSidKey,LocaleSidKey,EmailEncodingKey from user where id = : userId ];
        
        //New user to run test
        user user1 = new user();
        user1.Username=username;
        user1.firstname = 'aaaaa';
        user1.LastName='bbbb';
        user1.email=email;
        user1.Alias='fred';
        user1.TimeZoneSidKey=uss.TimeZoneSidKey;
        user1.LocaleSidKey=uss.LocaleSidKey;
        user1.EmailEncodingKey=uss.EmailEncodingKey;
        user1.ProfileId=p.Id;
        user1.LanguageLocaleKey=uss.LanguageLocaleKey;
        user1.IsActive=true;
        if (createUser){
            insert user1;
        }
        return user1;
    }
    
}