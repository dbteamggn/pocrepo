@isTest
public class ContactSubscriptionListControllerTest {
    
    public static testMethod void testGetAllSubscriptions(){
        
        Subscription__c sub = CommonTestUtils.createTestSubscription('test', 'Subscription');
        Contact c = CommonTestUtils.createTestContact('TestUser'); 
        insert c;
        Contact_Subscription_Link__c link = CommonTestUtils.createTestContactSubscription(c, sub);
        Test.startTest();
        ContactSubscriptionListController.SubscriptionResponse response = new ContactSubscriptionListController.SubscriptionResponse();
       //Get all the subscriptions associated with a contact.
        response = ContactSubscriptionListController.getAllSubscriptions(c.id,1);
        List<ContactSubscriptionListController.SubscriptionResult> resList = new List<ContactSubscriptionListController.SubscriptionResult>();
        resList.addAll(response.subscriptionResultList);
        
        for( ContactSubscriptionListController.SubscriptionResult res : resList){
                System.assertEquals(true, res.value);
        }
        Test.stopTest();
    }
    
    public static testMethod void testGetSubscriptionsByName(){
        
        Subscription__c sub = CommonTestUtils.createTestSubscription('test', 'Subscription');
        Contact c = CommonTestUtils.createTestContact('TestUser1');  
        insert c;
        Contact_Subscription_Link__c link = CommonTestUtils.createTestContactSubscription(c, sub);
        Subscription__c s = [select id, name from Subscription__c where id=:sub.id];
        String searchKey = s.name;     
        
        Test.startTest();
        ContactSubscriptionListController.SubscriptionResponse response = new ContactSubscriptionListController.SubscriptionResponse();         
        //Get the subscriptions based on the subscription number.
        response = ContactSubscriptionListController.getSubscriptionsByName(searchKey,c.id,1);
        List<ContactSubscriptionListController.SubscriptionResult> resultList = response.subscriptionResultList;
        for(ContactSubscriptionListController.SubscriptionResult result : resultList){
            System.assertEquals(searchKey, result.subscriptionNumber);    
        }
        // For the else block coverage : mallik
         delete link;
         response = ContactSubscriptionListController.getSubscriptionsByName(searchKey,c.id,1);      
        Test.stopTest();
    }
    public static testMethod void testVerifyCountries(){
        
        Subscription__c sub = CommonTestUtils.createTestSubscription('test', 'Subscription');
        Contact c = CommonTestUtils.createTestContact('TestUser1');
        
        insert c;
        Contact_Subscription_Link__c link = CommonTestUtils.createTestContactSubscription(c, sub);
        Test.startTest();
        if(sub != null){
            Subscription__c s = [select id, name, Limited_by_Countries__c from Subscription__c where id =:sub.id];
            ContactSubscriptionListController.CheckCountries verifyCountry = new ContactSubscriptionListController.CheckCountries();
           // Contact Mailing country as Empty.
            verifyCountry = ContactSubscriptionListController.verifyCountries(s.id,c.id,s.Limited_by_Countries__c);
            System.assertEquals('false', verifyCountry.countriesMatched);
            
            //Matching contact mailing country and subscription limited by country.
            c.Mailing_Country__c = 'Switzerland';
            c.Mailing_Street_Line_1__c ='xyz';
            c.Mailing_Street_Line_2__c ='xyz';
            c.Mailing_Street_Line_3__c ='xyz';
            c.Mailing_PostalCode__c = '560037';
            update c;
            s.Limited_by_Countries__c = 'Switzerland';
            update s;
            verifyCountry = ContactSubscriptionListController.verifyCountries(s.id,c.id,s.Limited_by_Countries__c);
            System.assertEquals('true', verifyCountry.countriesMatched);
            
            //Contact Mailing Country != Subscription Limited By Country.
            c.Mailing_Country__c = 'France';
            update c;
            verifyCountry = ContactSubscriptionListController.verifyCountries(s.id,c.id,s.Limited_by_Countries__c);
            System.assertEquals('false', verifyCountry.countriesMatched);
           //Passing null values to throw an exception. 
            verifyCountry = ContactSubscriptionListController.verifyCountries(null,null,null);
            
            
        }
        Test.stopTest();
    }
    public static testMethod void testUpdateSubscriptions(){
        
        Subscription__c sub = CommonTestUtils.createTestSubscription('test', 'Subscription');
        Subscription__c sub1 = CommonTestUtils.createTestSubscription('test', 'Subscription');
        Contact c = CommonTestUtils.createTestContact('TestUser1');
        insert c;
        Contact_Subscription_Link__c link = CommonTestUtils.createTestContactSubscription(c, sub);
        List<ID> addList = new List<ID>();
        List<ID> delList = new List<ID>();
        Id randomId = null;
        addList.add(sub1.id);
        
        delList.add(sub.id);
        Test.startTest();
        String subscribed;
         subscribed = ContactSubscriptionListController.updateContactSubscription(addList, delList, c.id);
        System.assertEquals('true', subscribed);
        //Creating exception by passing the null Id in the addList;
        addLIst.add(randomId);
        subscribed = ContactSubscriptionListController.updateContactSubscription(addList, delList, c.id);
        System.assertEquals('false', subscribed);
        // cover else conditions when addList and delList are empty
            addList.clear();      
            delList.clear();
        subscribed = ContactSubscriptionListController.updateContactSubscription(addList, delList, c.id);
        Test.stopTest();
    }
    public static testMethod void testFailedUpdateSubscriptions(){
        Profile profile = [select id from profile where name = 'Institutional User'];
        if(profile != null){
        List<User> user =  CommonTestUtils.createMultipleUsers(profile.id, 1);
        if(user == null || user.size() != 1){
            System.assert(false, 'Incorrect Number of Institutional Users');
        }
        System.runAs(user[0]){
            Subscription__c sub = CommonTestUtils.createTestSubscription('test', 'Subscription');
            Subscription__c sub1 = CommonTestUtils.createTestSubscription('test', 'Subscription');
            Contact c = CommonTestUtils.createTestContact('TestUser1');
            insert c;
            Contact_Subscription_Link__c link = CommonTestUtils.createTestContactSubscription(c, sub);
            List<ID> addList = new List<ID>();
            List<ID> delList = new List<ID>();
            addList.add(sub1.id);
            delList.add(sub.id);
            Test.startTest();
            String subscribed = ContactSubscriptionListController.updateContactSubscription(addList, delList, c.id);
            System.assertEquals('true', subscribed);
            Test.stopTest();
        }
            }
    }
}