@isTest
public class InstitutionalTaxiReportControllerTest {
    public static Id userId;
    public static Account account;
    public static testmethod void testInstiTaxiReportController(){
        Profile p1 = [select Id from Profile where Name like '%Insti%' limit 1];
        User u = CommonTestUtils.createTestUser(true);       
        u.ProfileId = p1.id;
        update u;
        
        System.runAs(u){
            InstitutionalTaxiReportControllerTest.userId = u.Id;
            InsertTestData();
            Test.startTest();
            ApexPages.StandardController ctrl = new ApexPages.StandardController(account);
            InstitutionalTaxiReportController ictrl = new InstitutionalTaxiReportController(ctrl);
            AccountTeamMember atm = new AccountTeamMember();
            atm.AccountId = account.Id;
            atm.UserId = u.Id;       
            insert atm;
            ictrl.setAccountId();
            System.debug('----> '+InstitutionalTaxiReportController.accountId);
            Account a = ictrl.accountDetails;
            System.assertEquals(account.Id, a.Id);
            Integer totalCalls = ictrl.totalNumberOfCalls;

            Integer totalMeetings = ictrl.totalNumberOfMeetings;
            List<InstitutionalTaxiReportController.ActivityHistory1> ac = ictrl.activities;
            List<Activity_Product_Relationship__c> strategies = ictrl.strategiesOfIntreast;
                      
            Test.stopTest();
        }
    }
    public static testmethod void testInstitutionalReport1(){
        Profile p1 = [select Id from Profile where Name like '%Insti%' limit 1];
        User u = CommonTestUtils.createTestUser(true);       
        u.ProfileId =p1.id;
        update u;
        
        System.runAs(u){
            InsertTestData();
            Test.startTest();
            ApexPages.StandardController ctrl = new ApexPages.StandardController(account);
            InstitutionalTaxiReportController ictrl = new InstitutionalTaxiReportController(ctrl);
            List<Opportunity> opportunities = ictrl.opportunities;
            List<InstitutionalTaxiReportController.ActivityHistory1> ac1 = ictrl.futureEvents;
            List<Campaign> campaigns = ictrl.getCampaigns;
            List<Client_Third_Party_Relationship__c> getConsultants = ictrl.getConsultants;
            Test.stopTest();
        }
    }

    public static testmethod void testInstitutionalReport2(){
        Profile p1 = [select Id from Profile where Name like '%Insti%' limit 1];
        User u = CommonTestUtils.createTestUser(true);       
        u.ProfileId = p1.id;
        update u;
        
        Test.startTest();
        System.runAs(u){
            InstitutionalTaxiReportControllerTest.userId = u.Id;
            InsertTestData();
            ApexPages.StandardController ctrl = new ApexPages.StandardController(account);
            InstitutionalTaxiReportController ictrl = new InstitutionalTaxiReportController(ctrl);
            AccountTeamMember atm = new AccountTeamMember();
            atm.AccountId = account.Id;
            atm.UserId = u.Id;       
            insert atm;
            ictrl.setAccountId();
            ictrl.getCurrentMandates();
            List<AccountContactRelation> keyContacts = ictrl.keyContacts;
            List<AccountTeamMember> accountTeam = ictrl.accountTeam;            
            Test.stopTest();
        }
    }
    
    public static void InsertTestData(){
        list<account> accountList = new list<account>();
        Account acc = CommonTestUtils.CreateTestClient('Client 1');
        accountList.add(acc);
        Account acc1 = CommonTestUtils.CreateTestClient('Client 2');
        accountList.add(acc1);
        Account tp = CommonTestUtils.CreateTestThirdParty('Thirdparty 1'); 
        tp.Third_Party_Type__c = 'Custodian';
        accountList.add(tp);
        insert accountList;        
		
        InstitutionalTaxiReportControllerTest.account = acc;
        
        //InsertTestData(acc,acc1,tp,u);       
        PageReference page = new PageReference('/apex/InstitutionalTaxiReportPage');
        page.getParameters().put('accountId',acc.Id);
        page.getParameters().put('isMobile','false');                
        Test.setCurrentPage(page);
        
        Campaign cam1 = CommonTestUtils.CreateTestCampaign('Test Campaign 1', false);
      //  Campaign cam2 = CommonTestUtils.CreateTestCampaign('Test Campaign 2');
        cam1.StartDate = Date.today().addDays(30);
        cam1.Type='Event';
        cam1.Objective__c = 'New Features Demo Campaign';  
        cam1.RecordTypeId = Campaign.sObjectType.getDescribe().getRecordTypeInfosByName().get('Strategic').getRecordTypeId();
        insert cam1;
        
        list<contact> contactList = new list<contact>();
        Contact c = CommonTestUtils.CreateTestContact('lastName');
        c.AccountId = acc.Id;            
        contactList.add(c);
        Contact c1 = CommonTestUtils.CreateTestContact('lastName');           
        c1.AccountId = acc1.Id; 
        contactList.add(c1);
        
        insert contactList;
        
        Campaign_Team__c cTeam = CommonTestUtils.createTestCampaignTeam(cam1, c);
        
        DateTime d1 = Datetime.newInstance(2016, 11, 13, 11, 30, 0);
        DateTime d2 = Datetime.newInstance(2016, 11, 13, 12, 30, 0);
        DateTime d3 = Datetime.newInstance(2017, 11, 23, 12, 30, 0);
        DateTime d4 = Datetime.newInstance(2017, 11, 23, 14, 30, 0);
        
        Activity_alias__c aalias = new Activity_alias__c();
        aalias.Contact__c = c1.Id;
        insert aalias;
        
        Investment_Strategy__c ivs = new Investment_Strategy__c();
        ivs.name='Test Strategy';
        insert ivs;
        
        Activity_Product_Relationship__c apr = new Activity_Product_Relationship__c();
        apr.Activity_alias__c = aalias.Id;
        apr.Investment_Strategy__c = ivs.Id;
        insert apr;
        
        list<event> eventList = new list<event>();        
        Event eve1 = CommonTestUtils.CreateTestEvent('Meeting Event',d1,d2,c.id,'Meeting','');
        eve1.Strategies_Funds__c = aalias.Id;
        eve1.WhatId = acc.Id;
        eventList.add(eve1);
        Event eve2 = CommonTestUtils.CreateTestEvent('Future Meeting Event',d3,d4,c.id,'Meeting','');
        eventList.add(eve2);  
        Event eve3 = CommonTestUtils.CreateTestEvent('Call Event',d1,d2,c.id,'Call','');
        eve3.WhatId = acc.Id;
        eventList.add(eve3);
        insert eventList;
        
        Opportunity opp = new Opportunity();
        opp.AccountId = acc.Id;
        opp.Name = 'Test Opportunity';
        opp.StageName='Won';
        opp.CloseDate = Date.today().addDays(20);
        insert opp;
        
        Asset asset = new Asset();
        asset.AccountId = acc.Id;
        asset.Name = 'Test Asset';
        insert asset;
        
        AccountContactRelation acr = new AccountContactRelation();
        acr.AccountId = acc.Id;
        acr.ContactId = c1.Id;
        acr.Key_Contact__c = true;
        insert acr;
        
        Client_Third_Party_Relationship__c ctpr = new Client_Third_Party_Relationship__c();
        ctpr.Client_Name__c = acc.Id;
        ctpr.Third_Party_Name__c = tp.Id;
        ctpr.Relationship_Type__c='Consultant';
        insert ctpr;

    }
}