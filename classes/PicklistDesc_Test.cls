@isTest
public class PicklistDesc_Test{
     
    /*
     * 
     * Description : Call 
     * Param : 
     * Returns :  
    */
  
  static testMethod void CallPicklistDesc(){
  	Test.startTest();
    RecordType recType = [Select Id,DeveloperName from RecordType Where SobjectType = 'Account' 
                                            AND DeveloperName = 'Client'];
  	
    List<String> options = PicklistDescriber.describe('Task', 'Type__c', 'Sub_Type__c', 'Call');
    List<String> options2 = PicklistDescriber.describe('Account', 'Industry');
    List<String> options3 = PicklistDescriber.describe('Account', recType.Id, 'Industry');
    
    Test.stopTest();
  }
   
   static testMethod void CallPicklistDescController(){ 
   	Test.startTest();
   	RecordType recType = [Select Id,DeveloperName from RecordType Where SobjectType = 'Account' 
                                            AND DeveloperName = 'Client'];
  	ApexPages.CurrentPage().getparameters().put('recordTypeId', recType.Id);
  	ApexPages.CurrentPage().getparameters().put('recordTypeName', recType.DeveloperName);
  	ApexPages.CurrentPage().getparameters().put('sobjectType', 'Account');
  	ApexPages.CurrentPage().getparameters().put('picklistFieldName', 'Client_Type__c');
  	ApexPages.CurrentPage().getparameters().put('ParpicklistFieldName', 'Client_Sub_Type__c');
  	ApexPages.CurrentPage().getparameters().put('ParpicklistFieldValue', 'Bank');
    PickListDescController Controller = new PickListDescController();
  	Test.stopTest();
   }
  
  static testMethod void CallPicklistDescController2(){ 
   	Test.startTest();
  	Account Acc = CommonTestUtils.CreateTestClient('TestAccount123');
  	insert Acc;
  	system.debug(Acc.Id);
  	ApexPages.CurrentPage().getparameters().put('id', Acc.Id);
  	ApexPages.CurrentPage().getparameters().put('recordTypeName', 'Client');
  	ApexPages.CurrentPage().getparameters().put('picklistFieldName', 'Client_Type__c');
  	ApexPages.CurrentPage().getparameters().put('ParpicklistFieldName', 'Client_Sub_Type__c');
  	ApexPages.CurrentPage().getparameters().put('ParpicklistFieldValue', 'Bank');
  	PickListDescController Controller = new PickListDescController();  
   	
   	Test.stopTest();
   }
    
}