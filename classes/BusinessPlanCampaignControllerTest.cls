@isTest
public class BusinessPlanCampaignControllerTest {

    public static testmethod void testBusinessPlanCampaigns(){
      
       Profile p1 = [select Id from Profile where Name like '%Retail%'];
        User u = CommonTestUtils.createTestUser(true);
        u.Business_Line__c = 'Retail';        
        u.ProfileId =p1.id;
        update u;
        
        Test.startTest();
        System.runAs(u){
        Campaign cam1 = CommonTestUtils.CreateTestCampaign('Test Campaign 1');
        Campaign cam2 = CommonTestUtils.CreateTestCampaign('Test Campaign 2');
        cam1.StartDate = Date.today().addDays(30);
        cam1.Type='Event';
        cam1.Objective__c = 'New Features Demo Campaign';
       
        update cam1;
        
        cam2.StartDate = Date.today().addDays(51);
        cam2.Type='Email';
        cam2.Objective__c = 'New Features Email Campaign';
        update cam2;
            List<ID> campIds = new List<ID>();
                        
            //Checking for Exception
            BusinessPlanCampaignController.getCampaign(null);
            //Checking with empty list
            BusinessPlanCampaignController.getCampaign(campIds);
            campIds.add(cam1.Id);
            campIds.add(cam2.Id);
            BusinessPlanCampaignController.getCampaign(campIds);
            
            //Testing the campaigns related to Business Plan
           Business_Plan__c bp = BusinessPlanCampaignControllerTest.createTestBusinessPlan('Business Plan 1');
            insert bp;           
           Business_Plan_Campaigns__c bpc1 = BusinessPlanCampaignControllerTest.createTestBusinessPlanCampaign(bp,cam1);
            insert bpc1;
           //Testing by sending wrong business plan Id
            BusinessPlanCampaignController.getCampaigns('businessPlanId');
            //Testing by sending empty business plan Id
            BusinessPlanCampaignController.getCampaigns('');
            BusinessPlanCampaignController.getCampaigns(String.valueOf(bp.Id));
           String campaignText = BusinessPlanCampaignController.getCampaignText(bp.Id);
            List<ID> delCamps = new List<ID>();
            delCamps.add(null);
            BusinessPlanCampaignController.createBusinessPlanCampaigns(String.valueOf(bp.Id), campIds, delCamps, campaignText);
            delCamps.add(cam1.id);
            BusinessPlanCampaignController.createBusinessPlanCampaigns(String.valueOf(bp.Id), campIds, delCamps, campaignText);
        }
        
        Test.stopTest();
    }
    
     public static Business_Plan__c createTestBusinessPlan(String businessPlanName){
        Business_Plan__c businessPlan = new Business_Plan__c();
        businessPlan.name = businessPlanName;
        businessPlan.RecordTypeId = Business_Plan__c.sObjectType.getDescribe().getRecordTypeInfosByName().get('Open').getRecordTypeId();
        businessPlan.Campaign_Details__c= 'Campaign 1 Test Details';
         return businessPlan;
    }
    public static Business_Plan_Campaigns__c createTestBusinessPlanCampaign(Business_Plan__c businessPlan, Campaign cam){
        Business_Plan_Campaigns__c bpo = new Business_Plan_Campaigns__c();
        bpo.Business_Plan__c = businessPlan.Id;
        bpo.Campaign__c = cam.Id;
        return bpo;
    }      
}