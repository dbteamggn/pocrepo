@isTest
public class TriggerHandler_Test {
    static testMethod void myUnitTest(){
        Object_Method_ByPass__c obByPass = new Object_Method_ByPass__c(Name = 'test', Object_Name__c = 'Account', User_Profile_ID__c = UserInfo.getUserId(), Full_Bypass__c = true);
        insert obByPass;
        Account acc0 = new Account(Name = 'Test Account9', Billing_PostalCode__c = '515050', Hierarchy_Level__c = 'L0');
        insert acc0;
        obByPass.Full_Bypass__c = false;
        obByPass.Method_Name__c = 'Account_Utility.copyChildRecords';
        update obByPass;
        Account acc5 = new Account(Name = 'Test Account5', Billing_PostalCode__c = '515150', Hierarchy_Level__c = 'L0');
        insert acc5;
        Profile_Exclusion__c orgEx = new Profile_Exclusion__c(Bypass_Triggers__c = TRUE, SetupOwnerId = UserInfo.getOrganizationId());
        insert orgEx;
        Account acc = new Account(Name = 'Test Account', Billing_PostalCode__c = '505050', Hierarchy_Level__c = 'L0');
        insert acc;
        Profile_Exclusion__c profEx = new Profile_Exclusion__c(Bypass_Triggers__c = TRUE, SetupOwnerId = UserInfo.getProfileId());
        insert profEx;
        Account acc1 = new Account(Name = 'Test Account1', Billing_PostalCode__c = '505051', Hierarchy_Level__c = 'L0');
        insert acc1;
        Profile_Exclusion__c userEx = new Profile_Exclusion__c(Bypass_Triggers__c = TRUE, SetupOwnerId = UserInfo.getUserId());
        insert userEx;
        Account acc2 = new Account(Name = 'Test Account2', Billing_PostalCode__c = '505052', Hierarchy_Level__c = 'L0');
        insert acc2;
    }
}