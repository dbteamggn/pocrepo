public class ContactListCompController {
	
	@AuraEnabled
    public static List<Contact> getContacts() {
        
        List<Contact> contacts = 
            [SELECT Id, Name, Email, AccountId, Account.Name FROM Contact order by Name limit 10000];
        
        //Add isAccessible() check
        return contacts;
    }
}