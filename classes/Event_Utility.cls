/*----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            Event_Utility
   Description:     Methods called by Event triggers
                    
   Date             Author                             Summary of Changes                  
   -----------      -----------------                  -------------------------------------------------------------------------------------
    28 Jun 2016      Sridhar Gurram                     Initial Release
------------------------------------------------------------------------------------------------------------------------------------------------------------*/
public class Event_Utility {

    /*
     * Description : Create new instance / return existing instance
     * Param : 
     * Returns :  
    */
    private static Event_Utility instance = null; 
    
    public static Event_Utility getInstance() {
        if (instance == null) {
            instance = new Event_Utility();
        }
        return instance;
    }
    
    /*
     * Description : Method to get Contact name and its Client name from Event.whoid
     * Param : Events from Trigger where whoid has changed
     * Returns :  
    */ 
    public void UpdateEventCustomFields(List<Event> evtLst) {
        //Get Event contact ids in a Set
        Set<Id> conids = new Set<Id>();
        for (Event evt : evtLst) {
            conids.add(evt.whoid);
        } 
    
        //Get Contact Id and account relationship into a Map
        Map<Id, Contact> conMap = new Map<Id, Contact>([Select Id, Name, Account.Name from Contact where Id in :conIds]);
    
        //Iterate through the event list and fetch/assign Contact name and its Client Name to the event custom fields
        for (Event evt : evtLst) {
            if (conMap.containskey(evt.whoid)){
                evt.Contact_Name__c = conMap.get(evt.whoid).Name;
                evt.Client_Name__c = conMap.get(evt.whoid).Account.Name;
            }
        }
        update evtLst;
    }
    
    
     /*
     * Description : Method to roll up the actual and budgeted cost from
     * events to its associated campaign
     * Param : List of campaign id to be updated
     * Param : Map of campaign id and its associated event
     * Returns :  None
    */ 
    public void rollUpCostFieldsToCampaign(Set<Id> set_campaginId, Map<Id, List<Event>> mapofCampaignAndEvent) {
        // aggregate the sum of actaul and budgeted cost from all the events associated to the cmapaign
        list<AggregateResult> listOfAggRes  = [Select   sum(Actual_Cost__c) ac, sum(Budgeted_Cost__c) bc,
                                                        whatId
                                               FROM     Event 
                                               WHERE    whatid IN:set_campaginId AND WhatId != NULL 
                                               Group by whatid]; 
                                               
        Map<Id, Campaign> mapOfCampaignId = new Map<Id, Campaign> ([Select id, currencyIsoCode, Budgeted_Activities_Cost__c, Actual_Activities_Cost__c From Campaign WHERE Id IN:set_campaginId]);                                                                             
        Map<Id, AggregateResult> mapOfIdAggRes = new Map<Id, AggregateResult>();
        Map<String , Decimal> mapOfCurrAndVal = CommonUtilities.getCurrencyInfo();
        List<Campaign> listOfCampaign = new List<Campaign>();
        if(listOfAggRes != null && listOfAggRes.size()>0){
            for(AggregateResult ar:listOfAggRes){
                mapOfIdAggRes.put((Id) ar.get('whatId'), ar);
            }
        }
        if(mapOfCampaignId != null && mapOfCampaignId .size()>0){  
            for(Campaign c:mapOfCampaignId.values()){
                Campaign o_campaign = mapOfCampaignId.get(c.id);
                if(mapOfIdAggRes.containsKey(c.id)){
                    Decimal acVal = (Decimal) mapOfIdAggRes.get(c.id).get('ac');
                    Decimal bcVal = (Decimal) mapOfIdAggRes.get(c.id).get('bc');
                    if(acVal == NULL){
                        acVal = 0;
                    }
                    if(bcVal == NULL){
                        bcVal = 0;
                    }
                    o_campaign.Actual_Activities_Cost__c = acVal * mapOfCurrAndVal.get(o_campaign.currencyIsoCode);
                    o_campaign.Budgeted_Activities_Cost__c = bcVal * mapOfCurrAndVal.get(o_campaign.currencyIsoCode);
                    
                }else{
                    o_campaign.Actual_Activities_Cost__c = 0;
                    o_campaign.Budgeted_Activities_Cost__c = 0;
                }
                listOfCampaign.add(o_campaign);
            }
            //update campaign
            if(listOfCampaign != null && listOfCampaign.size() > 0){
                try{
                    Database.saveResult[] l_dsr = Database.update(listOfCampaign, false);
                    // if campaign fails to update throw error message on all of its associated event 
                    for(Database.saveResult sr:l_dsr){
                        if(!sr.isSuccess()){
                            for(Event o_event:mapofCampaignAndEvent.get(sr.getId())){
                                o_event.addError(Label.GenericErrorMessage +' ' + sr.getErrors());
                            }
                        }
                    }
                }catch(Exception ex){
                    CommonUtilities.createExceptionLog(ex);
                }
            }
        }                                   
    }
    
    /* Description: Helper method to create/update G&B records 
       Author: Joshna*/
    public static void createGnB(TriggerHandler.TriggerParameter tParam){
        List<Event> newEvents = (List<Event>) tParam.newList;
        Set<Id> newEventIds = tParam.newMap.keySet();
        Map<Id, Event> oldEventsMap = new Map<Id, Event>();
        Map<Id, Map<Id, EventRelation>> eventAttendees = new Map<Id, Map<Id, EventRelation>>();
        List<Gift_Benefit__c> newGBRecords = new List<Gift_Benefit__c>();
        String contactPrefix = Schema.getGlobalDescribe().get('Contact').getDescribe().getKeyPrefix();
        Map<Id, Map<Id, Gift_Benefit__c>> existingGifts = new Map<Id, Map<Id, Gift_Benefit__c>>();
        List<Gift_Benefit__c> toDelete = new List<Gift_Benefit__c>();
        Set<Id> contactIds = new Set<Id>();
        Map<Id, Contact> contactMap = new Map<Id, Contact>();
        
        try{
            for(EventRelation each: [select Id, isInvitee, EventId, isParent, isWhat, RelationId from EventRelation where EventId IN :newEvents AND isWhat = false]){
                if(eventAttendees.containsKey(each.EventId)){
                    eventAttendees.get(each.EventId).put(each.RelationId, each);
                } else{
                    eventAttendees.put(each.EventId, new Map<Id, EventRelation> {each.RelationId => each});
                }
                contactIds.add(each.RelationId);
            }
            
            if(!contactIds.isEmpty()){
                contactMap = new Map<Id, Contact>([select Id, Name, AccountId from Contact where Id IN :contactIds]);
            }
            
            oldEventsMap = (Map<Id, Event>) tParam.oldMap;
            for(Gift_Benefit__c each: [select Id, Client__c, Related_Contact__c, Related_Activity_Id__c from Gift_Benefit__c where Related_Activity_Id__c IN :newEventIds]){
                if(existingGifts.containsKey(each.Related_Activity_Id__c)){
                    existingGifts.get(each.Related_Activity_Id__c).put(each.Related_Contact__c, each);
                } else{
                    existingGifts.put(each.Related_Activity_Id__c, new Map<Id, Gift_Benefit__c>{each.Related_Contact__c => each});
                }
                if(eventAttendees.containsKey(each.Related_Activity_Id__c)){
                    if(!eventAttendees.get(each.Related_Activity_Id__c).containsKey(each.Related_Contact__c)){
                        toDelete.add(each);
                    }
                }
            }
            
            for(Event each: newEvents){
                Boolean attendeesUpdated = false;
                if(each.Gift__c && eventAttendees.containsKey(each.Id)){
                    Integer attendeeCnt = eventAttendees.get(each.Id).size();
                    Boolean isUpdated = false;
                    
                    if(tParam.isUpdate){
                        Event oldEvent =  oldEventsMap.get(each.Id);
                        if(oldEvent.Total_Gift_Amount__c != each.Total_Gift_Amount__c || oldEvent.ActivityDate != each.ActivityDate || oldEvent.Subject != each.Subject || 
                            oldEvent.Given_Received__c != each.Given_Received__c || oldEvent.GBE_Type__c != each.GBE_Type__c || oldEvent.GBE_Sub_Type__c != each.GBE_Sub_Type__c 
                            || oldEvent.CurrencyISOCode != each.CurrencyISOCode || oldEvent.Cost_Type__c != each.Cost_Type__c || oldEvent.Description != each.Description || 
                            oldEvent.sub_Type__c != each.sub_Type__c || oldEvent.Type__c!= each.Type__c|| oldEvent.describe_the_gift__c!= each.describe_the_gift__c
                            || oldEvent.OwnerId != each.OwnerId){
                            isUpdated = true;
                        }
                    }
                    Decimal giftAmt = each.Total_Gift_Amount__c;
                    if(each.Cost_Type__c == System.Label.TotalCostType){
                        giftAmt = giftAmt/attendeeCnt;
                    }
                    if(!existingGifts.containsKey(each.Id) || (existingGifts.containsKey(each.Id) && eventAttendees.get(each.Id).size() <> existingGifts.get(each.Id).size())){
                        attendeesUpdated = true;
                    }
                    for(EventRelation eachAttendee: eventAttendees.get(each.Id).values()){
                        if(existingGifts.containsKey(each.Id) && existingGifts.get(each.Id).containsKey(eachAttendee.RelationId)){
                            if(isUpdated || attendeesUpdated){
                                if(eachAttendee.RelationId != NULL && String.valueOf(eachAttendee.RelationId).startsWith(contactPrefix)){
                                    Gift_Benefit__c newGBRec = new Gift_Benefit__c(Id = existingGifts.get(each.Id).get(eachAttendee.RelationId).Id, Amount__c = giftAmt, Date__c = each.ActivityDate, Comments__c = each.Description,
                                    Related_Activity_Name__c = each.Subject, Subtype__c = each.GBE_Sub_Type__c, Type__c = each.GBE_Type__c,Given_Received__c = each.Given_Received__c, CurrencyISOCode = each.CurrencyISOCode,
                                    Meeting_Subtype__c=each.sub_Type__c,Meeting_Type__c=each.Type__c,describe_the_gift__c=each.describe_the_gift__c, Owner__c=each.OwnerId);
                                   newGBRecords.add(newGBRec);
                                }
                            }
                        } else{
                            Gift_Benefit__c newGBRec = new Gift_Benefit__c(Amount__c = giftAmt, Date__c = each.ActivityDate, Related_Activity_Id__c = each.Id, Comments__c = each.Description, 
                                Related_Activity_Name__c = each.Subject, Subtype__c = each.GBE_Sub_Type__c, Type__c = each.GBE_Type__c, Given_Received__c = each.Given_Received__c, CurrencyISOCode = each.CurrencyISOCode,
                                Meeting_Subtype__c=each.sub_Type__c,Meeting_Type__c=each.Type__c,describe_the_gift__c=each.describe_the_gift__c, Owner__c=each.OwnerId);       
                            if(eachAttendee.RelationId != NULL && String.valueOf(eachAttendee.RelationId).startsWith(contactPrefix)){
                                newGBRec.Related_Contact__c = eachAttendee.RelationId;
                                if(contactMap.containsKey(eachAttendee.RelationId)){
                                    newGBRec.Client__c = contactMap.get(eachAttendee.RelationId).AccountId;
                                }
                            }
                            newGBRecords.add(newGBRec);
                        }
                    }
                }
            }
            if(!newGBRecords.isEmpty()){
                upsert newGBRecords;
            }
            if(!toDelete.isEmpty()){
                delete toDelete;
            }    
        }catch(Exception e){
            CommonUtilities.createExceptionLog(e);
        }
    }
    
    /* Description: Helper method to create/update G&B records 
       Author: Joshna*/
    public static void validateDelete(TriggerHandler.TriggerParameter tParam){
        List<Event> oldEvents = (List<Event>) tParam.oldList;
        for(Event each: oldEvents){
            if(each.Gift__c){
                each.addError(System.Label.CantDeleteGift);
            }
        }
    }
    
    /*
     * Description  : method to create Activity Client Relationship records. Added for US28000.
     * Param        : TriggerParameter
     * Returns      : none
    */
    public static void createActivityClientRelation(TriggerHandler.TriggerParameter tParam){
        List<Event> newEvents = (List<Event>) tParam.newList;
        Map<id, Event> newEventMap = (Map<id, Event>) tParam.newMap;
        Map<id, List<Activity_Client_Relationship__c>> newActClientRelMap = new Map<id, List<Activity_Client_Relationship__c>>();
        List<Activity_Client_Relationship__c> newActClientRelList = new List<Activity_Client_Relationship__c>();
        Set<Id> contactIds = new Set<Id>();
        Map<Id, Contact> contactMap;
        Map<id, Map<id, Activity_Client_Relationship__c>> contactIdEventIdACRMap = new Map<id, Map<id, Activity_Client_Relationship__c>>();
        String contactPrefix = Schema.getGlobalDescribe().get('Contact').getDescribe().getKeyPrefix();
        Set<id> filteredEventIdSet = new Set<id>();
        Map<id, List<Activity_Client_Relationship__c>> newACRsByClient = new Map<id, List<Activity_Client_Relationship__c>>();
        Map<id, Set<id>> existingACRsByClient = new Map<id, Set<id>>();
        Activity_Client_Relationship__c tempACR;
        
        try{
            
            for(Event each : newEvents){
                //if whatId != null AND AccountId != null, create ACR for event Id and AccountId
                if(each.whatId != null && each.AccountId != null){
                    tempACR = new Activity_Client_Relationship__c(Activity__c = each.id, Client__c = each.AccountId, Activity_Subject__c = each.subject);
                    newActClientRelList.add(tempACR);
                    if(newACRsByClient.isEmpty() || !newACRsByClient.containsKey(each.accountId)){
                        newACRsByClient.put(each.accountId, new List<Activity_Client_Relationship__c>{tempACR});
                    }
                    else if(!newACRsByClient.isEmpty() && newACRsByClient.containsKey(each.accountId)){
                        newACRsByClient.get(each.accountId).add(tempACR);
                    }
                }
                //if whatId == null, get info from EventRelation
                else if(each.whatId == null){
                    filteredEventIdSet.add(each.id);
                }
            }
            
            //get all EventRelations where relationId is a contact's id and create Activity_Client_Relationship__c records based on the EventRelation details
            for(EventRelation eventRel: [select Id, EventId, isWhat, RelationId, AccountId, Event.subject from EventRelation where EventId IN :filteredEventIdSet AND isWhat=false]){
                if(eventRel.RelationId != null && String.valueOf(eventRel.RelationId).startsWith(contactPrefix)){
                    if(!newActClientRelMap.isEmpty() && newActClientRelMap.containsKey(eventRel.RelationId)){
                        newActClientRelMap.get(eventRel.RelationId).add(new Activity_Client_Relationship__c(Activity__c = eventRel.EventId, Contact__c = eventRel.RelationId, Activity_Subject__c = eventRel.Event.subject));
                    } else{
                        newActClientRelMap.put(eventRel.RelationId, new List<Activity_Client_Relationship__c> {new Activity_Client_Relationship__c(Activity__c = eventRel.EventId, Contact__c = eventRel.RelationId, Activity_Subject__c = eventRel.Event.subject)});
                    }
                    //collect all contact ids from EventRelation records
                    contactIds.add(eventRel.RelationId);
                }
            }
            
            //get primary Account from collected contact records
            if(!contactIds.isEmpty()){
                contactMap = new Map<Id, Contact>([select Id, Name, AccountId from Contact where Id IN :contactIds]);
            }
            
            //update Client__c field with the associated contact's AccountId
            for(Id contactId : newActClientRelMap.keyset()){
                for(Activity_Client_Relationship__c acr : newActClientRelMap.get(contactId)){
                    if(!contactMap.isEmpty() && contactMap.containskey(contactId) && contactMap.get(contactId).AccountId != null){
                        acr.Client__c = contactMap.get(contactId).AccountId;
                        newActClientRelList.add(acr);
                    }
                }               
            }
            
            //AFTER UPDATE CONTEXT: Only insert the new records - START
            if(tParam.isAfter && tParam.isUpdate){
                
                //get existing Activity_Client_Relationship__c records
                for(Activity_Client_Relationship__c acr : [select id, Client__c, Activity__c, Contact__c from Activity_Client_Relationship__c where Activity__c IN: newEventMap.keyset()]){
                    //if contact__c != null, create a map of existing ACRs against contact
                    if(acr.Contact__c != null){
                        if(!contactIdEventIdACRMap.isEmpty() && contactIdEventIdACRMap.containsKey(acr.Contact__c)){
                            contactIdEventIdACRMap.get(acr.Contact__c).put(acr.Activity__c, acr);
                        } else{
                            contactIdEventIdACRMap.put(acr.Contact__c, new Map<id, Activity_Client_Relationship__c> {acr.Activity__c => acr});
                        }
                    }
                    //if contact__c == null, create a map of existing ACRs against client
                    else{
                        if(existingACRsByClient.isEmpty() || !existingACRsByClient.containsKey(acr.client__c)){
                            existingACRsByClient.put(acr.client__c, new set<id>{acr.activity__c});
                        }
                        else if(!existingACRsByClient.isEmpty() && existingACRsByClient.containsKey(acr.client__c)){
                            existingACRsByClient.get(acr.client__c).add(acr.activity__c);
                        }
                    }
                }
                
                newActClientRelList = new List<Activity_Client_Relationship__c>();
                
                //iterate over all records and get the new ones to be inserted
                for(Id contactId : newActClientRelMap.keyset()){
                    for(Activity_Client_Relationship__c acr : newActClientRelMap.get(contactId)){
                        if(!contactIdEventIdACRMap.isEmpty() && !contactIdEventIdACRMap.containskey(contactId)){
                            newActClientRelList.add(acr);
                        }
                        else if(!contactIdEventIdACRMap.isEmpty() && contactIdEventIdACRMap.containskey(contactId) && !contactIdEventIdACRMap.get(contactId).containsKey(acr.Activity__c)){
                            newActClientRelList.add(acr);
                        }
                    }               
                }
                for(Id accountId : newACRsByClient.keySet()){
                    for(Activity_Client_Relationship__c acr : newACRsByClient.get(accountId)){ 
                        if(!existingACRsByClient.isEmpty() && !existingACRsByClient.containsKey(accountId)){
                            newActClientRelList.add(acr);
                        }
                        else if(!existingACRsByClient.isEmpty() && existingACRsByClient.containsKey(accountId) && !existingACRsByClient.get(accountId).contains(acr.activity__c)){
                            newActClientRelList.add(acr);
                        }
                    }
                }
            }
            //AFTER UPDATE CONTEXT: Only insert the new records - END
            
            //insert Activity Client Relation records for each EventRelation
            if(!newActClientRelList.isEmpty()){
                insert newActClientRelList;
            }
        }
        catch(Exception e){
            CommonUtilities.createExceptionLog(e);
        }
    }
}