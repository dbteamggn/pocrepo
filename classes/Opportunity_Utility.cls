public class Opportunity_Utility {

    /*
     * Description : Create new instance / return existing instance
     * Param : 
     * Returns :  
    */
    private static Opportunity_Utility instance = null; 
    
    public static Opportunity_Utility getInstance() {
        if (instance == null) {
            instance = new Opportunity_Utility();
        }
        return instance;
    }
    
    
    /*
     * Description  : method called on before insert trigger event from Opportunity_Trigger
     * Param        : trigger.new
     * Returns      : none
    */ 
    public void beforeInsert(List<Opportunity> newList) {
        
        for(Opportunity opp : newList){
            //US30337 - Defaulting primary opportunity tema member to owner on opportunity creation
           // opp.primary_opportunity_team_member__c = opp.ownerId;
        }
    }
    
    
    /*
     * Description  : method to get Opportunity records based on opportunity Ids
     * Param        : set of opportunity IDs
     * Returns      : Map of opportunity id Opportunity records
    */
    public Map<id, Opportunity> getOpportunities(set<Id> opptyIdSet){
        
        List<Opportunity> oppList = new List<Opportunity>();
        Map<id, Opportunity> oppMap = new Map<id, Opportunity>();
        
        oppList = [select id, ownerId from Opportunity where id in: opptyIdSet];
        
        for(Opportunity opp : oppList){
            oppMap.put(opp.id, opp);
        }
        
        return oppMap;
    }
}