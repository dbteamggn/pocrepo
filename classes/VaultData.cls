public with sharing class VaultData{
    public string psiInfo {get;set;}
    Public Vault__c vaultRecord {get;set;}
    public VaultData(ApexPages.StandardController controller) {
        Id oppId1=ApexPages.currentPage().getParameters().get('id');
        
        
        try{
            Opportunity opp=[select id,Vault__c from Opportunity where id=:oppId1];
            System.debug('#Id1'+oppId1);
            vaultRecord=[select id,PSI_Info__c,name from Vault__c where id=:opp.Vault__c];
            system.debug('#vaultRecord'+vaultRecord);
        }
        
        Catch(Exception e){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Insufficient Privilage.Contact your adminstrator');
            ApexPages.addMessage(myMsg);
        }
        
        
    }

}