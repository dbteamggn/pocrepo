@isTest
public class BP_InstitutionalFlowCtrl_Test{
    static testMethod void myUnitTestMethod(){
        List<String> allVals = BP_InstitutionalFlowCtrl.getPicklistValues('business_plan__c', 'business_plan_duration__c');
        Id instBPId = BP_InstitutionalFlowCtrl.createNewInstiPlan();
        BP_InstitutionalFlowCtrl.updateBP(instBPId, 'Test overview', '2 years', 'Other Val', String.valueOf(Date.Today() + 1));
        BP_InstitutionalFlowCtrl.updateBP(NULL, 'Test overview', '2 years', 'Other Val', String.valueOf(Date.Today() + 1));
        BP_InstitutionalFlowCtrl.getBPRecord(instBPId);
        
        List<Account> accList = new List<Account>{new Account(Name = 'Test Account', Billing_PostalCode__c = '500505', Hierarchy_Level__c = 'L0'),
                                                  new Account(Name = 'Test Account1', Billing_PostalCode__c = '511505', Hierarchy_Level__c = 'L0')};
        insert accList;
        Business_Plan__c bpRec = [select Id, Starting_Fiscal_Year__c from Business_Plan__c where Id = :instBPId];
        Integer year = bpRec.Starting_Fiscal_Year__c.year() - 1;
        Event newEvent = new Event(Subject = 'Test Event', StartDateTime = DateTime.newInstance(year, 11, 20, 10, 0, 0), EndDateTime = DateTime.newInstance(year, 11, 20, 11, 0, 0), WhatId = accList[0].Id);
        insert newEvent;
        List<Investment_Strategy__c> strategies = new List<Investment_Strategy__c>{new Investment_Strategy__c(Name = 'Test1'), new Investment_Strategy__c(Name = 'Test2'),
                                                                                   new Investment_Strategy__c(Name = 'Test3'), new Investment_Strategy__c(Name = 'Test4')};
        insert strategies;
        List<Business_Plan_Client_Relation__c> bpcr = new List<Business_Plan_Client_Relation__c>{
                                                        new Business_Plan_Client_Relation__c(Client__c = accList[0].Id, Business_Plan__c = instBPId),
                                                        new Business_Plan_Client_Relation__c(Client__c = accList[1].Id, Business_Plan__c = instBPId, Expected_Meetings__c = 10)};
        insert bpcr;
        List<Business_Plan_Product_Relation__c> bppr = new List<Business_Plan_Product_Relation__c>{
                                                            new Business_Plan_Product_Relation__c(Business_Plan_Client__c= bpcr[0].Id, Investment_Strategy__c = strategies[0].Id, Business_Plan__c = instBPId),
                                                            new Business_Plan_Product_Relation__c(Business_Plan_Client__c= bpcr[0].Id, Investment_Strategy__c = strategies[1].Id, Business_Plan__c = instBPId)};
        insert bppr;
        BP_InstitutionalFlowCtrl.getStrategyNames(new List<Id>{strategies[0].Id, strategies[1].Id});
        List<BP_InstitutionalFlowCtrl.BPRecWrapper> testList = BP_InstitutionalFlowCtrl.getClientRecs(instBPId);
        testList[0].selectedIds = new List<Id>{strategies[1].Id, strategies[2].Id};
        testList[1].selectedIds = new List<Id>{strategies[0].Id, strategies[3].Id};
        BP_InstitutionalFlowCtrl.updateBPC(JSON.serialize(testList), instBPId, new List<String>{'Marketing', 'Leadership'}, new List<Decimal>{10, 20}, new List<String>{'test note', 'test'});
        System.assertEquals(2, testList.size());
        System.assertEquals(4, allVals.size());
    }
}