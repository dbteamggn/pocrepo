/*----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            DepartmentClientRelationship_Utility
   Description:     Methods called by Department_Client_Relationship__c triggers
                    
   Date             Author                             Summary of Changes                  
   -----------      -----------------                  -------------------------------------------------------------------------------------
    17 Sep 2016      Hemangini                    Initial Release
------------------------------------------------------------------------------------------------------------------------------------------------------------*/
public class DepartmentClientRelationship_Utility {

    /*
     * Description : Create new instance / return existing instance
     * Param : 
     * Returns :  
    */
    private static DepartmentClientRelationship_Utility instance = null; 
    
    public static DepartmentClientRelationship_Utility getInstance() {
        if (instance == null) {
            instance = new DepartmentClientRelationship_Utility();
        }
        return instance;
    }
    
    
    public static void deleteDeptClientRelationship(TriggerHandler.TriggerParameter tParam){
        if(!RecurssiveTriggerController.deptClientRelationRetrigger){
            List<Department_Client_Relationship__c> oldList = (List<Department_Client_Relationship__c>) tParam.oldList;
            Set<Id> accIds = new Set<Id>();
            Set<Id> depIds = new Set<Id>();
            List<Department_Client_Relationship__c> toDeleteList = new List<Department_Client_Relationship__c>();
            
            for(Department_Client_Relationship__c each: oldList){
                depIds.add(each.Department__c);
                accIds.add(each.Client__c);
            }

            List<Account> childAccs = [select Id, ParentId, Parent.ParentId, Parent.Parent.ParentId from Account where ParentId IN :accIds OR 
                                            Parent.ParentId IN :accIds OR Parent.Parent.ParentId IN :accIds];
            
            for(Department_Client_Relationship__c each: [select Id, Client__c, Client__r.ParentId, Client__r.Parent.ParentId, Client__r.Parent.Parent.ParentId, 
                        Department__c from Department_Client_Relationship__c where
                        Client__c IN :childAccs AND Department__c IN :depIds AND Is_Inherited_from_Hierarchy__c = TRUE]){
                toDeleteList.add(each);
            }
            RecurssiveTriggerController.deptClientRelationRetrigger = TRUE;
            List<Database.DeleteResult> drRecs = Database.delete(toDeleteList, false);
            for(Database.DeleteResult each: drRecs){
                if(!each.isSuccess()){
                    system.debug('****An error occurred: ' + each.getErrors());
                }
            }
        }
    }
    
    /*
     * Description  : method to replicate Department_Client_Relationship__c records in the client hierarchy.
     * Param        : TriggerParameter
     * Returns      : none
    */
    public static void replicateDeptClientRelationship(TriggerHandler.TriggerParameter tParam){
        List<Department_Client_Relationship__c> newList = (List<Department_Client_Relationship__c>) tParam.newList;
        Map<id, List<Department_Client_Relationship__c>> clientIdDeptListMap = new Map<id, List<Department_Client_Relationship__c>>();
        Department_Client_Relationship__c newDeptClientRel;
        List<Department_Client_Relationship__c> deptClientRelToInsertList = new List<Department_Client_Relationship__c>();
        Map<string, Department_Client_Relationship__c> uniqueDeptClientRelMap = new Map<string, Department_Client_Relationship__c>();
        Map<string, Department_Client_Relationship__c> existingDeptClientRelMap = new Map<string, Department_Client_Relationship__c>();
        List<Department_Client_Relationship__c> nonDuplicateNewList = new List<Department_Client_Relationship__c>();
        Set<id> clientIdSet = new Set<id>();
        Set<id> deptIdSet = new Set<id>();
        set<string> isAlreadyAdded = new set<string>();
        Id clientId;
        string duplicateError = String.isNotBlank(System.label.Duplicate_Record_Exists) ? System.label.Duplicate_Record_Exists : 'Duplicate record already exists.';
        
        try{
            //recursive check
            if(!RecurssiveTriggerController.deptClientRelationRetrigger){
                
                //get new records against identifier - unique combination of client id and departmnet  id
                for(Department_Client_Relationship__c each : newlist){
                    clientIdSet.add(each.client__c);
                    deptIdSet.add(each.department__c);
                    uniqueDeptClientRelMap.put(each.client__c +'*'+ each.department__c , each);
                }
                
                //get existing records against unique identifier
                for(Department_Client_Relationship__c existingDeptClientRel : [select id, client__c, department__c from Department_Client_Relationship__c where client__c in: clientIdSet AND department__c in: deptIdSet]){
                    existingDeptClientRelMap.put(existingDeptClientRel.client__c +'*'+ existingDeptClientRel.department__c , existingDeptClientRel);
                }
                
                //add error if duplicate record already exists, else proceed further
                for(string uniqueIdentifier : uniqueDeptClientRelMap.keySet()){
                    if(!existingDeptClientRelMap.containsKey(uniqueIdentifier)){                        
                        nonDuplicateNewList.add(uniqueDeptClientRelMap.get(uniqueIdentifier));
                    }
                    else{
                        uniqueDeptClientRelMap.get(uniqueIdentifier).addError(duplicateError);
                    }
                }
                
                for(Department_Client_Relationship__c deptClientRel : nonDuplicateNewList){
                    if(!clientIdDeptListMap.isEmpty() && clientIdDeptListMap.containsKey(deptClientRel.Client__c)){
                        clientIdDeptListMap.get(deptClientRel.Client__c).add(deptClientRel);
                    }
                    else{
                        clientIdDeptListMap.put(deptClientRel.Client__c, new List<Department_Client_Relationship__c>{deptClientRel});
                    }
                }             
                
                //Get child clients and create new Department_Client_Relationship__c records to be inserted for child records in hierarchy
                for(Account childClient : [select id, parentId, parent.parentId, parent.parent.parentId, (select Department__c from
                                Department_Client_Relationships__r) from Account where parentId in :clientIdDeptListMap.keyset() OR parent.parentId in :clientIdDeptListMap.keyset() OR parent.parent.parentId in :clientIdDeptListMap.keyset()]){
                    
                    clientId = null;
                    
                    if(!childClient.Department_Client_Relationships__r.isEmpty()){
                        for(Department_Client_Relationship__c each: childClient.Department_Client_Relationships__r){
                            isAlreadyAdded.add(childClient.Id +'*'+ each.department__c);
                        }
                    }
                    if (childClient.parentId != null && clientIdDeptListMap.containskey(childClient.parentId)){
                        clientId = childClient.parentId;
                    }                   
                    else if (childClient.parent.parentId != null && clientIdDeptListMap.containskey(childClient.parent.parentId))
                    {
                        clientId = childClient.parent.parentId;
                    }                   
                    else if (childClient.parent.parent.parentId != null && clientIdDeptListMap.containskey(childClient.parent.parent.parentId))
                    {
                        clientId = childClient.parent.parent.parentId;
                    }
                    
                    if(clientId != null){
                        for(Department_Client_Relationship__c depClientRel : clientIdDeptListMap.get(clientId)){
                            if(!isAlreadyAdded.contains(childClient.id + '*' + depClientRel.department__c)){
                                newDeptClientRel = new Department_Client_Relationship__c();
                                
                                newDeptClientRel.Client__c = childClient.id;
                                newDeptClientRel.Department__c = depClientRel.Department__c;
                                newDeptClientRel.Is_Reminder_Task_Created__c = depClientRel.Is_Reminder_Task_Created__c;
                                newDeptClientRel.Is_Inherited_from_Hierarchy__c = true;
                                newDeptClientRel.Created_via_API__c = true;
                                
                                deptClientRelToInsertList.add(newDeptClientRel);
                                isAlreadyAdded.add(childClient.id + '*' + depClientRel.department__c);
                            }
                        }
                    }
                }
                
                if(!deptClientRelToInsertList.isEmpty()){
                    //Set the value to true so that the trigger does not fire again when inserting for child records.
                    RecurssiveTriggerController.deptClientRelationRetrigger  = true;
                    insert deptClientRelToInsertList;
                }              
                
            }
        }
        catch(Exception e){
            CommonUtilities.createExceptionLog(e);
        }
    }   
    
}