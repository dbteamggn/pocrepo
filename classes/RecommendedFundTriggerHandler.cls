public class RecommendedFundTriggerHandler extends TriggerHandler {
    protected override void afterInsert(){
        if((triggerParams.objByPassMap.containsKey('RecommendedFund_Utility.linkRelatedFundsWithClients') && !triggerParams.objByPassMap.get(' RecommendedFund_Utility.linkRelatedFundsWithClients')) || 
           !triggerParams.objByPassMap.containsKey(' RecommendedFund_Utility.linkRelatedFundsWithClients')){
               RecommendedFund_Utility.linkRelatedFundsWithClients(triggerParams);        
           }
    }
    protected override void afterDelete(){
                if((triggerParams.objByPassMap.containsKey(' CampaignTeam_Utility.postCampaignUpdates') && !triggerParams.objByPassMap.get(' CampaignTeam_Utility.postCampaignUpdates')) || 
                    !triggerParams.objByPassMap.containsKey(' CampaignTeam_Utility.postCampaignUpdates')){
              RecommendedFund_Utility.deleteRecommendedFunds(triggerParams);        
                        }
    }
}