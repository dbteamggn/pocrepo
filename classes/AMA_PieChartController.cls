public class AMA_PieChartController {
	@auraEnabled
    public static DataWrapper prepareChartData(){
        Decimal totalAUM = 0.0;
        map<String,decimal> investmentStrategyAUM = new map<String,decimal>();
        list<PieDataWrapper> piedataWrapperList = new list<PieDataWrapper>();
        for(AUM_by_Strategy__c tempVar : [select Id,Total_Inst_AUM__c,Investment_Strategy__r.Name from AUM_by_Strategy__c]){
            totalAUM += tempVar.Total_Inst_AUM__c;
            if(!investmentStrategyAUM.containsKey(tempVar.Investment_Strategy__r.Name))
                investmentStrategyAUM.put(tempVar.Investment_Strategy__r.Name,tempVar.Total_Inst_AUM__c);
           	else
                investmentStrategyAUM.put(tempVar.Investment_Strategy__r.Name,investmentStrategyAUM.get(tempVar.Investment_Strategy__r.Name)+tempVar.Total_Inst_AUM__c);
        }
        for(String strategyName : investmentStrategyAUM.keySet()){
            decimal stratPer = (investmentStrategyAUM.get(strategyName)/totalAUM)*100;
            PieDataWrapper temp = new PieDataWrapper();
            temp.name = strategyName;
            temp.y = stratPer;
            piedataWrapperList.add(temp);
        }
        DataWrapper dw = new DataWrapper();
        SeriesWrapper sw = new SeriesWrapper();
        PieWrapper pw = new PieWrapper();
        pw.chartTitle = 'Strategies';
        dw.pieChartDetails = pw;
        sw.name = 'Strategy';
        sw.data = new list<PieDataWrapper>();
        sw.data.addAll(piedataWrapperList);
        dw.series = new list<SeriesWrapper>();
        dw.series.add(sw);
        return dw;
    }
    
    public class DataWrapper{
        @AuraEnabled
        public list<SeriesWrapper> series{get;set;}
        @AuraEnabled
        public PieWrapper pieChartDetails{get;set;}
    }
    
    public Class PieWrapper{
        @AuraEnabled
        public string chartTitle{get;set;}
    }
    
    public class PieDataWrapper{
        @AuraEnabled
        public string name{get;set;}
        @AuraEnabled
        public Decimal y{get;set;}
        
    }
    
    public class SeriesWrapper{
        @AuraEnabled
        public string name{get;set;}
        @AuraEnabled
        public list<PieDataWrapper> data{get;set;}
        public SeriesWrapper(){
            
        }
    }
}