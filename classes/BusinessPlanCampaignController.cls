public class BusinessPlanCampaignController {
    
    @AuraEnabled
    public static List<RelatedCampaigns> getCampaign(List<ID> campaignIds){
        System.debug('Campaing id s---> '+ campaignIds);           
        List<Campaign> campList = new List<Campaign>();
        RelatedCampaigns relatedCampaign;
        List<RelatedCampaigns> relatedCampaignList = new List<RelatedCampaigns>();
        try{
            campList = [select id, name,StartDate, Type, Objective__c from Campaign where id IN :campaignIds ];
            if(campList.size() > 0){
                for( Campaign camp : campList){
                    relatedCampaign = new RelatedCampaigns();           
                    relatedCampaign.campaignId = camp.Id;
                    relatedCampaign.campaignName = camp.name;
                    relatedCampaign.campaignStartDate = camp.StartDate;
                    relatedCampaign.campaignObjective = camp.Objective__c;
                    relatedCampaign.campaignType = camp.Type;
                    relatedCampaign.value = true;
                    relatedCampaignList.add(relatedCampaign);
                }
            }
        }catch(Exception e){
            CommonUtilities.createExceptionLog(e);
        }
        return relatedCampaignList;
    }
    
    @AuraEnabled
    public static List<RelatedCampaigns> getCampaigns(String businessPlanId){
        
        List<Campaign> campList = new List<Campaign>();
        RelatedCampaigns relatedCampaign;
        List<RelatedCampaigns> relatedCampaignList = new List<RelatedCampaigns>();
        List<Business_Plan_Campaigns__c> bpCampaignList = new List<Business_Plan_Campaigns__c>();
        Set<Id> campaignId = new Set<Id>();
        try{
            bpCampaignList = [select id, Business_Plan__c, Campaign__c from Business_Plan_Campaigns__c where Business_Plan__c =:businessPlanId ];            
            if(bpCampaignList.size() > 0){
                for(Business_Plan_Campaigns__c bpc : bpCampaignList){
                    campaignId.add(bpc.Campaign__c);
                }
                if(campaignId.size() > 0){
                    campList = [select id, name,StartDate, Type, Objective__c from Campaign where id IN :campaignId ];
                    if(campList.size() > 0){
                        for( Campaign camp : campList){
                            relatedCampaign = new RelatedCampaigns();           
                            relatedCampaign.campaignId = camp.Id;
                            relatedCampaign.campaignName = camp.name;
                            relatedCampaign.campaignStartDate = camp.StartDate;
                            relatedCampaign.campaignObjective = camp.Objective__c;
                            relatedCampaign.campaignType = camp.Type;
                            relatedCampaign.value = true;
                            relatedCampaignList.add(relatedCampaign);
                        }
                    }
                }
            }
            
        }catch(Exception e){
            CommonUtilities.createExceptionLog(e);
        }
        return relatedCampaignList;
    }
    
    @AuraEnabled
    public static String getCampaignText(Id businessPlanId){
        String businessPlanText = '';
        for(Business_Plan__c each: [select Id, Campaign_Details__c from Business_Plan__c where Id = :businessPlanId]){
            businessPlanText = each.Campaign_Details__c;
        }
        return businessPlanText;
    }
    
    @AuraEnabled
    public static Boolean createBusinessPlanCampaigns(String businessPlan, List<ID> campaigns, List<ID> delCampaigns, String campaignText){
        Id businessPlanId = (ID) businessPlan;
        Set<ID> campaignIds = new Set<ID>();
        Set<ID> delCampaignIds = new Set<ID>();
        Set<ID> existingcampaignIds = new Set<ID>();
        Boolean isCreated = false;
        
        Business_Plan__c bpRec = new Business_Plan__c(Id = businessPlanId, Campaign_Details__c = campaignText);
        update bpRec;

        if(!campaigns.isEmpty()){
            campaignIds.addAll(campaigns);
            delCampaignIds.addAll(delCampaigns);
            Business_Plan_Campaigns__c bpCampaign ;
            SavePoint sp ;
            List<Business_Plan_Campaigns__c> bpCampaignList = new List<Business_Plan_Campaigns__c>();
            List<Business_Plan_Campaigns__c> newBPCampaignList = new List<Business_Plan_Campaigns__c>();
            List<Business_Plan_Campaigns__c> delBPCampaignList = new List<Business_Plan_Campaigns__c>();
            
            sp= Database.setSavepoint();
            try{
                bpCampaignList = [select id, Business_Plan__c, Campaign__c from Business_Plan_Campaigns__c where Business_Plan__c =:businessPlanId ];            
                if(bpCampaignList.size() > 0){
                    for(Business_Plan_Campaigns__c bpo : bpCampaignList){
                        existingcampaignIds.add(bpo.Campaign__c);
                        isCreated = true;
                    }
                }
                for(ID campId : campaignIds){
                    
                    if(!existingCampaignIds.contains(campId)){                    
                        bpCampaign = new Business_Plan_Campaigns__c();
                        bpCampaign.Business_Plan__c = businessPlanId;
                        bpCampaign.Campaign__c = campId;
                        newBPCampaignList.add(bpCampaign);
                    }
                    
                }
                for(Business_Plan_Campaigns__c bp : bpCampaignList){               
                    if(delCampaignIds.contains(bp.Campaign__c)){                    
                        delBPCampaignList.add(bp);   
                    }
                    
                }
                if(newBPCampaignList.size()> 0){
                    Database.SaveResult[] result = Database.insert(newBPCampaignList, true);
                    for(Database.SaveResult res: result){
                        if(res.isSuccess()){
                            isCreated = true;
                        }else{
                            isCreated = false;
                        }
                    }
                }
                if(delBPCampaignList.size() > 0){
                    Database.DeleteResult[] res = Database.delete(delBPCampaignList, true);
                    for(Database.DeleteResult result: res){
                        if(result.isSuccess()){
                            isCreated = true;
                        }else{
                            isCreated=false;
                        }
                    }
                }
            }catch(Exception e){
                isCreated = false;
                Database.rollback(sp);
                System.debug('Exception details ----> '+e);
                CommonUtilities.createExceptionLog(e);
            }
        }else{
            isCreated = true;
        }
        return isCreated;
    }
    
    public class RelatedCampaigns{
        
        @AuraEnabled
        public String campaignId;
        @AuraEnabled
        public String campaignName;
        @AuraEnabled
        public Date campaignStartDate;
        @AuraEnabled
        public String campaignType;
        @AuraEnabled
        public String campaignObjective;
        @AuraEnabled
        public boolean value;
    }
}