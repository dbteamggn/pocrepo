public class AddContactsToEMDebtCampaignCtrl {
    
    @AuraEnabled
    public static string assignToEMDebtCampaign(String eventId){
        System.debug('Event Id : ' + eventId );
        
        if(eventId != null && eventId.length() > 0){
            //get list of events
            list<EventRelation> eventRelations = [select Id, RelationId, EventId from EventRelation where EventId = :eventId and IsParent = true and IsWhat = false limit 10000];
            
            if(eventRelations != null && eventRelations.size() > 0){
                list<CampaignMember> newCampaignMemberList = new list<CampaignMember>();
                
                list<CampaignMember> existCampMember = [select Id, ContactId from CampaignMember where CampaignId = '7010v0000001Y56AAE' limit 10000];
                set<Id> existContactIds = new set<Id>();
                
                if(existCampMember != null && existCampMember.size() > 0){
                    for(CampaignMember cmmeber : existCampMember){
                    	existContactIds.add(cmmeber.ContactId);    
                    }
                }
                
                for(EventRelation evntRel : eventRelations){
                    
                    if(existContactIds != null && existContactIds.size() > 0 && !existContactIds.contains(evntRel.RelationId)){
                        CampaignMember cm = new CampaignMember();
                        cm.CampaignId = '7010v0000001Y56AAE';
                        cm.ContactId = evntRel.RelationId;
                        cm.Status='Opt-In';
                        newCampaignMemberList.add(cm);
                    }
                    
                    CampaignMember cm = new CampaignMember();
                    cm.CampaignId = '7010v0000001Y56AAE';
                    cm.ContactId = evntRel.RelationId;
                    cm.Status='Opt-In';
                    if(existContactIds != null && existContactIds.size() > 0){
                        if(!existContactIds.contains(evntRel.RelationId)){
                            newCampaignMemberList.add(cm);
                        }
                    } else {
                        newCampaignMemberList.add(cm);
                    }
                }
                
                upsert newCampaignMemberList;
            }
        }
        
        return null;
    }

}