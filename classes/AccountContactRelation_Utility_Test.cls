/********************************************************************************
 * Name        : AccountContactRelation_Utility_Test
 * Release     : R2
 * Phase       : P1
 * Description : Used for AccountContactRelation_Utility Test Class.
 * Author      : Deloitte
 * Reviewed By : 
 *********************************************************************************/
 @isTest
public class AccountContactRelation_Utility_Test {
    /*
    * Description : Creates a client, a contact which has parent as client
    * Param : 
    * Returns : 
    */
    public static testmethod void copyKeyContact(){
        Account Acc = CommonTestUtils.CreateTestClient('Test Account');
        Acc.Hierarchy_Level__c = 'L0';
        insert Acc;
        test.startTest();
        Contact con = CommonTestUtils.CreateTestContact('Test Contact');
        con.AccountId=Acc.Id;
        insert con;
        con.Lastname='test contact 1';
        update con;
        AccountContactRelation acr = [select Id from AccountContactRelation where ContactId = :con.Id];
        update acr;
        test.stopTest();
    }
    
    /*
    * Description : Creates test data for updating Account Contact Relation record from direct to indirect, so that isActive flag is updated to false
    * Param : 
    * Returns : 
    */
    public static testmethod void updateActiveFlagTest(){
        
        List<Account> accountList = new List<Account>();
        Account acc1 = CommonTestUtils.CreateTestClient('Test Account1');
        acc1.Hierarchy_Level__c = 'L0';
        accountList.add(acc1);
        
        Account acc2 = CommonTestUtils.CreateTestClient('Test Account2');
        acc2.Hierarchy_Level__c = 'L0';
        accountList.add(acc2);
        insert accountList;
        
        test.startTest();
        Contact con = CommonTestUtils.CreateTestContact('Test Contact');
        con.AccountId=accountList[0].Id;
        insert con;
        
        con.AccountId=accountList[1].Id;
        update con;
        test.stopTest();
    }
}