public with sharing class ContactListController {
	
    public static List<contactResult> findAll(string queryParam,string recordId) {
        
        System.debug('queryParam'+queryParam);
        System.debug('recordId'+recordId);
        List<contactResult> contactResultList = new List<contactResult>();
        Id AccId;
        Set<Id> conRelationIds = new Set<Id>();
        if(recordId!=null && recordId != '' && String.isBlank(queryParam)){
            if(Id.valueOf(recordId).getSObjectType() == Schema.Contact.SObjectType){
                List<Contact> con = [Select Account.Id from Contact where Id = :recordId];
                if(con.size()>0){
                    AccId = con[0].Account.Id;
                }
            }else if(Id.valueOf(recordId).getSObjectType() == Schema.Account.SObjectType){
                AccId=recordId;
            }else if(Id.valueOf(recordId).getSObjectType() == Schema.Event.SObjectType){
                AccId = [select Id, AccountId from Contact where Id = :[select id, WhoId from Event where id=:recordId].WhoId].AccountId;
                List<EventWhoRelation> ERList = [Select RelationId from EventWhoRelation where EventId = :recordId];
                for(EventWhoRelation ER: ERList){
                    conRelationIds.add(ER.RelationId);
                }
            }
            else if(Id.valueOf(recordId).getSObjectType() == Schema.Task.SObjectType){
                AccId = [select Id, AccountId from Contact where Id = :[select id, WhoId from Task where id=:recordId].WhoId].AccountId;
                List<TaskWhoRelation> TRList = [Select RelationId from TaskWhoRelation where TaskId = :recordId];
                for(TaskWhoRelation TR: TRList){
                    conRelationIds.add(TR.RelationId);
                }
            }
            //This section is to add contacts from Events or Tasks
            if(!conRelationIds.isEmpty()){
                List<contact> contactList= [SELECT id, name, phone,Account.Name FROM Contact 
                                      WHERE Id in :conRelationIds];
                for(contact temp : contactList){
                    contactResult tempCon = new contactResult();
                    tempCon.contactId = temp.id;
                    tempCon.contactName = temp.name;
                    tempCon.accountName = temp.Account.Name;
                    tempCon.value = true;
                    contactResultList.add(tempCon);           
                }
            }
            List<Client_Third_Party_Relationship__c> thirdPartyAccount=[select Third_Party_Name__c from Client_Third_Party_Relationship__c where Client_Name__c=: AccId];
            Set<Id> AccountIds=new Set<id>();
            for(Client_Third_Party_Relationship__c temp : thirdPartyAccount){
                AccountIds.add(temp.Third_Party_Name__c);
            }
            AccountIds.add(AccId);
            List<contact> contactList= [SELECT id, name, phone,Account.Name FROM Contact 
                                      WHERE (Account.name='Deloitte' OR Accountid in: AccountIds) And LastViewedDate != NULL 
                                      ORDER BY LastViewedDate DESC 
                                      LIMIT 5];
            for(contact temp : contactList){
                contactResult tempCon = new contactResult();
                tempCon.contactId = temp.id;
                tempCon.contactName = temp.name;
                tempCon.accountName = temp.Account.Name;
                tempCon.value = false;
                contactResultList.add(tempCon);           
            }
        } else if(!String.isBlank(queryParam)){
            Set<Id> contactIds = new Set<Id>();
            for(RecentlyViewed recentItem : [select Id, Name FROM RecentlyViewed WHERE Type = 'Contact' ORDER BY LastViewedDate DESC LIMIT 100]){
                contactIds.add(recentItem.Id);
            }
            for(Contact each: [select Id, Name, Account.Name from Contact where Id IN :contactIds]){
                contactResult tempCon = new contactResult();
                tempCon.contactId = each.id;
                tempCon.contactName = each.name;
                tempCon.accountName = each.Account.Name;
                tempCon.value = false;
                contactResultList.add(tempCon);
            }
        }
        
        return contactResultList;
    }

    @AuraEnabled
    public static List<contactResult> findByName(String searchKey,string queryParam,string recordId) {
        System.debug('queryParam'+queryParam);
        System.debug('recordId'+recordId);
        List<contactResult> contactResultList = new List<contactResult>();
        id AccId;
        String name = '%' + searchKey + '%';
        if(recordId!=null && recordId != '' && String.isBlank(queryParam)){
            if(Id.valueOf(recordId).getSObjectType() == Schema.Contact.SObjectType){
                List<Contact> con = [Select Account.Id from Contact where Id = :recordId];
                if(con.size()>0){
                    AccId = con[0].Account.Id;
                }
            }else if(Id.valueOf(recordId).getSObjectType() == Schema.Account.SObjectType){
                AccId=recordId;
            }else if(Id.valueOf(recordId).getSObjectType() == Schema.Event.SObjectType){
                AccId = [select Id, AccountId from Contact where Id = :[select id, WhoId from Event where id=:recordId].WhoId].AccountId;
            }
            else if(Id.valueOf(recordId).getSObjectType() == Schema.Task.SObjectType){
                AccId = [select Id, AccountId from Contact where Id = :[select id, WhoId from Task where id=:recordId].WhoId].AccountId;
            }
            List<Client_Third_Party_Relationship__c> thirdPartyAccount=[select Third_Party_Name__c from Client_Third_Party_Relationship__c where Client_Name__c=: AccId];
            Set<Id> AccountIds=new Set<id>();
            for(Client_Third_Party_Relationship__c temp : thirdPartyAccount){
                AccountIds.add(temp.Third_Party_Name__c);
            }
            AccountIds.add(AccId);
            List<contact> contactList=  [SELECT id, name, phone,Account.Name 
                                                FROM Contact 
                                                WHERE (name LIKE :name OR Account.Name LIKE :name) AND
                                                (Account.name='Deloitte' OR Accountid in: AccountIds)
                                                ORDER BY name LIMIT 10];
            System.debug('contactList'+contactList); 
            for(contact temp : contactList){
                contactResult tempCon = new contactResult();
                tempCon.contactId = temp.id;
                tempCon.contactName = temp.name;
                tempCon.accountName = temp.Account.Name;
                tempCon.value = false;
                contactResultList.add(tempCon);           
            }
        } else if(!String.isBlank(queryParam)){
            system.debug('****here');
            for(Contact each: [select Id, Name, Account.Name from Contact where Name LIKE :name OR Account.Name LIKE :name LIMIT 100]){
                contactResult tempCon = new contactResult();
                tempCon.contactId = each.id;
                tempCon.contactName = each.name;
                tempCon.accountName = each.Account.Name;
                tempCon.value = false;
                contactResultList.add(tempCon);
            }
        }
        
        System.debug('contactResultList'+contactResultList);
        return contactResultList;
    }
    
    @AuraEnabled
    public static List<contactResult> findByListId(List<id> contactIds,string queryParam,string recordId) {
        System.debug('queryParam'+queryParam);
        List<contactResult> contactResultList = new List<contactResult>();
        if(recordId!=null){
            List<contact> contactList=  [SELECT id, name, phone,Account.Name 
                                                    FROM Contact 
                                                    WHERE Id in :contactIds ORDER BY name];
            for(contact temp : contactList){
                contactResult tempCon = new contactResult();
                tempCon.contactId = temp.id;
                tempCon.contactName = temp.name;
                tempCon.accountName = temp.Account.Name;
                tempCon.value = true;
                contactResultList.add(tempCon);           
            }
        }
        return contactResultList;
    }
        
    public class contactResult {
        @AuraEnabled
        public id contactId;
        
        @AuraEnabled
        public string contactName;

        @AuraEnabled
        public string accountName;
        
        @AuraEnabled
        public boolean value;
    }

}