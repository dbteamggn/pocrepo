public without sharing class ContactTriggerHandler {

    // Instance of class
    private static ContactTriggerHandler handler = null;

    // Boolean to control recursion
    public boolean isExecuting = false;

    /*
     * 
     * Description : Constructor
     * Param : 
     * Returns :  
    */ 
    public ContactTriggerHandler () {

    }

    /*
     * 
     * Description : Create new instance / return existing instance
     * Param : 
     * Returns :  
    */
    public static ContactTriggerHandler getInstance() {

        if (handler == null) {

            handler = new ContactTriggerHandler();
        }

        return handler;
    }


    
    /*
     * 
     * Description : Before Bulk - build collections etc. prior to running other Before logic.
     * Param : 
     * Returns :  
    */
    public void onBeforeBulk (List<Contact> newList) {
        
        // Only perform logic if this is the first time the trigger has been called in this context
        if (!isExecuting) {

            // Set isExecuting to true to prevent recursion
            isExecuting = true;

            // Perform trigger logic here

            // Now set isExecuting back to false as all logic has been performed
            isExecuting = false;
        }
    }

 
    /*
     * 
     * Description : Before Insert
     * Param : 
     * Returns :  
    */
    public void onBeforeInsert (List<Contact> newList) {
        
        // Only perform logic if this is the first time the trigger has been called in this context
        if (!isExecuting) {

            // Set isExecuting to true to prevent recursion
            isExecuting = true;

            // Now set isExecuting back to false as all logic has been performed
            isExecuting = false;
        }
    }

    
    /*
     * 
     * Description : Before Update
     * Param : 
     * Returns :  
    */
    public void onBeforeUpdate (List<Contact> oldList, List<Contact> newList, Map<Id, Contact> oldMap, Map<Id, Contact> newMap) {
        
        // Only perform logic if this is the first time the trigger has been called in this context
        if (!isExecuting) {

            // Set isExecuting to true to prevent recursion
            isExecuting = true;

            // Perform trigger logic here
            //Joshna: 10/01/2017 - commented out assign owner logic as it shouldn't run in update scenarios. (DE8467)
            //ContactTriggerHandler.assignOwner(newList, 'BeforeUpdate');
            // Now set isExecuting back to false as all logic has been performed
            isExecuting = false;
        }
    }

    
    /*
     * 
     * Description : Before Delete
     * Param : 
     * Returns :  
    */
    public void onBeforeDelete (List<Contact> oldList, Map<Id, Contact> oldMap) {
        
        // Only perform logic if this is the first time the trigger has been called in this context
        if (!isExecuting) {

            // Set isExecuting to true to prevent recursion
            isExecuting = true;

            // Perform trigger logic here

            // Now set isExecuting back to false as all logic has been performed
            isExecuting = false;
        }
    }

     
    
    /*
     * 
     * Description : After Bulk - build collections etc. prior to running other After logic.
     * Param : 
     * Returns :  
    */
    public void onAfterBulk (List<Contact> newList) {
        
        // Only perform logic if this is the first time the trigger has been called in this context
        if (!isExecuting) {

            // Set isExecuting to true to prevent recursion
            isExecuting = true;

            // Perform trigger logic here

            // Now set isExecuting back to false as all logic has been performed
            isExecuting = false;
        }
    }

     
    /*
     * 
     * Description : After Insert
     * Param : 
     * Returns :  
    */
    public void onAfterInsert (List<Contact> newList, Map<Id, Contact> newMap) {
        
        // Only perform logic if this is the first time the trigger has been called in this context
        if (!isExecuting) {

            // Set isExecuting to true to prevent recursion
            isExecuting = true;

            // Perform trigger logic here
            

            // Now set isExecuting back to false as all logic has been performed
            isExecuting = false;
        }
    }

     
    /*
     * 
     * Description : After Update
     * Param : 
     * Returns :  
    */
    public void onAfterUpdate (List<Contact> oldList, List<Contact> newList, Map<Id, Contact> oldMap, Map<Id, Contact> newMap) {
        
        // Only perform logic if this is the first time the trigger has been called in this context
        if (!isExecuting) {

            // Set isExecuting to true to prevent recursion
            isExecuting = true;

            // Perform trigger logic here
            //US28000 - Update the Previous Employee and Previous Employer fields on the Account Contact Relationship object.
            updatePreviousEmployeeAndEmployer(oldMap, newList);
            
            // Now set isExecuting back to false as all logic has been performed
            isExecuting = false;
        }       
        
    }

    /*
     * 
     * Description : After Delete
     * Param : 
     * Returns :  
    */
    public void onAfterDelete (List<Contact> oldList, Map<Id, Contact> oldMap) {
        
        // Only perform logic if this is the first time the trigger has been called in this context
        if (!isExecuting) {

            // Set isExecuting to true to prevent recursion
            isExecuting = true;

            // Perform trigger logic here
            
            // Now set isExecuting back to false as all logic has been performed
            isExecuting = false;
        }
    }

    /*
     * 
     * Description : After Undelete
     * Param : 
     * Returns :  
    */
    public void onAfterUndelete (List<Contact> newList) {  
        
        // Only perform logic if this is the first time the trigger has been called in this context
        if (!isExecuting) {

            // Set isExecuting to true to prevent recursion
            isExecuting = true;

            // Perform trigger logic here

            // Now set isExecuting back to false as all logic has been performed
            isExecuting = false;
        }
    }
    
    /* 
     * Description  : method to update the Previous Employee and Previous Employer fields on the Account Contact Relationship object when a contact moves companies
     * Param        : trigger.oldMap, trigger.new
     * Returns      : none 
    */
    public static void updatePreviousEmployeeAndEmployer(Map<Id, Contact> oldMap, List<Contact> newList){
        
        Map<id, id> conIdOldAccIdMap = new Map<id, id>();
        Map<id, id> conIdNewAccIdMap = new Map<id, id>();
        List<AccountContactRelation> acrToUpdateList = new List<AccountContactRelation>();
        
        //if the accountid has changed in update, get old and new AccountIds
        for(contact con : newList){
            if(con.AccountId != null && oldMap.get(con.id).AccountId !=null && oldMap.get(con.id).AccountId != con.AccountId){
                conIdOldAccIdMap.put(con.id, oldMap.get(con.id).AccountId);
                conIdNewAccIdMap.put(con.id, con.AccountId);
            }
        }
        
        if(!conIdOldAccIdMap.isEmpty()){
            //get the relevant record (with contact Id and Old Account Id) from AccountContactRelation and set previous flags to true. Get records with contact Id and New Account Id to check if Contact is moving back to a previous firm - uncheck previous flags in this case.
            for( AccountContactRelation acr : [select Id, AccountId, ContactId, Previous_Employee__c, Previous_Employer__c from AccountContactRelation where ContactId IN: conIdOldAccIdMap.keySet() AND (AccountId IN: conIdOldAccIdMap.values() OR AccountId IN: conIdNewAccIdMap.values())]){
                if(conIdOldAccIdMap.containsKey(acr.ContactId) && acr.AccountId == conIdOldAccIdMap.get(acr.ContactId)){
                    acr.Previous_Employee__c = true;
                    acr.Previous_Employer__c = true;
                    acrToUpdateList.add(acr);
                }
                if((acr.Previous_Employee__c || acr.Previous_Employer__c) && conIdNewAccIdMap.containsKey(acr.ContactId) && acr.AccountId == conIdNewAccIdMap.get(acr.ContactId)){
                    acr.Previous_Employee__c = false;
                    acr.Previous_Employer__c = false;
                    acrToUpdateList.add(acr);
                }
            }
        }
        
        if(!acrToUpdateList.isEmpty()){
            update acrToUpdateList;
        }
    }  
}