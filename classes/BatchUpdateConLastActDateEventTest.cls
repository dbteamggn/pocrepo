@isTest
public class BatchUpdateConLastActDateEventTest {
    
    /*
     * 
     * Description : Test the Scheduler
     * Param : 
     * Returns :  
    */
    private static testMethod void testScheduler() {
        
        User user = CommonTestUtils.createTestUser();        
        
        System.runAs(user){
            
            //Test the Scheduler Class
            Test.StartTest();
            string jobid = System.schedule('BatchUpdateConLastActDateSchedule Test', '0 0 10 18 * ? *', new BatchUpdateConLastActDateSchedule());           
            Test.StopTest();    
            
            CronTrigger ct = [select id, CronExpression, TimesTriggered, NextFireTime from CronTrigger where id = :jobId];      
            System.assertEquals('0 0 10 18 * ? *', ct.CronExpression); 

        }
    }
    
    /*
     * 
     * Description : Check that a past Event (End date earlier than today but later than the last batch run) 
     *               updates the contact that is associated with it.
     * Param : 
     * Returns :  
    */
    static testMethod void PastEvent(){
        User user = CommonTestUtils.createTestUser();        
        
        System.runAs(user){  
            
            Contact con = CommonTestUtils.CreateTestContact('a');
            insert con;
            
            datetime yesterday = system.now() - 1;
            date yesterdayDate = date.newInstance(yesterday.year(), yesterday.month(), yesterday.day());
            
            Event event = CommonTestUtils.CreateTestEvent('Test Subject', yesterday, yesterday + (1/24), con.id, 'Meeting', 'Completed');
            insert event;        
            
            
                        
            // start the test by triggering the batch job to run that does the update
            Test.startTest();
            Database.executeBatch( new BatchUpdateConLastActDateEvent());
            Test.stopTest();
            
            // Get the contacts after the batch process has completed
            list<Contact> resultContacts = [select id, name, last_activity_date__c, last_activity_type__c from contact limit 49000];
            
            // Should be 1 contact
            system.assertEquals(1, resultContacts.size());
            
            // Check that the last activity date is yesterday & type is 'Meeting'
            system.assertEquals(yesterdayDate, resultContacts[0].last_activity_date__c);
            system.assertEquals(event.type__c, resultContacts[0].last_activity_type__c);
        }
    }

    /*
     * 
     * Description : Check that a future Event (End date later than today) does not update the contact that is associated with it.
     * Param : 
     * Returns :  
    */    
    static testMethod void FutureEvent(){
        User user = CommonTestUtils.createTestUser();        
        
        System.runAs(user){  
            
            Contact con = CommonTestUtils.CreateTestContact('a');
            insert con;
            
            datetime tomorrow = system.now() + 1;
            date tomorrowDate = date.newInstance(tomorrow.year(), tomorrow.month(), tomorrow.day());
            
            Event event = CommonTestUtils.CreateTestEvent('Test Subject', tomorrow, tomorrow + (1/24), con.id, 'Meeting', 'Completed');
            insert event; 
            
            // start the test by triggering the batch job to run that does the update
            Test.startTest();
            Database.executeBatch( new BatchUpdateConLastActDateEvent());
            Test.stopTest();
            
            // Get the contacts after the batch process has completed
            list<Contact> resultContacts = [select id, name, last_activity_date__c, last_activity_type__c from contact limit 49000];
            
            // Should be 1 contact
            system.assertEquals(1, resultContacts.size());
            
            // Check that the last activity date is null & type is null
            system.assertEquals(null, resultContacts[0].last_activity_date__c);
            system.assertEquals(null, resultContacts[0].last_activity_type__c);
        }
    }
    
    

    /*
     * 
     * Description : Check that multiple past & future Events result in the most recent past event to update the Last Activity Date on contact.
     *               Also check that only the relevant contact is updated.
     * Param : 
     * Returns :  
    */ 
    static testMethod void MultipleEvents(){
        User user = CommonTestUtils.createTestUser();        
        
        System.runAs(user){  
            
            list<contact> contacts = new list<contact>();
            Contact con1 = CommonTestUtils.CreateTestContact('a');
            contacts.add(con1);
            Contact con2 = CommonTestUtils.CreateTestContact('b');
            contacts.add(con2);
            insert contacts;
            
            datetime today = system.now();
            datetime yesterday = today - 1;
            date yesterdayDate = date.newInstance(yesterday.year(), yesterday.month(), yesterday.day());
            
            list<Event> events = new list<event>();
            Event event1 = CommonTestUtils.CreateTestEvent('Test Subject', today - 3, today - 3 + (1/24), con1.id, 'Meeting', 'Completed');
            events.add(event1);
            Event event2 = CommonTestUtils.CreateTestEvent('Test Subject', today - 2, today - 2 + (1/24), con1.id, 'Event', 'Open');
            events.add(event2);
            Event event3 = CommonTestUtils.CreateTestEvent('Test Subject', today - 1, today - 1 + (1/24), con1.id, 'Call', 'Completed');
            events.add(event3);
            Event event4 = CommonTestUtils.CreateTestEvent('Test Subject', today, today + (1/24), con1.id, 'Meeting', 'Completed');
            events.add(event4);
            Event event5 = CommonTestUtils.CreateTestEvent('Test Subject', today + 1 , today + 1  + (1/24), con1.id, 'Call', 'Completed');
            events.add(event5);
            Event event6 = CommonTestUtils.CreateTestEvent('Test Subject', today + 2 , today + 2  + (1/24), con1.id, 'Event', 'Completed');
            events.add(event6);
            insert events; 
            
            // start the test by triggering the batch job to run that does the update
            Test.startTest();
            Database.executeBatch( new BatchUpdateConLastActDateEvent());
            Test.stopTest();
            
            // Get the contacts after the batch process has completed
            list<Contact> resultContacts = [select id, name, last_activity_date__c, last_activity_type__c from contact limit 49000];
            
            // Should be 2 contact
            system.assertEquals(2, resultContacts.size());
            
            for (contact resultContact : resultContacts){
                if (resultContact.id == con1.id){
                    // Check that the last activity date is yesterday & type is 'Call'
                    system.assertEquals(yesterdayDate, resultContact.last_activity_date__c);
                    system.assertEquals(event3.type, resultContact.last_activity_type__c);
                } else {
                    // Check that the last activity date & type are both null
                    system.assertEquals(null, resultContact.last_activity_date__c);
                    system.assertEquals(null, resultContact.last_activity_type__c);
                }
            }

        }
    }
    
    

    /*
     * 
     * Description : Check that all contacts referred to in an Event are updated
     * Param : 
     * Returns :  
    */ 
    static testMethod void CheckMultipleContactsOnEvent(){
        User user = CommonTestUtils.createTestUser();        
        
        System.runAs(user){  
            
            list<contact> contacts = new list<contact>();
            Contact con1 = CommonTestUtils.CreateTestContact('a');
            contacts.add(con1);
            Contact con2 = CommonTestUtils.CreateTestContact('b');
            contacts.add(con2);
            Contact con3 = CommonTestUtils.CreateTestContact('c');
            contacts.add(con3);
            insert contacts;
            
            datetime now = system.now();
            date todayDate = date.newInstance(now.year(), now.month(), now.day());            
            
            
            list<event> events = new list<event>();
            Event event1 = CommonTestUtils.CreateTestEvent('Test Subject', now - 3, now - 3 + (1/24), con1.id, 'Meeting', 'Completed');
            events.add(event1);
            insert events;
            
            // Add other contacts to the task
            list<EventRelation> eventRelations = new list<EventRelation>();
            EventRelation tr1 = new EventRelation(relationId = con2.id, eventId = event1.id);
            eventRelations.add(tr1);
            EventRelation tr2 = new EventRelation(relationId = con3.id, eventId = event1.id);
            eventRelations.add(tr2);
            insert eventRelations;
            
            // start the test by triggering the batch job to run that does the update
            Test.startTest();
            Database.executeBatch( new BatchUpdateConLastActDateEvent());
            Test.stopTest();
            
            // Get the contacts after the batch process has completed
            list<Contact> resultContacts = [select id, name, last_activity_date__c, last_activity_type__c from contact limit 49000];
            
            // Should be 3 contacts
            system.assertEquals(3, resultContacts.size());
            
            for (contact resultContact : resultContacts){
                system.assertEquals(todayDate - 3, resultContact.last_activity_date__c);
                system.assertEquals(event1.Type, resultContact.last_activity_type__c);
            }

        }
    }

}