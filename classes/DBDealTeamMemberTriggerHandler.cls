public class DBDealTeamMemberTriggerHandler {
    
    DBDealTeamMemberApexSharing instance = DBDealTeamMemberApexSharing.getInstance();
    
    private static DBDealTeamMemberTriggerHandler handler = null;
    
    public static DBDealTeamMemberTriggerHandler getInstance() {

        if (handler == null) {

            handler = new DBDealTeamMemberTriggerHandler();
        }

        return handler;
    }
    
    public void onBeforeInsert (List<DB_Deal_Team_Member__c> newList) {
        
        for (DB_Deal_Team_Member__c dtm : newList) {
            dtm.DB_Conflict_Clearing_Status__c = 'Onboarding';
        }
    }
    
    public void onAfterUpdate (List<DB_Deal_Team_Member__c> updatedList) {
        
        List<DB_Deal_Team_Member__c> createSharesList  = new List<DB_Deal_Team_Member__c>();
        List<DB_Deal_Team_Member__c> modifySharesList  = new List<DB_Deal_Team_Member__c>();
        
        for (DB_Deal_Team_Member__c dtm : updatedList) {
            if (dtm.DB_Conflict_Clearing_Status__c == 'Approved') {
                createSharesList.add(dtm);
            }
            if (dtm.DB_Conflict_Clearing_Status__c == 'Withdrawn') {
                modifySharesList.add(dtm);
            }
        }
        
        if(!createSharesList.isEmpty()) {
        	instance.createDealShares(createSharesList);
        }
        if(!modifySharesList.isEmpty()) {
        	instance.modifyDealShares(modifySharesList);
        }
    }
    
    public void onAfterDelete (List<DB_Deal_Team_Member__c> deletedList) {
        
        instance.modifyDealShares(deletedList);
    }
}