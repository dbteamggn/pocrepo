@isTest
public class AddContactsToCampaignControllerTest{
    
    public static void createCustomSettingData() {
        
        List<Add_to_Campaign_Config__c> configRecords = new List<Add_to_Campaign_Config__c>();
        
        configRecords.add(new Add_to_Campaign_Config__c(Name='Test Report', Object_Name__c='Department__c', Filters__c='FSU_Frequency__c',
            Query_1__c='Select id From Department__c where Name=\'Test_Dept\'', 
            Query_2__c='Select Client__c From Department_Client_Relationship__c where Department__c IN : QueryOneSetID', 
            Query_3__c='Select ID From Contact where AccountID IN : QueryOneSetID', 
            Query_Returning_Contact_IDs__c=3, Report_Name__c='Test Report'));
        configRecords.add(new Add_to_Campaign_Config__c(Name='Test Report1', Object_Name__c='Department__c', Filters__c='FSU_Frequency__c',
            Query_1__c='Select id From Department__c where Name=\'Test_Dept\'', 
            Query_2__c='Select Client__c From Department_Client_Relationship__c where Department__c IN : QueryOneSetID', 
            Query_3__c='Select ID From Contact where AccountID IN : QueryOneSetID', 
            Query_Returning_Contact_IDs__c=3, Report_Name__c='Test Report1'));
        
        insert configRecords;
    }
    
    
    
    public static void setupData() {
      
        List<Department__c> departments = new List<Department__c>();
        List<Department_Client_Relationship__c> departmentClients = new List<Department_Client_Relationship__c>();
        List<Account> clients = new List<Account>();
        Account client = new Account();
        List<Contact> contacts = new List<Contact>();
        Contact con = new Contact();
        
        //create client data
        for(integer i=0; i<100; i++){
            client = CommonTestUtils.CreateTestClient('Test Client '+ i);
            clients.add(client);
        }
        insert clients;
        
        //create department data
        departments.add( new Department__c (Name='Test_Dept', FSU_Frequency__c='Monthly', client_id__c=clients[0].id)); 
        insert departments;
        
        //create contact data
        for(integer i=0; i<100; i++){
            con = CommonTestUtils.CreateTestContact('Test Contact '+ i);
            con.accountId = clients[i].id;
            contacts.add(con);
        }
        insert contacts;
        
        //create department client relationship data
        for(Account cl : clients){
            departmentClients.add(new Department_Client_Relationship__c(Client__c = cl.id, Department__c=departments[0].id, Created_via_API__c=true));
        }
        insert departmentClients;
        
    }
    
    
    
    static testMethod void addContactsToCampaignTest1(){
        
        //insert config custom setting data
        createCustomSettingData();
        
        //setup Data for report
        setupData();
        
        Campaign campaignRec = CommonTestUtils.CreateTestCampaign('Test Campaign');
        
        PageReference pg = Page.AddContactsToCampaign;
        Test.setCurrentPageReference(pg);
        
        Test.startTest();
        ApexPages.currentPage().getParameters().put('id',campaignRec.id);
        AddContactsToCampaignController controller = new AddContactsToCampaignController(new ApexPages.Standardcontroller(campaignRec));
        List<AddContactsToCampaignController.SelectContactWrapper> selectConList = new List<AddContactsToCampaignController.SelectContactWrapper>();
        AddContactsToCampaignController.SelectContactWrapper selectCon = new AddContactsToCampaignController.SelectContactWrapper();
        selectCon.selected=true;
        selectCon.con = new contact();
        selectConList.add(selectCon);
        
        controller.selectedReportName = 'Test Report';
        controller.sObjectInstance = new Department__c(FSU_Frequency__c='Monthly');
        controller.renderFilters();
        controller.getHasContacts();
        controller.getContactDetails();
        controller.getMemberStatusValues();     
        controller.selectedContactList = selectConList;
        controller.addSelectedContactsToCampaign();
        controller.addAllContactsToCampaign();
        system.assertEquals(controller.getHasContacts(), true);
        Test.stopTest();
    }
    
    static testMethod void addContactsToCampaignTest2(){
        
        //insert config custom setting data
        createCustomSettingData();
        
        //setup Data for report
        setupData();

        Campaign campaignRec = CommonTestUtils.CreateTestCampaign('Test Campaign');
        
        PageReference pg = Page.AddContactsToCampaign;
        Test.setCurrentPageReference(pg);
        
        Test.startTest();
        ApexPages.currentPage().getParameters().put('id',campaignRec.id);
        AddContactsToCampaignController controller = new AddContactsToCampaignController(new ApexPages.Standardcontroller(campaignRec));
        
        controller.selectedReportName = 'Test Report1';
        controller.sObjectInstance = new Department__c(FSU_Frequency__c='Monthly');
        controller.renderFilters();
        controller.getHasContacts();
        controller.getContactDetails();   
        controller.getMemberStatusValues();
        controller.addSelectedContactsToCampaign();
        controller.addAllContactsToCampaign();
        System.assertEquals(controller.allMemberStatus!=null, true);
        Test.stopTest();
    }
}