/**************************************************************************************
 Name        :    AccountTriggerHandler
 Description :    Trigger Handler for Account
 Created By  :    Deloitte
 Created On  :    19 Sep 2016
 --------------------------------------------------------------------------------------
 Modification Log
 -----------------
 Name                Date            Comments
 -----------------   -------------   --------------------------------------------------
 Joshna              19-Sep-2016      Created
 --------------------------------------------------------------------------------------
 Review Log
 ----------    
 Name                Date            Comments
 -----------------   -------------   --------------------------------------------------
 
***************************************************************************************/
public class AccountTriggerHandler extends TriggerHandler{    
    protected override void beforeInsert(){  
        if((triggerParams.objByPassMap.containsKey('Account_Utility.updateBusinessLineCheckboxes') && !triggerParams.objByPassMap.get('Account_Utility.updateBusinessLineCheckboxes')) || 
                !triggerParams.objByPassMap.containsKey('Account_Utility.updateBusinessLineCheckboxes')){
            Account_Utility.updateBusinessLineCheckboxes(triggerParams);
        }
        //if((triggerParams.objByPassMap.containsKey('Account_Utility.checkDuplicateAccout') && !triggerParams.objByPassMap.get('Account_Utility.checkDuplicateAccout')) || 
                //!triggerParams.objByPassMap.containsKey('Account_Utility.checkDuplicateAccout')){
           
        //}
    }
    protected override void beforeUpdate(){  
        if((triggerParams.objByPassMap.containsKey('Account_Utility.updateBusinessLineCheckboxes') && !triggerParams.objByPassMap.get('Account_Utility.updateBusinessLineCheckboxes')) || 
                !triggerParams.objByPassMap.containsKey('Account_Utility.updateBusinessLineCheckboxes')){
            Account_Utility.updateBusinessLineCheckboxes(triggerParams);
        }
        if((triggerParams.objByPassMap.containsKey('Account_Utility.validateParentHierarchy') && !triggerParams.objByPassMap.get('Account_Utility.validateParentHierarchy')) || 
                !triggerParams.objByPassMap.containsKey('Account_Utility.validateParentHierarchy')){
            Account_Utility.validateParentHierarchy(triggerParams);
        }
    }
    protected override void afterInsert(){        
        if((triggerParams.objByPassMap.containsKey('Account_Utility.copyChildRecords') && !triggerParams.objByPassMap.get('Account_Utility.copyChildRecords')) || 
                !triggerParams.objByPassMap.containsKey('Account_Utility.copyChildRecords')){
            Account_Utility.copyChildRecords(triggerParams);
        }
        /*if((triggerParams.objByPassMap.containsKey('Account_Utility.setClientTerritoryRecords') && !triggerParams.objByPassMap.get('Account_Utility.setClientTerritoryRecords')) || 
                !triggerParams.objByPassMap.containsKey('Account_Utility.setClientTerritoryRecords')){
            Account_Utility.setClientTerritoryRecords(triggerParams);
        }*/
    }
     
    protected override void afterUpdate(){
        if((triggerParams.objByPassMap.containsKey('Account_Utility.moveChildRecords') && !triggerParams.objByPassMap.get('Account_Utility.moveChildRecords')) || 
                !triggerParams.objByPassMap.containsKey('Account_Utility.moveChildRecords')){
            Account_Utility.moveChildRecords(triggerParams);
        }
        /*if((triggerParams.objByPassMap.containsKey('Account_Utility.setClientTerritoryRecords') && !triggerParams.objByPassMap.get('Account_Utility.setClientTerritoryRecords')) || 
                !triggerParams.objByPassMap.containsKey('Account_Utility.setClientTerritoryRecords')){
            Account_Utility.setClientTerritoryRecords(triggerParams);
        }*/
    }
    
    protected override void afterDelete(){
        if((triggerParams.objByPassMap.containsKey('Account_Utility.updateExternalIdOnMerge') && !triggerParams.objByPassMap.get('Account_Utility.updateExternalIdOnMerge')) || 
                !triggerParams.objByPassMap.containsKey('Account_Utility.updateExternalIdOnMerge')){
            Account_Utility.updateExternalIdOnMerge(triggerParams);
        }
    }
}