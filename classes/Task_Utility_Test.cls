@isTest
public class Task_Utility_Test {
    /*
     * 
     * Description : to validate updateTaskCustomFields
     * Param : 
     * Returns :  
    */ 
     static testMethod void testUpdateTaskCustomFields(){
        User user = CommonTestUtils.createTestUser();
         System.runAs(user){ 
        Task_Utility tutil = Task_Utility.getInstance();
        List<Task> tskLst = new List<Task>();
        Account acc = CommonTestUtils.CreateTestClient('test client');
         insert acc;
        Contact con = CommonTestUtils.CreateTestContact('test contact');
         con.AccountId = acc.Id;
         insert con;
        Task tsk =  CommonTestUtils.CreateTestTask('sample', user.id, 'Normal', 'Open', con.id);
         insert tsk;
         tskLst.add(tsk);
         Test.startTest();
         tutil.UpdateTaskCustomFields(tskLst);
         Test.stopTest();
     }
     }
     
   /*
     * 
     * Description : to validate TaskCreation
     * Param : 
     * Returns :  
    */ 
     static testMethod void testTaskCreation(){
        User user = CommonTestUtils.createTestUser();
         //Task_Utility tutil = Task_Utility.getInstance();
         String recId = '';
         System.runAs(user){ 
         Contact con = CommonTestUtils.CreateTestContact('test contact');
         insert con;
		Task tsk =  CommonTestUtils.CreateTestTask('sample', user.id, 'Normal', 'Open', con.id);
        tsk.Subject='';
             List<Id> contactIds = new List<Id>();
             Contact temp;
             for(integer i=0;i<3;i++){
                 temp = CommonTestUtils.CreateTestContact('test contact'+i);
                 insert temp;
                 contactIds.add(temp.Id);
             }
             // record ID is a contact
              Contact contact = CommonTestUtils.CreateTestContact('test contact for recordID');
              insert contact;
             
             // record ID is an Event
              Event ev = CommonTestUtils.CreateTestEvent('sample event', System.now(), System.now(), contact.Id, 'Normal', 'open');
              insert ev;
             
             // record ID is a task
         	   Task tk =  CommonTestUtils.CreateTestTask('sample', user.id, 'Normal', 'Open', con.id);
             insert tk;
             
             // When record ID is empty
             Test.startTest();
             String result = Task_Utility.TaskCreation(tsk,contactIds,recId );
             
             // When record ID is contact ID
             recId= contact.Id;           
             Task task =  CommonTestUtils.CreateTestTask('sample', user.id, 'Normal', 'Open', con.id);
             task.Subject='sample task';
             result = Task_Utility.TaskCreation(task,contactIds,recId );
             
             // When record ID is an Event ID
             recId= ev.Id;           
             Task task2 =  CommonTestUtils.CreateTestTask('sample', user.id, 'Normal', 'Open', con.Id);
             task2.Subject='sample task';
             result = Task_Utility.TaskCreation(task2,contactIds,recId );
             
             
             // When record ID is a Task ID
             recId= tk.Id;           
             Task task3 =  CommonTestUtils.CreateTestTask('sample', user.id, 'Normal', 'Open', con.Id);
             task3.Subject='sample task';
             result = Task_Utility.TaskCreation(task3,contactIds,recId );
             Test.stopTest();
             
          
            
             
}
     }
    
     /*
     * 
     * Description : to validate createActivityClientRelation
     * Param : 
     * Returns :  
    */ 
    static testMethod void testCreateActivityClientRelation(){
        User user = CommonTestUtils.createTestUser();
        Account acc = CommonTestUtils.CreateTestClient('test acc');
        insert acc;
        Contact con = CommonTestUtils.CreateTestContact('test contact');
        con.AccountId = acc.Id;
        insert con;       
        
        Account acc2 = CommonTestUtils.CreateTestClient('test client');
        insert acc2;
        Contact con2 = CommonTestUtils.CreateTestContact('test contact again');
        con2.AccountId=acc2.id;
        insert con2;
//        Task master =  CommonTestUtils.CreateTestTask('Master', user.id, 'Normal', 'Open', con.Id);
//      insert master;
        Task task;
        List<ID> taskIDs = new List<ID>();
        List<Activity_Client_Relationship__c> acrs = new List<Activity_Client_Relationship__c>();
        Activity_Client_Relationship__c acr;
        for(integer i=0;i<5;i++ ){
            task =  CommonTestUtils.CreateTestTask('sample', user.id, 'Normal', 'Open', con.id);
            task.WhatId = acc.Id;
            insert task;
            acr = new Activity_Client_Relationship__c();
            acr.Activity__c = task.id;
            acr.Client__c = acc.id;
            acr.Contact__c= con.id;
            acrs.add(acr);
            taskIDs.add(task.Id);
        }
        insert acrs;
        List<TaskRelation> trs = new List<TaskRelation>();
        TaskRelation tr;
        //for(integer i=0;i<;i++ ){
            task =  CommonTestUtils.CreateTestTask('sample', user.id, 'Normal', 'Open', null);
            task.WhatId = null;
        	insert task;
            tr = new TaskRelation();
            tr.TaskId = task.id;
            tr.IsWhat = false;
            tr.RelationId = con2.id;
            insert tr;
        	taskIDs.add(task.id);
       // }
        
        Test.startTest();
        Task_Utility.createActivityClientRelation(taskIds);
        Test.stopTest();
    }
    
    /*
     * 
     * Description : to validate GetDpndntPckLstValues
     * Param : 
     * Returns :  
    */ 
    static testMethod void testGetPckLstValues(){
        User user = CommonTestUtils.createTestUser();
        List<String> values;
        System.runAs(user){
            Test.startTest();
             values=Task_Utility.GetDpndntPckLstValues('Contact', 'CtrlField', 'DependField', 'CtrlFieldValue');
             values=Task_Utility.GetPckLstValues('Account', 'picklistField');
            Test.stopTest();
        }
    }
}