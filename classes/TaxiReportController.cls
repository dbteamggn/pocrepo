/*-------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            TaxiReportController.cls
   Description:     Controller used by TaxiReport.
                    The Taxi Report is a PDF summary which is so named as it provides a summary of a company that a member of the sales team can refer 
                    to while in a taxi on their way to a meeting.
                    It can be run for a Company or for a particular contact.  The only difference, apart from the heading, is which events are shown.
                    
   Created date:    06-Oct-2016               
   
   
   Date             Version         Author                             Issue Number     Summary of Changes                   
   -----------      ----------      -----------------                  -------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------------------------------------------*/

public with sharing class TaxiReportController {
    
    public id accountId;
    public id contactId;
    
    public string LogoUrl {get;set;}
    public Assets_Under_Management__c DummyForTots {get;set;}
    public list<Contact> KeyContacts{ get; set;}
    public map<id, contact> RecentEventContactsMap { get; set; }
    public map<id, contact> FutureEventContactsMap { get; set; }
    public map<id, contact> PrevCompanyEventContactsMap { get; set; }
    public List<String> fsuRelatedToClient{ get; set; }
    public Date nextFSUReviewDate{ get; set; }
    public String fsuHeads{ get; set; }
    public String fsuReviewed{ get; set; }
    public integer limit5 = 5;
    public integer limit6 = 6;
    public integer limit10 = 10;
    Map<String , Decimal> mapOfCurrAndVal;
    Decimal conversionRate = 1;
    
    public ApexPages.StandardController Ctrl;
    
    public List<Account> TheAccounts{
        get;
        private set;
    }
    
    public Account TheAccount{
        get;
        private set;
    }
    
    public List<Contact> TheContacts{
        get;
        private set;
    }
    
    public Set<Id> PreviousCompanies{
        get;
        private set;
    } 
    
    public Contact TheContact{
        get;
        private set;
    }
    
    public list<Event> TheRecentEvents{
        get;
        private set;
    }  
    
    public list<Event> TheFutureEvents{
        get;
        private set;
    }   
    
    public list<Event> ThePrevCompanyEvents{
        get;
        private set;
    }
    
    public list<Product2> IPproducts{
        get;
        private set;
    }
    
    //Get data for 'External Fund Selection Units' section
    public list<Rated_Fund__c> ExternalPanels{
        get
        {
            if (ExternalPanels == null){
                ExternalPanels = new list<Rated_Fund__c>();
                
                ExternalPanels = [select Client__r.name, Fund__r.name, Fund__r.family, Fund__r.fund_manager_name__c 
                                  from Rated_Fund__c 
                                  where Client__c in (select Third_Party_Name__c from Client_Third_Party_Relationship__c where Client_Name__c = :accountId) 
                                  and Fund__r.our_product__c = true 
                                  order by Client__r.name, Fund__r.name];
            }               
                
            return ExternalPanels;    
        }
        private set;
    } 
    
    
    // Get the id of the top level company.  Required for NDAM rollups
    public string Id18String { 
        get{
            if (Id18String == null){
                if (TheAccount.Top_Level_Firm_Id_Part1__c != null){
                    Id18String = TheAccount.Top_Level_Firm_Id_Part1__c;
                } else if (TheAccount.Top_Level_Firm_Id_Part2__c != null){
                    Id18String = TheAccount.Top_Level_Firm_Id_Part2__c;
                } else if(TheAccount.Top_Level_Firm_Id_Part3__c != null){
                    Id18String = TheAccount.Top_Level_Firm_Id_Part3__c;
                } else{
                    Id18String = TheAccount.Top_Level_Firm_Id_Part4__c;
                }
            }
            return Id18String;
            
        } 
        set;
    }
    
    // Get/Set the AUM record type Id depending on if running for NDAM and/or selected account settings
    public Id rtTotalSummaryAUMId {
        get {
            if (rtTotalSummaryAUMId == null){
                rtTotalSummaryAUMId = Schema.SObjectType.Assets_Under_Management__c.getRecordTypeInfosByName().get('Total Summary').getRecordTypeId();
            }
            return rtTotalSummaryAUMId;
        }
        set;
    }
    
    
    // Get/Set the AUM record type Id depending on if running for NDAM and/or selected account settings
    public Id rtStandardAUMId {
        get {
            if (rtStandardAUMId == null){
                rtStandardAUMId = Schema.SObjectType.Assets_Under_Management__c.getRecordTypeInfosByName().get('Standard').getRecordTypeId();
            }
            return rtStandardAUMId;
        }
        set;
    }
    
    //Logged In user details
    public User RunByUser{
        get
        {
            if (RunByUser == null){
                RunByUser = [select name, Business_Line__c, profileId from user where id = :UserInfo.getUserId() limit 1];
            }               
                
            return RunByUser;    
        }
        private set;
    }
    
    //Flag to determine if Key Contacts exist
    public boolean hasKeyContacts {
        get{
            if (keyContacts== null) return false;
            if (keyContacts.size() == 0) 
            {
                return false;
            } 
            else 
            {  
                return true;
            }
        }
    }
    
    //Flag to determine if past events exist
    public boolean hasRecentEvents {
        get {
            if (TheRecentEvents == null) return false;
            if (TheRecentEvents.size() == 0){
                return false;
            } else {
                return true;
            }
        }
    }
    
    //Flag to determine if future events exist
    public boolean hasFutureEvents {
        get {
            if (TheFutureEvents == null) return false;
            if (TheFutureEvents.size() == 0){
                return false;
            } else {
                return true;
            }
        }
    }
    
    //Flag to determine if events exist for previous employer
    public boolean hasPrevCompanyEvents {
        get {
            if (ThePrevCompanyEvents == null) return false;
            if (ThePrevCompanyEvents.size() == 0){
                return false;
            } else {
                return true;
            }
        }
    }
    
    //Flag to determine if Rated Funds exist
    public boolean hasIPproducts {
        get{
            if (IPproducts== null) return false;
            if (IPproducts.size() == 0) 
            {
                return false;
            } 
            else 
            {  
                return true;
            }
        }
    }
    
    //Flag to determine if External FSUs exist
    public boolean hasExternalPanels {
        get{
            if (ExternalPanels== null) return false;
            if (ExternalPanels.size() == 0) 
            {
                return false; 
            } 
            else 
            {  
                return true;
            }
        }
    }
    
    //Get the value of previous year
    public string PreviousYear {
        get {
            if (PreviousYear ==  null){
                date dt = system.today().addYears(-1);
                PreviousYear = string.valueof(dt.year()); 
            }
            return PreviousYear;        
        }   
        private set;    
    }
    
    //Get the value of the year before the previous year
    public string PreviousPreviousYear {
        get {
            if (PreviousPreviousYear ==  null){
                date dt = system.today().addYears(-2);
                PreviousPreviousYear = string.valueof(dt.year());
            }   
            return PreviousPreviousYear;        
        }   
        private set;    
    }

    //Flag to determine if logged in user is FIG user
    public boolean IsNdam{
        get
        {
            User RunByUser = [select name, Is_Fig__c from user where id = :UserInfo.getUserId() limit 1];
            
            if(RunByUser.Is_Fig__c){
                IsNdam = true;
            }
            else{
                IsNdam = false;
            }                
            return IsNdam;    
        }
        private set;  
    }

    //checked for NDAM roll ups
    public boolean SummaryExists {
        get {
            if (SummaryExists == null){
             list<Assets_Under_Management__c> tempAum = [SELECT Fund_Name__c
                         FROM   Assets_Under_Management__c
                         WHERE (Company_Name__r.top_level_firm_id_part1__c = :Id18String 
                                             or Company_Name__r.top_level_firm_id_part2__c = :Id18String
                                             or Company_Name__r.top_level_firm_id_part3__c = :Id18String
                                             or Company_Name__r.top_level_firm_id_part4__c = :Id18String)
                         AND  RecordTypeId = :rtTotalSummaryAUMId];
                if (tempAum.size()==0){
                    SummaryExists = false;
                } else {
                    SummaryExists = true;
                }
            }
            return SummaryExists;
        }
        set;
    }
    
    public Decimal getConvertedValue(Object fieldValue){
        if(fieldValue != null) return (((Decimal)fieldValue) * conversionRate);
        else return 0;
    }
    
    //Get data for Top 10 Funds
    public list<Assets_Under_Management__c> AUMsbyAUM{
        get {
            if (AUMsbyAUM == null){
                // Rollup if running as a FIG user (NDAM) and not running for a specific contact
                if (IsNdam && SummaryExists){
                    AggregateResult[] AUMsbyAUMAgg = [SELECT Fund_Name__c, sum(Current_AUM__c) Current_AUM
                         FROM   Assets_Under_Management__c
                         WHERE (Company_Name__r.top_level_firm_id_part1__c = :Id18String 
                                             or Company_Name__r.top_level_firm_id_part2__c = :Id18String
                                             or Company_Name__r.top_level_firm_id_part3__c = :Id18String
                                             or Company_Name__r.top_level_firm_id_part4__c = :Id18String)
                         AND  RecordTypeId = :rtTotalSummaryAUMId
                         group by Fund_Name__c
                         ORDER BY  sum(Current_AUM__c) DESC NULLS LAST  LIMIT :limit10 ];   
                    
                    AUMsbyAUM = new list<Assets_Under_Management__c>();
                                   
                    for (AggregateResult aums : AUMsbyAUMAgg)  {
                        Assets_Under_Management__c aum = new Assets_Under_Management__c();
                        aum.Fund_Name__c = (String)aums.get('Fund_Name__c');
                        aum.Current_AUM__c = getConvertedValue(aums.get('Current_AUM'));
                        AUMsbyAUM.add(aum);
                    }
                    
                } else {
                    AUMsbyAUM = [SELECT Fund_Name__c, Current_AUM__c
                                 FROM   Assets_Under_Management__c
                                 WHERE Company_Name__c = :accountId
                                 AND  RecordTypeId = :rtStandardAUMId
                                 ORDER BY  Current_AUM__c DESC NULLS LAST  LIMIT : limit10 ]; 
                }
            }
            return AUMsbyAUM;
        }
        private set;
    }    

    //Get data for Top 10 Funds
    public list<Assets_Under_Management__c> AUMsbyRedemptions{
        get {
            if (AUMsbyRedemptions == null){
                // Rollup if running as a FIG user (NDAM) and not running for a specific contact
                if (IsNdam && SummaryExists){
                    AggregateResult[] AUMsbyRedemptionsAgg = [SELECT Fund_Name__c, sum(Redemptions_YTD__c) Redemptions_YTD
                         FROM   Assets_Under_Management__c
                         WHERE (Company_Name__r.top_level_firm_id_part1__c = :Id18String 
                                             or Company_Name__r.top_level_firm_id_part2__c = :Id18String
                                             or Company_Name__r.top_level_firm_id_part3__c = :Id18String
                                             or Company_Name__r.top_level_firm_id_part4__c = :Id18String)
                         AND  RecordTypeId = :rtTotalSummaryAUMId
                         group by Fund_Name__c
                         ORDER BY sum(Redemptions_YTD__c) DESC NULLS LAST  LIMIT : limit10 ]    ;   
                    
                    AUMsbyRedemptions = new list<Assets_Under_Management__c>();
                                   
                    for (AggregateResult aums : AUMsbyRedemptionsAgg)  {
                        Assets_Under_Management__c aum = new Assets_Under_Management__c();
                        aum.Fund_Name__c = (String)aums.get('Fund_Name__c');
                        aum.Redemptions_YTD__c = getConvertedValue(aums.get('Redemptions_YTD'));
                        AUMsbyRedemptions.add(aum);
                    } 
                } else {
                    AUMsbyRedemptions = [SELECT Redemptions_YTD__c, Fund_Name__c
                                         FROM   Assets_Under_Management__c
                                         WHERE Company_Name__c = :accountId
                                         AND  RecordTypeId = :rtStandardAUMId
                                         ORDER BY Redemptions_YTD__c DESC NULLS LAST  LIMIT : Limit10 ]    ;  
                }
            }
            return AUMsbyRedemptions;
        }
        private set;
    }
    
    //Get data for Top 10 Funds
    public list<Assets_Under_Management__c> AUMsbyGrossSales{
        get {
            if (AUMsbyGrossSales == null){
                // Rollup if running as a FIG user (NDAM) and not running for a specific contact
                if (IsNdam && SummaryExists){
                    AggregateResult[] AUMsbyGrossSalesAgg = [SELECT Fund_Name__c, sum(Gross_Sales_YTD__c) Gross_Sales_YTD
                                                             FROM   Assets_Under_Management__c
                                                             WHERE (Company_Name__r.top_level_firm_id_part1__c = :Id18String 
                                                                                 or Company_Name__r.top_level_firm_id_part2__c = :Id18String
                                                                                 or Company_Name__r.top_level_firm_id_part3__c = :Id18String
                                                                                 or Company_Name__r.top_level_firm_id_part4__c = :Id18String)
                                                             AND  RecordTypeId = :rtTotalSummaryAUMId 
                                                             group by Fund_Name__c
                                                             ORDER BY sum(Gross_Sales_YTD__c) DESC NULLS LAST LIMIT : limit10] ;   
                    
                    AUMsbyGrossSales = new list<Assets_Under_Management__c>();
                                   
                    for (AggregateResult aums : AUMsbyGrossSalesAgg)  {
                        Assets_Under_Management__c aum = new Assets_Under_Management__c();
                        aum.Fund_Name__c = (String)aums.get('Fund_Name__c');
                        aum.Gross_Sales_YTD__c = getConvertedValue(aums.get('Gross_Sales_YTD'));
                        AUMsbyGrossSales.add(aum);
                    } 
                } else {
                    AUMsbyGrossSales = [SELECT Gross_Sales_YTD__c, Fund_Name__c
                                         FROM   Assets_Under_Management__c
                                         WHERE Company_Name__c = :accountId
                                         AND  RecordTypeId = :rtStandardAUMId 
                                         ORDER BY Gross_Sales_YTD__c DESC NULLS LAST LIMIT : limit10] ; 
                }
            }
            return AUMsbyGrossSales;
        }
        private set;
    }
    
    
    //Get data for Company Profile
    public Set<String> fundsOnRecommendedList{
        get{
            if(fundsOnRecommendedList == null || fundsOnRecommendedList.isEmpty()){
                fundsOnRecommendedList = new Set<String>();
                List<Client_Fund__c> recommendedFunds = [SELECT product__r.name
                                                         FROM Client_Fund__c
                                                         WHERE Client__c =: accountId];
                
                for(client_fund__c each : recommendedFunds){
                    fundsOnRecommendedList.add(each.product__r.name);
                }
            }
            return fundsOnRecommendedList;
        }
        private set;
    }
    
    //Get data for Company Profile
    public Set<String> currentIPOpportunities{
        get{
            
            if(currentIPOpportunities == null || currentIPOpportunities.isEmpty()){
                currentIPOpportunities = new Set<String>();
                List<Opportunity> opportunities = [SELECT name
                                                     FROM Opportunity
                                                     WHERE AccountId =: accountId];
                
                for(Opportunity each : opportunities){
                    currentIPOpportunities.add(each.name);
                }
            }
            return currentIPOpportunities;
        }
        private set;
    }
    
    //Get data for Company Profile
    public Set<String> competitorFundsForClient{
        get{
            if(competitorFundsForClient == null || competitorFundsForClient.isEmpty()){
                competitorFundsForClient = new Set<String>();
                List<Competitor_Fund__c> competitorFunds = [SELECT product__r.name
                                                             FROM Competitor_Fund__c
                                                             WHERE Company__c =: accountId];
                
                for(Competitor_Fund__c each : competitorFunds){
                    competitorFundsForClient.add(each.product__r.name);
                }
            }
            return competitorFundsForClient;
        }
        private set;
    }
    
    //Constructor for desktop version of Taxi Reports
    public TaxiReportController(){
        accountId = ApexPages.currentPage().getParameters().get('accountId'); 
        contactId = ApexPages.currentPage().getParameters().get('contactId');
        system.debug('accountId1 : ' + accountId);
        system.debug('contactId1 : ' + contactId);
        init();
    }   
    
    //Constructor for SF1 version of Taxi Reports
    public TaxiReportController(ApexPages.StandardController c){
        Ctrl = c;
        if(Ctrl.getRecord() instanceOf Account){
            Account account = (Account) Ctrl.getRecord();
            accountId = account.id;
            contactId = null;
        } else if(Ctrl.getRecord() instanceOf Contact){
            Contact contact = (Contact) Ctrl.getRecord();
            contactId = contact.Id;
            if (contactId != null){
                Contact tempContact = [select accountId from contact where id = :contactId limit 1];
                if (tempContact != null){
                    accountId = tempContact.accountId;
                }
            }
        } 
        // Required this for US23332 as parameters from the Show More SF1 option differ
        // from those when run in mobile card form on SF1. 
        if (accountId == null && contactId == null){ // passing in params via URL
            accountId = ApexPages.currentPage().getParameters().get('accountId'); 
          contactId = ApexPages.currentPage().getParameters().get('contactId');
        }
        system.debug('accountId2 : ' + accountId);
        system.debug('contactId2 : ' + contactId);
        init();
    } 
    
    
    private void init(){
        
        LogoUrl = UtilsDocuments.getDocUrl('Logo');
        mapOfCurrAndVal = CommonUtilities.getCurrencyInfo();
        if(mapOfCurrAndVal.containsKey(Label.TaxiReportsCurrency)){
            conversionRate = mapOfCurrAndVal.get(Label.TaxiReportsCurrency);
        }
            
        
        //redirected here from contact version of Taxi reports
        if (contactId != null){
            theContacts = [select c.accountId, c.firstName, c.lastName from Contact c where c.id = :contactId limit 1];
            theContact = theContacts[0];
            accountId = theContact.accountId;
            
            PreviousCompanies = new Set<id>();
            for(AccountContactRelation acr : [select accountId from AccountContactRelation where contactId =: contactId 
                                                   and   Previous_Employee__c = true and   isDirect = false]){
                PreviousCompanies.add(acr.accountId);
            }
            
        }
        
        map<id, Contact> AllContacts = new Map<id, contact>([select c.id, c.accountId, c.account.name, c.firstName, c.lastName, 
                                                                    status__c, c.LeftCompany__c 
                                                             from Contact c 
                                                             where c.accountId = :accountId]);
        
        List<AccountContactRelation> acrList = [select previous_employee__c, contactId from AccountContactRelation where accountId =: accountId and contactId IN: AllContacts.keySet()];
        contact tempContact;
        if(acrList != null){
            for(AccountContactRelation acr : acrList){
                if(AllContacts.containsKey(acr.contactId)){
                    tempContact = AllContacts.get(acr.contactId);
                    tempContact.LeftCompany__c = acr.Previous_Employee__c;
                    AllContacts.put(acr.contactId, tempContact);
                }
            }
        }
        
        theAccounts = [Select 
                                Name,  
                                FCA_Number__c, 
                                Total_AUM_in_Hierarchy__c, 
                                Billing_Street_Line_1__c,
                                Billing_Street_Line_2__c,
                                Billing_Street_Line_3__c,
                                Billing_City__c, 
                                Billing_State__c, 
                                Billing_PostalCode__c, 
                                Billing_Country__c, 
                                phone,
                                Asset_Allocation_Method__c,
                                Influenceable__c,
                                Dealing_Method__c,
                                Asset_Allocation_Individual__c,
                                Asset_Allocation_Individual__r.firstname,
                                Asset_Allocation_Individual__r.lastname,
                                Relationship_with_Decision_Maker__c,
                                Relationship_with_Operational__c,
                                Top_Level_Firm_Id_Part1__c,
                                Top_Level_Firm_Id_Part2__c,
                                Top_Level_Firm_Id_Part3__c,
                                Top_Level_Firm_Id_Part4__c
                        From Account 
                        where id = :accountId
                        limit 1];          
        theAccount = theAccounts[0];
        
        //Get Company Profile information related to FSU
        Id rtFSUDepartmentId = Schema.SObjectType.Department__c.getRecordTypeInfosByName().get('Fund Selection Unit').getRecordTypeId();        
        List<Department_Client_Relationship__c> deptClientRelationList = [SELECT Department__r.Next_FSU_Review_Date__c, Department__r.name,
                                                                             Department__r.FSU_Head__r.FirstName, Department__r.FSU_Head__r.LastName
                                                                             FROM Department_Client_Relationship__c
                                                                             WHERE Client__c =: accountId
                                                                             AND Department__r.RecordTypeId = :rtFSUDepartmentId
                                                                             ORDER BY Department__r.Next_FSU_Review_Date__c desc];
            
        if(nextFSUReviewDate == null && deptClientRelationList!=null && !deptClientRelationList.isEmpty()){
            nextFSUReviewDate = deptClientRelationList[0].Department__r.Next_FSU_Review_Date__c;
        }
        if((fsuRelatedToClient == null || fsuRelatedToClient.isEmpty()) && deptClientRelationList!=null && !deptClientRelationList.isEmpty()){
            fsuRelatedToClient = new List<String>();
            for(Department_Client_Relationship__c each : deptClientRelationList){
                fsuRelatedToClient.add(each.Department__r.name);
            }
        }
        if(fsuHeads == null && deptClientRelationList!=null && !deptClientRelationList.isEmpty()){
            for(Department_Client_Relationship__c each : deptClientRelationList){
                if(String.isBlank(fsuHeads)){
                    fsuHeads = (String.isBlank(each.Department__r.FSU_Head__r.FirstName) ? '' : each.Department__r.FSU_Head__r.FirstName+' ') + (String.isBlank(each.Department__r.FSU_Head__r.LastName) ? '' : each.Department__r.FSU_Head__r.LastName);
                }else{
                    fsuHeads += (String.isBlank(each.Department__r.FSU_Head__r.FirstName) ? '' : ', '+each.Department__r.FSU_Head__r.FirstName+' ') + (String.isBlank(each.Department__r.FSU_Head__r.LastName) ? '' : each.Department__r.FSU_Head__r.LastName);
                }
            }
        }
        if(fsuReviewed == null && deptClientRelationList!=null && !deptClientRelationList.isEmpty()){
            for(Department_Client_Relationship__c each : deptClientRelationList){
                if(String.isBlank(fsuReviewed)){
                    fsuReviewed = '';
                }else{
                    fsuReviewed += '';
                }
            }
        }
        
        DummyForTots = new Assets_Under_Management__c(
            Fund_Name__c = 'Total',
            Current_AUM__c = 0,
            Gross_Sales_YTD__c = 0
        );
        
        List<AccountContactRelation> keyAccountContactRelations= new List<AccountContactRelation>();
        
        //Get Key Contacts
        if (IsNdam){
            keyAccountContactRelations = [Select contactId, contact.Title, contact.Preferred_Name__c,
                                                contact.Phone, contact.LastName, contact.Email,
                                                primary_contact__c, previous_employee__c
                                            From AccountContactRelation
                                            where accountId =: accountId
                                            and national_key_contact__c = true
                                            and contact.status__c = 'Active'
                                            order by contact.lastName, contact.Preferred_Name__c
                                            limit : limit6];
            
        } else {                               
            keyAccountContactRelations = [Select contactId, contact.Title, contact.Preferred_Name__c,
                                                contact.Phone, contact.LastName, contact.Email,
                                                primary_contact__c, previous_employee__c
                                            From AccountContactRelation
                                            where accountId =: accountId
                                            and (primary_contact__c = true OR key_contact__c = true)
                                            and contact.status__c = 'Active'
                                            order by contact.lastName, contact.Preferred_Name__c
                                            limit : limit6];
        
        }
        keyContacts = new List<Contact>();
        for(AccountContactRelation acr : keyAccountContactRelations){
            keyContacts.add(new contact(
                Title = acr.contact.Title,
                Preferred_Name__c = acr.contact.Preferred_Name__c,
                phone = acr.contact.Phone, 
                lastname = acr.contact.LastName, 
                email = acr.contact.Email,
                primary_contact__c = acr.primary_contact__c, 
                LeftCompany__c = acr.previous_employee__c
            ));
        }
        
        
        //Get Events information
        Date today = date.today();
        
        if (contactId == null){

            TheRecentEvents = [Select e.Id,  e.Description, e.Subject, e.AccountId, e.StartDateTime, e.whoId, e.whoCount, (select relationId, relation.firstname, relation.lastname from eventRelations where accountId=:accountId and isDeleted=false and status != 'Declined')  
                                        From Event e
                                        where  e.id in (select er.eventId from eventRelation er where er.accountId=:accountId) 
                                        and  e.startDateTime < :today
                                        order by e.StartDateTime DESC limit : limit5];
             

            TheFutureEvents = [Select e.Id,  e.Description, e.Subject, e.AccountId, e.StartDateTime, e.whoId, e.whoCount, (select relationId, relation.firstname, relation.lastname from eventRelations where accountId=:accountId and isDeleted=false and status != 'Declined' limit 1)  From Event e
                                        where  e.id in (select er.eventId from eventRelation er where er.accountId=:accountId) 
                                        and  e.startDateTime >= :today
                                        order by e.StartDateTime limit : limit5];
            
            
            ThePrevCompanyEvents = new list<Event>();
                      
        } else {
            TheRecentEvents = [Select e.Id,  e.Description, e.Subject, e.AccountId, e.StartDateTime, e.whoId, e.whoCount, (select relationId, relation.firstname, relation.lastname from eventRelations where relationId=:contactId and isDeleted=false and status != 'Declined' limit 1)  From Event e
                                        where  e.id in (select er.eventId from eventRelation er where er.relationId=:contactId) 
                                        and  e.startDateTime < :today
                                        order by e.StartDateTime DESC limit : limit5];
                          
            TheFutureEvents = [Select e.Id,  e.Description, e.Subject, e.AccountId, e.StartDateTime, e.whoId, e.whoCount, (select relationId, relation.firstname, relation.lastname from eventRelations where relationId=:contactId and isDeleted=false and status != 'Declined' limit 1)  From Event e
                                        where  e.id in (select er.eventId from eventRelation er where er.relationId=:contactId) 
                                        and  e.startDateTime >= :today
                                        order by e.StartDateTime limit : limit5];
            
            if (PreviousCompanies != null) {                           
              ThePrevCompanyEvents = [Select e.Id,  e.Description, e.Subject, e.AccountId, e.StartDateTime,
                                           e.whoId, e.whoCount, e.account.name,
                                        (select relationId, relation.firstname, relation.lastname from eventRelations where accountId in :PreviousCompanies and isDeleted=false and status != 'Declined' limit 1)
                                        From Event e
                                        where  e.id in (select er.eventId from eventRelation er 
                                                        where er.accountId in :PreviousCompanies ) 
                                        order by e.StartDateTime desc limit : limit5];
            } else {
              ThePrevCompanyEvents = new list<Event>();
            }
        }
        
        // Work around for those events that the company is invited but no contact from that company.
        System.debug('Start RecentEventContactsMap'); 
        RecentEventContactsMap = new map<id, contact>();
        for(Event e : TheRecentEvents){
            if (e.eventRelations.isEmpty() || (!e.eventRelations.isEmpty() && contactId == null)){
                RecentEventContactsMap.put(e.id, new contact(firstname='N/A'));
            }
            else {
              // Else dealing with the Contact variant of the Taxi Report so just interested in this contact
              // regardless of if left company or not.
              if(contactId != null){
                  Contact eventContact = allContacts.get(e.eventRelations[0].relationId);
                  if (eventContact == null){
                    RecentEventContactsMap.put(e.id, new Contact(firstname='N/A'));
                } else {
                  RecentEventContactsMap.put(e.id, eventContact);
                }
              }
            }            
        }
        
        
        System.debug('Start FutureEventContactsMap'); 
        FutureEventContactsMap = new map<id, contact>();
        for(Event e : TheFutureEvents){
            if (e.eventRelations.isEmpty()  || (!e.eventRelations.isEmpty() && contactId == null)){
                FutureEventContactsMap.put(e.id, new Contact(firstname='N/A'));
            } else {
              // Else dealing with the Contact variant of the Taxi Report so just interested in this contact
              // regardless of if left company or not.
                if (contactId != null){                
                  Contact eventContact = allContacts.get(e.eventRelations[0].relationId);
                  if (eventContact == null){
                    FutureEventContactsMap.put(e.id, new Contact(firstname='N/A'));
                } else {
                  FutureEventContactsMap.put(e.id, eventContact);
                }
              }
            }
        }
        
        
        System.debug('Start PrevCompanyEventContactsMap'); 
        System.debug('ThePrevCompanyEvents:' + ThePrevCompanyEvents );
        PrevCompanyEventContactsMap = new map<id, contact>();
        for(Event e : ThePrevCompanyEvents){
            //no related contact/lead (i.e isWhat=true for all related eventrelations for the event)
            if (e.eventRelations.isEmpty()){
                PrevCompanyEventContactsMap.put(e.id, new Contact(firstname='N/A'));
            } 
            //related contact/lead
            else {
                // Else dealing with the Contact variant of the Taxi Report so just interested in this contact
                // regardless of if left company or not.
                if (contactId != null){
                  Contact eventContact = allContacts.get(e.eventRelations[0].relationId);
                  if (eventContact == null){
                    PrevCompanyEventContactsMap.put(e.id, new Contact(firstname='N/A'));
                  } else {
                    PrevCompanyEventContactsMap.put(e.id, eventContact);
                  }
              }
            }
        }
        
        IPproducts = [select pr.name, pr.family, pr.Fund_Manager_Name__c 
                    from Product2 pr 
                    where pr.our_product__c = true
                    and pr.id in (select Fund__c from Rated_Fund__c where Client__c = :accountId)
                    order by pr.name ];
        
    }
    
    //Method to determine if this is SF1 app or not and redirect to the correct page
    public PageReference redirect(){
        
        if(String.isNotBlank(ApexPages.currentPage().getParameters().get('sfdcIFrameHost')) ||
           String.isNotBlank(ApexPages.currentPage().getParameters().get('sfdcIFrameOrigin')) ||
           ApexPages.currentPage().getParameters().get('isdtp') == 'p1' ||
           (ApexPages.currentPage().getParameters().get('retURL') != null && ApexPages.currentPage().getParameters().get('retURL').contains('projectone') )
          ){
              // In SF1 so build up the correct page reference that we need to re-route to.
              PageReference sf1TaxiReportPR = null;
              if (String.isNotBlank(ApexPages.currentPage().getParameters().get('accountId'))){
                  
                  sf1TaxiReportPR = new PageReference('/apex/Sf1TaxiReportCompany');
                    sf1TaxiReportPR.getParameters().put('accountId', ApexPages.currentPage().getParameters().get('accountId'));
              } else {
                    sf1TaxiReportPR = new PageReference('/apex/Sf1TaxiReportContact');
                    sf1TaxiReportPR.getParameters().put('contactId', ApexPages.currentPage().getParameters().get('contactId'));
              }
              return sf1TaxiReportPR;
                            
          } else {

              // In SF so build up the correct page reference that we need to re-route to.
              PageReference taxiReportPR = new PageReference('/apex/TaxiReport' + 
                                                              ((String.isNotBlank(ApexPages.currentPage().getParameters().get('accountId')))
                                                              ?'?accountId=' + ApexPages.currentPage().getParameters().get('accountId')
                                                              :'?contactId=' + ApexPages.currentPage().getParameters().get('contactId')
                                                              ));
              return taxiReportPR;
          }

    }
}