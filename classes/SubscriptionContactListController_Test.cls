@isTest
public class SubscriptionContactListController_Test{
  /*
     * 
     * Description : Check that a past Event (End date earlier than today but later than the last batch run) 
     *               updates the contact that is associated with it.
     * Param : 
     * Returns :  
    */
    /*
     * 
     * Description : to validate creation of subscription association for activity
    */
    static testMethod void TestContactSubscription(){
        User user = CommonTestUtils.createTestUser();
        // create subscription record
        Subscription__c o_subscription = CommonTestUtils.CreateTestSubscription('Test', 'Subscription');              
                
        // create multiple contacts to be associated to an event
        List<Contact> listOfCon = new List<Contact>();
        List<id> addList = new List<ID>();
        List<id> delList = new List<ID>();
        Contact c1 = CommonTestUtils.CreateTestContact('ConSub1' );
        Contact c2 = CommonTestUtils.CreateTestContact('ConSub2' );
        insert c1;
        insert c2;
        Contact_Subscription_Link__c link = CommonTestUtils.createTestContactSubscription(c1, o_subscription);
        addList.add(c2.id);
        delList.add(c1.id);
        
        // perform the test 
        Test.startTest();
        System.runAs(user){ 
            
            // negative test : to search for contact that doesn't exists
            SubscriptionContactListController.getContactsByName('test','',o_subscription.id, 1);
            
            // positive test: search for above created contact
            SubscriptionContactListController.getContactsByName('ConSub','',o_subscription.id, 1);
            
            // positive test: search for above created contact
            SubscriptionContactListController.getContactsByName('ConSub','',o_subscription.id, 2);
            
            // positive test: search for above created contact
            SubscriptionContactListController.updateContactSubscription(addList,delList,o_subscription.id);
            
            // negative test : to search for contact that doesn't exists
            SubscriptionContactListController.getAllContacts('',o_subscription.id, 1);
            
        }
        Test.stopTest();
    }
}