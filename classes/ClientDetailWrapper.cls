public class ClientDetailWrapper{
        @AuraEnabled
        public string sName{get;set;}
          @AuraEnabled
        public string sAddress{get;set;}
          @AuraEnabled
        public string sWebsite{get;set;}
          @AuraEnabled
        public string sPhone{get;set;}
          @AuraEnabled
        public string sLogoId{get;set;}
         public ClientDetailWrapper (){}
        
    }