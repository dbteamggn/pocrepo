public with sharing class BusinessPlanOpportunitiesController {
    
    @AuraEnabled
    public static List<RelatedOpportunities> getRelatedOpportunities(String businessPlan){
        List<Business_Plan_Client_Relation__c> businessPlanRelationRecords = new List<Business_Plan_Client_Relation__c>();
        List<Opportunity> opportunities = new List<Opportunity>(); 
        List<Opportunity> relatedOpportunities = new  List<Opportunity>();
        Id businessPlanId = (Id) businessPlan;
        Set<Id> accountIds = new Set<Id>();
        List<RelatedOpportunities> opportunitiesRelated = new List<RelatedOpportunities>();
        List<OpportunityLineItem> opportunityLineItem ;
        RelatedOpportunities opportunity;
        Date todayDate = Date.Today();
        List<Business_Plan_Opportunity__c> bpOpportunityList = new List<Business_Plan_Opportunity__c>();
        Set<ID> oppIDs = new Set<ID>();        
        try{                      
            bpOpportunityList= [select id, Business_Plan__c, Opportunity__c from Business_Plan_Opportunity__c where Business_Plan__c =: businessPlan];
            system.debug('List ----> '+bpOpportunityList.size());                
            if(bpOpportunityList.size() > 0){
                for(Business_Plan_Opportunity__c bpo : bpOpportunityList){
                    oppIDs.add(bpo.Opportunity__c);
                }
                opportunities = [select id, name,CloseDate, Account.Name, Account.Owner.Name, Amount, Type, Investment_Strategy__r.Name, (select Product2.Name from OpportunityLineItems) from opportunity where ID IN :oppIDs ];            

                for(Opportunity opp : opportunities){                               
                    opportunity = new RelatedOpportunities();
                    opportunityLineItem = new List<OpportunityLineItem>();
                    opportunity.opportunityId = opp.Id;
                    opportunity.opptyRec = opp;
                    opportunity.clientName = opp.Account.Name;
                    opportunity.clientOwner = opp.Account.Owner.Name;
                    opportunity.opportunityType = opp.Type;
                    opportunity.value= true;                                                
                    
                    if(opp.Investment_Strategy__c != NULL){
                        opportunity.productOrStrategies = opp.Investment_Strategy__r.Name;    
                    }
                    opportunityLineItem.addAll(opp.OpportunityLineItems);
                    if(opportunityLineItem.size() > 0){
                        for(OpportunityLineItem opl : opportunityLineItem ){
                            if(opportunity.productOrStrategies != null)
                                opportunity.productOrStrategies = opl.Product2.Name;
                            else
                                opportunity.productOrStrategies = opportunity.productOrStrategies +';'+opl.Product2.Name;
                        }
                    }                                           
                    opportunitiesRelated.add(opportunity);
                } 
            }else{
                opportunities = [select id, name,CloseDate, Account.Name, Account.Owner.Name, Amount, Type, Investment_Strategy__r.Name, (select Product2.Name from OpportunityLineItems) from opportunity where CloseDate >= :todayDate  ];            
                for(Opportunity opp : opportunities){                               
                    opportunity = new RelatedOpportunities();
                    opportunityLineItem = new List<OpportunityLineItem>();
                    opportunity.opportunityId = opp.Id;
                    opportunity.clientName = opp.Account.Name;
                    opportunity.opptyRec = opp;
                    opportunity.clientOwner = opp.Account.Owner.Name;
                    opportunity.opportunityType = opp.Type;
                    if(opp.Investment_Strategy__c != NULL){
                        opportunity.productOrStrategies = opp.Investment_Strategy__r.Name;    
                    }
                    opportunityLineItem.addAll(opp.OpportunityLineItems);
                    if(opportunityLineItem.size() > 0){
                        for(OpportunityLineItem opl : opportunityLineItem ){
                            if(opportunity.productOrStrategies != null)
                                opportunity.productOrStrategies = opl.Product2.Name;
                            else
                                opportunity.productOrStrategies = opportunity.productOrStrategies +';'+opl.Product2.Name;
                        }
                    }
                    if( todayDate.monthsBetween(opp.CloseDate)<=12){                                                                        
                        opportunitiesRelated.add(opportunity);
                    }
                    
                } 
            }
        }catch(Exception e){
            CommonUtilities.createExceptionLog(e);
        }
        return opportunitiesRelated;
    }
    
    @AuraEnabled
    public static List<RelatedOpportunities> getOpportunity(List<ID> opportunityId){
        List<Opportunity> opp = new List<Opportunity>();
        List<OpportunityLineItem>  opportunityLineItem ;
        RelatedOpportunities relatedOpportunity;
        List<RelatedOpportunities> relatedOpportunityList = new List<RelatedOpportunities>();
        try{
            opp = [select id, name,CloseDate, Account.Name, Account.Owner.Name, Amount, Type, Investment_Strategy__r.Name, (select Product2.Name from OpportunityLineItems) from opportunity where id IN :opportunityId ];
            if(opp.size() >0){
                for(Opportunity op : opp){
                    relatedOpportunity = new RelatedOpportunities();
                    opportunityLineItem = new List<OpportunityLineItem>();
                    relatedOpportunity.opportunityId = op.Id;
                    relatedOpportunity.opptyRec = op;
                    relatedOpportunity.clientName = op.Account.Name;
                    relatedOpportunity.clientOwner = op.Account.Owner.Name;
                    relatedOpportunity.opportunityType = op.Type;
                    relatedOpportunity.value = true;
                    if(op.Investment_Strategy__c != NULL){
                        relatedOpportunity.productOrStrategies = op.Investment_Strategy__r.Name;    
                    }
                    opportunityLineItem.addAll(op.OpportunityLineItems);
                    if(opportunityLineItem.size() > 0){
                        for(OpportunityLineItem opl : opportunityLineItem ){
                            relatedOpportunity.productOrStrategies = relatedOpportunity.productOrStrategies +';'+opl.Product2.Name;
                        }
                    }    
                    relatedOpportunityList.add(relatedOpportunity);
                }
            }
            
        }catch(Exception e){
            CommonUtilities.createExceptionLog(e);
        }
        return relatedOpportunityList;
    }
    
    @AuraEnabled
    public static Boolean createBusinessPlanOpportunities(String businessPlan, List<ID> opportunities, List<ID> delOpportunities){
        Id businessPlanId = (ID) businessPlan;
        Set<ID> opportunityIds = new Set<ID>();
        Set<ID> delOpportunityIds = new Set<ID>();
        Set<ID> existingopportunityIds = new Set<ID>();
        opportunityIds.addAll(opportunities);
        delOpportunityIds.addAll(delOpportunities);
        Business_Plan_Opportunity__c bpOpportunity ;
        SavePoint sp ;
        List<Business_Plan_Opportunity__c> bpOpportunityList = new List<Business_Plan_Opportunity__c>();
        List<Business_Plan_Opportunity__c> newBPOpportunityList = new List<Business_Plan_Opportunity__c>();
        List<Business_Plan_Opportunity__c> delBPOpportunityList = new List<Business_Plan_Opportunity__c>();
        Boolean isCreated = true;
        sp= Database.setSavepoint();
        try{
            bpOpportunityList = [select id, Business_Plan__c, Opportunity__c from Business_Plan_Opportunity__c where Business_Plan__c =:businessPlanId ];            
            if(bpOpportunityList.size() > 0){
                for(Business_Plan_Opportunity__c bpo : bpOpportunityList){
                    existingopportunityIds.add(bpo.Opportunity__c);
                }
            }
            for(ID oppId : opportunityIds){
                bpOpportunity = new Business_Plan_Opportunity__c();
                if(!existingOpportunityIds.contains(oppId)){                    
                    bpOpportunity.Business_Plan__c = businessPlanId;
                    bpOpportunity.Opportunity__c = oppId;
                    newBPOpportunityList.add(bpOpportunity);
                }
            }
            for(Business_Plan_Opportunity__c bp : bpOpportunityList){               
                if(delOpportunityIds.contains(bp.Opportunity__c)){                    
                    delBPOpportunityList.add(bp);   
                }
                
            }
            if(newBPOpportunityList.size()> 0){
                Database.SaveResult[] result = Database.insert(newBPOpportunityList, true);
                for(Database.SaveResult res: result){
                    if(res.isSuccess()){
                        isCreated = true;
                    }else{
                        isCreated = false;
                    }
                }
            }
            if(delBPOpportunityList.size() > 0){
                Database.DeleteResult[] res = Database.delete(delBPOpportunityList, true);
                for(Database.DeleteResult result: res){
                    if(result.isSuccess()){
                        isCreated = true;
                    }else{
                        isCreated=false;
                    }
                }
            }
        }catch(Exception e){
            isCreated = false;
            Database.rollback(sp);
            CommonUtilities.createExceptionLog(e);
        }
        
        return isCreated;
    }
    
    public class RelatedOpportunities{
        
        @AuraEnabled
        public String opportunityId;
        @AuraEnabled
        public Opportunity opptyRec;
        @AuraEnabled
        public String clientName;
        @AuraEnabled
        public String clientOwner;
        @AuraEnabled
        public String opportunityType;
        @AuraEnabled
        public String productOrStrategies;
        @AuraEnabled
        public boolean value;
    }
}