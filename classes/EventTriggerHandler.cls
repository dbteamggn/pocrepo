/**************************************************************************************
 Name        :    EventTriggerHandler
 Description :    Trigger Handler for Event
 Created By  :    Deloitte
 Created On  :    02 August 2016
 --------------------------------------------------------------------------------------
 Modification Log
 -----------------
 Name                Date            Comments
 -----------------   -------------   --------------------------------------------------
 Joshna              08-02-2016      Created
 --------------------------------------------------------------------------------------
 Review Log
 ----------    
 Name                Date            Comments
 -----------------   -------------   --------------------------------------------------
 Hemangini          30-Aug-2016     US28000: Creating junction object records for Activity Client Relationship
***************************************************************************************/
public class EventTriggerHandler extends TriggerHandler{
    protected override void afterInsert(){
        //Existing logic. Need to add bypass logic to this method
        existingLogic();
        if((triggerParams.objByPassMap.containsKey('Event_Utility.createGnB') && !triggerParams.objByPassMap.get('Event_Utility.createGnB')) || 
                !triggerParams.objByPassMap.containsKey('Event_Utility.createGnB')){
            Event_Utility.createGnB(triggerParams);
        }
        //US28000-START
        if((triggerParams.objByPassMap.containsKey('Event_Utility.createActivityClientRelation') && !triggerParams.objByPassMap.get('Event_Utility.createActivityClientRelation')) || 
                !triggerParams.objByPassMap.containsKey('Event_Utility.createActivityClientRelation')){
            Event_Utility.createActivityClientRelation(triggerParams);
        }
        //US28000-END
    }
    
    protected override void afterUpdate(){
        //Existing logic. Need to add bypass logic to this method
        existingLogic();
        if((triggerParams.objByPassMap.containsKey('Event_Utility.createGnB') && !triggerParams.objByPassMap.get('Event_Utility.createGnB')) || 
                !triggerParams.objByPassMap.containsKey('Event_Utility.createGnB')){
            Event_Utility.createGnB(triggerParams);
        }
        //US28000-START
        if((triggerParams.objByPassMap.containsKey('Event_Utility.createActivityClientRelation') && !triggerParams.objByPassMap.get('Event_Utility.createActivityClientRelation')) || 
                !triggerParams.objByPassMap.containsKey('Event_Utility.createActivityClientRelation')){
            Event_Utility.createActivityClientRelation(triggerParams);
        }
        //US28000-END

    }
    
    protected override void beforeDelete(){
        if((triggerParams.objByPassMap.containsKey('Event_Utility.validateDelete') && !triggerParams.objByPassMap.get('Event_Utility.validateDelete')) || 
                !triggerParams.objByPassMap.containsKey('Event_Utility.validateDelete')){
            Event_Utility.validateDelete(triggerParams);
        }
    }
        
    protected override void afterDelete(){
        //Existing logic. Need to add bypass logic to this method
        existingLogic();        

    }
    
    protected override void afterUnDelete(){
        //Existing logic. Need to add bypass logic to this method
        existingLogic();

    }
    
    public void existingLogic(){
        Event_Utility util = Event_Utility.getInstance();
        Map<Id, Event> oldMap = (Map<Id, Event>) triggerParams.oldMap;
        List<Event> newList = (List<Event>) triggerParams.newList;
        List<Event> oldList = (List<Event>) triggerParams.oldList;
        
        if (triggerParams.isAfter && (triggerParams.isInsert || triggerParams.isUpdate)) {
            List<Event> evtList = [Select Id, whoid,Contact_Name__c,Client_Name__c, 
                                   subject, type__c, sub_type__c, ActivityDate from Event WHERE Id IN: triggerParams.newMap.keySet()];
                        
            List<Event> evtList2 = new List<Event>();
            //loop through events and check if who is changed
            for (Event evt : evtList) {
                /* Note: for insert, check current state, 
                    for update, check current state and prior state */
                if ((evt.whoid != null) && (triggerParams.isInsert || (triggerParams.isUpdate && evt.whoid != oldMap.get(evt.id).whoid)))
                    evtList2.add(evt); 
            }
            
            //send eligible records to Event_Utility
            if (!evtList2.isEmpty()) 
                util.UpdateEventCustomFields(evtList2);
    
        }
        
        /********** Code block to roll-up actual cost and budgeted cost to Campaign from events **********/
        Set<Id> set_campaginId = new Set<Id>();
        Map<Id, List<Event>> mapOfCampaignAndEvent = new Map<Id, List<Event>>();
        if(triggerParams.isAfter && !triggerParams.isDelete){
            for(Event o_event:newList){
                // to identify if the event is related to Campaign
                if(o_event.whatId != NULL && CommonUtilities.getObjectName(o_event.whatId) == 'Campaign'){
                    if((triggerParams.isInsert || triggerParams.isUndelete) 
                        && (o_event.Actual_Cost__c != null || o_event.Budgeted_Cost__c != null)){ 
                        set_campaginId.add(o_event.whatId); 
                        if(mapOfCampaignAndEvent.containsKey(o_event.whatId)){
                            mapOfCampaignAndEvent.get(o_event.whatId).add(o_event);
                        }else{
                            mapOfCampaignAndEvent.put(o_event.whatId , new List<Event>{o_event});
                        }                                  
                    }else if(triggerParams.isUpdate && 
                        (o_event.whatId != oldMap.get(o_event.id).whatId ||
                         o_event.Actual_Cost__c != oldMap.get(o_event.id).Actual_Cost__c ||
                         o_event.Budgeted_Cost__c != oldMap.get(o_event.id).Budgeted_Cost__c ||
                         o_event.currencyisocode != oldMap.get(o_event.id).currencyisocode                      
                        )){
                        set_campaginId.add(o_event.whatId);
                        set_campaginId.add(oldMap.get(o_event.id).whatId);
                        if(mapOfCampaignAndEvent.containsKey(o_event.whatId)){
                            mapOfCampaignAndEvent.get(o_event.whatId).add(o_event);
                        }else{
                            mapOfCampaignAndEvent.put(o_event.whatId , new List<Event>{o_event});
                        } 
                    } 
                }
            }
        }else if(triggerParams.isAfter && triggerParams.isDelete){
            for(Event o_event: oldList){
                if(o_event.whatId != NULL && CommonUtilities.getObjectName(o_event.whatId) == 'Campaign'){
                    set_campaginId.add(o_event.whatId);
                    if(mapOfCampaignAndEvent.containsKey(o_event.whatId)){
                        mapOfCampaignAndEvent.get(o_event.whatId).add(o_event);
                    }else{
                        mapOfCampaignAndEvent.put(o_event.whatId , new List<Event>{o_event});
                    } 
                }
            }
        }
        
        //pass list of campagin ids for which roll up needs to be re calculated 
        if (!set_campaginId.isEmpty()) 
            util.rollUpCostFieldsToCampaign(set_campaginId, mapOfCampaignAndEvent);
        
        /********** End of code block to roll-up actual cost and budgeted cost to Campaign from events **********/
    }
}