/**************************************************************************************
 Name        :    TaskTriggerHandler
 Description :    Trigger Handler for Task
 Created By  :    Deloitte
 Created On  :    31 August 2016
 --------------------------------------------------------------------------------------
 Modification Log
 -----------------
 Name                Date            Comments
 -----------------   -------------   --------------------------------------------------
 Hemangini          31-Aug-2016      Created
 --------------------------------------------------------------------------------------
 Review Log
 ----------    
 Name                Date            Comments
 -----------------   -------------   --------------------------------------------------
 
***************************************************************************************/
public class TaskTriggerHandler extends TriggerHandler{
    
    protected override void afterInsert(){
        //Existing logic. Need to add bypass logic to this method
        existingLogic();
    }
    
    protected override void afterUpdate(){
        //Existing logic. Need to add bypass logic to this method
        existingLogic();  
    }
    
    protected override void beforeDelete(){
        
    }
        
    protected override void afterDelete(){
        
    }
    
    protected override void afterUnDelete(){
        
    }
    
    public void existingLogic(){
        Task_Utility util;
        Map<Id, Task> oldMap = (Map<Id, Task>) triggerParams.oldMap;
        
        
        if (triggerParams.isAfter && (triggerParams.isInsert || triggerParams.isUpdate)) {
            List<Task> tskList = [Select Id, whoid,Contact_Name__c,Client_Name__c from Task WHERE Id IN: triggerParams.newMap.keySet()];
            util = Task_Utility.getInstance();
            
            List<Task> tskList2 = new List<Task>();
            //loop through Tasks and check if who is changed
            for (Task tsk : tskList) {
                /* Note: for insert, check current state, 
                    for update, check current state and prior state */
                if ((tsk.whoid != null) && (triggerParams.isInsert || 
                        (triggerParams.isUpdate && tsk.whoid != oldMap.get(tsk.id).whoid)))
                    tskList2.add(tsk); 
            }
            
            //send eligible records to Task_Utility
            if (!tskList2.isEmpty()) 
                util.UpdateTaskCustomFields(tskList2);    
        }
    }
}