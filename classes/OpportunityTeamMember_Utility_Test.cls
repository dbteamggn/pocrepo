@isTest
public class OpportunityTeamMember_Utility_Test{
     
    /*
     * 
     * Description  : creates opportunities for test
     * Param        : 
     * Returns      :  
    */
    public static opportunity createOpportunity() {
        
        //create account
        account acc = CommonTestUtils.CreateTestClient('Test Client');
        insert acc;
        
        //create opportunity
        opportunity testOpp = new Opportunity(name='Test Opportunity', accountId=acc.id, stagename='New', CloseDate=system.today());

        return testOpp;
    }
    
    static testMethod void beforeInsertBeforeUpdateTest(){
        
        OpportunityTeamMember oppTeamMember1, oppTeamMember2, oppTeamMember3, oppTeamMember4;
        opportunity opp = createOpportunity();
        List<User> userList = new List<User>();
        user usr;
        
        //creating Users
        userList = CommonTestUtils.createMultipleUsers(UserInfo.getProfileId(), 5);
        
        Test.startTest();
        
        //Inserting data
        insert userList;
        insert opp;
        
        //creating OpportunityTeamMembers - START
        
        oppTeamMember1 = new OpportunityTeamMember( OpportunityAccessLevel='Read', OpportunityId = opp.id, TeamMemberRole = 'Service', UserId = userList[0].id); //non-primary
        insert oppTeamMember1;
        
        oppTeamMember1.primary__c = true; //updating non-primary to primary
        update oppTeamMember1;
        
        oppTeamMember2 = new OpportunityTeamMember( OpportunityAccessLevel='Read', OpportunityId = opp.id, TeamMemberRole = 'Senior Sales', UserId = userList[1].id); //default primary role, this should become primary
        insert oppTeamMember2;
        
        oppTeamMember3 = new OpportunityTeamMember( OpportunityAccessLevel='Read', OpportunityId = opp.id, TeamMemberRole = 'Senior Sales', UserId = userList[2].id, primary__c=true); //as primary member has role of senior sales, error to be thrown
        Database.Saveresult sr1 = database.insert (oppTeamMember3, false);
        if(!sr1.isSuccess()){
            System.assertEquals(sr1.getErrors()[0].getMessage(), 'Primary Opportunity Team Member already exists.');
        }
        
        oppTeamMember4 = new OpportunityTeamMember( OpportunityAccessLevel='Read', OpportunityId = opp.id, TeamMemberRole = 'Associate', UserId = userList[3].id, primary__c=true); //primary already exists, error to be thrown
        Database.Saveresult sr2 = database.insert (oppTeamMember4, false);
        if(!sr2.isSuccess()){
            System.assertEquals(sr2.getErrors()[0].getMessage(), 'Primary Opportunity Team Member already exists.');
        }
        //creating OpportunityTeamMembers - END
        
        System.assertEquals(true, [select primary__c from OpportunityTeamMember where id =: oppTeamMember2.id].primary__c);
        Test.stopTest();
    }
    
        static testMethod void beforeDeleteTest(){
        
        OpportunityTeamMember oppTeamMember1;
        opportunity opp = createOpportunity();
        List<User> userList = new List<User>();
        
        //creating Users
        userList = CommonTestUtils.createMultipleUsers(UserInfo.getProfileId(), 1);
        
        Test.startTest();
        
        //Inserting data
        insert userList;
        insert opp;
        
        //creating OpportunityTeamMember        
        oppTeamMember1 = new OpportunityTeamMember( OpportunityAccessLevel='Read', OpportunityId = opp.id, TeamMemberRole = 'Service', UserId = userList[0].id, primary__c=true); //primary
        insert oppTeamMember1;
        
        //deleting OpportunityTeamMember
        delete oppTeamMember1;
        
        Test.stopTest();
    }
 }