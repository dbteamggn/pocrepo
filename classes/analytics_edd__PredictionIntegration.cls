/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PredictionIntegration {
    global PredictionIntegration() {

    }
    @Future(callout=true)
    global static void setPrediction(String configName, String fieldsJson) {

    }
    global static void setPredictionNow(String configName, String fieldsJson) {

    }
}
