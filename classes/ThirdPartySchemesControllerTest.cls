/*----------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            ThirdPartySchemesControllerTest
Description:     Test Methods to test the Trigger which assigns schemes related to third party

Date             Author                             Summary of Changes                  
-----------      -----------------                  -------------------------------------------------------------------------------------
06 Oct 2016      Dilipkumar Nithyanandam             Initial Release 
------------------------------------------------------------------------------------------------------------------------------------------------------------*/

@isTest
public class ThirdPartySchemesControllerTest {
    /*
    * Description : Create the data needed for the test method
    * Param : Third Party record Id
    * Returns :  Related Schemes
    */
    public static Id createData(){
        // Client Creation
        Account client = CommonTestUtils.CreateTestClient('ABC Consulting');
        insert client;
        
        //Thirdparty Creation
        Account thirdParty = CommonTestUtils.CreateTestThirdParty('Shell Corp');
        insert thirdParty;
        
        //Associating Created client with created thirdparty
        Client_Third_Party_Relationship__c clientThirdParty = CommonTestUtils.CreateTestClientThirdPartyAss(client.Id, thirdParty.Id);
        insert clientThirdParty;
        
        //Creating a list of four schemes for the created client
        List<Pools_of_Capital__c> testSchemes = new List<Pools_of_Capital__c>();
        Pools_of_Capital__c testScheme = new Pools_of_Capital__c();
        for(Integer iter=0;iter<3;iter++){
            testScheme= CommonTestUtils.createTestScheme(client);
            testSchemes.add(testScheme);
        }
        
        //Associating Schmes with Thirdparty
        //Associating Thirdparty1->Scheme1, Thirdparty2->Scheme2,Thirdparty3->Scheme1,Thirdparty4->Scheme2
        List<Scheme_Third_Party_Relationship__c> allStps = new List<Scheme_Third_Party_Relationship__c>();
        Scheme_Third_Party_Relationship__c stp = new Scheme_Third_Party_Relationship__c();
        for(Integer iter=1;iter<7;iter++){
            if(Math.mod(iter,3)==1)
                stp = CommonTestUtils.createTestSchemeThirdPartyAss(testSchemes.get(0),clientThirdParty);
            else if (Math.mod(iter,3)==2)
                stp = CommonTestUtils.createTestSchemeThirdPartyAss(testSchemes.get(1),clientThirdParty);
            else
                stp = CommonTestUtils.createTestSchemeThirdPartyAss(testSchemes.get(2),clientThirdParty);
            allStps.add(stp); 
        }
        insert allStps;
        
        //Returning the thirdparty id to query and assert in test method
        return thirdParty.Id;
    }
    /*
    * Description : Test method for getRelatedSchemes()
    * Param : 
    * Returns :  
    */
    public static testMethod void getRelatedSchemes(){
        //Getting the Id of third party to query
        Id thirdParty = createData(); 
        String recordID = String.valueOf(thirdParty);
        List<Client_Third_Party_Relationship__c> clientThirdPartyList = new List<Client_Third_Party_Relationship__c>();
        Test.startTest();
        //Getting all the schemes for this particular thirdparty
        clientThirdPartyList = [select id, (select Scheme__r.id,Scheme__r.name, Scheme__r.Client_Name__r.Name, Scheme__r.Scheme_Type__c from Scheme_Third_Party_Relationship__r) from Client_Third_Party_Relationship__c where Third_Party_Name__r.id =: recordID]; 
        //Creating a Response object which has the list of all schemes and details
        ThirdPartySchemesController.Response response = new ThirdPartySchemesController.Response();
        //Calling the method to be tested
        response = ThirdPartySchemesController.getRelatedSchemes(recordID); 
        Test.stopTest();
        //No. of schemes assocaited with the client and thirdparty must be eqaul to the schemecount in the response object
        //System.assertEquals(clientThirdPartyList.size(), response.schemeCount);
    }
}