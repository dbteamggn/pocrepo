@isTest
public class BatchGiftBenefitRollUpOnConTest {

    /* 
     * Description : Method to validate the amount roll up on 
     *               Contact from Gifts and benefits  
    */
    static testMethod void testRollUpOnCOntact(){
        User user = CommonTestUtils.createTestUser(); 
        system.runAs(user){       
        Contact Con = CommonTestUtils.CreateTestContact('test');
        insert con;
        Account acc = CommonTestUtils.CreateTestClient('testacc');
        insert acc;
        List<Gift_Benefit__c>  listOfGB = new List<Gift_Benefit__c>();
        Gift_Benefit__c obj = CommonTestUtils.createTestGiftBenefit(con, acc);
        obj.Date__c = system.today();
        obj.Given_Received__c = 'Given';
        listOfGB.add(obj);
        
        // G&B which not for current year
        obj = CommonTestUtils.createTestGiftBenefit(con, acc);
        obj.Date__c = system.today().addYears(-4);
        obj.Given_Received__c = 'Given';
        listOfGB.add(obj);
        
        obj = CommonTestUtils.createTestGiftBenefit(con, acc);
        obj.Date__c = system.today();
        obj.Given_Received__c = 'Received';
        listOfGB.add(obj);
        
        obj = CommonTestUtils.createTestGiftBenefit(con, acc);
        obj.Date__c = system.today();
        obj.Given_Received__c = 'Given';
        listOfGB.add(obj);
        insert listOfGB;
        // test batch class
        Test.StartTest();
        Database.executeBatch( new BatchGiftBenefitRollUpOnCon());
        Test.StopTest();
        con = [SELECT ID, Given_Amount__c , Received_Amount__c FROM Contact WHERE id =:con.id];
        system.assertEquals(1000, con.Received_Amount__c);
        system.assertEquals(2000, con.Given_Amount__c);
        }
    }
    
    /* 
     * Description : Method to validate the scheduler
    */
    static testMethod void testRollUpOnCOntactScheduler(){
        User user = CommonTestUtils.createTestUser(); 
        System.runAs(user){ 
            //Test the Scheduler Class
            Test.StartTest();
            string jobid = System.schedule('Batch Test', '0 0 0 1 1 ?', new BatchGiftBenefitRollUpOnConSchedule());
            Test.StopTest(); 
            CronTrigger ct = [select id, CronExpression, TimesTriggered, NextFireTime from CronTrigger where id = :jobId];      
            System.assertEquals('0 0 0 1 1 ?', ct.CronExpression); 
        }
    }
}