/*----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            UtilsFinAum.class
   Description:     utilities for dealing with Assets_Under_Management, Fund_Flows and Platform_Splits
                    
                    
   Date             Author                             Summary of Changes                  
   -----------      -----------------                  -------------------------------------------------------------------------------------
   Sep 2016         Andee Weir/Andy Wallis             US33690 - Create 3 views for the Financial Summary Section.  
   
--------------------------------------------------------------------------------------------------------------------------------------------*/

public without sharing class UtilsFinAum {
    
    public static map<String, RecordType> RecTypesMap {
        get {
            if (RecTypesMap == null) RecTypesMap = getRecTypesMap();
            return RecTypesMap;
        }
        private set;
    }

    public static map<String, RecordType> getRecTypesMap() {
        
        map<String, RecordType> recTypes = new map<String, RecordType>();
        
        list<string> rqdRecordTypes = new string[]{'Assets_Under_Management__c', 'Fund_Flow__c', 'Platform_Split__c'};
        
        list<RecordType> rts = [SELECT Id, DeveloperName, SobjectType FROM RecordType WHERE SObjecttype in :rqdRecordTypes];

        for (recordtype rt : rts) {
            if (rt.DeveloperName == 'Standard'){
                if (rt.SobjectType == 'Assets_Under_Management__c'){
                    recTypes.put('StandardAUM', rt);
                    //rtStandardAum = rt;
                } else if (rt.SobjectType == 'Fund_Flow__c'){
                    //rtStandardFlow = rt;
                    recTypes.put('StandardFlow', rt);
                    
                } else {
                    //rtStandardPlatSplit = rt;
                    recTypes.put('StandardPlatSplit', rt);
                }
            }
            else if (rt.DeveloperName == 'Summary'){
                if (rt.SobjectType == 'Assets_Under_Management__c'){
                    
                    recTypes.put('SummaryAUM', rt);
                    //rtSummaryAum = rt;
                } else if (rt.SobjectType == 'Fund_Flow__c'){
                    
                    recTypes.put('SummaryFlow', rt);
                    //rtSummaryFlow = rt;
                } else {
                    
                    recTypes.put('SummaryPlatSplit', rt);
                    //rtSummaryPlatSplit = rt;
                }
            }
            else if (rt.DeveloperName == 'Region_Summary'){
                if (rt.SobjectType == 'Assets_Under_Management__c'){
                    
                    recTypes.put('RegionSummaryAUM', rt);
                } else if (rt.SobjectType == 'Fund_Flow__c'){
                    
                    recTypes.put('RegionSummaryFlow', rt);
                } else {
                    
                    recTypes.put('RegionSummaryPlatSplit', rt);
                }
            }
            else if (rt.DeveloperName == 'Total_Summary'){
                if (rt.SobjectType == 'Assets_Under_Management__c'){
                    
                    recTypes.put('TotalSummaryAUM', rt);
                } else if (rt.SobjectType == 'Fund_Flow__c'){
                    
                    recTypes.put('TotalSummaryFlow', rt);
                } else {
                    
                    recTypes.put('TotalSummaryPlatSplit', rt);
                }
            }
        }
        
        return recTypes;
    }
    


}