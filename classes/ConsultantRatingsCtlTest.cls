@isTest
public class ConsultantRatingsCtlTest {
  /*
     * 
     * Description : Check that a past Event (End date earlier than today but later than the last batch run) 
     *               updates the contact that is associated with it.
     * Param : 
     * Returns :  
    */
    static testMethod void TestAuraFunctionReturns(){
        User user = CommonTestUtils.createTestUser();        
        
        System.runAs(user){
            
            list<account> accounts = new list<account>();
            
            // Add 250 third Parties
            for (integer i=0; i<250; i++){
                Account tp = CommonTestUtils.CreateTestThirdParty('Third Party ' + (i<10?'00'+ i:(i<100)?'0'+i: '' + i));
              accounts.add(tp);
            }
            
            insert accounts;
            
            // Now create some Investment Strategies
            list<Investment_Strategy__c> investmentStrategies = new list<Investment_Strategy__c>();
            
            for (integer i=0; i<10; i++){
                Investment_Strategy__c p = CommonTestUtils.CreateTestInvestmentStrategy('InvStrategy ' + (i<10?'00'+ i:(i<100)?'0'+i: '' + i));
              investmentStrategies.add(p);
            }
            insert investmentStrategies;
            
            // Now create some Rated Strategy records that give a third party's rating
            // to a product
            list<String> currentRatings = new list<String>{'Buy+',
                                        'Buy',
                                        'Hold',
                                        'Res+',
                                        'Res',
                                        'DU',
                                        'Sell',
                            'Discovery',
                            'Not Rated'};
            list<rated_strategy__c> ratings = new list<rated_strategy__c>();
            for (integer i=0; i<currentRatings.size(); i++){
                rated_strategy__c rating = new rated_strategy__c();
                rating.investment_strategy__c = investmentStrategies[i].id;
                rating.third_party__c = accounts[i].id;
                rating.current_rating__c = currentRatings[i];                 
                rating.comments__c = 'Some comments ' + i;
                ratings.add(rating);
            }
            insert ratings;
            
            list<User_Setting__c> userSettings = new list<User_Setting__c>();
            for (integer i=0; i<10; i++){
                User_Setting__c userSetting = new User_Setting__c();
                userSetting.user__c = UserInfo.getUserId();
                userSetting.setting__c = accounts[i*2].id;      // Only do every second one 
                userSetting.RecordTypeId = User_Setting__c.sObjectType.getDescribe().getRecordTypeInfosByName().get('Followed Third Parties').getRecordTypeId();
                userSettings.add(userSetting);
            }
            for (integer i=0; i<10; i++){
                User_Setting__c userSetting = new User_Setting__c();
                userSetting.user__c = UserInfo.getUserId();
                userSetting.setting__c = investmentStrategies[i].id;    // do the first 10   
                userSetting.RecordTypeId = User_Setting__c.sObjectType.getDescribe().getRecordTypeInfosByName().get('Followed Strategies').getRecordTypeId();
                userSettings.add(userSetting);
            }
            
            insert userSettings;
            
            // perform the test 
            Test.startTest();
            list<Account> thirdPartyResults = ConsultantRatingsCtl.GetThirdParties();
            list<Investment_Strategy__c> strategyResults = ConsultantRatingsCtl.GetStrategies();
            map<string, string> strategyRatingResults = ConsultantRatingsCtl.GetStrategyRating();
            Test.stopTest();
            
            // Check that the correct number & value of third parties have been returned
            system.assertEquals(10, thirdPartyResults.size());
            for (integer i=0; i<thirdPartyResults.size(); i++){
                integer j = i*2;
                system.assertEquals('Third Party ' + (j<10?'00'+ j:(i<100)?'0'+j: '' + j), thirdPartyResults[i].name);
            }
            
            // Check that the correct number & value of strategies have been returned
            system.assertEquals(10, strategyResults.size());
            for (integer i=0; i<strategyResults.size(); i++){
                system.assertEquals('InvStrategy ' + (i<10?'00'+ i:(i<100)?'0'+i: '' + i), strategyResults[i].name);
            }
            
            // Check that the correct number & value of Strategy Ratings have been returned
            system.assertEquals(9, strategyRatingResults.size());
            for (integer i=0; i<strategyRatingResults.size(); i++){
                string currentRatingResult = strategyRatingResults.get(accounts[i].id+ ':' + investmentStrategies[i].id);
                system.assertEquals(currentRatings[i], currentRatingResult);
            }      
            
            // Delete the first two Third Party references & check that they are correctly removed
            string thirdPartyRefsToDelete = accounts[0].id + ':' + accounts[2].id;
            list<ConsultantRatingsCtl.ThirdPartyWrapper> returnedThirdPartyIds = ConsultantRatingsCtl.DeleteThirdPartyRefs(thirdPartyRefsToDelete);
            
            system.assertEquals(8, returnedThirdPartyIds.size());
            for (integer i=0; i<returnedThirdPartyIds.size(); i++){
                integer j = (i+2)*2;  // first 2 should now be deleted so '+2'.  '*2' as only referencing every second one
                system.assertEquals('Third Party ' + (j<10?'00'+ j:(i<100)?'0'+j: '' + j), returnedThirdPartyIds[i].acc.name);
            }
            
            
            // Delete the first three Strategy references & check that they are correctly removed
            string strategyRefsToDelete = investmentStrategies[0].id + ':' + investmentStrategies[1].id + ':' + investmentStrategies[2].id;
            list<ConsultantRatingsCtl.StrategyWrapper> returnedStrategyIds = ConsultantRatingsCtl.DeleteStrategyRefs(strategyRefsToDelete);

            system.assertEquals(7, returnedStrategyIds.size());
            for (integer i=0; i<returnedStrategyIds.size(); i++){
                integer j = i+3;  // first 3 should now be deleted so '+3'.  
                system.assertEquals('InvStrategy ' + (j<10?'00'+ j:(i<100)?'0'+j: '' + j), returnedStrategyIds[i].investmentStrategy.name);
            }
            
            // Search third parties on the name 'Third Party 00'. Seven should be returned - '000, 001, 002, 003, 005, 007, 009'
            list<Account> thirdPartySearchResults = ConsultantRatingsCtl.GetThirdPartySearchResults('Third Party 00');
            system.assertEquals(7, thirdPartySearchResults.size());
            list<string> expectedthirdPartyNames = new list<string>{'000','001','002','003','005','007','009'};
            for (integer i=0; i<expectedthirdPartyNames.size(); i++){
              system.assertEquals('Third Party ' + expectedthirdPartyNames[i], thirdPartySearchResults[i].name);        
            }
            
            // Add the first returned third party reference
            list<ConsultantRatingsCtl.ThirdPartyWrapper> thirdPartyAddResults = ConsultantRatingsCtl.AddThirdPartyRef(thirdPartySearchResults[0].id);
            system.assertEquals(9, thirdPartyAddResults.size());
            
            // Search strategies on the name 'InvStrategy 00'. Three should be returned - '000, 001, 002'
            list<Investment_Strategy__c> strategySearchResults = ConsultantRatingsCtl.GetStrategySearchResults('InvStrategy 00');
            system.assertEquals(3, strategySearchResults.size());
            list<string> expectedStrategyNames = new list<string>{'000','001','002'};
            for (integer i=0; i<expectedStrategyNames.size(); i++){
              system.assertEquals('InvStrategy ' + expectedStrategyNames[i], strategySearchResults[i].name);        
            }
            
            // Add the first returned strategy reference
            list<ConsultantRatingsCtl.StrategyWrapper> strategyAddResults = ConsultantRatingsCtl.AddStrategyRef(strategySearchResults[0].id);
            system.assertEquals(8, strategyAddResults.size());
            
            
            
        }
    }
    
    
    /* 
     * Description : Method to test the Consultant Rating defaults functionality
     */
    static testMethod void TestConsultantRatingDefaults(){
        User user = CommonTestUtils.createTestUser(true);        
        
        System.runAs(user){
            list<account> accounts = new list<account>();
            
            // Add third Parties
            for (integer i=0; i<10; i++){
                Account tp = CommonTestUtils.CreateTestThirdParty('Third Party ' + '00' + i);
                accounts.add(tp);
            }           
            insert accounts;
            
            // Create some Investment Strategies
            list<Investment_Strategy__c> investmentStrategies = new list<Investment_Strategy__c>();
            
            for (integer i=0; i<10; i++){
                Investment_Strategy__c p = CommonTestUtils.CreateTestInvestmentStrategy('InvStrategy ' + '00' + i);
                investmentStrategies.add(p);
            }
            insert investmentStrategies;
            
            // Now create some Rated Strategy records that give a third party's rating
            // to a product
            list<String> currentRatings = new list<String>{'Buy+',
                                        'Buy',
                                        'Hold',
                                        'Res+',
                                        'Res',
                                        'DU',
                                        'Sell',
                            'Discovery',
                            'Not Rated'};
            list<rated_strategy__c> ratings = new list<rated_strategy__c>();
            for (integer i=0; i<currentRatings.size(); i++){
                rated_strategy__c rating = new rated_strategy__c();
                rating.investment_strategy__c = investmentStrategies[i].id;
                rating.third_party__c = accounts[i].id;
                rating.current_rating__c = currentRatings[i]; 
                rating.comments__c = 'Some comments ' + i;
                ratings.add(rating);
            }
            insert ratings;
            
            //Insert Default values in the custom setting
            List<Consultant_Ratings_Default_Config__c> defaults = new List<Consultant_Ratings_Default_Config__c>();
            
            for(integer i=0; i<currentRatings.size(); i++){
                defaults.add(new Consultant_Ratings_Default_Config__c(Name = accounts[i].id, Record_Name__c = accounts[i].name, Type__c = 'Third_Party'));
                defaults.add(new Consultant_Ratings_Default_Config__c(Name = investmentStrategies[i].id, Record_Name__c = investmentStrategies[i].name, Type__c = 'Investment_Strategy'));
            }
            insert defaults;
            
            //perform Test
            Test.startTest();
            list<Account> thirdPartyResults = ConsultantRatingsCtl.GetThirdParties();
            list<Investment_Strategy__c> strategyResults = ConsultantRatingsCtl.GetStrategies();
            map<string, string> strategyRatingResults = ConsultantRatingsCtl.GetStrategyRating();
            Test.stopTest();
            
            //verify results
            system.assertEquals(currentRatings.size(), thirdPartyResults.size());
            system.assertEquals(currentRatings.size(), strategyResults.size());
            system.assertEquals(currentRatings.size(), strategyRatingResults.size());
        }
    }
    
    static testMethod void testGetEvents(){
        ConsultantRatingsCtl testInst = new ConsultantRatingsCtl();
        ConsultantRatingsCtl.GetThirdPartyWrappers();
        ConsultantRatingsCtl.GetStrategyWrappers();
        
        Account acc = CommonTestUtils.CreateTestThirdParty('Test Account'); 
        acc.Billing_City__c = 'Billing City';
        insert acc;
        Account acc1 = CommonTestUtils.CreateTestThirdParty('Test');
        acc1.Billing_City__c='Billing01 City';
        insert acc1;
        List<Contact> cntList = new List<Contact>{
                                    new Contact(LastName = 'Cnt 1', AccountId = acc.Id,status__c='Active'),
                                    new Contact(LastName = 'Cnt 2', AccountId = acc1.Id,status__c='Active')};
        insert cntList;
        Event anEvnt = new Event(StartDateTime = System.now(), EndDateTime = System.now().addMinutes(30), Subject = 'Test', WhoId = cntList[0].Id, WhatId = acc.Id);
        insert anEvnt;
        EventRelation er = new EventRelation(RelationId = cntList[1].Id, EventId = anEvnt.Id, isParent = true);
        insert er;
        Activity_Alias__c act = new Activity_Alias__c(Activity_Id__c = anEvnt.Id);
        insert act;
        anEvnt.Strategies_Funds__c = act.Id;
        update anEvnt;
        Investment_Strategy__c ivStrat = new Investment_Strategy__c(Name = 'Test strat');
        insert ivStrat;
        Activity_Product_Relationship__c actPr = new Activity_Product_Relationship__c(Investment_Strategy__c = ivStrat.Id, Activity_Alias__c = act.Id);
        insert actPr;
        List<ConsultantRatingsCtl.EventWrapper> testList = ConsultantRatingsCtl.getEvents(acc.Id, ivStrat.Id);
        System.assertEquals(1, testList.size());
    }
}