public class createEventController{
    public String subject;  //Testing
    public dateTime startDateTime;    //2016-07-28T19:00:00.000Z
    public dateTime endDateTime;  //2016-07-30T19:00:00.000Z
    public boolean allDayEvent;
    public cls_selectedContact[] selectedContact;
    public cls_selectedFund[] selectedFund;
    public String location; //Test
    public id whatid;
    public id whoId;
    public String type;
    public String subType;
    public String givenReceived;
    public Decimal amt;
    public String currencyType;
    public Id ownerId;
    public String costType;
    public String description;
    public Boolean gnb;
    
    class cls_selectedContact {
        public String id;   //0032800000VIJ5cAAH
        public String name; //Rose Gonzalez
    }
    class cls_selectedFund {
        public String id;   
        public String name; 
    }
    public static createEventController parse(String json){
        return (createEventController) System.JSON.deserialize(json, createEventController.class);
    }
    @AuraEnabled
    public static id createEvent(string param){
        system.debug('**********param: '+param);
        Boolean gnb = false;
        createEventController temp = parse(param);
        system.debug('**********temp: '+temp);
        if(temp.gnb != NULL && Boolean.valueOf(temp.gnb)){
            gnb = true;
        } else{
            gnb = false;
        }
        
        Event e=new Event();
        e.subject=temp.subject;
        e.startDateTime=dateTime.valueof(temp.startDateTime);
        System.debug(temp.startDateTime);
        e.endDateTime=dateTime.valueof(temp.endDateTime);
        Activity_Alias__c alias=new Activity_Alias__c();
        
        if(!gnb){
            e.IsallDayEvent=temp.allDayEvent;
            e.location=temp.location;
            if(temp.whatId!=null){
                e.whatId=temp.whatId;
            }
            if(temp.OwnerId != NULL){
                e.OwnerId = temp.OwnerId;
            }
            System.debug('temp.selectedContact'+temp.selectedContact);
            if(temp.selectedContact.size()>0)
                e.WhoId=temp.selectedContact[0].id;
            
            alias.Contact__c = e.WhoId;
            insert alias;
            e.Strategies_Funds__c=alias.id;
        }
        System.debug('**********temp results :'+temp);
        System.debug('**********owner ID :'+temp.OwnerId);
        if(gnb){
            e.Gift__c = true;
            e.Description = temp.description;
            e.GBE_Type__c = temp.type;
            e.GBE_Sub_Type__c = temp.subType;
            e.CurrencyISOCode = temp.currencyType;
            e.Given_Received__c = temp.givenReceived;
            e.Total_Gift_Amount__c = temp.amt;
            e.Whoid = temp.whoid;
            if(temp.OwnerId != NULL){
                e.OwnerId = temp.OwnerId;
            }
            e.Cost_Type__c = temp.costType;
        }
        insert e;
        EventRelation er= new EventRelation ();
        List<EventRelation> erList = new List<EventRelation>();
        for(cls_selectedContact  tempc : temp.selectedContact){
            if(tempc.id!=e.WhoId){
                er= new EventRelation ();
                er.RelationId=tempc.id;
                er.EventId=e.id;
                er.isParent=true;
                erList.add(er);
            }
        }
        System.debug(erList);
        insert erList;
        
        if(!gnb){
            //insert Funds/Strategies
            Activity_Product_Relationship__c actPro = new Activity_Product_Relationship__c();
            List<Activity_Product_Relationship__c> apList = new List<Activity_Product_Relationship__c>();
            System.debug('************temp.selectedFund: '+temp.selectedFund);
            for(cls_selectedFund  tempsf : temp.selectedFund){
                    actPro= new Activity_Product_Relationship__c ();
                    actPro.Activity_Alias__c=alias.id;
                    if(Id.valueOf(tempsf.id).getSObjectType() == Schema.Product2.SObjectType)
                        actPro.fund__c=tempsf.id;
                    else if(Id.valueOf(tempsf.id).getSObjectType() == Schema.Investment_Strategy__c.SObjectType)
                        actPro.Investment_Strategy__c=tempsf.id;
                    apList.add(actPro);
            }
            insert apList;
        } else{
            //update the event to ensure EventRelation records are added as G&B records.
            update e;
        }
        return e.id;
    }
    
    @AuraEnabled
    public static List<String> getPicklistValues(String fieldName){
        List<String> options = new List<String>();       
        Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get('Event').getDescribe().fields.getMap();
        system.debug('Value: ' + objectFields);
        system.debug('***' + fieldName.toLowerCase());
        system.debug('***' + objectFields.get(fieldName.toLowerCase()));
        List<Schema.PicklistEntry> ple = objectFields.get(fieldName.toLowerCase()).getDescribe().getPicklistValues();
        for( Schema.PicklistEntry f : ple){
            options.add(f.getValue());
        }
        system.debug('***' + fieldName + ': ' + options);
        return options;
    }
}