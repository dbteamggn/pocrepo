/**************************************************************************************
 Name        :    AddContactsToCampaignController
 Description :    Controller class for visualforce page AddContactsToCampaign
 Created By  :    Deloitte
 Created On  :    27 September 2016
 --------------------------------------------------------------------------------------
 Modification Log
 -----------------
 Name                Date            Comments
 -----------------   -------------   --------------------------------------------------
 Hemangini           27-Sep-2016      Created
 --------------------------------------------------------------------------------------
 Review Log
 ----------    
 Name                Date            Comments
 -----------------   -------------   --------------------------------------------------
 
***************************************************************************************/
public with sharing class AddContactsToCampaignController{
    
    public List<SelectContactWrapper> selectedContactList{get;set;}
    public Map<String,Add_to_Campaign_Config__c> campaignCustSettMap = new Map<String,Add_to_Campaign_Config__c>();
    public List<SelectOption> allReportNames{get;set;}
    public String selectedReportName{get;set;}
    public SObject sObjectInstance{get;set;}
    public Map<String, boolean> fieldFiltersMap{get;set;}
    public String selectedMemberStatus{get;set;}
    public List<SelectOption> allMemberStatus{get;set;}
    public boolean getHasContacts(){
        return ((selectedReportName != null && selectedReportName != '' && selectedContactList !=null && selectedContactList.size() > 0));
    }
    
    List<Contact> allContactList = new List<Contact>();
    public String sObjectName{get;set;}
    Campaign campaignRecord;
    
    
    public AddContactsToCampaignController(ApexPages.StandardController stdController) {
        this.campaignRecord = (Campaign)stdController.getRecord();
        
        getReportDetails();
        getMemberStatusValues();
    }
    
    /*
     * Description : Method to get report names from the custom setting
     */
    public void getReportDetails(){
        allReportNames = new List<SelectOption>();
        String networkPlatformObjNameStr = 'Network_Platform_Relationship__c';
        boolean isInstitutionalUser = false;
        if(UserInfo.getProfileId() == System.Label.Inst_APAC_Profile_Id || UserInfo.getProfileId() == System.Label.Inst_EMEA_Profile_Id || UserInfo.getProfileId() == System.Label.Inst_NA_Profile_Id){
            isInstitutionalUser = true;
        }
        
        List<Add_to_Campaign_Config__c> campaignCustSettList = Add_to_Campaign_Config__c.getall().values();
        allReportNames.add(new SelectOption('','--None--'));
        for(Add_to_Campaign_Config__c campaignCustSettRec : campaignCustSettList){
            campaignCustSettMap.put(campaignCustSettRec.Report_Name__c,campaignCustSettRec);
            //display reports to be queried from Network_Platform_Relationship__c object only when logged in user is not Institutional User
            if(!(networkPlatformObjNameStr.equalsIgnoreCase(campaignCustSettRec.Object_Name__c) && isInstitutionalUser)){
                allReportNames.add(new SelectOption(campaignCustSettRec.Report_Name__c,campaignCustSettRec.Report_Name__c));
            }
        }
    }
    
    /*
     * Description : Method to get Campaign Member status values in the picklist
     */
    public void getMemberStatusValues(){
        allMemberStatus = new List<SelectOption>();
        allMemberStatus.add(new SelectOption('Sent','Sent'));
        allMemberStatus.add(new SelectOption('Responded','Responded'));
    }
            
    /*
     * Description : Method to get contacts based on custom setting filters. The custom setting Add_to_Campaign_Config__c has 3 query fields. 
                    The filters mentioned in the custom setting record are applied on the Query 1 by appending where clause in this method. Query 2 field uses the returned ids from Query 1 field as filter. Similarly, Query 3 uses the returned ids from Query 2 field as filter.
     */
    public void getContactDetails(){
         
        String contactQuery = '';       
        set<id> contactIdSet = new set<id>();
        List<SObject> queriedSObjectList = new List<SObject>();
        selectedContactList = new List<SelectContactWrapper>();
        Set<ID> QueryOneSetID = new Set<ID>();
        SelectContactWrapper contactWrapperRec;
        Add_to_Campaign_Config__c selectedReportDetails = new Add_to_Campaign_Config__c();
        selectedReportDetails = campaignCustSettMap.get(selectedReportName);
        String whereClause = ' where ';
        Double queryReturningContactCurrent = 0;
        Schema.DisplayType fieldType;
        
        if(selectedReportDetails != null){
            sObjectName = selectedReportDetails.Object_Name__c;
            if(selectedReportDetails.Query_1__c != null){
                
                String soqlQuery = selectedReportDetails.Query_1__c;
                String queriedFieldName = soqlQuery.substringBetween('Select ',' From');
                
                if(fieldFiltersMap != null && !fieldFiltersMap.isEmpty()){
                    //contructing first query by adding where clause based on data types of filter fields
                    for(String fieldName : fieldFiltersMap.keySet()){
                        fieldType = getFieldType(fieldName, sObjectName).getType();
                        if(fieldType == Schema.DisplayType.String || fieldType == Schema.DisplayType.Reference){
                            if((String)sObjectInstance.get(fieldName) != null){
                                whereClause = whereClause + fieldName + ' = \'' + (String)sObjectInstance.get(fieldName) + '\' and ';
                            }
                        }
                        else if(fieldType == Schema.DisplayType.Boolean){
                            if((boolean)sObjectInstance.get(fieldName) != null){ 
                                whereClause = whereClause + fieldName + ' = ' + (boolean)sObjectInstance.get(fieldName) + ' and ';
                            }
                        }
                    }
                }
                if(whereClause.endswithIgnoreCase('and ')){
                    whereClause = whereClause.removeEndIgnoreCase('and ');              
                }
                soqlQuery = soqlQuery + whereClause;
                
                if(soqlQuery.endswithIgnoreCase(' where ')){
                    soqlQuery = soqlQuery.removeEndIgnoreCase(' where ');              
                }
                //execute first constructed query
                queriedSObjectList = Database.query(soqlQuery);
                
                //add resultant Ids from query 1 to QueryOneSetID, which in turn will be used by query 2
                for(SObject rec : queriedSObjectList){
                    QueryOneSetID.add((ID)rec.get(queriedFieldName));
                }
                
                //counter to check which query has been executed. This counter is used for comparision with Query_Returning_Contact_IDs__c field of custom setting that holds which query will return contact Ids. The contact Ids will finally be usd to query on contact and display the details on the page.
                queryReturningContactCurrent = queryReturningContactCurrent + 1;
            }
            
            //if counter < Query_Returning_Contact_IDs__c value, execute Query 2
            if(selectedReportDetails.Query_2__c != null && queryReturningContactCurrent < selectedReportDetails.Query_Returning_Contact_IDs__c){
                //execute Query 2
                queriedSObjectList = Database.query(selectedReportDetails.Query_2__c);
                String soqlQuery = selectedReportDetails.Query_2__c;
                String queriedFieldName = soqlQuery.substringBetween('Select ',' From');
                QueryOneSetID = new Set<ID>();
                for(SObject rec : queriedSObjectList){
                    //overwrite result from Query 1 with Ids from Query 2 result. This will be used by Query 3 field to filter, or by Contact Query (if it returns contact Ids).
                    QueryOneSetID.add((ID)rec.get(queriedFieldName));
                }
                //counter incremented
                queryReturningContactCurrent = queryReturningContactCurrent + 1;
            }
            
            //if counter < Query_Returning_Contact_IDs__c value, execute Query 3
            if(selectedReportDetails.Query_3__c != null && queryReturningContactCurrent < selectedReportDetails.Query_Returning_Contact_IDs__c){
                //execute Query 3
                queriedSObjectList = Database.query(selectedReportDetails.Query_3__c);
                String soqlQuery = selectedReportDetails.Query_3__c;
                String queriedFieldName = soqlQuery.substringBetween('Select ',' From');
                QueryOneSetID = new Set<ID>();
                for(SObject rec : queriedSObjectList){
                    //overwrite result from Query 1 with Ids from Query 2 result. This will be used by Contact Query.
                    QueryOneSetID.add((ID)rec.get(queriedFieldName));
                }
                //counter incremented
                queryReturningContactCurrent = queryReturningContactCurrent + 1;
            }
            
            //counter comparision with Query_Returning_Contact_IDs__c, execute contact query if count matches
            if(queryReturningContactCurrent == selectedReportDetails.Query_Returning_Contact_IDs__c){
                
                contactQuery = 'SELECT Id, name, email, phone, account.name from contact where id in : QueryOneSetID limit 1000';
                allContactList = Database.Query(contactQuery);
                
                for(Contact contactRec : allContactList){
                    contactWrapperRec = new SelectContactWrapper(contactRec);
                    selectedContactList.add(contactWrapperRec);
                }  
                
            }
        }
    }
    
    
    /*
     * Description : Method to display filters from the custom setting on page
     */
    public void renderFilters(){
        String fieldFilters = '';
        fieldFiltersMap = new Map<String,boolean>();
        
        if(String.isNotBlank(selectedReportName) && campaignCustSettMap.get(selectedReportName) != null){
            sObjectName = campaignCustSettMap.get(selectedReportName).Object_Name__c;
            sObjectInstance = Schema.getGlobalDescribe().get(sObjectName).newSObject();
            fieldFilters = campaignCustSettMap.get(selectedReportName).Filters__c;
            
            if(String.isNotBlank(fieldFilters) && fieldFilters.contains(';')){
                for(string fieldApiName : fieldFilters.split(';')){
                    fieldFiltersMap.put(fieldApiName, getFieldType(fieldApiName, sObjectName).isCalculated());
                }
            }else if(String.isNotBlank(fieldFilters) && !fieldFilters.contains(';')){
                fieldFiltersMap.put(fieldFilters , getFieldType(fieldFilters , sObjectName).isCalculated());
            }
            
        }
    }
    
    
    /*
     * Method to add selected contacts to campaign
     */
    public PageReference addSelectedContactsToCampaign(){
        
        list<CampaignMember> campaignMemberToInsertList = new list<CampaignMember>();
        set<id> contactIdsToAdd = new set<id>();
        
        //getting selected contacts from page
        for(SelectContactWrapper wrapperObj : selectedContactList){
            if(wrapperObj.selected == true){
                contactIdsToAdd.add(wrapperObj.con.id);
                wrapperObj.selected = false;
            }
        }
        
        
        for(Id conId : contactIdsToAdd){
            campaignMemberToInsertList.add(new CampaignMember(CampaignId = campaignRecord.id, ContactId = conId, status = selectedMemberStatus));
        }
        
        //insert campaign members
        List<Database.SaveResult> srList =  Database.insert(campaignMemberToInsertList, false);
        for(Database.SaveResult sr : srList){
            if(!sr.isSuccess()){
                system.debug('Error Occured: -->'+sr.getErrors()[0]);
            }
        }
        
        //redirect to campaign record page
        pageReference pg = new pageReference('/'+ campaignRecord.id); 
        return pg.setRedirect(true);
    }
    
    
    /*
     * Method to add All contacts to campaign
     */
    public PageReference addAllContactsToCampaign(){
        
        list<CampaignMember> campaignMemberToInsertList = new list<CampaignMember>();
        
        for(Contact contactRec : allContactList){
            campaignMemberToInsertList.add(new CampaignMember(CampaignId = campaignRecord.id, ContactId = contactRec.id, status = selectedMemberStatus));
        }
        
        //insert Campaign Members
        List<Database.SaveResult> srList =  Database.insert(campaignMemberToInsertList, false);
        for(Database.SaveResult sr : srList){
            if(!sr.isSuccess()){
                system.debug('Error Occured: -->'+sr.getErrors()[0]);
            }
        }
        
        //redirect to Campaign record page
        pageReference pg = new pageReference('/'+ campaignRecord.id); 
        return pg.setRedirect(true);
    }
    
    
    /*
     * Wrapper Class to display input checkbox
     */
    public class SelectContactWrapper{
        public boolean selected{get;set;}
        public Contact con{get;set;}
        
        //no argument contructor
        public SelectContactWrapper(){
            this.selected = false;
            this.con = new Contact();
        }
        //parameterised constructor
        public SelectContactWrapper(Contact c){
            this.selected = false;
            this.con = c;
        }
    }
    
    
    /*
     * Description : Method to get field type
     */
    public static Schema.DescribeFieldResult getFieldType(String fieldName, String sObjectName){
        Schema.SObjectType t = Schema.getGlobalDescribe().get(sObjectName);
         
        Schema.DescribeSObjectResult r = t.getDescribe();
        Schema.DescribeFieldResult f = r.fields.getMap().get(fieldName).getDescribe();
        return f;
    }
}