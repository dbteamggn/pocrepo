public class DemoDataFactory {

    public static void createDemoData() {

    //create your opportunity line item.  This assumes you already have an opportunity created, called opp
   
    Account L0,L1,L2,L5,acctReference;
    List<Account> ThirdPartyaccts = new List<Account>();
    //Institutional Third Parties
    //Aon Hewitt Hierarchy
    L0 = new Account(Name='Aon Hewitt',RecordTypeId='0120Y000000wggE', Ext_Id__c='Aon_L0',Legacy_Record_Id__c='DEMO',Hierarchy_Level__c='L0',Institutional_NA__c=True,Business_Line__c='Institutional NA',Region__c='NA',Third_Party_Type__c='Consultant', Billing_City__c='New York',Billing_Country__c='United States'); 
    ThirdPartyaccts.add(L0);
    insert L0;
    L1 = new Account(Name='Aon Hewitt',ParentId=L0.Id,RecordTypeId='0120Y000000wggE', Ext_Id__c='Aon_L1',Legacy_Record_Id__c='DEMO',Hierarchy_Level__c='L1',Institutional_EMEA__c=True,Business_Line__c='Institutional EMEA',Region__c='EMEA',Third_Party_Type__c='Consultant', Billing_City__c='London',Billing_Country__c='United Kingdom');
    ThirdPartyaccts.add(L1);
    insert L1;
    //Mercer Hierarchy
    L0 = new Account(Name='Mercer',RecordTypeId='0120Y000000wggE', Ext_Id__c='Mercer_L0',Legacy_Record_Id__c='DEMO',Hierarchy_Level__c='L0',Institutional_NA__c=True,Business_Line__c='Institutional NA',Region__c='NA',Third_Party_Type__c='Consultant', Billing_City__c='New York',Billing_Country__c='United States'); 
    ThirdPartyaccts.add(L0);
        insert L0;
    L1 = new Account(Name='Mercer',ParentId=L0.Id,RecordTypeId='0120Y000000wggE', Ext_Id__c='Mercer_L1',Legacy_Record_Id__c='DEMO',Hierarchy_Level__c='L1',Institutional_EMEA__c=True,Business_Line__c='Institutional EMEA',Region__c='EMEA',Third_Party_Type__c='Consultant', Billing_City__c='London',Billing_Country__c='United Kingdom');
    ThirdPartyaccts.add(L1);
        insert L1;
    //Willis Towers Watson Hierarchy    
    L0 = new Account(Name='Willis Towers Watson',RecordTypeId='0120Y000000wggE', Ext_Id__c='WTW_L0',Legacy_Record_Id__c='DEMO',Hierarchy_Level__c='L0',Institutional_EMEA__c=True,Business_Line__c='Institutional EMEA',Region__c='EMEA',Third_Party_Type__c='Consultant', Billing_City__c='London',Billing_Country__c='United Kingdom'); 
    ThirdPartyaccts.add(L0);
        insert L0;
    L1 = new Account(Name='Willis Towers Watson',ParentId=L0.Id,RecordTypeId='0120Y000000wggE', Ext_Id__c='WTW_L1',Legacy_Record_Id__c='DEMO',Hierarchy_Level__c='L1',Institutional_APAC__c=True,Business_Line__c='Institutional APAC',Region__c='APAC',Third_Party_Type__c='Consultant', Billing_City__c='Melbourne',Billing_Country__c='Australia');
    ThirdPartyaccts.add(L1);
        insert L1;
    //Add Rated Strategies to Institutional Third Parties
    addRatedStrategies(ThirdPartyaccts);
    addCoverageTeam(ThirdPartyaccts);
        
    //Add Institutional Clients
    List<Account> instiClientaccts = new List<Account>();
    L0 = new Account(Name='Shell International',Client_Third_Party_Alias__c='Shell',RecordTypeId='0120Y000000wggD',Fixed_Income__c=10,Equities__c=20,Real_Assets__c=15,Private_Equity__c=10,Hedge_Fund__c=5,Multi_Asset__c=10,Cash__c=30, website='http://shell.com',Ext_Id__c='Shell_L0',Legacy_Record_Id__c='DEMO',Hierarchy_Level__c='L0',Institutional_EMEA__c=True,Business_Line__c='Institutional EMEA',Region__c='EMEA',Client_Type__c='Corporate', Billing_City__c='The Hague',Billing_Country__c='Netherlands'); 
    instiClientaccts.add(L0);
    L1 = new Account(Name='Shell Japan Ltd.',Client_Third_Party_Alias__c='Shell',/*ParentId=L0.Id,*/RecordTypeId='0120Y000000wggD', Fixed_Income__c=10,Equities__c=20,Real_Assets__c=15,Private_Equity__c=10,Hedge_Fund__c=5,Multi_Asset__c=10,Cash__c=30,website='http://shell.com',Ext_Id__c='ShellJapan_L1',Legacy_Record_Id__c='DEMO',Hierarchy_Level__c='L1',Institutional_APAC__c=True,Business_Line__c='Institutional APAC',Region__c='APAC',Client_Type__c='Corporate', Billing_City__c='Tokyo',Billing_Country__c='Japan');
    instiClientaccts.add(L1);   
    L1 = new Account(Name='Shell Oil Co',Client_Third_Party_Alias__c='Shell',/*ParentId=L0.Id,*/RecordTypeId='0120Y000000wggD', Fixed_Income__c=10,Equities__c=20,Real_Assets__c=15,Private_Equity__c=5,Hedge_Fund__c=10,Multi_Asset__c=10,Cash__c=30,website='http://shell.com',Ext_Id__c='ShellHouston_L1',Legacy_Record_Id__c='DEMO',Hierarchy_Level__c='L1',Institutional_NA__c=True,Business_Line__c='Institutional NA',Region__c='NA',Client_Type__c='Corporate', Billing_City__c='Houston',Billing_Country__c='United States');
    instiClientaccts.add(L1);    
    L1 = new Account(Name='Shell U.K. Limited',Client_Third_Party_Alias__c='Shell',/*ParentId=L0.Id,*/RecordTypeId='0120Y000000wggD', Fixed_Income__c=10,Equities__c=30,Real_Assets__c=15,Private_Equity__c=10,Hedge_Fund__c=10,Multi_Asset__c=5,Cash__c=20,website='http://shell.com',Ext_Id__c='ShellUK_L1',Legacy_Record_Id__c='DEMO',Hierarchy_Level__c='L1',Institutional_EMEA__c=True,Business_Line__c='Institutional EMEA',Region__c='EMEA',Client_Type__c='Corporate', Billing_City__c='London',Billing_Country__c='United Kingdom');
    instiClientaccts.add(L1);    
    //Unions
    L1 = new Account(Name='National Education Association of the United States',Client_Third_Party_Alias__c='NEA',RecordTypeId='0120Y000000wggD', Fixed_Income__c=10,Equities__c=15,Real_Assets__c=20,Private_Equity__c=10,Hedge_Fund__c=30,Multi_Asset__c=10,Cash__c=5,website='http://www.nea.org',Ext_Id__c='NEA_L1',Legacy_Record_Id__c='DEMO',Hierarchy_Level__c='L1',Institutional_NA__c=True,Business_Line__c='Institutional NA',Region__c='NA',Client_Type__c='Union/Multi-Employer', Billing_City__c='Washington DC',Billing_Country__c='United States');
    instiClientaccts.add(L1);     
    L1 = new Account(Name='International Union of Painters and Allied Trades',Client_Third_Party_Alias__c='IUPAT',RecordTypeId='0120Y000000wggD', Fixed_Income__c=20,Equities__c=10,Real_Assets__c=15,Private_Equity__c=5,Hedge_Fund__c=10,Multi_Asset__c=10,Cash__c=30,website='https://iupat.org/',Ext_Id__c='IUPAT_L1',Legacy_Record_Id__c='DEMO',Hierarchy_Level__c='L1',Institutional_NA__c=True,Business_Line__c='Institutional NA',Region__c='NA',Client_Type__c='Union/Multi-Employer', Billing_City__c='North York',Billing_Country__c='Canada');
    instiClientaccts.add(L1); 
    
    try {
            insert instiClientaccts;
        } catch (DmlException e) {
            System.debug('ERROR:' + e);
        }
    addCoverageTeam(instiClientaccts);   
    createInstiContacts(instiClientaccts);
        //Retail Clients
    List<Account> retailClientaccts = new List<Account>();
    L0 = new Account(   Name=' Zurich Financial Services Group',
                        Client_Third_Party_Alias__c='ZG',
                        RecordTypeId='0120Y000000wggD', 
                        Website='https://www.zurich.com/',
                        Ext_Id__c='ZG_L0',Legacy_Record_Id__c='DEMO',
                        Hierarchy_Level__c='L0',Retail_EMEA__c=True,
                        Business_Line__c='Retail EMEA',Region__c='EMEA',
                        Client_Type__c='Insurance', 
                        Billing_City__c='Frankfurt Am Main',
                        Billing_Country__c='Germany');
    retailClientaccts.add(L0);   
    
    L1 = new Account(   Name='Zurich Europe General Insurance',
                       
                        Client_Third_Party_Alias__c='ZG',
                        RecordTypeId='0120Y000000wggD', 
                        Website='https://www.zurich.com/',
                        Ext_Id__c='ZgEmeaGI_L1',
                        Legacy_Record_Id__c='DEMO',
                        Hierarchy_Level__c='L1',
                        Retail_NA__c=True,
                        Business_Line__c='Retail EMEA',
                        Region__c='EMEA',
                        Client_Type__c='Insurance', 
                        Billing_City__c='Dublin',
                        Billing_Country__c='Ireland');
    retailClientaccts.add(L1);

     L2 = new Account(   Name='Zurich Europe General Insurance - UK',
                        Client_Third_Party_Alias__c='ZG',
                        RecordTypeId='0120Y000000wggD', 
                        website='https://www.zurich.com/',
                        Ext_Id__c='ZgEmeaUk_L2',Legacy_Record_Id__c='DEMO',
                        Hierarchy_Level__c='L2',
                        Retail_EMEA__c=True,
                        Business_Line__c='Retail EMEA',
                        Region__c='EMEA',Client_Type__c='Insurance', 
                        Billing_City__c='London',
                        Billing_Country__c='United Kingdom');
    retailClientaccts.add(L2);
    
    L5 = new Account(   Name='Zurich Financial Lines UK',
                        Client_Third_Party_Alias__c='ZG',
                        RecordTypeId='0120Y000000wggD', 
                        website='https://www.zurich.com/',
                        Ext_Id__c='ZgFineLinesUk_L5',Legacy_Record_Id__c='DEMO',
                        Hierarchy_Level__c='L5',
                        Retail_EMEA__c=True,
                        Business_Line__c='Retail EMEA',
                        Region__c='EMEA',Client_Type__c='Insurance', 
                        Billing_City__c='London',
                        Billing_Country__c='United Kingdom');
    retailClientaccts.add(L5);
   
       L5 = new Account(   Name='Zurich Private Clients UK',
                        Client_Third_Party_Alias__c='ZG',
                        RecordTypeId='0120Y000000wggD', 
                        website='https://www.zurich.com/',
                        Ext_Id__c='ZgPrivateClientsUk_L5',Legacy_Record_Id__c='DEMO',
                        Hierarchy_Level__c='L5',
                        Retail_EMEA__c=True,
                        Business_Line__c='Retail EMEA',
                        Region__c='EMEA',Client_Type__c='Insurance', 
                        Billing_City__c='Manchester',
                        Billing_Country__c='United Kingdom');
    retailClientaccts.add(L5); 

        L5 = new Account(   Name='Zurich International Life Ltd UK',
                        Client_Third_Party_Alias__c='ZG',
                        RecordTypeId='0120Y000000wggD', 
                        website='https://www.zurich.com/',
                        Ext_Id__c='ZgIntlLifeUk_L5',Legacy_Record_Id__c='DEMO',
                        Hierarchy_Level__c='L5',
                        Retail_EMEA__c=True,
                        Business_Line__c='Retail EMEA',
                        Region__c='EMEA',Client_Type__c='Insurance', 
                        Billing_City__c='Douglas',
                        Billing_Country__c='United Kingdom');
    retailClientaccts.add(L5); 
    
    L1 = new Account(   Name='Zurich Global Corporate',
                        Client_Third_Party_Alias__c='ZG',
                        RecordTypeId='0120Y000000wggD', 
                        Website='https://www.zurich.com/',
                        Ext_Id__c='ZgGlobalCorp_L1',
                        Legacy_Record_Id__c='DEMO',
                        Hierarchy_Level__c='L1',
                        Retail_EMEA__c=True,
                        Business_Line__c='Retail NA',
                        Region__c='NA',
                        Client_Type__c='Insurance', 
                        Billing_City__c='New York',
                        Billing_Country__c='United States');
    retailClientaccts.add(L1);

    L2 = new Account(   Name='Zurich Global Corporate - EMEA',
                        Client_Third_Party_Alias__c='ZG',
                        RecordTypeId='0120Y000000wggD', 
                        website='https://www.zurich.com/',
                        Ext_Id__c='ZgGlobCorpBl_L2',Legacy_Record_Id__c='DEMO',
                        Hierarchy_Level__c='L2',
                        Retail_EMEA__c=True,
                        Business_Line__c='Retail EMEA',
                        Region__c='EMEA',Client_Type__c='Insurance', 
                        Billing_City__c='Brussels',
                        Billing_Country__c='Belgium');
    retailClientaccts.add(L2);

    L5 = new Account(   Name='Zurich Global Corporate - UK',
                        Client_Third_Party_Alias__c='ZG',
                        RecordTypeId='0120Y000000wggD', 
                        website='https://www.zurich.com/',
                        Ext_Id__c='ZgGlobCorpUk_L5',Legacy_Record_Id__c='DEMO',
                        Hierarchy_Level__c='L5',
                        Retail_EMEA__c=True,
                        Business_Line__c='Retail EMEA',
                        Region__c='EMEA',Client_Type__c='Insurance', 
                        Billing_City__c='London',
                        Billing_Country__c='United Kingdom');
    retailClientaccts.add(L5); 
        
    L1 = new Account(   Name='Zurich North America Commercial',
                        Client_Third_Party_Alias__c='ZG',
                        RecordTypeId='0120Y000000wggD', 
                        Website='https://www.zurich.com/',
                        Ext_Id__c='ZgNaCommercial_L1',
                        Legacy_Record_Id__c='DEMO',
                        Hierarchy_Level__c='L1',
                        Retail_APAC__c=True,
                        Business_Line__c='Retail NA',
                        Region__c='NA',
                        Client_Type__c='Insurance', 
                        Billing_City__c='New York',
                        Billing_Country__c='United States');
    retailClientaccts.add(L1);


    L1 = new Account(   Name='Zurich Global Life Insurance',
                        Client_Third_Party_Alias__c='ZG',
                        RecordTypeId='0120Y000000wggD', 
                        website='https://www.zurich.com/',
                        Ext_Id__c='ZgGlobalLife_L1',
                        Legacy_Record_Id__c='DEMO',
                        Hierarchy_Level__c='L1',
                        Retail_EMEA__c=True,
                        Business_Line__c='Retail EMEA',
                        Region__c='EMEA',
                        Client_Type__c='Insurance', 
                        Billing_City__c='London',
                        Billing_Country__c='United Kingdom');
    retailClientaccts.add(L1);

    L1 = new Account(   Name='Zurich International Businesses',
                        Client_Third_Party_Alias__c='ZG',
                        RecordTypeId='0120Y000000wggD', 
                        website='https://www.zurich.com/',
                        Ext_Id__c='ZgInternational_L1',Legacy_Record_Id__c='DEMO',
                        Hierarchy_Level__c='L5',
                        Retail_EMEA__c=True,
                        Business_Line__c='Retail APAC',
                        Region__c='APAC',Client_Type__c='Insurance', 
                        Billing_City__c=' Sydney',
                        Billing_Country__c='Australia');
    retailClientaccts.add(L1);

   


        
    try {
            insert retailClientaccts;
        } catch (DmlException e) {
            System.debug('ERROR:' + e);
        }
    createRetailOpportunities(retailClientaccts);  
    addCoverageTeam(retailClientaccts);
    createRetailActivities(retailClientaccts);
    createContacts(retailClientaccts);
        
    }
    public static void deleteDemoData() {
        Account[] doomedAccts = [SELECT Id, Name FROM Account 
                         WHERE Legacy_Record_Id__c = 'DEMO']; 
        try {
            delete doomedAccts;
        } catch (DmlException e) {
            System.debug('ERROR:' + e);
        }
        Contact[] doomedContacts = [SELECT Id, Name FROM Contact 
                         WHERE Legacy_Record_Id__c = 'DEMO']; 
        try {
            delete doomedContacts;
        } catch (DmlException e) {
            System.debug('ERROR:' + e);
        }

    }
    //Add Rated Strategies to all Institutional Third Party Accounts
    public static void addRatedStrategies(List<Account> ThirdPartyaccts){
        List<Rated_Strategy__c> RatedStrategies = new List<Rated_Strategy__c>();
        Rated_Strategy__c RS;
        integer numAccts=ThirdPartyaccts.size();
        Investment_Strategy__c[] iStrategies = [SELECT Id, Name FROM Investment_Strategy__c ];
        integer numStrategies=iStrategies.size();
        for (Integer j=0;j<numAccts;j++) {
            Account acct = ThirdPartyaccts[j];
            for (Integer k=0;k<numStrategies;k++) {
                if(k / 2 != 0){
                RS = new Rated_Strategy__c( Legacy_Record_Id__c = 'DEMO',Comments__c='Consultant expected to increase rating at year end',Third_Party__c=acct.Id,Investment_Strategy__c=iStrategies[k].Id,Current_Rating__c='Buy',Year_End_Expected_Rating__c='Buy+');
                }
                else{
                RS = new Rated_Strategy__c( Legacy_Record_Id__c = 'DEMO',Comments__c='Consultant expected to increase rating at year end',Third_Party__c=acct.Id,Investment_Strategy__c=iStrategies[k].Id,Current_Rating__c='Hold',Year_End_Expected_Rating__c='Buy');    
                }
                    RatedStrategies.add(RS);
            }
        }
        try {
            insert RatedStrategies;
        } catch (DmlException e) {
            System.debug('ERROR:' + e);
        }
        
    }
    //Add Rated Strategies to all Institutional Third Party Accounts
    public static void addCoverageTeam(List<Account> accts){
        integer numAccts=accts.size();
        List<User> userList = [SELECT Id, Name FROM User WHERE Id NOT IN('0050Y000001ocJ5')];
        integer numUsers =userList.size();
        List<AccountTeamMember> teamList = new List<AccountTeamMember>();
        AccountTeamMember newTeam;
        for (Integer j=0;j<numAccts;j++) {
            Account acct = accts[j];
            for (Integer k=0;k<numUsers;k++) {
                if(k / 2 != 0){
                newTeam = new AccountTeamMember(AccountId = accts[j].ID,TeamMemberRole = 'Sales Team Member',UserId = userList[k].ID);  
                }
                else{
                newTeam = new AccountTeamMember(AccountId = accts[j].ID,TeamMemberRole = 'Service',UserId = userList[k].ID);
                }
                teamList.add(newTeam);
            }
        }
        try {
            insert teamList;
        } catch (DmlException e) {
            System.debug('ERROR:' + e);
        }
        
    }
    
   
   public static void createRetailOpportunities(List<Account> retailClientaccts) {
        integer numAccts=retailClientaccts.size();
        List<Opportunity> opps = new List<Opportunity>();
        integer numOppsPerAcct = 5;  
        for (Integer j=0;j<numAccts;j++) {
            Account acct = retailClientaccts[j];
            // For each account just inserted, add opportunities
            for (Integer k=0;k<numOppsPerAcct;k++) {
                opps.add(new Opportunity(Name=acct.Name + ' Opportunity ' + k,
                                       StageName='Lead',
                                       Type='Driving Flow',
                                       Amount=Math.mod(Math.round(Math.random()*1000),1000)*10000,
                                       CloseDate=System.today().addMonths(k+1),
                                       AccountId=acct.Id,
                                       Business_Line__c='Institutional'));
            }
        }

        numOppsPerAcct = 4;  
        for (Integer j=0;j<numAccts;j++) {
            Account acct = retailClientaccts[j];
            // For each account just inserted, add opportunities
            for (Integer k=0;k<numOppsPerAcct;k++) {
                opps.add(new Opportunity(Name=acct.Name + ' Opportunity ' + k,
                                       StageName='Qualified',
                                       Type='Driving Flow',
                                       Amount=Math.mod(Math.round(Math.random()*1000),1000)*10000,
                                       CloseDate=System.today().addMonths(k+1),
                                       AccountId=acct.Id,
                                       Business_Line__c='Institutional'));
            }
        }

        numOppsPerAcct = 2;  
        for (Integer j=0;j<numAccts;j++) {
            Account acct = retailClientaccts[j];
            // For each account just inserted, add opportunities
            for (Integer k=0;k<numOppsPerAcct;k++) {
                opps.add(new Opportunity(Name=acct.Name + ' Opportunity ' + k,
                                       StageName='Finals',
                                       Type='Driving Flow',
                                       Amount=Math.mod(Math.round(Math.random()*1000),1000)*10000,
                                       CloseDate=System.today().addMonths(k+1),
                                       AccountId=acct.Id,
                                       Business_Line__c='Institutional'));
            }
        }

        numOppsPerAcct = 2;  
        for (Integer j=0;j<numAccts;j++) {
            Account acct = retailClientaccts[j];
            // For each account just inserted, add opportunities
            for (Integer k=0;k<numOppsPerAcct;k++) {
                opps.add(new Opportunity(Name=acct.Name + ' Opportunity ' + k,
                                       StageName='Won-Not Funded',
                                       Type='Driving Flow',
                                       Amount=Math.mod(Math.round(Math.random()*1000),1000)*10000,
                                       CloseDate=System.today().addMonths(k+1),
                                       AccountId=acct.Id,
                                       Business_Line__c='Institutional'));
            }
        }
        numOppsPerAcct = 4;  
        for (Integer j=0;j<numAccts;j++) {
            Account acct = retailClientaccts[j];
            // For each account just inserted, add opportunities
            for (Integer k=0;k<numOppsPerAcct;k++) {
                opps.add(new Opportunity(Name=acct.Name + ' Opportunity ' + k,
                                       StageName='Finals',
                                       Type='Won-Funded',
                                       Amount=Math.mod(Math.round(Math.random()*1000),1000)*10000,
                                       CloseDate=System.today().addMonths(k+1),
                                       AccountId=acct.Id,
                                       Business_Line__c='Institutional'));
            }
        }

        numOppsPerAcct = 5;  
        for (Integer j=0;j<numAccts;j++) {
            Account acct = retailClientaccts[j];
            // For each account just inserted, add opportunities
            for (Integer k=0;k<numOppsPerAcct;k++) {
                opps.add(new Opportunity(Name=acct.Name + ' Opportunity ' + k,
                                       StageName='Lost',
                                       Type='Driving Flow',
                                       Amount=Math.mod(Math.round(Math.random()*1000),1000)*10000,
                                       CloseDate=System.today().addMonths(k+1),
                                       AccountId=acct.Id,
                                       Business_Line__c='Institutional'));
            }
         }
        // Insert all opportunities for all accounts.
        try {
            insert opps;
        } catch (DmlException e) {
            System.debug('ERROR:' + e);
        }

    }
     public static void createRetailActivities(List<Account> retailClientaccts) {
        integer numAccts=retailClientaccts.size();
        List<Event> evts = new List<Event>();
        List<String> subjects = new List<String>();
        subjects.add('Relationship Development');
        subjects.add('Portfolio Review');
        subjects.add('Follow Up');
        subjects.add('Fund Update');
        subjects.add('Non-Material Complaint');
        integer numEvtPerAcct = subjects.size();
        for (Integer j=0;j<numAccts;j++) {
            Account acct = retailClientaccts[j];
            // For each account just inserted, add events
            for (Integer k=0;k<numEvtPerAcct;k++) {
                if(k / 2 != 0){
                evts.add(new Event(Subject=subjects[k],Type__c='Meeting',status__c='open',Sub_Type__c=subjects[k],
                        StartDateTime=DateTime.newInstance(2017, System.today().Month(), System.today().Day()+k,16,00,00),
                        EndDateTime=Datetime.newInstance(2017, System.today().Month(), System.today().Day()+k,17,00,00),WhatId=acct.Id,RecordTypeId='0120Y000000wgge'));
                }
                else{
                 evts.add(new Event(Subject=subjects[k],Type__c='Call',status__c='open',Sub_Type__c=subjects[k],
                        StartDateTime=DateTime.newInstance(2017, System.today().Month(), System.today().Day()+k,09,00,00),
                        EndDateTime=Datetime.newInstance(2017, System.today().Month(), System.today().Day()+k,10,00,00),WhatId=acct.Id,RecordTypeId='0120Y000000wgge'));
                 evts.add(new Event(Subject=subjects[k],Type__c='Call',status__c='open',Sub_Type__c=subjects[k],
                        StartDateTime=DateTime.newInstance(2017, System.today().Month()+k, System.today().Day()+k,14,00,00),
                        EndDateTime=Datetime.newInstance(2017, System.today().Month()+k, System.today().Day()+k,15,00,00),WhatId=acct.Id,RecordTypeId='0120Y000000wgge'));
                  evts.add(new Event(Subject=subjects[k],Type__c='Admin',status__c='open',Sub_Type__c=subjects[k],
                        StartDateTime=DateTime.newInstance(2017, System.today().Month(), System.today().Day(),11,00,00),
                        EndDateTime=Datetime.newInstance(2017, System.today().Month(), System.today().Day(),12,00,00),WhatId=acct.Id,RecordTypeId='0120Y000000wgge'));
   
                }
                }
        }
        // Insert all opportunities for all accounts.
        try {
            insert evts;
        } catch (DmlException e) {
            System.debug('ERROR:' + e);
        }

    }
public static void createContacts(List<Account> Clientaccts) {
        integer numAccts=Clientaccts.size();
        List<Contact> contacts = new List<Contact>();
        List<String> firstname = new List<String>();
        List<String> lastname = new List<String>();
        firstname.add('James');
        firstname.add('Stuart');
        firstname.add('Sean');
        firstname.add('Sarah');
        firstname.add('Rachel');
        lastname.add('Smith');
        lastname.add('Jones');
        lastname.add('Brown');
        lastname.add('Hopwood');
        lastname.add('Fredricks');
        integer numConPerAcct = firstname.size();
        string contactemail;
        for (Integer j=0;j<numAccts;j++) {
            Account acct = Clientaccts[j];
            // For each account just inserted, add events
            for (Integer k=0;k<numConPerAcct;k++) {
                contactemail=firstname[k]+'.'+lastname[k]+'@test.com';
                contacts.add(new Contact(Legacy_Record_Id__c = 'DEMO',FirstName=firstname[k],LastName=lastname[k],AccountId=Clientaccts[j].Id,email=contactemail));
            }
        }
        // Insert all opportunities for all accounts.
        try {
            insert contacts;
        } catch (DmlException e) {
            System.debug('ERROR:' + e);
        }

    }
public static void createInstiContacts(List<Account> Clientaccts) {
        integer numAccts=Clientaccts.size();
        List<Contact> contacts = new List<Contact>();
        List<String> firstname = new List<String>();
        List<String> lastname = new List<String>();
        firstname.add('Ben');
        firstname.add('Fred');
        firstname.add('Amber');
        firstname.add('Lauren');
        firstname.add('Josh');
        lastname.add('Jones');
        lastname.add('Black');
        lastname.add('Green');
        lastname.add('Brown');
        lastname.add('Johnson');
        integer numConPerAcct = firstname.size();
        string contactemail;
        Double randomNumber = Math.random();
        Integer randomfirst = (randomNumber *(numConPerAcct-1)).intValue();
     	integer randomlast = (randomNumber *(numConPerAcct-1)).intValue();
        for (Integer j=0;j<numAccts;j++) {
            Account acct = Clientaccts[j];
            // For each account just inserted, add events
            for (Integer k=0;k<numConPerAcct;k++) {
                randomfirst = (randomNumber *(numConPerAcct-1)).intValue();
                randomlast = (randomNumber *(numConPerAcct-1)).intValue();
                if(k / 2 != 0){
                contactemail=firstname[k]+'.'+lastname[k]+'@test.com';
                contacts.add(new Contact(Legacy_Record_Id__c = 'DEMO',FirstName=firstname[k],LastName=lastname[k],AccountId=Clientaccts[j].Id,email=contactemail));
                }
                else{
                contactemail=firstname[k]+'.'+lastname[k]+'@test.com';
                contacts.add(new Contact(Legacy_Record_Id__c = 'DEMO',FirstName=firstname[k],LastName=lastname[k],AccountId=Clientaccts[j].Id,email=contactemail)); 
                }
                }
        }
        // Insert all opportunities for all accounts.
        try {
            insert contacts;
        } catch (DmlException e) {
            System.debug('ERROR:' + e);
        }

    }


}