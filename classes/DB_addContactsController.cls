public class DB_addContactsController {
    @AuraEnabled
    public static List<Results> getCallReportClientParticipantDetails(id crId){
        List<DB_Client_Participants__c> cp=[select id, DB_Call_Report__c, DB_Client_Participant__c from DB_Client_Participants__c
                                            where DB_Call_Report__c=: crId];
        List<Results> Contacts=new List<Results>();
        for(DB_Client_Participants__c temp : cp){
            Contact c = [select id from Contact where id=: temp.DB_Client_Participant__c];
            Results cr=new Results();
            cr.id=c.id;
            Contacts.add(cr);
        }      
        System.debug('Contacts'+Contacts);
        return Contacts;
    }
    @AuraEnabled
    public static id updateClientParticipants(List<id> contactIds,id crId){
        Set<id> tempContactIds = new Set<id>();
        tempContactIds.addAll(contactIds);
        List<DB_Client_Participants__c> cp =[select id, DB_Call_Report__c, DB_Client_Participant__c from DB_Client_Participants__c
                                             where DB_Call_Report__c=: crId]; 
        List<DB_Client_Participants__c> toDelete = new List<DB_Client_Participants__c>();
        List<DB_Client_Participants__c> toAdd = new List<DB_Client_Participants__c>();
        DB_Client_Participants__c cpTemp;
        for (DB_Client_Participants__c temp : cp){
            if(!tempContactIds.contains(temp.DB_Client_Participant__c)){
                toDelete.add(temp);
            }else{
                tempContactIds.remove(temp.DB_Client_Participant__c);
            }
        }
        for(id temp : tempContactIds){
            cpTemp= new DB_Client_Participants__c();
            cpTemp.DB_Client_Participant__c=temp;
            cpTemp.DB_Call_Report__c=crId;
            toAdd.add(cpTemp);
        }
        insert toAdd;
        delete toDelete;
        return crId;
    }
    @AuraEnabled
    public static List<Results> getCallReportDBParticipantDetails(id crId){
        List<DB_Participants__c> dp=[select id, DB_Call_Report__c, DB_Participant__c from DB_Participants__c
                                     where DB_Call_Report__c=: crId];
        List<Results> Users=new List<Results>();
        for(DB_Participants__c temp : dp){
            if(temp.DB_Participant__c !=null){
                User u = [select id from User where id=: temp.DB_Participant__c];
                Results ur=new Results();
                ur.id=u.id;
                Users.add(ur);
            }  
        }      
        System.debug('Users'+Users);
        return Users;
    }
    @AuraEnabled
    public static id updateDBParticipants(List<id> userIds,id crId){
        System.debug('#Notsurewhetheractioniscoming here'+userIds);
        Set<id> tempUserIds = new Set<id>();
        tempUserIds.addAll(userIds);
        List<DB_Participants__c> dp =[select id, DB_Call_Report__c, DB_Participant__c from DB_Participants__c
                                      where DB_Call_Report__c=: crId]; 
        List<DB_Participants__c> toDelete = new List<DB_Participants__c>();
        List<DB_Participants__c> toAdd = new List<DB_Participants__c>();
        DB_Participants__c dpTemp;
        for (DB_Participants__c temp : dp){
            if(!tempUserIds.contains(temp.DB_Participant__c)){
                toDelete.add(temp);
            }else{
                tempUserIds.remove(temp.DB_Participant__c);
            }
        }
        for(id temp : tempUserIds){
            dpTemp= new DB_Participants__c();
            dpTemp.DB_Participant__c=temp;
            dpTemp.DB_Call_Report__c=crId;
            toAdd.add(dpTemp);
        }
        insert toAdd;
        delete toDelete;
        return crId;
    }
    @AuraEnabled
    public static List<Results> getCallReportProductDetails(id crId){
        List<DB_Products__c> prods=[select id, DB_Call_Report__c, DB_Product__c from DB_Products__c
                                    where DB_Call_Report__c=: crId];
        List<Results> Products=new List<Results>();
        for(DB_Products__c temp : prods){
            if(temp.DB_Product__c !=null){
                Product2 p = [select id from Product2 where id=: temp.DB_Product__c];
                Results pr=new Results();
                pr.id=p.id;
                Products.add(pr);
            }
        }      
        System.debug('Products'+Products);
        return Products;
    }
    @AuraEnabled
    public static id updateProducts(List<id> productIds,id crId){
        Set<id> tempProductIds = new Set<id>();
        tempProductIds.addAll(productIds);
        List<DB_Products__c> prods =[select id, DB_Call_Report__c, DB_Product__c from DB_Products__c
                                     where DB_Call_Report__c=: crId]; 
        List<DB_Products__c> toDelete = new List<DB_Products__c>();
        List<DB_Products__c> toAdd = new List<DB_Products__c>();
        DB_Products__c pTemp;
        for (DB_Products__c temp : prods){
            if(!tempProductIds.contains(temp.DB_Product__c)){
                toDelete.add(temp);
            }else{
                tempProductIds.remove(temp.DB_Product__c);
            }
        }
        for(id temp : tempProductIds){
            pTemp= new DB_Products__c();
            pTemp.DB_Product__c=temp;
            pTemp.DB_Call_Report__c=crId;
            toAdd.add(pTemp);
            System.debug('#insideUpdateProducts'+toAdd);
        }
        insert toAdd;
        delete toDelete;
        return crId;
    }
    /*@AuraEnabled
public static List<Results> getEventDetails(id eventid){
List<eventrelation> er=[select id ,relationId  from eventrelation where eventid=: eventid];
List<Results> Contacts=new List<Results>();
for(eventrelation temp : er){
Results cr=new Results();
cr.id=temp.relationId;
Contacts.add(cr);
}      
System.debug('Contacts'+Contacts);
return Contacts;
}*/
    /*@AuraEnabled
public static List<Results> getEventDetailsFundsAndStrategies(id eventid){
List<Event> ev=[select id , Strategies_Funds__c from Event where id =: eventid];
Id AliasId;
if(ev.size()>0){
AliasId=ev[0].Strategies_Funds__c;
}         
List<Activity_Product_Relationship__c> Funds=[select id ,Activity_Alias__c,fund__c,Investment_Strategy__c
from Activity_Product_Relationship__c 
where Activity_Alias__c=: AliasId];
List<Results> FundStrat=new List<Results>();
for(Activity_Product_Relationship__c temp : Funds){
Results fs=new Results();
if(temp.fund__c!=null)
fs.id=temp.fund__c;
else if(temp.Investment_Strategy__c!=null)
fs.id=temp.Investment_Strategy__c;
FundStrat.add(fs);
}      
System.debug('FundStrat'+FundStrat);
return FundStrat;
}*/
    /*@AuraEnabled
public static id updateEvent(List<id> contactIds,id eventId){
Set<id> tempContactIds = new Set<id>();
tempContactIds.addAll(contactIds);
List<eventrelation> er=[select id , relationId  from eventrelation where eventid=: eventid];   
List<eventrelation> toDelete=new List<eventrelation>();
List<eventrelation> toAdd=new List<eventrelation>();
EventRelation erTemp;
for(eventrelation temp : er){
if(!tempContactIds.contains(temp.relationId)){
toDelete.add(temp);
}else{
tempContactIds.remove(temp.relationId);
}
}
for(id temp : tempContactIds){
erTemp= new EventRelation();
erTemp.RelationId=temp;
erTemp.EventId=eventId;
erTemp.isParent=true;
toAdd.add(erTemp);
}
insert toAdd;
delete toDelete;
return eventId;
}*/
    /*@AuraEnabled
public static id updateEventFundsAndStrategies(List<id> FundStratIds,id eventId){
List<Event> ev=[select id , Strategies_Funds__c,whoId from Event where id =: eventid];
Id AliasId;
if(ev.size()>0){
AliasId=ev[0].Strategies_Funds__c;
if(AliasId==null){
Activity_alias__c alias = new Activity_alias__c(Activity_ID__c = ev[0].Id);
alias.Contact__c=ev[0].whoid;
insert alias;
ev[0].Strategies_Funds__c=alias.id;
update ev;
AliasId=alias.id;
System.debug('AliasId'+AliasId);
}
} 
System.debug('AliasId'+AliasId);

Set<id> tempFundIds = new Set<id>();
Set<id> tempStratIds = new Set<id>();
List<Activity_Product_Relationship__c> apr=[select id ,Activity_Alias__c,fund__c,Investment_Strategy__c
from Activity_Product_Relationship__c 
where Activity_Alias__c=: AliasId];   
List<Activity_Product_Relationship__c> toDelete=new List<Activity_Product_Relationship__c>();
List<Activity_Product_Relationship__c> toAdd=new List<Activity_Product_Relationship__c>();
for(id tempid : FundStratIds){
if(Id.valueOf(tempid ).getSObjectType() == Schema.Product2.SObjectType)
tempFundIds.add(tempid);
else if(Id.valueOf(tempid).getSObjectType() == Schema.Investment_Strategy__c.SObjectType)
tempStratIds.add(tempid);
}

Activity_Product_Relationship__c aprTemp;
for(Activity_Product_Relationship__c temp : apr){
if(temp.fund__c!= null){
if(!tempFundIds.contains(temp.fund__c)){
toDelete.add(temp);
}else{
tempFundIds.remove(temp.fund__c);
}
}
if(temp.Investment_Strategy__c!= null){
if(!tempStratIds.contains(temp.Investment_Strategy__c)){
toDelete.add(temp);
}else{
tempStratIds.remove(temp.Investment_Strategy__c);
}
}
}

System.debug('AliasId'+AliasId);
for(id temp : tempFundIds){
aprTemp= new Activity_Product_Relationship__c();
aprTemp.fund__c=temp;
aprTemp.Activity_Alias__c=AliasId;
toAdd.add(aprTemp);
}
for(id temp : tempStratIds){
aprTemp= new Activity_Product_Relationship__c();
aprTemp.Investment_Strategy__c=temp;
aprTemp.Activity_Alias__c=AliasId;
toAdd.add(aprTemp);
}
insert toAdd;
delete toDelete;
return eventId;
}*/
    public class Results {
        @AuraEnabled
        public id id;
        
        @AuraEnabled
        public string name;
    }
}