public class AMA_BarChartController {
	
    
    @auraEnabled
    public static DataWrapper prepareChartData(String accId){
        DataWrapper wrapperIns = new DataWrapper();
        BarWrapper barWrapperIns = new BarWrapper();
        SeriesWrapper seriesWrappedIns = new SeriesWrapper();
        list<SeriesWrapper> seriesWrapperList = new list<SeriesWrapper>();
        map<String,map<Integer,Integer>> monthWiseActivityCount = new map<String,map<Integer,Integer>>();
        map<Integer,Integer> monthIndexCount = new map<Integer,Integer>();
        Map <Integer, String> monthNames = new Map <Integer, String> {1=>'Jan', 2=>'Feb', 3=>'Mar', 4=>'Apr', 5=>'May', 6=>'Jun', 7=>'Jul', 8=>'Aug', 9=>'Sep', 10=>'Oct', 11=>'Nov', 12=>'Dec'};
        barWrapperIns.chartTitle = 'Recent Activities';
        barWrapperIns.categories = new list<String>();
       
        for(Event ev : [select Id,ActivityDate,Type__c  from Event where whatID =:accId order by ActivityDate desc]){
            if(!monthIndexCount.containsKey(ev.ActivityDate.month())){
                monthIndexCount.put(ev.ActivityDate.month(),barWrapperIns.categories.size());
                barWrapperIns.categories.add(monthNames.get(ev.ActivityDate.month()));
                
            }
            if(!monthWiseActivityCount.containskey(ev.Type__c )){
                	monthWiseActivityCount.put(ev.Type__c ,new map<Integer,Integer>());
                	monthWiseActivityCount.get(ev.Type__c ).put(ev.ActivityDate.month(),1);
            }
            else{
                if(!monthWiseActivityCount.get(ev.Type__c ).containskey(ev.ActivityDate.month()))
                	monthWiseActivityCount.get(ev.Type__c ).put(ev.ActivityDate.month(),1);
                else
                    monthWiseActivityCount.get(ev.Type__c ).put(ev.ActivityDate.month(),monthWiseActivityCount.get(ev.Type__c ).get(ev.ActivityDate.month())+1);
            }
        }
        wrapperIns.barChartDetails = barWrapperIns;
        for(String mapKey : monthWiseActivityCount.keySet()){
            seriesWrappedIns = new SeriesWrapper();
            seriesWrappedIns.name = mapKey;
        	seriesWrappedIns.data = new Integer[barWrapperIns.categories.size()];
            for(Integer month : monthWiseActivityCount.get(mapKey).keySet()){
                seriesWrappedIns.data.set(monthIndexCount.get(month),monthWiseActivityCount.get(mapKey).get(month));
            }
            for(integer i=0;i<seriesWrappedIns.data.size();i++){
                if(seriesWrappedIns.data.get(i)==null)
                    seriesWrappedIns.data.set(i,0);
            }
            seriesWrapperList.add(seriesWrappedIns);
        }
        
        
       
        wrapperIns.series = seriesWrapperList;
        
         system.debug(wrapperIns);
        return wrapperIns;
       
    }
    
    public class DataWrapper{
        @AuraEnabled
        public list<SeriesWrapper> series{get;set;}
        @AuraEnabled
        public BarWrapper barChartDetails{get;set;}
    }
    
    public Class BarWrapper{
        @AuraEnabled
        public string chartTitle{get;set;}
        @AuraEnabled
        public string yaxisTitle{get;set;}
        @AuraEnabled
        public list<String> categories{get;set;}
        
    }
    public class SeriesWrapper{
        @AuraEnabled
        public string name{get;set;}
        @AuraEnabled
        public list<Integer> data{get;set;}
        public SeriesWrapper(){
            
        }
    }
}