/**************************************************************************************
 Name        :    DeptClientRelationshipTriggerHandler
 Description :    Trigger Handler for Department_Client_Relationship__c
 Created By  :    Deloitte
 Created On  :    17 Sep 2016
 --------------------------------------------------------------------------------------
 Modification Log
 -----------------
 Name                Date            Comments
 -----------------   -------------   --------------------------------------------------
 Hemangini              17-Sep-2016      Created
 --------------------------------------------------------------------------------------
 Review Log
 ----------    
 Name                Date            Comments
 -----------------   -------------   --------------------------------------------------
 
***************************************************************************************/
public class DeptClientRelationshipTriggerHandler extends TriggerHandler{    
    protected override void beforeInsert(){        
        if((triggerParams.objByPassMap.containsKey('DepartmentClientRelationship_Utility.replicateDeptClientRelationship') && !triggerParams.objByPassMap.get('DepartmentClientRelationship_Utility.replicateDeptClientRelationship')) || 
                !triggerParams.objByPassMap.containsKey('DepartmentClientRelationship_Utility.replicateDeptClientRelationship')){
            DepartmentClientRelationship_Utility.replicateDeptClientRelationship(triggerParams);
        }
    }
     
    protected override void afterDelete(){
        if((triggerParams.objByPassMap.containsKey('DepartmentClientRelationship_Utility.deleteDeptClientRelationship') && !triggerParams.objByPassMap.get('DepartmentClientRelationship_Utility.deleteDeptClientRelationship')) || 
                !triggerParams.objByPassMap.containsKey('DepartmentClientRelationship_Utility.deleteDeptClientRelationship')){
            DepartmentClientRelationship_Utility.deleteDeptClientRelationship(triggerParams);
        }
    }
}