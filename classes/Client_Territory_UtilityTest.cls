@isTest
public class Client_Territory_UtilityTest {
    /*
     * Description : Creates a client, a region and retunrs the association
     * Param : Business Line of the territory
     * Returns :  Client territory association
    */
    public static Client_Territory__c createClientTerritory(String Bus_line){
        Account testClients = CommonTestUtils.CreateTestClient('TestClient');
        insert testClients;
        Region__c testRegion = new Region__c(Name='65C',Business_Line__c =Bus_line, Sales_Manager__c = UserInfo.getUserId(), Sales_Account_Manager__c = UserInfo.getUserId());
        insert testRegion;
        Client_Territory__c testClTerritory = new Client_Territory__c(Client_Third_Party__c=testClients.id,Territory__c=testRegion.Id);
        return testClTerritory;
    }
    /*
     * Description : Test method to initiate the insert trigger for APAC business line
     * Param : 
     * Returns :  
    */
    public static testmethod void addAPACClientTerritoryTest(){
        Account testClients = CommonTestUtils.CreateTestClient('TestClientAPAC');
        insert testClients;
        Region__c testRegion = new Region__c(Name='HK Pension',Business_Line__c ='Institutional APAC - Greater China', Sales_Manager__c = UserInfo.getUserId(), Sales_Account_Manager__c = UserInfo.getUserId());
        insert testRegion;
        Region__c testRegion1 = new Region__c(Name='HK Institutional',Business_Line__c ='Institutional APAC - Japan', Sales_Manager__c = UserInfo.getUserId(), Sales_Account_Manager__c = UserInfo.getUserId());
        insert testRegion1;
        Client_Territory__c testClTerritory = new Client_Territory__c(Client_Third_Party__c=testClients.id,Territory__c=testRegion.Id);
        Client_Territory__c testClTerritory1 = new Client_Territory__c(Client_Third_Party__c=testClients.id,Territory__c=testRegion1.Id);
        List<Client_Territory__c> testCLTerritoryList = new List<Client_Territory__c>{testClTerritory,testClTerritory1};
        Test.startTest();
        insert testClTerritoryList;    
        delete testCLTerritoryList[0];
        delete testCLTerritoryList[1];
        Test.stopTest();
    }
    
    /*
     * Description : Test method to initiate the insert trigger for retail business line
     * Param : 
     * Returns :  
    */
    public static testmethod void addClientTerritoryTest(){
        Client_Territory__c testClTerritory = createClientTerritory('Retail');
        Account testClients = CommonTestUtils.CreateTestClient('TestClient2');
        insert testClients;
        List<Client_Territory__c> testCLTerritoryList = new List<Client_Territory__c>{new Client_Territory__c(Client_Third_Party__c=testClients.id,Territory__c=testClTerritory.Territory__c),testClTerritory};
        Test.startTest();
        insert testClTerritoryList;
        Region__c prRegion = [Select Business_Line__C from Region__c where id=: testClTerritory.Territory__c limit 1];
        System.assertEquals('Retail', prRegion.Business_Line__C);        
        Test.stopTest();
    }
    
    /*
     * Description : Test method to initiate the delete trigger for retail business line
     * Param : 
     * Returns :  
    */
    public static testmethod void  TestExceptions1(){
        User usr =  CommonTestUtils.CreateTestUser(true);
        usr.Alias = 'iuser';
        update usr;
        Id currUserId = UserInfo.getUserId();
        
        System.RunAs(usr){
            Account TestClient2 = CommonTestUtils.CreateTestClient('TestClientwithoutPostalCode');
            TestClient2.Billing_PostalCode__c = '';
            insert TestClient2;
            
            AccountTeamMember ATM = new AccountTeamMember(AccountId = TestClient2.Id, TeamMemberRole = Label.Customer_Relationship_Manager, UserId = currUserId, AccountAccessLevel = Label.Read_Write, CaseAccessLevel = Label.Private, OpportunityAccessLevel = Label.Private);
            insert ATM;
        }
        List<Account> TestClient2 = [Select Id,Territory_Code__c from Account];
        
        Client_Territory__c testClTerritory = createClientTerritory('Retail');
        List<Client_Territory__c> testCLTerritoryList = new List<Client_Territory__c>{new Client_Territory__c(Client_Third_Party__c=TestClient2[0].Id,Territory__c=testClTerritory.Territory__c),testClTerritory};
        
        Test.startTest();
        try{
            insert testClTerritoryList;
        }catch (Exception ex){
            System.assertEquals(ex.getMessage().contains('Postal Code is a required field'), true);
        }   
        list<Client_Territory__c> results1 = [select id from Client_Territory__c];
        TestClient2 = [Select Id,Territory_Code__c from Account];
        System.assertEquals(2, results1.size());
        Test.stopTest();
    }
    
    public static testmethod void  TestExceptions2(){
        User usr =  CommonTestUtils.CreateTestUser(true);
        usr.Alias = 'iuser';
        update usr;
        Id currUserId = UserInfo.getUserId();
        
        System.RunAs(usr){
            Account TestClient2 = CommonTestUtils.CreateTestClient('TestClientwithoutPostalCode');
            TestClient2.Billing_PostalCode__c = '';
            insert TestClient2;
            
            AccountTeamMember ATM = new AccountTeamMember(AccountId = TestClient2.Id, TeamMemberRole = Label.Customer_Relationship_Manager, UserId = currUserId, AccountAccessLevel = Label.Read_Write, CaseAccessLevel = Label.Private, OpportunityAccessLevel = Label.Private);
            insert ATM;
        
            Client_Territory__c testClTerritory = createClientTerritory('Retail');
            Account TestClient3 = CommonTestUtils.CreateTestClient('TestClientwithoutPostalCode2');
            TestClient3.Billing_PostalCode__c = '';
            insert TestClient3;
            ATM = new AccountTeamMember(AccountId = TestClient3.Id, TeamMemberRole = Label.Customer_Relationship_Manager, UserId = currUserId, AccountAccessLevel = Label.Read_Write, CaseAccessLevel = Label.Private, OpportunityAccessLevel = Label.Private);
            insert ATM;
            
            List<Client_Territory__c> testCLTerritoryList = new List<Client_Territory__c>{new Client_Territory__c(Client_Third_Party__c=TestClient3.Id,Territory__c=testClTerritory.Territory__c),testClTerritory};
            insert testClTerritoryList;
        }
        list<Client_Territory__c> cltTer = [select id from Client_Territory__c];
        /*list<Account> TestClient2 = [Select Id,Territory_Code__c from Account];
        region__c region1 = new region__c(business_line__c = 'Retail', Name='61', is_active__c = true);*/
        Test.startTest();
        //cltTer[0].Territory__c = region1.Id;
        try{
            delete cltTer;
        }catch (Exception ex){
            System.assertEquals(ex.getMessage().contains('Postal Code is a required field'), true);
        }
        Test.StopTest();
        /*
        Test.startTest();
        try{
            delete cltTer;
        }catch (Exception ex){
            System.assertEquals(ex.getMessage().contains('Postal Code is a required field'), true);
        }
        Test.StopTest();*/
    }
    
    /*
     * Description : Test method to initiate the delete trigger for retail business line
     * Param : 
     * Returns :  
    */
    public static testmethod void  removeClientTerritoryTest(){
        Client_Territory__c testClTerritory = createClientTerritory('Retail');
        Account testClients = CommonTestUtils.CreateTestClient('TestClient2');
        insert testClients;
        List<Client_Territory__c> testCLTerritoryList = new List<Client_Territory__c>{new Client_Territory__c(Client_Third_Party__c=testClients.id,Territory__c=testClTerritory.Territory__c),testClTerritory};
        insert testCLTerritoryList;
        ID tcID = testClTerritory.Id;
        Test.startTest();
        delete testCLTerritoryList ;
        System.assertEquals(0, [Select COUNT() from Client_Territory__c where Id=: tcID limit 1]);
        Test.stopTest();
    }  
    
    
    /*
     * Description : Test method to check only 1 uk region allowed
     * Param : 
     * Returns :  
    */
    public static testmethod void  errorIfMoreThanOneUkRegionTest(){
        Account testClient = CommonTestUtils.CreateTestClient('Client 00001');
        testclient.Billing_PostalCode__c = 'AA11 11AA';
        insert testClient;
        
        list<Region__c> regions = new list<region__c>();
        region__c region1 = new region__c(business_line__c = 'Retail', Name='61', is_active__c = true);
        regions.add(region1);
        region__c region2 = new region__c(business_line__c = 'Retail', Name='63', is_active__c = true);
        regions.add(region2);
        region__c region3 = new region__c(business_line__c = 'Retail', Name='PS', is_active__c = true);
        regions.add(region3);
        insert regions;
        
        Test.startTest();
        // Add a UK region (allowed)
        client_territory__c ct1 = new client_territory__c(client_third_party__c = testClient.id, territory__c = region1.id);
        insert ct1;
        list<Client_Territory__c> results1 = [select id from Client_Territory__c];
        System.assertEquals(1, results1.size());
        
        // Add a non-UK region (allowed)
        client_territory__c ct2 = new client_territory__c(client_third_party__c = testClient.id, territory__c = region3.id);
        insert ct2;
        list<Client_Territory__c> results2 = [select id from Client_Territory__c];
        System.assertEquals(2, results2.size());
        
        // Add a second UK region (not allowed)
        client_territory__c ct3 = new client_territory__c(client_third_party__c = testClient.id, territory__c = region2.id);
        try {
            insert ct3;
            System.assert(true, 'Should have received an error message as 1 UK Region has already been assigned to this client');
        } catch(Exception ex)  {
            System.assert(ex.getMessage().contains('A UK territory has already been added for this client.'), 'Failed with unexpected exception');
        } 
        list<Client_Territory__c> results3 = [select id from Client_Territory__c];
        System.assertEquals(2, results3.size());
        
      
        list<Client_Territory__c> results4 = [select id from Client_Territory__c];
        System.assertEquals(2, results4.size());

        Test.stopTest();
    } 
}