/*----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            AccountPrimaryCoverageController
   Description:     Controller for AccountTeamMemberList Lightning component
                    
   Date             Author                             Summary of Changes                  
   -----------      -----------------                  -------------------------------------------------------------------------------------
   09 Sep 2016      Sridhar Gurram                     Initial Release 
------------------------------------------------------------------------------------------------------------------------------------------------------------*/

public with sharing class AccountPrimaryCoverageController{
    //Empty constructor
    public void AccountPrimaryCoverageController(){
    
    }
    
    /*
     * Description : Method to retrieve all Account Team Members for an Account
     * Param : AccountId passed as recordId
     * Returns : List of WrapperClass object 
    */
    @AuraEnabled
    public static List<AccountTMResult> findAll(string recordId) {
        System.debug('recordId'+recordId);
        List<AccountTMResult> ATMList = new List<AccountTMResult>();
        if(recordId!=null && recordId != ''){
            Account acc = [Select Primary_Coverage_Member__c from Account where id = :recordId];
            List<AccountTeamMember> TMList = [Select AccountId, UserId, User.Name, TeamMemberRole from AccountTeamMember where AccountId = :recordId];
            AccountTMResult ATM;
            System.debug(TMList);
            if(TMList.size()>0){
              for(AccountTeamMember TM: TMList){
                ATM = new AccountTMResult();
                ATM.AccountTMId = TM.Id;
                ATM.AccountTMName = TM.User.Name;
                ATM.AccountTMRole = TM.TeamMemberRole;
                ATM.UserId = TM.UserId;
                //Check if the Account already has a primary
                if(acc.Id != null && acc.Primary_Coverage_Member__c == TM.UserId ){
                    ATM.value = true;
                }
                else ATM.value = false;
                ATMList.add(ATM);
              }
            }
       }
       system.debug(ATMList);
       return ATMList;
    }
 
 	public class MyException extends Exception {} 
 	   
    /*
     * Description : Method to update the Primary Coverage Member on Account
     * Param : AccountId and UserId
     * Returns : Boolean stating the update was successful or not  
    */
    @AuraEnabled
    public static String updatePrimary(string accountId, string primaryId, string errMsg) {
    
        String stat =  '';
        if((accountId!=null && accountId!= '')){// && (primaryId!=null && primaryId!= '')){
            Account acc = [Select Id, Primary_Coverage_Member__c from Account where id = :accountId];
            if(acc != null && Account.Primary_Coverage_Member__c.getDescribe().isUpdateable()){
                if(String.isBlank(primaryId)) acc.Primary_Coverage_Member__c = NULL;
                else acc.Primary_Coverage_Member__c = primaryId;
                //Try catch block for error handling
                 try{
                    update acc;
                    if(Test.isRunningTest() && errMsg != ''){
                    	throw new MyException(errMsg);
                    }else 
                    	stat = 'Success';
                 }catch(Exception e){
                    CommonUtilities.createExceptionLog(e);
                    if (e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')){
                        stat = e.getMessage().substringAfterLast('FIELD_CUSTOM_VALIDATION_EXCEPTION, ').substringBefore(':');
                    }else if (e.getMessage().contains('INSUFFICIENT_ACCESS_OR_READONLY')){
                        stat = e.getMessage().substringAfterLast('INSUFFICIENT_ACCESS_OR_READONLY, ').substringBefore(':');
                    }else if (e.getMessage().contains('UNABLE_TO_LOCK_ROW')){
                        stat = e.getMessage().substringAfterLast('UNABLE_TO_LOCK_ROW, ').substringBefore(':');
                    }else stat = e.getMessage();    
                 }
            }
        }
        return stat;
    }
    
    /*
     * Description : Wrapper Class
     * Param : 
     * Returns :  
    */
    public class AccountTMResult{
        @AuraEnabled
        public id AccountTMId;
        
        @AuraEnabled
        public string AccountTMName;
        
        @AuraEnabled
        public string AccountTMRole;

        @AuraEnabled
        public boolean value;
        
        @AuraEnabled
        public String UserId;
    }

}