public with sharing class Account_Utility{

    /*
     * Description : Create new instance / return existing instance
     * Param : 
     * Returns :  
    */
    private static Account_Utility instance = null; 
    
    public static Account_Utility getInstance() {
        if (instance == null) {
            instance = new Account_Utility();
        }
        return instance;
    }
    
    public static void copyChildRecords(TriggerHandler.TriggerParameter tParam){
        copyChildRecords((List<Account>) tParam.newList);
    }
    
    public static void copyChildRecords(List<Account> newList){
        System.debug('The new List values are ---> '+ newList);
        Map<Id, Set<Id>> parentIdsMap = new Map<Id, Set<Id>>();
        List<Id> allParentIds = new List<Id>();
        
        for(Account each: [select Id, ParentId, Parent.ParentId, Parent.Parent.ParentId from Account where Id IN :newList]){
              Set<Id> parentIds = new Set<Id>();
              Set<Id> L1parentIds = new Set<Id>();
              Set<Id> L2parentIds = new Set<Id>();
              Set<Id> L5parentIds = new Set<Id>();
            if(each.ParentId != NULL){
                L1parentIds.add(each.ParentId);
                if(each.Parent.ParentId != NULL){
                    L2parentIds.add(each.Parent.ParentId);
                    if(each.Parent.Parent.ParentId != NULL){
                        L5parentIds.add(each.Parent.Parent.ParentId);
                         parentIdsMap.put(each.Id, L5parentIds);
                    }
                    parentIdsMap.put(each.Id, L2parentIds);
                }
                parentIdsMap.put(each.Id, L1parentIds);
            }
            allParentIds.addAll(L5parentIds);
            allParentIds.addAll(L2parentIds);
            allParentIds.addAll(L1parentIds);
        }
        Set<String> alreadyAdded = new Set<String>();
        String emptyString = '';
        String depClientObj = 'Department_Client_Relationship__c';
        String recFundObj = 'Client_Fund__c';
        
        List<sObject> allChildRecs = new List<sObject>();
        
        
        
        Map<Id, Account> withChildRecs = new Map<Id, Account>(
                                            [select Id, (select Department__c from Department_Client_Relationships__r), 
                                              (select Fund_Status_at_Company__c, Product__c from Client_Funds__r)
                                              from Account where Id IN :allParentIds]);

        for(Account each: newList){
            if(parentIdsMap.containsKey(each.Id) && !parentIdsMap.get(each.Id).isEmpty()){
                for(Id eachParentId: parentIdsMap.get(each.Id)){
                    if(withChildRecs.containsKey(eachParentId)){

                        if(!withChildRecs.get(eachParentId).Department_Client_Relationships__r.isEmpty()){
                            for(Department_Client_Relationship__c eachChild: withChildRecs.get(eachParentId).Department_Client_Relationships__r){
                                if(!alreadyAdded.contains(emptyString + each.Id + eachChild.Department__c)){
                                    sObject childRec = Schema.getGlobalDescribe().get(depClientObj).newSObject();
                                    childRec.put('Is_Inherited_from_Hierarchy__c', true);
                                    childRec.put('Client__c', emptyString + each.Id);
                                    childRec.put('Department__c', emptyString + eachChild.Department__c);
                                    allChildRecs.add(childRec);
                                    alreadyAdded.add(emptyString + each.Id + eachChild.Department__c);
                                }
                            }
                        }
                      
                        
                        if(!withChildRecs.get(eachParentId).Client_Funds__r.isEmpty()){
                            for(Client_Fund__c eachChild: withChildRecs.get(eachParentId).Client_Funds__r){

                                if(!alreadyAdded.contains(emptyString + each.Id + eachChild.Product__c)){

                                    sObject childRec = Schema.getGlobalDescribe().get(recFundObj).newSObject();
                                    childRec.put('Is_Inherited_From_Hierarchy__c', true);
                                    childRec.put('Client__c', emptyString + each.Id);
                                    childRec.put('Product__c', emptyString + eachChild.Product__c);
                                    childRec.put('Fund_Status_at_Company__c', eachChild.Fund_Status_at_Company__c);
                                    allChildRecs.add(childRec);
                                    alreadyAdded.add(emptyString + each.Id + eachChild.Product__c);
                                }

                            }
                        }
                    }
                }
            }
        }
        allChildRecs.sort();

        Database.SaveResult[] sResult = Database.insert(allChildRecs, false);        
        for(Database.SaveResult each: sResult){
            if(!each.isSuccess()){
                system.debug('****An unexpected exception occurred: ' + each.getErrors());
            }
        }
       
    }
    public static void updateBusinessLineCheckboxes(TriggerHandler.TriggerParameter tParam){
        List<Account> newList = (List<Account>) tParam.newList;
        string pickListValue='';
        string fieldName='';
        Map<String,Schema.SObjectField> fieldsMap = Schema.getGlobalDescribe().get('Account').getDescribe().fields.getMap(); 
        List<String> pickListValueList = New List<String>();
        for(Account acc : newList){
            pickListValue = acc.Business_Line__c;
            for(string key: Busines_Line_Mapping__c.getAll().keySet()){
                acc.put(Busines_Line_Mapping__c.getValues(key).field_Name__c,false);
            }
            if(pickListValue !=null){
                pickListValueList = pickListValue.split(';');
                for(string key : pickListValueList){
                    acc.put(Busines_Line_Mapping__c.getValues(key).field_Name__c,true);
                }
            }
        }
    }
    
    public static void validateParentHierarchy(TriggerHandler.TriggerParameter tParam){
        Map<string,integer> hierarchyMap = new Map<string,integer>();
        hierarchyMap.put('L0',0);
        hierarchyMap.put('L1',1);
        hierarchyMap.put('L2',2);
        hierarchyMap.put('L5',5);
        Map<id,Account> newMap = (Map<id,Account>) tParam.newMap;
        String parentAcclevel ;
        try{
            List<Account> childAccountPerParent = new List<Account>();
            List<Account> childRecord = [select id,ParentId,Hierarchy_Level__c from Account where parentId in : newMap.keySet()];
            Map<id,List<Account>> childRecordMap = new Map<id,List<Account>>();
            for(Account acc : childRecord){
                if(childRecordMap.containsKey(acc.ParentId)) {
                    childRecordMap.get(acc.ParentId).add(acc);
                   
                } else {
                    childRecordMap.put(acc.ParentId, new List<account> { acc });
                }
            }
            for(id parentAccId : newMap.keyset()){
                parentAcclevel = newMap.get(parentAccId).Hierarchy_Level__c;
                childAccountPerParent = childRecordMap.get(parentAccId);
                if(childAccountPerParent!=null){
                    for(account acc : childAccountPerParent ){
                        if(hierarchyMap.get(parentAcclevel)>=hierarchyMap.get(acc.Hierarchy_Level__c)){
                            newMap.get(parentAccId).addError(Label.Parent_Level_cant_be_same_or_lower_level_than_child);
                        }
                    }
                }
            }  
        }Catch(Exception Ex){
            CommonUtilities.createExceptionLog(ex);
        }
    }
    //@Joshna, 18 Oct - updated to fix issue listed in DE6859
    public static void moveChildRecords(TriggerHandler.TriggerParameter tParam){
        Map<Id, Account> oldMap = (Map<Id, Account>) tParam.oldMap;
        List<Account> newList = (List<Account>) tParam.newList;
        List<Account> toBeProcessed = new List<Account>();
        List<sObject> toBeDeleted = new List<sObject>();
        List<sObject> toBeDeleted1 = new List<sObject>();
        List<sObject> toBeDeleted2 = new List<sObject>();
        
        List<Account> toBeReProcessed = new List<Account>();
       
        Map<Id, List<Client_Fund__c>> parentFunds = new Map<Id, List<Client_Fund__c>>();
        for(Account each: newList){
            //@Joshna, 18 Oct - @DE6859, commented a clause in if condition below
            if(/*each.ParentId != NULL && */each.ParentId != oldMap.get(each.Id).ParentId){
                toBeProcessed.add(each);
            }
        }        
        //@Joshna, 18 Oct - @DE6859, included Parent Id clauses to the where clause
        for(Account each: [select Id, ParentId, (select Id from Department_Client_Relationships__r), (select Id,Is_Inherited_From_Hierarchy__c from Client_Funds__r) from Account 
                            where (Id IN :toBeProcessed OR ParentId IN :toBeProcessed OR Parent.ParentId IN :toBeProcessed OR Parent.Parent.ParentId IN :toBeProcessed)]){
                             
            if(!each.Department_Client_Relationships__r.isEmpty()){   
                toBeDeleted.addAll((List<sObject>)each.Department_Client_Relationships__r);
            }
            if(!each.Client_Funds__r.isEmpty()){            
               for(Client_Fund__c cf : each.Client_Funds__r){
                    if(cf.Is_Inherited_From_Hierarchy__c == true)                    
                         toBeDeleted1.add(cf);                 
                }
            }
            if(each.ParentId != NULL){
                toBeReProcessed.add(each);
            }
        }
        
        for(Contact each: [select Id, (select Id from Department_Contact_Relationships__r) from Contact where AccountId IN :toBeProcessed]){
            if(!each.Department_Contact_Relationships__r.isEmpty()){
                toBeDeleted2.addAll((List<sObject>) each.Department_Contact_Relationships__r);
            }
        }
        //sorting the list
        //toBeDeleted.sort();
        
        //Splitting into multiple statements as bulk load throws chunking error inspite of sorting the list
        if(!toBeDeleted.isEmpty()){
            List<Database.DeleteResult> drRecs = Database.delete(toBeDeleted, false);
            for(Database.DeleteResult each: drRecs){         
                if(!each.isSuccess()){                
                    system.debug('****An error occurred: ' + each.getErrors());
                }
            }
        }
        if(!toBeDeleted1.isEmpty()){
            List<Database.DeleteResult> drRecsSecond = Database.delete(toBeDeleted1, false);
            for(Database.DeleteResult each: drRecsSecond){         
                if(!each.isSuccess()){                
                    system.debug('****An error occurred: ' + each.getErrors());
                }
            }
        }
        if(!toBeDeleted2.isEmpty()){
            List<Database.DeleteResult> drRecsThird = Database.delete(toBeDeleted2, false);
            for(Database.DeleteResult each: drRecsThird){         
                if(!each.isSuccess()){                
                    system.debug('****An error occurred: ' + each.getErrors());
                }
            }
        }
        
        //@Joshna, 18 Oct - @DE6859, updated parameter to toBeReProcessed
        copyChildRecords(toBeReProcessed);
    }
    
    /*
     * Description : Method to update the external id field on change of relationship
     *                during merge of account
     * Param : TriggerHandler.TriggerParameter 
     * Returns :  
    */
    public static void updateExternalIdOnMerge(TriggerHandler.TriggerParameter tParam){
        
        Map<Id, Account> oldMap = (Map<Id, Account>) tParam.oldMap;
        Set<Id> setOfaccountId = new Set<Id>();
        for(Account each: oldMap.values()){
            //indetify the records being merged
            if(each.MasterRecordId != NULL){
                setOfaccountId.add(each.MasterRecordId);
            }
        }
        
        if(!setOfaccountId.isEmpty()){
            List<Client_Third_Party_Relationship__c> listOfCP = [SELECT ID,  Client_Name__c ,
                                                                      Third_Party_Name__c
                                                               FROM   Client_Third_Party_Relationship__c 
                                                               WHERE  Client_Name__c IN: setOfaccountId 
                                                               OR     Third_Party_Name__c  IN:setOfaccountId];
                                                           
            if(listOfCP != null && listOfCP.size() > 0){
                for(Client_Third_Party_Relationship__c each:listOfCP){
                    each.External_Id__c = each.Client_Name__c + '|' + each.Third_Party_Name__c;
                }
            }
            
            Savepoint sp = Database.setSavepoint();
            try{
                if(listOfCP != null && listOfCP.size() > 0)
                    update listOfCP;
            }catch(Exception ex){
                Database.rollBack(sp);
                CommonUtilities.createExceptionLog(ex);
            }
            
        }
    }
    public static void checkDuplicateAccout(List<Account> acc){
        
        string accName = acc[0].Name;
        List<account> allAccount = new List<Account>();
        allAccount = [ SELECT Id, Name from Account where Name = :accName];
        
        //system.assert(false, allAccount.size());
        if(allAccount.size() > 0 && allAccount != null)
        {
            //system.assert(false, 'aaa' + allAccount.size() + allAccount);
            acc[0].addError('This account already exists. The duplicate account is: <a href=\'/'+ acc[0].Id +'\'>Agency Name '+ acc[0].Name + '</a>' );
        
        }
        //system.assert(false,acc);
    }
    
}