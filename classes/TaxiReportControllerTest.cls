@isTest
public class TaxiReportControllerTest {
    
    //Fig user
    static User getFigUser(){
        //New user to create owner for company
        id userId = UserInfo.getUserId() ;
        user uss=[select LanguageLocaleKey,ProfileId,TimeZoneSidKey,LocaleSidKey,EmailEncodingKey from user where id = : userId ];
        User user = new User();
        user.Username='deloitte89837298123@deloitte.com';
        user.firstname = 'Rachel';
        user.LastName='Green';
        user.email='deloitte89837298123@deloitte.com';
        user.Alias='Rach';
        user.TimeZoneSidKey=uss.TimeZoneSidKey;
        user.LocaleSidKey=uss.LocaleSidKey;
        user.EmailEncodingKey=uss.EmailEncodingKey;
        user.ProfileId=uss.ProfileId;
        user.LanguageLocaleKey=uss.LanguageLocaleKey;
        user.Is_FIG__c = true;
        insert user;
        return user;
    }
    
    //Normal user
    static User getNormUser(){
        //New user to create owner for company
        id userId = UserInfo.getUserId() ;
        user uss=[select LanguageLocaleKey,ProfileId,TimeZoneSidKey,LocaleSidKey,EmailEncodingKey from user where id = : userId ];
        User user = new User();
        user.Username='deloitte89837298667@deloitte.com';
        user.firstname = 'Ross';
        user.LastName='Geller';
        user.email='deloitte89837298166@deloitte.com';
        user.Alias='Ron';
        user.TimeZoneSidKey=uss.TimeZoneSidKey;
        user.LocaleSidKey=uss.LocaleSidKey;
        user.EmailEncodingKey=uss.EmailEncodingKey;
        user.ProfileId=uss.ProfileId;
        user.LanguageLocaleKey=uss.LanguageLocaleKey;
        insert user;
        return user;
    }    
 
    // code coverage for account's Taxi Report
    static testMethod void coverLoadPageAccount(){ 
        User user = getFigUser();
        System.runAs(user){
            // Create test Company
            Account com = CommonTestUtils.CreateTestClient('Test Company');
            //com.Gross_Sales_YTD__c = 50;
            com.Current_AUM__c = 50;
            com.OwnerId = user.Id;   
            insert com;
            
            // Create test account
            Account acc = CommonTestUtils.CreateTestClient('Test Client');
            acc.OwnerId = user.Id;      
            acc.Billing_Street_Line_1__c ='a';
            acc.Billing_Street_Line_2__c ='a';
            acc.Billing_Street_Line_3__c ='a';
            acc.Billing_City__c = 'c';
            acc.Billing_Country__c='India';
            acc.Billing_PostalCode__c ='560037';
            acc.Phone = '9849858728';
            acc.FCA_Number__c = 'FCA11001';
            acc.Hierarchy_Level__c = 'L2';
            acc.ParentId = com.Id;          
            insert acc;
            
            Id rtFlowId = UtilsFinAum.RecTypesMap.get('TotalSummaryFlow').Id;
            
            
            
            
            Id rtAumId = UtilsFinAum.RecTypesMap.get('TotalSummaryAUM').Id;
            
            list<Assets_Under_Management__c> AUMs = new list<Assets_Under_Management__c>();
            Assets_Under_Management__c aum1 = new Assets_Under_Management__c();
            aum1.Company_Name__c = acc.id;
            aum1.Fund_Name__c = 'IP High Income'; 
            aum1.Current_AUM__c = 11; 
            aum1.Gross_Sales_YTD__c = 1;
            aum1.Redemptions_YTD__c = 5;
            aum1.RecordTypeId = rtAumId;
            aums.add(aum1); 
            
            Assets_Under_Management__c aum2 = new Assets_Under_Management__c();
            aum2.Company_Name__c = acc.id;
            aum2.Fund_Name__c = 'IP Asia Balanced';
            aum2.Current_AUM__c = 12;            
            aum2.Gross_Sales_YTD__c = 2;
            aum2.Redemptions_YTD__c = 6;            
            aum2.RecordTypeId = rtAumId;
            aums.add(aum2);     
            
            insert aums;    
            
            Account anotherAcc = new Account(Name = '3rd party', Billing_PostalCode__c = '505050', Hierarchy_Level__c = 'L0', Region__c = 'EMEA', Business_Line__c = 'Retail', Status__c = 'Active');
            insert anotherAcc;
            Client_Third_Party_Relationship__c newRel = new Client_Third_Party_Relationship__c(Client_Name__c = acc.Id, Third_Party_Name__c = anotherAcc.Id);
            insert newRel;
            Product2 productRec = new Product2(Name = 'Our Product', Asset_Manager_Name__c = 'AXA');
            insert productRec;
            Rated_Fund__c newFund = new Rated_Fund__c(Client__c = anotherAcc.Id, Fund__c = productRec.Id);
            insert newFund;
            // Load Taxi Report for account
            ApexPages.currentPage().getParameters().put('accountId', acc.Id);
            TaxiReportController h = new TaxiReportController();
            
            Id rmId = h.rtStandardAUMId;
            Boolean kc = h.hasKeyContacts;
            Boolean re = h.hasRecentEvents;
            Boolean fe = h.hasFutureEvents;
            Boolean pce = h.hasPrevCompanyEvents;
            Boolean ip = h.hasIPproducts;
            Boolean ep = h.hasExternalPanels;
            list<Rated_Fund__c> rf = h.ExternalPanels;
            ep = h.hasExternalPanels;
            String py = h.PreviousYear;
            String ppy = h.PreviousPreviousYear;
            Set<String> setOfVal = h.fundsOnRecommendedList;
            setOfVal = h.currentIPOpportunities;
            setOfVal = h.competitorFundsForClient;
        }
    }
    
    // Check URL returned by redirect for SF Contact & SF1 Contact
    static testMethod void redirectSfContact(){
        User user = getFigUser();
        
        System.runAs(user){
            
            // Create test Company
            Account com = CommonTestUtils.CreateTestClient('Test Company232');
            //com.Gross_Sales_YTD__c = 51;
            com.Current_AUM__c = 51;
            com.OwnerId = user.Id;   
            insert com;
            // Create test account
            Account acc = CommonTestUtils.CreateTestClient('Test Client232');
            acc.OwnerId = user.Id;      
            acc.Billing_Street_Line_1__c ='b';
            acc.Billing_Street_Line_2__c ='b';
            acc.Billing_Street_Line_3__c ='b';
            acc.Billing_City__c = 'd';
            acc.Billing_Country__c='India';
            acc.Billing_PostalCode__c ='560077';
            acc.Phone = '9849859728';
            acc.FCA_Number__c = 'FCA11442';
            acc.Hierarchy_Level__c = 'L2';
            acc.ParentId = com.Id;   
            
            insert acc; 
            
            Contact con = new Contact(LastName = 'Test Contact 2',
                                      AccountId = acc.id,status__c='Active');
               
            insert con;
            
            
            // Load Taxi Report for contact
            ApexPages.currentPage().getParameters().put('contactId', con.Id);
            TaxiReportController trc = new TaxiReportController();
            PageReference pr = trc.redirect();
            system.assertEquals('/apex/TaxiReport?contactId=' + con.id, pr.getUrl());            
           
        }
        
        
    }
    
    static testMethod void coverKeyContacts(){
        User user = getFigUser();
        
        System.runAs(user){
            
            list<Account> accounts = new list<Account>();
            Account com = CommonTestUtils.CreateTestClient('Test Company1');
            //com.Gross_Sales_YTD__c = 50;
            com.Current_AUM__c = 50;
            com.OwnerId = user.Id;   
            insert com;
            // Create test account
            Account acc = CommonTestUtils.CreateTestClient('Test Client1');
            acc.OwnerId = user.Id;      
            acc.Billing_Street_Line_1__c ='e';
            acc.Billing_Street_Line_2__c ='b';
            acc.Billing_Street_Line_3__c ='b';
            acc.Billing_City__c = 'c';
            acc.Billing_Country__c='India';
            acc.Billing_PostalCode__c ='560338';
            acc.Phone = '9849858929';
            acc.FCA_Number__c = 'FCA11093';
            acc.Hierarchy_Level__c = 'L2';
            acc.ParentId = com.Id; 
            
            
            insert acc; 
            
            // Create test account
            Account acc2 = CommonTestUtils.CreateTestClient('Test Client2');
            acc2.OwnerId = user.Id;      
            acc2.Billing_Street_Line_1__c ='5';
            acc2.Billing_Street_Line_2__c ='a';
            acc2.Billing_Street_Line_3__c ='a';
            acc2.Billing_City__c = 'c';
            acc2.Billing_Country__c='India';
            acc2.Billing_PostalCode__c ='560031';
            acc2.Phone = '9849858720';
            acc2.FCA_Number__c = 'FCA11011';
            acc2.Hierarchy_Level__c = 'L5';
            acc2.ParentId = acc.Id;  
            insert acc2; 
            
            // create test contact
            
            Contact con = CommonTestUtils.CreateTestContact('test contact11');
            con.Title ='title'; 
            con.Preferred_Name__c='Phoebe2';
            con.Phone ='9849858991';
            con.Email='abc11@delta.com';
            con.Status__c ='Active';
            con.AccountId = acc.id;
            insert con;
            
            // create test contact
            
            Contact con2 = CommonTestUtils.CreateTestContact(' contact11');
            con2.Title ='aaa11'; 
            con2.Preferred_Name__c='Phoebe44';
            con2.Phone ='9846558727';
            con2.Email='abcg55f@delta.com';
            con2.Status__c ='Active';
            con.AccountId = acc2.id;
            insert con2;
            // create test AccountContactRelation
            /*AccountContactRelation acr = new AccountContactRelation();
            acr.ContactId = con.id;
            acr.AccountId = acc.id;
            acr.Key_Contact__c = true;
            insert acr;*/
            ApexPages.currentPage().getParameters().put('accountId', acc2.Id);
            TaxiReportController h = new TaxiReportController();
        }
        
        
    }
    
    
    static testMethod void coverAllEvents(){
        
        User user = getFigUser();
        
        System.runAs(user){
            
            list<Account> accounts = new list<Account>();
            Account com = CommonTestUtils.CreateTestClient('Test Company');
            //com.Gross_Sales_YTD__c = 50;
            com.Current_AUM__c = 50;
            com.OwnerId = user.Id;   
            insert com;
            // Create test account
            Account acc = CommonTestUtils.CreateTestClient('Test Client');
            acc.OwnerId = user.Id;      
            acc.Billing_Street_Line_1__c ='bbb';
            acc.Billing_Street_Line_2__c ='bbbb';
            acc.Billing_Street_Line_3__c ='bbb';
            acc.Billing_City__c = 'cbb';
            acc.Billing_Country__c='India';
            acc.Billing_PostalCode__c ='590337';
            acc.Phone = '9849859928';
            acc.FCA_Number__c = 'FCA11991';
            acc.Hierarchy_Level__c = 'L2';
            acc.ParentId = com.Id; 
            
            
            insert acc; 
            
            // Create test account
            Account acc2 = CommonTestUtils.CreateTestClient('Test Client99');
            acc2.OwnerId = user.Id;      
            acc2.Billing_Street_Line_1__c ='a99';
            acc2.Billing_Street_Line_2__c ='a99';
            acc2.Billing_Street_Line_3__c ='a';
            acc2.Billing_City__c = 'c';
            acc2.Billing_Country__c='India';
            acc2.Billing_PostalCode__c ='560111';
            acc2.Phone = '9849858000';
            acc2.FCA_Number__c = 'FCA11222';
            acc2.Hierarchy_Level__c = 'L5';
            acc2.ParentId = acc.Id;  
            insert acc2; 
            
            // create test contact
            
            Contact con = CommonTestUtils.CreateTestContact('test contact33');
            con.Title ='title'; 
            con.Preferred_Name__c='Phoebe33';
            con.Phone ='9849888888';
            con.Email='abc111@delta.com';
            con.Status__c ='Active';
            
            con.AccountId = acc2.id;
            insert con;            
            //create Recent Events
            //Master Event
            Event mev = new Event();
            //mev.AccountId =acc.id;
            mev.Description= 'master event';
            mev.WhatId = acc.id;
            mev.StartDateTime = System.now()-1;
            mev.DurationInMinutes__c = 15;
            mev.DurationInMinutes =15;
            //create Event
            Event ev = new Event();
            //ev.AccountId = acc2.id;
            ev.Description= 'play event';
            ev.Subject = 'all';
            ev.WhatId = acc2.id;
            ev.WhoId = mev.id;
            ev.DurationInMinutes__c = 15;
            ev.DurationInMinutes =15;
            ev.StartDateTime = System.now()-1;
            
            Event ev1 = new Event();
            //ev.AccountId = acc2.id;
            ev1.Description= 'play event';
            ev1.Subject = 'all';
            ev1.WhatId = acc2.id;
            ev1.WhoId = mev.id;
            ev1.DurationInMinutes__c = 15;
            ev1.DurationInMinutes =15;
            ev1.StartDateTime = System.now()+1;
            List<Event> events = new List<Event>();
            events.add(mev);
            events.add(ev);
            events.add(ev1);
            insert events;
            
            EventRelation mer = new EventRelation();
            //mer.AccountId = acc.id;
            mer.EventId = mev.Id;
            mer.RelationId = con.id;
            insert mer;
            EventRelation er = new EventRelation();
            //er.AccountId = acc2.id;
            er.EventId = ev.Id;
            er.RelationId = con.id;
            insert er;
            ApexPages.currentPage().getParameters().put('accountId', acc2.Id);
            ApexPages.currentPage().getParameters().put('contactId', con.id);
            TaxiReportController h = new TaxiReportController();
            /* ApexPages.currentPage().getParameters().put('accountId', acc2.Id);
                TaxiReportController h1 = new TaxiReportController();*/
        }
        
        
    }
  
      // test for FIG users - Should rollup figures
    static testMethod void coverRollup(){
        User runAs = getFigUser();
        
        system.runAs(runAs){
            Account acc1 = new Account(
                            Name='Company A', 
                            Billing_PostalCode__c = '561111',
                            Hierarchy_Level__c = 'L0',
                            ownerId = runAs.id,
                            owner = runAs, Region__c = 'EMEA', Business_Line__c = 'Retail', Status__c = 'Active');
            insert acc1;
            
            Account acc2 = new Account(
                                Name='Company B', 
                                parentid=acc1.id,
                                Billing_PostalCode__c = '561112',
                                Hierarchy_Level__c = 'L1',
                                ownerId = runAs.id,
                                owner = runAs, Region__c = 'EMEA', Business_Line__c = 'Retail', Status__c = 'Active');
            insert acc2;
            
            Account acc3 = new Account(
                                Name='Company C', 
                                parentid=acc2.id,
                                Billing_PostalCode__c = '561113',
                                Hierarchy_Level__c = 'L2',
                                ownerId = runAs.id,
                                owner = runAs, Region__c = 'EMEA', Business_Line__c = 'Retail', Status__c = 'Active');
            insert acc3;
            
            // Not part of hierarchy so AUM etc should be ignored
            Account acc4 = new Account(
                                Name='Company D',
                                Billing_PostalCode__c = '561114',
                                Hierarchy_Level__c = 'L0',
                                ownerId = runAs.id,
                                owner = runAs, Region__c = 'EMEA', Business_Line__c = 'Retail', Status__c = 'Active');
            insert acc4;
            
            
            // AUM
            Id rtAUMId = UtilsFinAum.RecTypesMap.get('TotalSummaryAUM').Id;
            list<Assets_Under_Management__c> aums = new list<Assets_Under_Management__c>();
            aums.add(TaxiReportControllerTest.createTestAum(acc1, 'Fund 1', rtAUMId, 100));            
            aums.add(TaxiReportControllerTest.createTestAum(acc1, 'Fund 2', rtAUMId, 500));
            aums.add(TaxiReportControllerTest.createTestAum(acc1, 'Fund 3', rtAUMId, 10));
            aums.add(TaxiReportControllerTest.createTestAum(acc1, 'Fund 4', rtAUMId, 11));
            aums.add(TaxiReportControllerTest.createTestAum(acc1, 'Fund 5', rtAUMId, 12));
            aums.add(TaxiReportControllerTest.createTestAum(acc1, 'Fund 6', rtAUMId, 13));
            aums.add(TaxiReportControllerTest.createTestAum(acc1, 'Fund 7', rtAUMId, 1)); // Should be ignored as max of 10 Funds returned
            aums.add(TaxiReportControllerTest.createTestAum(acc1, 'Fund 8', rtAUMId, 14));
            aums.add(TaxiReportControllerTest.createTestAum(acc1, 'Fund 9', rtAUMId, 15));
            aums.add(TaxiReportControllerTest.createTestAum(acc1, 'Fund 10', rtAUMId, 16));
            aums.add(TaxiReportControllerTest.createTestAum(acc1, 'Fund 11', rtAUMId, 17));
            aums.add(TaxiReportControllerTest.createTestAum(acc2, 'Fund 1', rtAUMId, 1000));            
            aums.add(TaxiReportControllerTest.createTestAum(acc3, 'Fund 3', rtAUMId, 2000));           
            aums.add(TaxiReportControllerTest.createTestAum(acc4, 'Fund 1', rtAUMId, 10000));
            insert aums;
            
            // Load Taxi Report for account
            Test.startTest();
            ApexPages.currentPage().getParameters().put('accountId', acc1.Id);
            TaxiReportController h = new TaxiReportController();
            Test.stopTest();
            
            Map<String , Decimal> mapOfCurrAndVal = CommonUtilities.getCurrencyInfo();
            Decimal conversionRate = 1;
            if(mapOfCurrAndVal.containsKey(Label.TaxiReportsCurrency)){
                conversionRate = mapOfCurrAndVal.get(Label.TaxiReportsCurrency);
            }
            
            // Test AUMs by AUM
            system.assertEquals('Fund 3', h.AUMsbyAUM[0].Fund_Name__c); 
            system.assertEquals(2010 * conversionRate, h.AUMsbyAUM[0].Current_AUM__c); // Sum of 2 values
            system.assertEquals('Fund 1', h.AUMsbyAUM[1].Fund_Name__c); 
            system.assertEquals(1100 * conversionRate, h.AUMsbyAUM[1].Current_AUM__c);
            system.assertEquals('Fund 2', h.AUMsbyAUM[2].Fund_Name__c); // Sum of 2 values
            system.assertEquals(500 * conversionRate, h.AUMsbyAUM[2].Current_AUM__c);
            system.assertEquals('Fund 11', h.AUMsbyAUM[3].Fund_Name__c);    
            system.assertEquals(17 * conversionRate, h.AUMsbyAUM[3].Current_AUM__c);
            system.assertEquals('Fund 10', h.AUMsbyAUM[4].Fund_Name__c);    
            system.assertEquals(16 * conversionRate, h.AUMsbyAUM[4].Current_AUM__c);
            system.assertEquals('Fund 9', h.AUMsbyAUM[5].Fund_Name__c); 
            system.assertEquals(15 * conversionRate, h.AUMsbyAUM[5].Current_AUM__c);
            system.assertEquals('Fund 8', h.AUMsbyAUM[6].Fund_Name__c); 
            system.assertEquals(14 * conversionRate, h.AUMsbyAUM[6].Current_AUM__c);
            system.assertEquals('Fund 6', h.AUMsbyAUM[7].Fund_Name__c); 
            system.assertEquals(13 * conversionRate, h.AUMsbyAUM[7].Current_AUM__c);
            system.assertEquals('Fund 5', h.AUMsbyAUM[8].Fund_Name__c); 
            system.assertEquals(12 * conversionRate, h.AUMsbyAUM[8].Current_AUM__c);
            system.assertEquals('Fund 4', h.AUMsbyAUM[9].Fund_Name__c); 
            system.assertEquals(11 * conversionRate, h.AUMsbyAUM[9].Current_AUM__c);           
            
            // Test AUMs by Gross Sales YTD
            system.assertEquals('Fund 3', h.AUMsbyGrossSales[0].Fund_Name__c);  
            system.assertEquals(2012 * conversionRate, h.AUMsbyGrossSales[0].Gross_Sales_YTD__c); // Sum of 2 values
            system.assertEquals('Fund 1', h.AUMsbyGrossSales[1].Fund_Name__c);  
            system.assertEquals(1102 * conversionRate, h.AUMsbyGrossSales[1].Gross_Sales_YTD__c); // Sum of 2 values
            system.assertEquals('Fund 2', h.AUMsbyGrossSales[2].Fund_Name__c);  
            system.assertEquals(501 * conversionRate, h.AUMsbyGrossSales[2].Gross_Sales_YTD__c);
            system.assertEquals('Fund 11', h.AUMsbyGrossSales[3].Fund_Name__c); 
            system.assertEquals(18 * conversionRate, h.AUMsbyGrossSales[3].Gross_Sales_YTD__c);
            system.assertEquals('Fund 10', h.AUMsbyGrossSales[4].Fund_Name__c); 
            system.assertEquals(17 * conversionRate, h.AUMsbyGrossSales[4].Gross_Sales_YTD__c);
            system.assertEquals('Fund 9', h.AUMsbyGrossSales[5].Fund_Name__c);  
            system.assertEquals(16 * conversionRate, h.AUMsbyGrossSales[5].Gross_Sales_YTD__c);
            system.assertEquals('Fund 8', h.AUMsbyGrossSales[6].Fund_Name__c);  
            system.assertEquals(15 * conversionRate, h.AUMsbyGrossSales[6].Gross_Sales_YTD__c);
            system.assertEquals('Fund 6', h.AUMsbyGrossSales[7].Fund_Name__c);  
            system.assertEquals(14 * conversionRate, h.AUMsbyGrossSales[7].Gross_Sales_YTD__c);
            system.assertEquals('Fund 5', h.AUMsbyGrossSales[8].Fund_Name__c);  
            system.assertEquals(13 * conversionRate, h.AUMsbyGrossSales[8].Gross_Sales_YTD__c);
            system.assertEquals('Fund 4', h.AUMsbyGrossSales[9].Fund_Name__c);  
            system.assertEquals(12 * conversionRate, h.AUMsbyGrossSales[9].Gross_Sales_YTD__c);           
            
            // Test AUMs by Redemptions
            system.assertEquals('Fund 3', h.AUMsbyRedemptions[0].Fund_Name__c); 
            system.assertEquals(2014 * conversionRate, h.AUMsbyRedemptions[0].Redemptions_YTD__c); // Sum of 2 values
            system.assertEquals('Fund 1', h.AUMsbyRedemptions[1].Fund_Name__c); 
            system.assertEquals(1104 * conversionRate, h.AUMsbyRedemptions[1].Redemptions_YTD__c); // Sum of 2 values
            system.assertEquals('Fund 2', h.AUMsbyRedemptions[2].Fund_Name__c); 
            system.assertEquals(502 * conversionRate, h.AUMsbyRedemptions[2].Redemptions_YTD__c);
            system.assertEquals('Fund 11', h.AUMsbyRedemptions[3].Fund_Name__c);    
            system.assertEquals(19 * conversionRate, h.AUMsbyRedemptions[3].Redemptions_YTD__c);
            system.assertEquals('Fund 10', h.AUMsbyRedemptions[4].Fund_Name__c);    
            system.assertEquals(18 * conversionRate, h.AUMsbyRedemptions[4].Redemptions_YTD__c);
            system.assertEquals('Fund 9', h.AUMsbyRedemptions[5].Fund_Name__c); 
            system.assertEquals(17 * conversionRate, h.AUMsbyRedemptions[5].Redemptions_YTD__c);
            system.assertEquals('Fund 8', h.AUMsbyRedemptions[6].Fund_Name__c); 
            system.assertEquals(16 * conversionRate, h.AUMsbyRedemptions[6].Redemptions_YTD__c);
            system.assertEquals('Fund 6', h.AUMsbyRedemptions[7].Fund_Name__c); 
            system.assertEquals(15 * conversionRate, h.AUMsbyRedemptions[7].Redemptions_YTD__c);
            system.assertEquals('Fund 5', h.AUMsbyRedemptions[8].Fund_Name__c); 
            system.assertEquals(14 * conversionRate, h.AUMsbyRedemptions[8].Redemptions_YTD__c);
            system.assertEquals('Fund 4', h.AUMsbyRedemptions[9].Fund_Name__c); 
            system.assertEquals(13 * conversionRate, h.AUMsbyRedemptions[9].Redemptions_YTD__c);
        
        ApexPages.currentPage().getParameters().put('accountId', acc1.Id);
        TaxiReportController h1 = new TaxiReportController();
        list<Assets_Under_Management__c> aumList = h1.AUMsbyAUM;
        aumList = h1.AUMsbyRedemptions;
        aumList = h1.AUMsbyGrossSales;
        Set<String> vals = h1.fundsOnRecommendedList;
        Boolean check = h1.hasExternalPanels;
        }
    }
    
    static Assets_Under_Management__c createTestAum(Account acc, String fundName, Id recordTypeId, integer value){
        Assets_Under_Management__c aum = new Assets_Under_Management__c();
        aum.Company_Name__c = acc.id;
        aum.Fund_Name__c = fundName;
        aum.Current_AUM__c = value;        
        aum.Gross_Sales_YTD__c = value + 1;
        aum.Redemptions_YTD__c = value + 2;
        aum.RecordTypeId = recordTypeId;
        return aum; 
    }
    
    static testMethod void myUnitTest_Account(){
        List<Account> newAccounts = new List<Account>{new Account(Name = 'Test Account', Billing_PostalCode__c = '505050', Hierarchy_Level__c = 'L0', Region__c = 'EMEA', Business_Line__c = 'Retail', Status__c = 'Active'),
                                    new Account(Name = 'Prev Acc', Billing_PostalCode__c = '5050505', Hierarchy_Level__c = 'L0', Region__c = 'EMEA', Business_Line__c = 'Retail', Status__c = 'Active')};
        insert newAccounts;
        Product2 productRec = new Product2(Name = 'Our Product', Asset_Manager_Name__c = 'AXA');
        insert productRec;
        Product2 nProductRec = new Product2(Name = 'Not Our Product', Asset_Manager_Name__c = 'Aberdeen');
        insert nProductRec;
        Rated_Fund__c newFund = new Rated_Fund__c(Client__c = newAccounts[0].Id, Fund__c = productRec.Id);
        insert newFund;
        Client_Fund__c newCF = new Client_Fund__c(Client__c = newAccounts[0].Id, Product__c = productRec.Id);
        insert newCF;
        Opportunity opp = new Opportunity(Name = 'Opp1', AccountId = newAccounts[0].Id, Amount = 100, CloseDate = Date.Today() + 1, StageName = 'Lead');
        insert opp;
        Competitor_Fund__c cf = new Competitor_Fund__c(Product__c = nProductRec.Id, Company__c = newAccounts[0].Id);
        insert cf;
        List<Contact> newContacts = new List<Contact>{new Contact(AccountId = newAccounts[0].Id, LastName = 'Test Contact', Status__c = 'Active'),
                                                      new Contact(LastName = 'TC1', AccountId = newAccounts[1].Id, Status__c = 'Active'),
                                                      new Contact(LastName = 'TC2', AccountId = newAccounts[0].Id, Status__c = 'Active')};
        insert newContacts;
        List<AccountContactRelation> acrList = new List<AccountContactRelation>
                                                {new AccountContactRelation(ContactId = newContacts[1].Id, AccountId = newAccounts[0].Id, national_key_contact__c = true, primary_contact__c = true, Previous_Employee__c = true),
                                                 new AccountContactRelation(ContactId = newContacts[0].Id, AccountId = newAccounts[1].Id, national_key_contact__c = true, primary_contact__c = true)};
        insert acrList;
        List<Event> allEvents = new List<Event>{new Event(Subject = 'Past Event', StartDateTime = System.Now() - 10, EndDateTime = System.Now() - 9, WhoId = newContacts[0].Id, WhatId = newAccounts[0].Id),
                                                new Event(Subject = 'Future Event', StartDateTime = System.Now() + 10, EndDateTime = System.Now() + 11, WhoId = newContacts[0].Id, WhatId = newAccounts[0].Id),
                                                new Event(Subject = 'Prev Firm Event', StartDateTime = System.Now() - 10, EndDateTime = System.Now() - 9, WhoId = newContacts[0].Id, WhatId = newAccounts[1].Id)};
        insert allEvents;
        List<EventRelation> allERs = new List<EventRelation>{new EventRelation(EventId = allEvents[0].Id, RelationId = newContacts[2].Id, Status = 'Accepted'),
                                                             new EventRelation(EventId = allEvents[1].Id, RelationId = newContacts[2].Id, Status = 'Accepted')};
        insert allERs;
        Id rtFSUDepartmentId = Schema.SObjectType.Department__c.getRecordTypeInfosByName().get('Fund Selection Unit').getRecordTypeId();
        Department__c dept = new Department__c(RecordTypeId = rtFSUDepartmentId, Name = 'Test Dep', Next_FSU_Review_Date__c = Date.Today() + 1, FSU_Head__c = newContacts[0].Id);
        insert dept;
        List<Department_Client_Relationship__c> dcrList = new List<Department_Client_Relationship__c>{
                                                            new Department_Client_Relationship__c(Client__c = newAccounts[0].Id, Department__c = dept.Id),
                                                            new Department_Client_Relationship__c(Client__c = newAccounts[0].Id, Department__c = dept.Id)};
        insert dcrList;
        TaxiReportController testInst = new TaxiReportController(new ApexPages.StandardController(newAccounts[0]));
        Boolean hasKC = testInst.hasKeyContacts;
        hasKC = testInst.hasRecentEvents;
        hasKC = testInst.hasFutureEvents;
        hasKC = testInst.hasPrevCompanyEvents;
        hasKC = testInst.hasIPproducts;
        Set<String> vals = testInst.fundsOnRecommendedList;
        vals = testInst.currentIPOpportunities;
        vals = testInst.competitorFundsForClient;
        
        System.assertEquals(true, hasKC);
        
        try{
            TaxiReportController testInst1 = new TaxiReportController(new ApexPages.StandardController(new Account()));
        } catch(Exception ex){}
    }
    
    static testMethod void myUnitTest_Contact(){
        List<Account> accList = new List<Account>{new Account(Name = 'Test Account', Billing_PostalCode__c = '505050', Hierarchy_Level__c = 'L0', Region__c = 'EMEA', Business_Line__c = 'Retail', Status__c = 'Active'), 
                                                  new Account(Name = 'Prev Acc', Billing_PostalCode__c = '5050505', Hierarchy_Level__c = 'L0', Region__c = 'EMEA', Business_Line__c = 'Retail', Status__c = 'Active')};
        insert accList;
        List<Contact> cntList = new List<Contact>{new Contact(LastName = 'Test Contact', AccountId = accList[0].Id, Status__c = 'Active'),
                                                  new Contact(LastName = 'TC1', AccountId = accList[1].Id, Status__c = 'Active')};
        insert cntList;
        List<AccountContactRelation> acrList = new List<AccountContactRelation>{
                                                new AccountContactRelation(ContactId = cntList[1].Id, AccountId = accList[0].Id, national_key_contact__c = true, primary_contact__c = true),
                                                new AccountContactRelation(ContactId = cntList[0].Id, AccountId = accList[1].Id, national_key_contact__c = true, primary_contact__c = true, Previous_Employee__c = true)};
        insert acrList;
        TaxiReportController testInst = new TaxiReportController(new ApexPages.StandardController(cntList[0]));
        Boolean hasKC = testInst.hasPrevCompanyEvents;
        List<Event> allEvents = new List<Event>{new Event(Subject = 'Past Event', StartDateTime = System.Now() - 10, EndDateTime = System.Now() - 9, WhoId = cntList[0].Id, WhatId = accList[0].Id),
                                                new Event(Subject = 'Future Event', StartDateTime = System.Now() + 10, EndDateTime = System.Now() + 11, WhoId = cntList[0].Id, WhatId = accList[0].Id),
                                                new Event(Subject = 'Prev Firm Event', StartDateTime = System.Now() - 10, EndDateTime = System.Now() - 9, WhoId = cntList[0].Id, WhatId = accList[1].Id)};
        insert allEvents;
        
        Document doc = new Document(Name = 'Logo', FolderId = UserInfo.getUserId());
        insert doc;
        TaxiReportController testInst1 = new TaxiReportController(new ApexPages.StandardController(cntList[0]));
        hasKC = testInst1.hasPrevCompanyEvents;
        ApexPages.currentPage().getParameters().put('accountId', accList[0].Id);
        testInst.redirect();
    }
    
    static testMethod void myUnitTest_UltimateParent(){
        Account acc = new Account(Name = 'Test Account', Billing_PostalCode__c = '505050', Hierarchy_Level__c = 'L0', Region__c = 'EMEA', Business_Line__c = 'Retail', Status__c = 'Active');
        insert acc;
        Account acc1 = new Account(Name = 'Test Account1', Billing_PostalCode__c = '515050', Hierarchy_Level__c = 'L1', ParentId = acc.Id, Region__c = 'EMEA', Business_Line__c = 'Retail', Status__c = 'Active');
        insert acc1;
        Account acc2 = new Account(Name = 'Test Account2', Billing_PostalCode__c = '525050', Hierarchy_Level__c = 'L1', ParentId = acc.Id, Region__c = 'EMEA', Business_Line__c = 'Retail', Status__c = 'Active');
        insert acc2;
        Account acc3 = new Account(Name = 'Test Account3', Billing_PostalCode__c = '535050', Hierarchy_Level__c = 'L1', ParentId = acc.Id, Region__c = 'EMEA', Business_Line__c = 'Retail', Status__c = 'Active');
        insert acc3;
        Account acc4 = new Account(Name = 'Test Account4', Billing_PostalCode__c = '545050', Hierarchy_Level__c = 'L1', ParentId = acc.Id, Region__c = 'EMEA', Business_Line__c = 'Retail', Status__c = 'Active');
        insert acc4;
        TaxiReportController testInst = new TaxiReportController(new ApexPages.StandardController(acc4));
        Id val = testInst.Id18String;
        testInst.redirect();
        ApexPages.currentPage().getParameters().put('isdtp', 'p1');
        testInst.redirect();
        ApexPages.currentPage().getParameters().put('accountId', acc.Id);
        testInst.redirect();
    }
}