@isTest
public class AccountHierarchyController_Test {
    /*
     * 
     * Description : Check that a past Event (End date earlier than today but later than the last batch run) 
     *               updates the contact that is associated with it.
     * Param : 
     * Returns :  
    */
    public static string Thrdparty='Thirdparty';
    public static string Networks='Networks';
    public static string Schemes='Schemes';
    
    static testMethod void testAccountThirdpartyFetch(){
        User user = CommonTestUtils.createTestUser();    
        List<Account> listOfAccount = new List<Account>();
        List<Account> listOfAccount1 = new List<Account>();
        List<Account> listOftp = new List<Account>();
        List<Client_Third_Party_Relationship__c> listOfAsc = new List<Client_Third_Party_Relationship__c>();
        // Creating client and third parties in heararchy.
        Account acc = CommonTestUtils.CreateTestClient('Client');
        acc.Hierarchy_Level__c='L0';
        insert acc;
        
        Account thirdParty = CommonTestUtils.CreateTestThirdParty('TP');
        insert thirdParty;
        System.debug('thirdparty id '+thirdParty);
        Account netw = CommonTestUtils.CreateTestClient('Network');
        insert netw;
        Client_Third_Party_Relationship__c  obj = CommonTestUtils.CreateTestClientThirdPartyAss(acc.id, thirdParty.Id);
        insert obj;
        for(integer i = 0; i<2; i++){
            Account acc1 = CommonTestUtils.CreateTestClient(i+'ClientL20');
            acc1.ParentId = acc.id;
            listOfAccount.add(acc1);
        }
        insert listOfAccount;
        for(integer i = 0; i<2; i++){
            Account acc1 = CommonTestUtils.CreateTestClient(i+'ClientL30');
            acc1.Hierarchy_Level__c='L2';
            acc1.ParentId = listOfAccount[i].id;
            Account acc2 = CommonTestUtils.CreateTestThirdParty(i+'TP30');
            listOfAccount1.add(acc1);
            listOftp.add(acc2);
        }
        insert listOfAccount1;
        insert listOftp;
        listOfAccount.clear();
        for(integer i = 0; i<2; i++){
            Client_Third_Party_Relationship__c  obj1 = CommonTestUtils.CreateTestClientThirdPartyAss(listOfAccount1[i].id, listOftp[i].Id);
            listOfAsc.add(obj1);
        }
        insert(listOfAsc); 

        // perform the test 
        Test.startTest();
        System.runAs(user){ 
             
            AccountHierarchyController.AccountResult obj_ar= AccountHierarchyController.getAccountInContext(acc.id, '');
           /* AccountHierarchyController.AccountResult obj_ar= AccountHierarchyController.getAccountInContext(acc.id, Networks);
            AccountHierarchyController.AccountResult obj_ar= AccountHierarchyController.getAccountInContext(acc.id, SchemesLabel);*/
            //to verify only one account is loaded on page load
            system.assert(obj_ar.Account.id==acc.id);
            //to verify no third party account is loaded
            system.assert(obj_ar.thirdPartyaccounts==null);
            //system.assert(obj_ar);
            obj_ar = AccountHierarchyController.getAccountInContext(acc.id, Thrdparty);
            //to verify only one account is loaded on page load
            system.assert(obj_ar.Account.id==acc.id);
            //to verify one third party account is loaded
            system.assert(obj_ar.thirdPartyaccounts[0].Third_Party_Name__c==thirdParty.id);
            
            List<AccountHierarchyController.AccountResult> l_ar = AccountHierarchyController.getAccounts(acc.id, Thrdparty);
            //to verify only two child account is loaded on click
            system.assert(l_ar.size()==2);
            //to verify one third party account is loaded
            system.assert(obj_ar.thirdPartyaccounts.size()==1);
            
            l_ar = AccountHierarchyController.getAccounts(acc.id, Thrdparty);
            //to verify only two child account is loaded on click
            system.assert(l_ar.size()==2);
            
            String ids = AccountHierarchyController.getAccountUltimateParent(acc.id);
            System.assert(ids==acc.id);
            
            //to cover Network/Platform relation with the client
            obj_ar = AccountHierarchyController.getAccountInContext(acc.id, Networks);
            
           //to cover Schemes relation with the client
            Pools_of_Capital__c schm = new Pools_of_Capital__c();
            schm.Client_Name__c = acc.id;
            schm.Scheme_Type__c ='DB Pension';
            insert schm;
             obj_ar = AccountHierarchyController.getAccountInContext(acc.id, Schemes);
        }
        Test.stopTest();
    }
    
    static testMethod void testAccountNetworkFetch(){
        User user = CommonTestUtils.createTestUser();    
        List<Account> listOfAccount = new List<Account>();
        List<Account> listOfAccount1 = new List<Account>();
        List<Account> listOftp = new List<Account>();
        List<Client_Third_Party_Relationship__c> listOfAsc = new List<Client_Third_Party_Relationship__c>();
        // Creating client and third parties in heararchy.
        Account acc = CommonTestUtils.CreateTestClient('Client');
        acc.Hierarchy_Level__c='L0';
        insert acc;
        
        Account thirdParty = CommonTestUtils.CreateTestThirdParty('TP');
        insert thirdParty;
        System.debug('thirdparty id '+thirdParty);
        Client_Third_Party_Relationship__c  obj = CommonTestUtils.CreateTestClientThirdPartyAss(acc.id, thirdParty.Id);
        insert obj;
        for(integer i = 0; i<2; i++){
            Account acc1 = CommonTestUtils.CreateTestClient(i+'ClientL20');
            acc1.ParentId = acc.id;
            listOfAccount.add(acc1);
        }
        insert listOfAccount;
        for(integer i = 0; i<2; i++){
            Account acc1 = CommonTestUtils.CreateTestClient(i+'ClientL30');
            acc1.Hierarchy_Level__c='L2';
            acc1.ParentId = listOfAccount[i].id;
            Account acc2 = CommonTestUtils.CreateTestThirdParty(i+'TP30');
            listOfAccount1.add(acc1);
            listOftp.add(acc2);
        }
        insert listOfAccount1;
        insert listOftp;
        listOfAccount.clear();
        for(integer i = 0; i<2; i++){
            Client_Third_Party_Relationship__c  obj1 = CommonTestUtils.CreateTestClientThirdPartyAss(listOfAccount1[i].id, listOftp[i].Id);
            listOfAsc.add(obj1);
        }
        insert(listOfAsc); 

        // perform the test 
        Test.startTest();
        System.runAs(user){ 
             
            AccountHierarchyController.AccountResult obj_ar= AccountHierarchyController.getAccountInContext(acc.id, '');
           /* AccountHierarchyController.AccountResult obj_ar= AccountHierarchyController.getAccountInContext(acc.id, Networks);
            AccountHierarchyController.AccountResult obj_ar= AccountHierarchyController.getAccountInContext(acc.id, SchemesLabel);*/
            //to verify only one account is loaded on page load
            system.assert(obj_ar.Account.id==acc.id);
            //to verify no third party account is loaded
            system.assert(obj_ar.thirdPartyaccounts==null);
            //system.assert(obj_ar);
            obj_ar = AccountHierarchyController.getAccountInContext(acc.id, Thrdparty);
            //to verify only one account is loaded on page load
            system.assert(obj_ar.Account.id==acc.id);
            //to verify one third party account is loaded
            system.assert(obj_ar.thirdPartyaccounts[0].Third_Party_Name__c==thirdParty.id);
            
            List<AccountHierarchyController.AccountResult> l_ar = AccountHierarchyController.getAccounts(acc.id, Thrdparty);
            //to verify only two child account is loaded on click
            system.assert(l_ar.size()==2);
            //to verify one third party account is loaded
            system.assert(obj_ar.thirdPartyaccounts.size()==1);
            
            l_ar = AccountHierarchyController.getAccounts(acc.id, Thrdparty);
            //to verify only two child account is loaded on click
            system.assert(l_ar.size()==2);
            
            String ids = AccountHierarchyController.getAccountUltimateParent(acc.id);
            System.assert(ids==acc.id);
        }
        Test.stopTest();
    }
}