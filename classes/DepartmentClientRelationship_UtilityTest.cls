/**************************************************************************************
 Name:            DepartmentClientRelationship_UtilityTest
 Description:     Test Methods for Department_Client_Relationship__c triggers
 Created By  :    Deloitte
 Created On  :    05 OCT 2016
 --------------------------------------------------------------------------------------
 Modification Log
 -----------------
    Name                 Date                      Comments
 -----------------   -------------   --------------------------------------------------
 Dilipkumar          05 OCT 2016                   Created
 --------------------------------------------------------------------------------------
 Review Log
 ----------    
 Name                Date            Comments
 -----------------   -------------   --------------------------------------------------
 
***************************************************************************************/

@isTest
public class DepartmentClientRelationship_UtilityTest{
    /*
     * Description : Create new Clients
     * Param : Number of Clients to create
     * Returns :  List of Clients
    */
    public static List<Account> createClients(Integer count){
        List<Account> testClients = new List<Account>();
        Account testClient = new Account();
        for(Integer iter=0;iter<count;iter++){
            testClient = CommonTestUtils.CreateTestClient('Test Client '+(iter+1));
            testClients.add(testClient);
        }   
        insert testClients;
        return testClients;
    }
    
     /*
     * Description : Create new Department
     * Param : Number of Department to create
     * Returns :  List of Departments
    */
    public static List<Department__c> createDepartments(Integer count){
        List<Department__c> testDepartments = new List<Department__c>();
        Department__c testDepartment = new Department__c();
        for(Integer iter=0;iter<count;iter++){
            insert testDepartment;
            testDepartments.add(testDepartment);
        }
        return testDepartments;
    }
    
    /*
     * Description : Create new Client - Departments relationship records
     * Param : Number of Clients to create relationship for
     * Returns :  List of Client - Departments relationship records
    */
    public static List<Department_Client_Relationship__c> createDeptClRltn(Integer noClients){
        //Creating Clients
        List<Account> testClients = createClients(noClients);
        Account testAcc = new Account(Name = 'Test', Billing_PostalCode__c = '501012', Hierarchy_Level__c = 'L0');
        insert testAcc;
        //Creating Hierarchy TC4->TC3->TC2->TC1->NULL
        for(Integer iter=0;iter<noClients-1;iter++){
            testClients.get(iter).ParentId = testAcc.Id;
            testClients.get(iter).Hierarchy_Level__c = 'L1';
        }
        update testClients;
        
        //Create a department
        List<Department__c> testDepart = createDepartments(1);
        
        //Asserting there are no record with same Ids previously
        Map<string, Department_Client_Relationship__c> existingDeptClientRelMap = new Map<string, Department_Client_Relationship__c>();
        for(Department_Client_Relationship__c existingDeptClientRel : [select id, client__c, department__c from Department_Client_Relationship__c where client__c in: testClients AND department__c in: testDepart]){
            existingDeptClientRelMap.put(existingDeptClientRel.client__c +'*'+ existingDeptClientRel.department__c , existingDeptClientRel);
        }
        System.assertEquals(existingDeptClientRelMap.isEmpty(), True);
        
        //Assigning Department to Clients (TC4,testDept)->(TC3,testDept)->(TC2,testDept)->(TC1,testDept)->NULL
        List<Department_Client_Relationship__c> testDeptClientRels = new List<Department_Client_Relationship__c>();
        for(Integer iter=noClients-1;iter>=0;iter--){
            Department_Client_Relationship__c testDeptClientRel = new Department_Client_Relationship__c();
            testDeptClientRel.Client__c = (testClients.get(iter)).Id;
            testDeptClientRel.Department__c = testDepart.get(0).Id;
            testDeptClientRel.Is_Inherited_from_Hierarchy__c = TRUE;
            testDeptClientRels.add(testDeptClientRel);
        }
        return testDeptClientRels;
    }
    
    /*
     * Description : Test method to check L0,L1,L2,L5 client hierarchy level insert
     * Param : 
     * Returns :  
    */
    public static testmethod void replicateDptCltRltInsL0(){
        List<Department_Client_Relationship__c> testDeptClientRel= createDeptClRltn(5);
        Test.startTest();
        DepartmentClientRelationship_Utility.getInstance();
        insert testDeptClientRel;
        Test.stopTest();
    }
    /*
     * Description : Test method to check L2,L0,L1,L5 client hierarchy level insert
     * Param : 
     * Returns :  
    */
    public static testmethod void replicateDptCltRltInsL2(){
        List<Department_Client_Relationship__c> testDeptClientRel= createDeptClRltn(5);
        Test.startTest();
        DepartmentClientRelationship_Utility.getInstance();
        insert testDeptClientRel.get(2);
        RecurssiveTriggerController.deptClientRelationRetrigger = false;
        insert testDeptClientRel.get(0);
        Test.stopTest();
    }
    /*
     * Description : Test method to delete L0 client hierarchy level
     * Param : 
     * Returns :  
    */
    public static testMethod void deleteDeptClientRelationship(){
        List<Department_Client_Relationship__c> testDeptClientRel= createDeptClRltn(5);
        insert testDeptClientRel;
        RecurssiveTriggerController.deptClientRelationRetrigger = false;
        Test.startTest();
        DepartmentClientRelationship_Utility.getInstance();
        delete testDeptClientRel.get(0);
        Test.stopTest();
    }
}