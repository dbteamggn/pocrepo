@isTest
public class BP_EmeaClientsCtrlTest {
	/*
     * 
     * Description : Add a specified client to the specified business plan & flag as a key client
     *               
    */
    private static testmethod void testAddClient(){
        
        User user = CommonTestUtils.createTestUser();        
        System.runAs(user){
            
            business_plan__c bp = new Business_Plan__c(name='Test');
            insert bp;
            
            list<id> clientIds = new list<id>();
            account client = CommonTestUtils.CreateTestClient('Client');
            insert client;
            clientIds.add(client.id);
            
        	Test.startTest();
            BP_EmeaClientsCtrl.addClient(String.valueOf(bp.Id), clientIds);
            Test.stopTest();
            
            list<Business_Plan_Client_Relation__c> bpcrResults = [select id, business_plan__c, client__c, key_client__c from Business_Plan_Client_Relation__c];
            system.assertEquals(1, bpcrResults.size());
            system.assertEquals(bp.id, bpcrResults[0].business_plan__c);
            system.assertEquals(client.id, bpcrResults[0].client__c);
            system.assertEquals(true, bpcrResults[0].key_client__c);
            
        }
    }
    
    /*
     * 
     * Description : Remove the specified business plan client relation records
     *               
    */
    private static testmethod void testRemoveClients(){
        
        User user = CommonTestUtils.createTestUser();        
        System.runAs(user){
                        
            list<business_plan__c> bps = new list<business_plan__c>();
            business_plan__c bp1 = new Business_Plan__c(name='Test1');
            bps.add(bp1);
            business_plan__c bp2 = new Business_Plan__c(name='Test2');
            bps.add(bp2);
            insert bps;
            
            list<account> clients = new list<account>();
            account client1 = CommonTestUtils.CreateTestClient('Client1');
            clients.add(client1);
            account client2 = CommonTestUtils.CreateTestClient('Client2');
            clients.add(client2);
            account client3 = CommonTestUtils.CreateTestClient('Client3');
            clients.add(client3);
            insert clients;
            
            list<Business_Plan_Client_Relation__c> bpcrs = new list<Business_Plan_Client_Relation__c>();
            Business_Plan_Client_Relation__c bpcr1 = new Business_Plan_Client_Relation__c(business_plan__c=bp1.id, client__c=client1.id, key_client__c= true);
            bpcrs.add(bpcr1);
            Business_Plan_Client_Relation__c bpcr2 = new Business_Plan_Client_Relation__c(business_plan__c=bp1.id, client__c=client2.id, key_client__c= true);
            bpcrs.add(bpcr2);
            Business_Plan_Client_Relation__c bpcr3 = new Business_Plan_Client_Relation__c(business_plan__c=bp1.id, client__c=client3.id, key_client__c= true);
            bpcrs.add(bpcr3);
            Business_Plan_Client_Relation__c bpcr4 = new Business_Plan_Client_Relation__c(business_plan__c=bp2.id, client__c=client1.id, key_client__c= true);
            bpcrs.add(bpcr4);
            insert bpcrs;
            
            list<string> bpcrsToRemove = new list<string>();
            bpcrsToRemove.add(String.valueOf(bpcr1.id));
            bpcrsToRemove.add(String.valueOf(bpcr2.id));
            
        	Test.startTest();
            // Remove bpcr1 & bpcr2
            BP_EmeaClientsCtrl.removeClients(bpcrsToRemove);
            Test.stopTest();
            
            list<Business_Plan_Client_Relation__c> bpcrResults = [select id, business_plan__c, client__c, key_client__c from Business_Plan_Client_Relation__c order by client__r.name];
            
            // Check the that only the unremoved bpcrs remain (bpcr3 & bpcr4)
            system.assertEquals(2, bpcrResults.size());
            
            // Because ordered by client name the first remaining bpcr chould be bpcr4
            system.assertEquals(bp2.id, bpcrResults[0].business_plan__c);
            system.assertEquals(client1.id, bpcrResults[0].client__c);
            system.assertEquals(true, bpcrResults[0].key_client__c);
            
            // & the second should be bpcr3
            system.assertEquals(bp1.id, bpcrResults[1].business_plan__c);
            system.assertEquals(client3.id, bpcrResults[1].client__c);
            system.assertEquals(true, bpcrResults[1].key_client__c);
            
            
        }
    }
    
    /*
     * 
     * Description : Test that getClientIdList returns all key client ids that exist for a business plan
     *               
    */
    private static testmethod void testGetClientIdList(){
        
        User user = CommonTestUtils.createTestUser();        
        System.runAs(user){
                        
            list<business_plan__c> bps = new list<business_plan__c>();
            business_plan__c bp1 = new Business_Plan__c(name='Test1');
            bps.add(bp1);
            business_plan__c bp2 = new Business_Plan__c(name='Test2');
            bps.add(bp2);
            insert bps;
            
            list<account> clients = new list<account>();
            account client1 = CommonTestUtils.CreateTestClient('Client1');
            clients.add(client1);
            account client2 = CommonTestUtils.CreateTestClient('Client2');
            clients.add(client2);
            account client3 = CommonTestUtils.CreateTestClient('Client3');
            clients.add(client3);
            account client4 = CommonTestUtils.CreateTestClient('Client4');
            clients.add(client4);
            insert clients;
            
            list<Business_Plan_Client_Relation__c> bpcrs = new list<Business_Plan_Client_Relation__c>();
            Business_Plan_Client_Relation__c bpcr1 = new Business_Plan_Client_Relation__c(business_plan__c=bp1.id, client__c=client1.id, key_client__c= true);
            bpcrs.add(bpcr1);
            Business_Plan_Client_Relation__c bpcr2 = new Business_Plan_Client_Relation__c(business_plan__c=bp1.id, client__c=client2.id, key_client__c= true);
            bpcrs.add(bpcr2);
            Business_Plan_Client_Relation__c bpcr3 = new Business_Plan_Client_Relation__c(business_plan__c=bp1.id, client__c=client3.id, key_client__c= false);
            bpcrs.add(bpcr3);
            Business_Plan_Client_Relation__c bpcr4 = new Business_Plan_Client_Relation__c(business_plan__c=bp2.id, client__c=client4.id, key_client__c= true);
            bpcrs.add(bpcr4);
            insert bpcrs;
            
        	Test.startTest();
            list<id> bpIdResults = BP_EmeaClientsCtrl.getClientIdList(String.valueOf(bp1.Id));
            Test.stopTest();
            
            // Check that only 2 ids were returned (client1.id & client2.id)
            system.assertEquals(2, bpIdResults.size());

            // Not sure of the order in which things will be returned.  Need to make sure that only client1 & client2 are returned.
            if (bpIdResults[0]==client1.id){
            	system.assertEquals(client2.id, bpIdResults[1]);
            } else if (bpIdResults[0]==client2.id){
            	system.assertEquals(client1.id, bpIdResults[1]);
            } else {
                system.assert(false, 'Unexpected id :' + bpIdResults[0] + ' when expected was ' + client1.id + ' & ' + client2.id);
            }
            
            
        }
    }
    
    
    /*
     * 
     * Description : Test that country & Territory are correctly returned from the business plan
     *               
    */
    private static testmethod void testGetBPSettings(){
        
        User user = CommonTestUtils.createTestUser();        
        System.runAs(user){
                        
            list<region__c> terrs = new list<region__c>();
            region__c terr1 = new region__c(Name = '63');
            terrs.add(terr1);
            insert terrs;
            
            // Get available picklist values & later on use the first one found
            List<Schema.PicklistEntry> ple = Business_Plan__c.Country__c.getDescribe().getPicklistValues();
            system.assertNotEquals(0, ple.size(), 'No picklist values are set for Business_Plan__c.Country__c');
            
            
            list<business_plan__c> bps = new list<business_plan__c>();
            business_plan__c bp1 = new Business_Plan__c(name='Test1', country__c=ple[0].getLabel(), bp_territory__c = terr1.id);
            bps.add(bp1);
            insert bps;
            
            
        	Test.startTest();
            business_plan__c bpIdResult = BP_EmeaClientsCtrl.getBPSettings(bp1.Id);
            Test.stopTest();
            
            system.assertEquals(ple[0].getLabel(), bpIdResult.country__c);
            system.assertEquals(terr1.id, bpIdResult.bp_territory__c);
            
            
            
        }
    }
    
    
    /*
     * 
     * Description : Test that setting & unsetting of key client works
     *               
    */
    private static testmethod void testSetKeyClient(){
        
        User user = CommonTestUtils.createTestUser();        
        System.runAs(user){
                        
            list<business_plan__c> bps = new list<business_plan__c>();
            business_plan__c bp1 = new Business_Plan__c(name='Test1');
            bps.add(bp1);
            insert bps;
            
            list<account> clients = new list<account>();
            account client1 = CommonTestUtils.CreateTestClient('Client1');
            clients.add(client1);
            account client2 = CommonTestUtils.CreateTestClient('Client2');
            clients.add(client2);
            insert clients;
            
            list<id> clientIds = new list<id>();
            clientIds.add(client1.id);
            clientIds.add(client2.id);
            
            list<Business_Plan_Client_Relation__c> bpcrs = new list<Business_Plan_Client_Relation__c>();
            Business_Plan_Client_Relation__c bpcr1 = new Business_Plan_Client_Relation__c(business_plan__c=bp1.id, client__c=client1.id, key_client__c= false);
            bpcrs.add(bpcr1);
            Business_Plan_Client_Relation__c bpcr2 = new Business_Plan_Client_Relation__c(business_plan__c=bp1.id, client__c=client2.id, key_client__c= true);
            bpcrs.add(bpcr2);
            insert bpcrs;
            
            list<string> removedBpcrIds = new list<string>();
            removedBpcrIds.add(String.valueOf(bpcr2.Id));
            
        	Test.startTest();
            BP_EmeaClientsCtrl.setKeyClient(String.valueOf(bp1.Id), clientIds);
            BP_EmeaClientsCtrl.removeKeyClient(removedBpcrIds);
            Test.stopTest();
            
            list<Business_Plan_Client_Relation__c> bpcrResults = [select id, business_plan__c, client__c, key_client__c from Business_Plan_Client_Relation__c order by client__r.name];
                        
            // Check that only 2 ids were returned (client1.id & client2.id)
            system.assertEquals(2, bpcrResults.size());

            // Client1 should now be a key client & client2 should not (results are ordered by name)
            system.assertEquals(true, bpcrResults[0].key_client__c);
            system.assertEquals(false, bpcrResults[1].key_client__c);           
            
            
            
        }
    }
    
    
    
}