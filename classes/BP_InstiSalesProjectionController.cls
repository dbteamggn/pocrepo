public class BP_InstiSalesProjectionController {
    
    /*
    * Get the sales projections based on the business plan id.
    */
    
    @AuraEnabled
    public static SalesProjections getProjections(ID businessPlanId){
        
        List<string> stageName=new List<string>{'Lead','Qualified','Finals','Won Not-Funded'};
        
        SalesProjections projections = new SalesProjections();
        try{ 
            Business_Plan__c businessPlanRecord = [SELECT id, Business_Plan_Duration__c,Business_Plan_Summary__c, Progress_to_Plan__c, Status__c,Sales_YTD__c,Starting_Fiscal_Year__c,FY_Sales_Projections__c FROM Business_Plan__c where ID =:businessPlanId];
            
            Integer businessPlanYear = businessPlanRecord.Starting_Fiscal_Year__c.Year();
            
            AggregateResult pyOpportunities =[SELECT SUM(Total_Funding_Amount__c) total FROM Opportunity where Calendar_year(closedate) = :businessPlanYear-1 and (stagename=: Label.Opportunity_Won_Stage_Name OR stagename=: Label.Opportunity_Closed_Stage_Name)];
            
            
            AggregateResult immediatePipeline =[SELECT SUM(Amount) TotalAmount FROM Opportunity where Calendar_year(closedate) = :businessPlanYear and StageName in: stageName ];
            
            AggregateResult extendedPipeline =[SELECT SUM(Amount) TotalAmount FROM Opportunity where Calendar_year(closedate) > :businessPlanYear  and StageName in: stageName  ];        
            
            AggregateResult immediatePipelineWeighted =[SELECT SUM(ExpectedRevenue) TotalAmount FROM Opportunity where Calendar_year(closedate) = :businessPlanYear and StageName in: stageName ];
            
            AggregateResult extendedPipelineWeighted =[SELECT SUM(ExpectedRevenue) TotalAmount FROM Opportunity where Calendar_year(closedate) > :businessPlanYear  and StageName in: stageName  ];        
           
            
            AggregateResult progress =[SELECT Count(ID) total, SUM(Amount) sales, Sum(Estimated_Revenue__c) estimatedRevenue FROM Opportunity where Calendar_year(closedate) = :businessPlanYear  and (stagename = :Label.Opportunity_Won_Stage_Name OR stagename='Won') ];        
            
            projections.PYSales= (Decimal) pyOpportunities.get('total');
            projections.immediatePipelineTotal = (Decimal) immediatePipeline.get('TotalAmount');
            projections.extendedPipelineTotal= (Decimal) extendedPipeline.get('TotalAmount');          
            projections.immediatePipelineTotalWeighted = (Decimal) immediatePipelineWeighted.get('TotalAmount');
            projections.extendedPipelineTotalWeighted = (Decimal) extendedPipelineWeighted.get('TotalAmount');          
            projections.FY17SalesProjection = businessPlanRecord.FY_Sales_Projections__c;
            projections.mandatesWon = (Decimal)  progress.get('total');
            projections.salesYTD = (Decimal)  progress.get('sales');
            projections.estimatedRevenueYTD = (Decimal)  progress.get('estimatedRevenue');      
            projections.progressToPlan = businessPlanRecord.Progress_to_Plan__c;         
            
            if( businessPlanRecord.Status__c == 'Open')
                projections.isComplete =  false;
            else if( businessPlanRecord.Status__c == 'Closed')
                projections.isComplete = true ;
                
        }catch(Exception ex){
        
            System.debug('Exception Details are ---> '+ex);
            CommonUtilities.createExceptionLog(ex);
            
        }
        
        return projections;
        
    }
    
    /*
    * Update the business plan with the calculated projections.
    */
    
    @AuraEnabled
    public static Boolean updateBusinessPlan(String businessPlan, String projections){
        
        Boolean isUpdated;
        Id businessPlanID = (ID) businessPlan;
        try{
            SalesProjections sp = (SalesProjections) JSON.deserialize(projections, SalesProjections.class);
            
            
            Business_Plan__c businessPlanRecord = [SELECT id, Business_Plan_Duration__c,Business_Plan_Summary__c, Sales_YTD__c, PY_Sales__c, Progress_to_Plan__c, Mandates_Won_YTD__c, Estimated_Revenue_YTD__c, Immediate_Pipeline_Total__c, Extended_Pipeline_Total__c, Is_Open__c, Starting_Fiscal_Year__c FROM Business_Plan__c where ID =:businessPlanID];
            
            businessPlanRecord.PY_Sales__c = sp.PYSales;
            businessPlanRecord.Progress_to_Plan__c = sp.progressToPlan;
            businessPlanRecord.Sales_YTD__c = sp.salesYTD;
            businessPlanRecord.Mandates_Won_YTD__c = sp.mandatesWon;
            businessPlanRecord.Estimated_Revenue_YTD__c = sp.estimatedRevenueYTD;
            businessPlanRecord.Immediate_Pipeline_Total__c = sp.immediatePipelineTotal;
            businessPlanRecord.Extended_Pipeline_Total__c = sp.extendedPipelineTotal;
            businessPlanRecord.FY_Sales_Projections__c = sp.FY17SalesProjection;
            system.debug('Projections ----> '+ sp.FY17SalesProjection);
            
            Database.SaveResult  result = Database.update(businessPlanRecord, false);
            if(result.isSuccess()){
                isUpdated = true;
            }else{
                isUpdated=false;
            }
        }catch(Exception ex){
            System.debug('Exception details are ----> '+ ex);
            CommonUtilities.createExceptionLog(ex);
        }
        return isUpdated;
    }            
    
    public class SalesProjections{
        @AuraEnabled
        public Decimal PYSales;
        
        @AuraEnabled
        public Decimal immediatePipelineTotal;
        
        @AuraEnabled
        public Decimal extendedPipelineTotal;
        
        @AuraEnabled
        public Decimal immediatePipelineTotalWeighted;
        
        @AuraEnabled
        public Decimal extendedPipelineTotalWeighted;
        
        @AuraEnabled
        public Decimal FY17SalesProjection;
        
        @AuraEnabled
        public Boolean isComplete;
        
        @AuraEnabled
        public Decimal mandatesWon;
        
        @AuraEnabled
        public Decimal salesYTD;
        
        @AuraEnabled
        public Decimal estimatedRevenueYTD;
        
        @AuraEnabled
        public Decimal progressToPlan;
        
    }
}