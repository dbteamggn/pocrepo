/**************************************************************************************
 Name:            Account_UtilityTest
 Description:     Test Methods for Account_Utility Class
 Created By  :    Deloitte
 Created On  :    25 OCT 2016
 --------------------------------------------------------------------------------------
 Modification Log
 -----------------
    Name                 Date                       Comments
 -----------------   -------------   --------------------------------------------------
 Dilipkumar           25 OCT 2016                   Created
 --------------------------------------------------------------------------------------
 Review Log
 ----------    
 Name                Date            Comments
 -----------------   -------------   --------------------------------------------------
 
***************************************************************************************/

@isTest
public class Account_UtilityTest {
    /*
     * Description : Create new Clients
     * Param : Number of Clients to create
     * Returns :  List of Clients
    */
   public static List<Account> createClients(Integer count, String clientName){
        List<Account> testparentClients = new List<Account>();
        for(Integer iter=0;iter<count;iter++){
            Account testparClient = CommonTestUtils.CreateTestClient(clientName+'parent'+(iter+1));
            testparClient.Hierarchy_Level__c='L0';
            testparentClients.add(testparClient );
        }
        Database.SaveResult[] resparClients = Database.insert(testparentClients);
        List<Account> testClients = new List<Account>();
        for(Integer iter=0;iter<count;iter++){
            Account testClient = CommonTestUtils.CreateTestClient(clientName+(iter+1));
            testClient.parentId=testparentClients[0].id; 
            testClients.add(testClient);
        }
        Database.SaveResult[] resClients = Database.insert(testClients);
        /*List<Id> insID = new List<Id>();
        for(Database.SaveResult srIter : resClients){
            insID.add(srIter.getID());
        }
        List<Account> insClients = [Select id, Name from Account where id IN:insID];
        //Creating Hierarchy TC4->TC3->TC2->TC1->NULL
        for(Integer iter=0;iter<count-1;iter++){
            insClients.get(iter).ParentId = insClients.get(iter+1).Id;
        }
        update insClients;*/
        return testClients;
    }
    
    /*
     * Description : Create new Department
     * Param : Number of Department to create
     * Returns :  List of Departments
    */
    public static List<Department__c> createDepartments(Integer count){
        List<Department__c> testDepartments = new List<Department__c>();
        Department__c testDepartment = new Department__c();
        for(Integer iter=0;iter<count;iter++){
            testDepartments.add(testDepartment);
        }
        insert testDepartments;
        return testDepartments;
    }
    
     /*
     * Description : Create new Client - Departments relationship records
     * Param : Number of Clients to create relationship for
     * Returns :  List of Client - Departments relationship records
    */
    public static List<Department_Client_Relationship__c> createDeptClRltn(Integer noClients){
        //Creating Clients
        List<Account> testClients = createClients(noClients, 'Test Client');
        
        //Creating Hierarchy TC4->TC3->TC2->TC1->NULL
        /*for(Integer iter=0;iter<noClients-1;iter++)
            testClients.get(iter).ParentId = testClients.get(iter+1).Id;
        
        update testClients;*/
        
        //Create a department
        List<Department__c> testDepart = createDepartments(1);
        
        //Asserting there are no record with same Ids previously
        Map<string, Department_Client_Relationship__c> existingDeptClientRelMap = new Map<string, Department_Client_Relationship__c>();
        for(Department_Client_Relationship__c existingDeptClientRel : [select id, client__c, department__c from Department_Client_Relationship__c where client__c in: testClients AND department__c in: testDepart]){
            existingDeptClientRelMap.put(existingDeptClientRel.client__c +'*'+ existingDeptClientRel.department__c , existingDeptClientRel);
        }
        System.assertEquals(existingDeptClientRelMap.isEmpty(), True);
        
        //Assigning Department to Clients (TC4,testDept)->(TC3,testDept)->(TC2,testDept)->(TC1,testDept)->NULL
        List<Department_Client_Relationship__c> testDeptClientRels = new List<Department_Client_Relationship__c>();
        for(Integer iter=noClients-1;iter>=0;iter--){
            Department_Client_Relationship__c testDeptClientRel = new Department_Client_Relationship__c();
            testDeptClientRel.Client__c = (testClients.get(iter)).Id;
            testDeptClientRel.Department__c = testDepart.get(0).Id;
            testDeptClientRel.Is_Inherited_from_Hierarchy__c = TRUE;
            testDeptClientRels.add(testDeptClientRel);
        }
        return testDeptClientRels;
    }
    
    /*
     * Description : Create new Price book entry and add a prodcut to it
     * Param : Product name
     * Returns :  Product
    */
    public static Product2 createCustomPB(String prdName){
        // Pricebook2 custPB = new Pricebook2(Name=prdName,isActive=True,isStandard=True);
        //insert custPB;
        Product2 prod=new Product2(Name=prdName,IsActive=True);
        insert prod;
        Id priceBookId = Test.getStandardPricebookId();
        //Creating a price book entry for standard price book and created product
        PricebookEntry sPrice = new PricebookEntry(IsActive = True,Pricebook2ID = priceBookId,Product2Id = prod.Id,UnitPrice=100.0);
        insert sPrice;
        return prod;
        
    }
    
    /*
     * Description : Create new Client Fund association
     * Param : Number of clients to create, Product name
     * Returns :  Client Fund association list
    */
    public static void createClientFunds(Integer noClients, String clientName, String prdName){
        List<Account> testClients = createClients(noClients,clientName);
        Product2 testProduct = createCustomPB(prdName);
        //Create association between funds and clients
        List<Client_Fund__c> cFunds = new List<Client_Fund__c>();
        Client_Fund__c cFund ;    
        for(Integer iter=0;iter<noClients;iter++){
            cFund = new Client_Fund__c();
            cFund.Client__c = testClients.get(iter).ID;
            cFund.Product__c = testProduct.Id;
            cFund.Is_Inherited_From_Hierarchy__c=TRUE;
            cFunds.add(cFund);
        }
        insert cFunds;
        delete cFunds.get(0);
        delete cFunds.get(2);
    }
    
    public static testmethod void copyChildRecordsTest(){
        List<Department_Client_Relationship__c> testDeptClntRltnshp = createDeptClRltn(5);
        Test.startTest();
        insert testDeptClntRltnshp;
        delete testDeptClntRltnshp.get(3);
        upsert testDeptClntRltnshp.get(2);
        delete testDeptClntRltnshp.get(4);
        createClientFunds(5,'TC','PRD1');
        Test.stopTest();
    }
    
    public static testmethod void updateExternalIdOnMergeTest(){
        List<Account> testClients = createClients(5, 'Test Client');
        Account testAcc = CommonTestUtils.CreateTestClient('Masteraccount');
        insert testAcc;
        for(Account iter:testClients){
            iter.Master_Record_Id__c = testAcc.Id;
        }
        Account testThParty = CommonTestUtils.CreateTestThirdParty('TestTP');
        insert testThParty;
        Client_Third_Party_Relationship__c testCTP = CommonTestUtils.CreateTestClientTPRel(testAcc.Id,testThParty.Id);
        testCTP.External_ID__c = testAcc.Id;
        insert testCTP;
        Account testIFA = CommonTestUtils.CreateTestClient('TestIFA');
        insert testIFA;
        Test.startTest();
        delete testAcc;
        Test.stopTest();
    }
    
    static testMethod void myUnitTest(){
        Account_Utility newInst = Account_Utility.getInstance();
        List<Account> parentAccs = new List<Account>{new Account(Name = 'parent acc1', Billing_PostalCode__c = '505050', Hierarchy_Level__c='L0',Status__c='Active', Region__c = 'EMEA', Business_Line__c = 'Retail'),
                         new Account(Name = 'parent acc2', Billing_PostalCode__c = '515151', Hierarchy_Level__c='L0',Status__c='Active', Region__c = 'EMEA', Business_Line__c = 'Retail'),
                         new Account(Name = '3rd party', Billing_PostalCode__c = '525353', RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Third Party').getRecordTypeId(), Hierarchy_Level__c='L0',Status__c='Active', Region__c = 'EMEA', Business_Line__c = 'Retail')};
        insert parentAccs;
        Client_Third_Party_Relationship__c newCT = new Client_Third_Party_Relationship__c(Client_Name__c = parentAccs[1].Id, Third_Party_Name__c = parentAccs[2].Id);
        insert newCT;
        Account oneChildAcc = new Account(Name = 'Child Acc', Billing_PostalCode__c = '515102', ParentId = parentAccs[0].Id, Hierarchy_Level__c='L1', Region__c = 'EMEA', Business_Line__c = 'Retail');
        insert oneChildAcc;
        Account oneChildAcc1 = new Account(Name = 'Child Acc1', Billing_PostalCode__c = '515102', ParentId = parentAccs[0].Id, Hierarchy_Level__c='L1',Top_Level_Firm__c=oneChildAcc.id, Region__c = 'EMEA', Business_Line__c = 'Retail');
        insert oneChildAcc1;
        Contact cnt = new Contact(LastName = 'Test Cnt', AccountId = oneChildAcc1.Id);
        cnt.Status__c='Active';
        insert cnt;
        List<Department__c> testDepart = new List<Department__c>{
                                            new Department__c(Name = 'Test 1'),
                                            new Department__c(Name = 'Test 2'),
                                            new Department__c(Name = 'Test 3')};
        insert testDepart;
        List<Department_Client_Relationship__c> newDCRs = new List<Department_Client_Relationship__c>{
                                                            new Department_Client_Relationship__c(Department__c = testDepart[0].Id, Client__c = parentAccs[0].Id),
                                                            new Department_Client_Relationship__c(Department__c = testDepart[1].Id, Client__c = parentAccs[1].Id),
                                                            new Department_Client_Relationship__c(Department__c = testDepart[2].Id, Client__c = oneChildAcc.Id)};
        insert newDCRs;
        Department_Contact_Relationship__c newDCnR = new Department_Contact_Relationship__c(Department__c = testDepart[2].Id, Contact__c = cnt.Id);
        
        System.debug('newDCnR1'+[select id,name,Account.Top_Level_Firm__r.Name from contact where id=:cnt.Id]);
        System.debug('newDCnR2'+[select id,name,Client__r.Name from department__c where id=:testDepart[2].Id]);
        //insert newDCnR;
        List<Product2> newProds = new List<Product2>{
                                      new Product2(Name = 'Product', Asset_Manager_Name__c = 'AXA'),
                                      new Product2(Name = 'Product1', Asset_Manager_Name__c = 'AXA'),
                                      new Product2(Name = 'Product2', Asset_Manager_Name__c = 'AXA')
                                  };
        insert newProds;
        List<Client_Fund__c> newCFs = new List<Client_Fund__c>{
                                           new Client_Fund__c(Client__c = parentAccs[0].Id, Product__c = newProds[0].Id),
                                           new Client_Fund__c(Client__c = parentAccs[1].Id, Product__c = newProds[1].Id),
                                           new Client_Fund__c(Client__c = oneChildAcc.Id, Product__c = newProds[2].Id)};
        insert newCFs;
        Test.startTest();
            oneChildAcc.ParentId = parentAccs[1].Id;
            update oneChildAcc;
            merge parentAccs[0] parentAccs[1];
        Test.stopTest();
    }
}