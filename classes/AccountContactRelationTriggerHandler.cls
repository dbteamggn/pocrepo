/********************************************************************************
 * Name        : AccountContactRelationTriggerHandler 
 * Release     : R2
 * Phase       : P1
 * Description : Used for AccountContactRelationTrigger as Handler Class.
 * Author      : Deloitte
 * Reviewed By : 
 *********************************************************************************/
public class AccountContactRelationTriggerHandler extends TriggerHandler{
    /* This method does the following:
        1. Checks if the method is bypassed for users
        2. If not, it will copy the fields on accountcontactrelation to parent contact
    */
    protected override void beforeInsert(){
        if((triggerParams.objByPassMap.containsKey('AccountContactRelation_Utility.updateIsActiveFlag') && !triggerParams.objByPassMap.get('AccountContactRelation_Utility.updateIsActiveFlag')) || 
                !triggerParams.objByPassMap.containsKey('AccountContactRelation_Utility.updateIsActiveFlag')){
            AccountContactRelation_Utility.updateIsActiveFlag(triggerParams);
        }
    }
    protected override void beforeUpdate(){
        if((triggerParams.objByPassMap.containsKey('AccountContactRelation_Utility.updateIsActiveFlag') && !triggerParams.objByPassMap.get('AccountContactRelation_Utility.updateIsActiveFlag')) || 
                !triggerParams.objByPassMap.containsKey('AccountContactRelation_Utility.updateIsActiveFlag')){
            AccountContactRelation_Utility.updateIsActiveFlag(triggerParams);
        }
    }
    protected override void afterInsert(){
        if((triggerParams.objByPassMap.containsKey('AccountContactRelation_Utility.copyKeyData') && !triggerParams.objByPassMap.get('AccountContactRelation_Utility.copyKeyData')) || 
                !triggerParams.objByPassMap.containsKey('AccountContactRelation_Utility.copyKeyData')){
            AccountContactRelation_Utility.copyKeyData(triggerParams);
        }
    }
    protected override void afterUpdate(){
        if((triggerParams.objByPassMap.containsKey('AccountContactRelation_Utility.copyKeyData') && !triggerParams.objByPassMap.get('AccountContactRelation_Utility.copyKeyData')) || 
                !triggerParams.objByPassMap.containsKey('AccountContactRelation_Utility.copyKeyData')){
            AccountContactRelation_Utility.copyKeyData(triggerParams);
        }
    }
}