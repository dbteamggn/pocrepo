/********************************************************************************
* Name        : AccountContactRelationTriggerHandler 
* Release     : R2
* Phase       : P1
* Description : Trigger Utility for AccountClientRelation
* Author      : Deloitte
* Reviewed By : 
*********************************************************************************/
public class AccountContactRelation_Utility{
    
    public static void updateIsActiveFlag(TriggerHandler.TriggerParameter tParam){
        List<AccountContactRelation> newACRelationsList = (List<AccountContactRelation>) tParam.newList;
        Map<Id, AccountContactRelation> oldMap = (Map<Id, AccountContactRelation>)tParam.oldMap; //Added for US40140
         for(AccountContactRelation acr : newACRelationsList) {
            if(acr.isDirect){
                acr.isActive=true;
            }
            //US40140 updates- START
            else{
                if(tParam.isBefore && tParam.isUpdate && acr.isDirect != oldMap.get(acr.Id).isDirect){
                    acr.isActive = false;
                }
            }
            //US40140 updates- END
         }
    }
    
    
    public static void copyKeyData(TriggerHandler.TriggerParameter tParam){
        List<AccountContactRelation> newACRelationsList = (List<AccountContactRelation>) tParam.newList;
        Map<Id, AccountContactRelation> oldACRelationsMap = new Map<Id, AccountContactRelation>();
        Map<Id, List<AccountContactRelation>> contactIdAcrListMap = new Map<Id, List<AccountContactRelation>>();
        Map<Id, dataWrapperClass> conIdValueMap = new Map<Id, dataWrapperClass>();
        Set<id> contactIds=new Set<id>();
        //If trigger is is Update Copying Ids to contactId after comparing with old map
        if(tParam.isAfter && tParam.isUpdate){
            oldACRelationsMap = (Map<Id, AccountContactRelation>) tParam.oldMap;
            for(AccountContactRelation acr: newACRelationsList){
                if(acr.isDirect != oldACRelationsMap.get(acr.Id).isDirect || acr.Primary_Contact__c != oldACRelationsMap.get(acr.Id).Primary_Contact__c || acr.Key_Contact__c!= oldACRelationsMap.get(acr.Id).Key_Contact__c){
                    contactIds.add(acr.contactId);
                }
            }  
        }
        //If trigger is is Insert Copying Ids to contactId directly
        if(tParam.isAfter && tParam.isInsert){
            for(AccountContactRelation acr : newACRelationsList) {
                contactIds.add(acr.contactId);
            }
        }
              
        List<AccountContactRelation> acrList = [SELECT Id,IsDirect,Key_Contact__c,Primary_Contact__c,contactId FROM AccountContactRelation WHERE contactId IN : contactIds];
        List<Contact> conList = new List<contact>();
        dataWrapperClass wrap = new dataWrapperClass();
        if(contactIds.size() > 0) {
            if(acrList.size() > 0) {
                for(AccountContactRelation acr : acrList) {
                    if(!contactIdAcrListMap.containsKey(acr.contactId)) {
                        contactIdAcrListMap.put(acr.contactId, new List<AccountContactRelation>());
                    }
                    //Creating map of contactId to List of account contact relation 
                    contactIdAcrListMap.get(acr.contactId).add(acr);
                }    
                
                for(Id conID : contactIdAcrListMap.keySet()) {
                    List<AccountContactRelation> tempAcrList = new List<AccountContactRelation>();
                    tempAcrList = contactIdAcrListMap.get(conID);
                    for(AccountContactRelation acr : tempAcrList){
                        if(acr.isDirect){
                            wrap = new dataWrapperClass();
                            wrap.primaryContact=acr.Primary_Contact__c;
                            wrap.keyContact=acr.Key_Contact__c;
                            //Creating map of contactId to the data need to be copied 
                            conIdValueMap.put(conID,wrap);
                        }
                    }
                }           
            }
            contact con=new contact();
            //Copying data from map to the list that needs to be updated.
            for(id conId : contactIds) {
                if(conIdValueMap.containsKey(conId)){
                    con=new contact(id=conId);
                    con.Primary_Contact__c=conIdValueMap.get(con.Id).primaryContact;
                    con.Key_Contact__c=conIdValueMap.get(con.Id).keyContact;
                    conlist.add(con);
                }
            }
            Database.SaveResult[] srList = Database.update(conList);
            for(Database.SaveResult each: srList){
                if(!each.isSuccess()){
                    system.debug('****A record failed with the error: ' + each.getErrors());
                }
            }
        }
        
    }
    //Wrapper class to bundle boolean data
    public class dataWrapperClass{
        boolean nationalKeyContact;
        boolean primaryContact;
        boolean keyContact;
    }
}