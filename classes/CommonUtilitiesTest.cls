@isTest
public class CommonUtilitiesTest {
    /*
     * 
     * Description : Test sending an email.  Unfortunately nothing to Assert for this
     * Param : 
     * Returns :  
    */
    static testMethod void sendEmailTest() {
        
        User user = CommonTestUtils.createTestUser();        
        
        System.runAs(user){
            String[] addrs = new String[]{'a.b@deloitte.com'};
            if (!CommonTestUtils.runningInASandbox()) { // Required as NO_MASS_MAIL_PERMISSION is turned off in sandboxes to stop accidental emailing
                CommonUtilities.SendEmail('Subject', 'Plain Text Body', addrs);
            }
        }
    }
    
    /*
     * 
     * Description : Check that the required number of colours are returned by GetBrandedColours
     * Param : 
     * Returns :  
    */
    static testMethod void getBrandedColours() {
        
        User user = CommonTestUtils.createTestUser();        
        
        System.runAs(user){
            Test.startTest();
            list<String> coloursLessThanDefined = CommonUtilities.GetBrandedColours(10);
            list<String> coloursMoreThanDefined = CommonUtilities.GetBrandedColours(20);
            list<String> coloursLessThanDefinedOp = CommonUtilities.GetBrandedColours(10, 0.5);
            list<String> coloursMoreThanDefinedOp = CommonUtilities.GetBrandedColours(20, 0.2);
            Test.stopTest();
            
            system.assertEquals(10, coloursLessThanDefined.size());
            system.assertEquals(20, coloursMoreThanDefined.size());
            system.assertEquals(10, coloursLessThanDefinedOp.size());
            system.assertEquals(20, coloursMoreThanDefinedOp.size());
            
            // Check that the opacity has defaulted to 1 if not supplied
            for(string colour : coloursLessThanDefined){
                system.assertEquals(' 1.0)', colour.right(5));
            }
            
            // Check that the opacity has defaulted to 0.2 if that was passed into procedure
            for(string colour : coloursMoreThanDefinedOp){
                system.assertEquals(' 0.2)', colour.right(5));
            }
        }
    }
    
    static testMethod void myUnitTest(){
        CommonUtilities.GetFieldPicklistValues('Opportunity', 'StageName');
        CommonUtilities.getCurrencyInfo();
        CommonUtilities.GetDependentOptionsImpl(Contact_Subscription_Link__c.Contact__c.getDescribe().getReferenceTo()[0], 'Contact_Type__c', 'Contact_Sub_Type__c');
        CommonUtilities.GetDependentOptions('Contact', 'Contact_Type__c', 'Contact_Sub_Type__c');
        CommonUtilities.createExceptionLog(new NullPointerException());
        CommonUtilities.sendEmail('Test Mail', 'Test Content', new List<String>{'jpalla@deloitte.com'});
        CommonUtilities.TPicklistEntry testInst = new CommonUtilities.TPicklistEntry();
        testInst.active = 'Test';
        testInst.defaultValue = 'Test';
        testInst.label = 'Test';
        testInst.value = 'Test';
        testInst.validFor = 'Test';
        CommonUtilities.getObjectName(NULL);
        Account acc = new Account(Billing_PostalCode__c = '5050505', Name = 'Test Account', Hierarchy_Level__c='L0');
        insert acc;
        CommonUtilities.getObjectName(acc.Id);
    }
}