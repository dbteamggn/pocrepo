@isTest
public class BP_InstImmediatePipelineCtrl_Test{
    
    public static List<opportunity> setupTestData(string num){
        
        //create accounts
        List<account> accList = new List<account>();
        accList.add(CommonTestUtils.CreateTestClient('TestClient'));
        insert accList;
        
        //create Opportunities
        List<opportunity> oppList = new List<opportunity>();
        oppList.add(CommonTestUtils.CreateTestOpportunityMandate('TestOpp1', accList[0].id, null, 'Lead')); // will have senior sales team member
        oppList.add(CommonTestUtils.CreateTestOpportunityMandate('TestOpp2', accList[0].id, null, 'Lead')); // no team member
        insert oppList;
        
        //create users
        List<User> userList = new List<User>();
        userList.add(CommonTestUtils.createTestUser(false));
            user u = CommonTestUtils.createTestUser(false);
            u.username='TestUser23New'+num+'@test.com';
        userList.add(u);
        insert userList;
        
        //create opportunity team members
        List<OpportunityTeamMember> oppTeamMemList = new List<OpportunityTeamMember>();
        oppTeamMemList.add(new OpportunityTeamMember(OpportunityId=oppList[0].id, userId=userList[0].id, TeamMemberRole='Senior Sales', OpportunityAccessLevel='Edit'));
        oppTeamMemList.add(new OpportunityTeamMember(OpportunityId=oppList[0].id, userId=userList[1].id, TeamMemberRole='Senior Sales', OpportunityAccessLevel='Edit'));
        insert oppTeamMemList;
        
        return oppList;
    }
    
    static testMethod void getBusinessPlanOppsTest(){
        //opportunities
        List<opportunity> oppList = setupTestData('1');
        
        //create Business Plans
        List<Business_Plan__c> bpList = new List<Business_Plan__c>();       
        bpList.add(new Business_Plan__c(name='TestPlan',Business_Plan_Opportunities_Inserted__c=false));
        insert bpList;
        
        //Business Plan Opportunities
        List<Business_Plan_Opportunity__c> bpoList = new List<Business_Plan_Opportunity__c>();
        bpoList.add(new Business_Plan_Opportunity__c(Opportunity__c=oppList[0].id, Business_Plan__c=bpList[0].id)); // this opp has team member
        bpoList.add(new Business_Plan_Opportunity__c(Opportunity__c=oppList[1].id, Business_Plan__c=bpList[0].id)); //this opp does not have team member
        insert bpoList;
        
        Test.startTest();
        List<BP_InstImmediatePipelineCtrl.RelatedOpportunity> returnedList = BP_InstImmediatePipelineCtrl.getBusinessPlanOpps(bpList[0].id);
        system.assertEquals(returnedList.size(), 2);
        
        system.assertEquals(false, BP_InstImmediatePipelineCtrl.hasBusinessPlanOpp(bpList[0].id));      
        Test.stopTest();
    }
    
    static testMethod void getNonBusinessPlanOppsTest(){
        //opportunities
        List<opportunity> oppList = setupTestData('2');
        
        //create Business Plans
        List<Business_Plan__c> bpList = new List<Business_Plan__c>();       
        bpList.add(new Business_Plan__c(name='TestPlan',Business_Plan_Opportunities_Inserted__c=false));
        bpList.add(new Business_Plan__c(name='TestPlan2',Business_Plan_Opportunities_Inserted__c=true));
        insert bpList;
        
        Test.startTest();
        List<BP_InstImmediatePipelineCtrl.RelatedOpportunity> returnedList1 = BP_InstImmediatePipelineCtrl.getNonBusinessPlanOpps(bpList[0].id, false);
        system.debug('returnedList1.size()-->'+returnedList1.size());
        //system.assertEquals(returnedList1.size(), 2);
        List<BP_InstImmediatePipelineCtrl.RelatedOpportunity> returnedList2 = BP_InstImmediatePipelineCtrl.getNonBusinessPlanOpps(bpList[0].id, true);
        system.debug('returnedList2.size()-->'+returnedList2.size());
        //system.assertEquals(returnedList2.size(), 2);
        Test.stopTest();
    }
}