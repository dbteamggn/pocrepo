@isTest
public class FundListController_Test{
    /*
     * 
     * Description : Find all Funds.
     * Param : 
     * Returns :  Funds as a wrapper class
    */
    static testMethod void TestFundFindAll(){
        
        User user = CommonTestUtils.createTestUser();        
        
        product2 p= new product2();
        p.Name='test Product';
        insert p;
        
        Investment_Strategy__c is=new Investment_Strategy__c ();
        is.Name='test strat';
        insert is;
        
        Configuration_Util__c cu=new Configuration_Util__c();
        cu.Name='FundProfile';
        cu.Value__c='System Administrator';
        insert cu;
        
        Configuration_Util__c cu1=new Configuration_Util__c();
        cu1.Name='InvestmentProfile';
        cu1.Value__c='System Administrator';
        insert cu1;
        
        System.runAs(user){
            Test.startTest();
            List<FundListController.Result> ViewedFund =  FundListController.findAll(); 
            Test.stopTest();            
        }
    }
    static testMethod void TestFundFindByName(){
        
        User user = CommonTestUtils.createTestUser();        
        
        product2 p= new product2();
        p.Name='test Product';
        insert p;
        
        Investment_Strategy__c is=new Investment_Strategy__c ();
        is.Name='test strat';
        insert is;
        
        Configuration_Util__c cu=new Configuration_Util__c();
        cu.Name='FundProfile';
        cu.Value__c='System Administrator';
        insert cu;
        
        Configuration_Util__c cu1=new Configuration_Util__c();
        cu1.Name='InvestmentProfile';
        cu1.Value__c='System Administrator';
        insert cu1;
        
        System.runAs(user){
            Test.startTest();
            List<FundListController.Result> ViewedFund =  FundListController.findByName('test'); 
            Test.stopTest();            
        }
    }
    static testMethod void TestFundFindByIds(){
        
        User user = CommonTestUtils.createTestUser();        
        
        product2 p= new product2();
        p.Name='test Product';
        insert p;
        
        Investment_Strategy__c is=new Investment_Strategy__c ();
        is.Name='test strat';
        insert is;
        
        Configuration_Util__c cu=new Configuration_Util__c();
        cu.Name='FundProfile';
        cu.Value__c='System Administrator';
        insert cu;
        
        Configuration_Util__c cu1=new Configuration_Util__c();
        cu1.Name='InvestmentProfile';
        cu1.Value__c='System Administrator';
        insert cu1;
        
        List<id> ids=new List<id>();
        ids.add(p.id);
        ids.add(is.id);
        
        System.runAs(user){
            Test.startTest();
            List<FundListController.Result> ViewedFund =  FundListController.findByListId(ids); 
            Test.stopTest();            
        }
    }
}