/*----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            createMeetingController
   Description:     Methods called by creareMeetingForm
                    
   Date             Author                             Summary of Changes                  
   -----------      -----------------                  -------------------------------------------------------------------------------------
   8 Aug 2016       Deloitte                           Initial Release 
------------------------------------------------------------------------------------------------------------------------------------------------------------*/

public class createMeetingController {
    public id contactId;
    public string subType;
    public string assetClass;
    public string notes;
    public cls_selectedContact[] selectedContact;
    public cls_selectedFund[] selectedFund;
    class cls_selectedContact {
        public String id;   //0032800000VIJ5cAAH
        public String name; //Rose Gonzalez
    }
    class cls_selectedFund {
        public String id;   
        public String name; 
    }
    /*
     * Description : Method to get dependent picklist values
     * Param : Object name, Controlling Field name, dependent field name, controlling field value
     * Returns :  
    */
    @AuraEnabled
    public static List<String> GetDpndntPckLstValues(String ObjectName, String CtrlField, String DependField, String CtrlFieldValue) {
        //Map<String, List<String>> picklistMap = CommonUtilities.GetDependentOptions(ObjectName, CtrlField, DependField);
        //List<String> values = picklistMap.get(CtrlFieldValue);
        List<String> values = PicklistDescriber.describe(ObjectName, CtrlField, DependField, CtrlFieldValue);
        return values;
    }
    
    /*
     * Description : Method to get picklist values for a field
     * Param : Object name, Picklist Field name
     * Returns :  
    */
    @AuraEnabled
    public static List<String> GetPckLstValues (String ObjectApi_name, String picklistField){
        List<String> values = CommonUtilities.GetFieldPicklistValues(ObjectApi_name, picklistField);
        return values;
    }
    public static createMeetingController parse(String json){
        return (createMeetingController) System.JSON.deserialize(json, createMeetingController.class);
    }
    @AuraEnabled
    public static id createMeeting(string param){
        createMeetingController temp = parse(param);
        system.debug(temp);
        Activity_Alias__c alias=new Activity_Alias__c();
        Event e=new Event();
        
        e.subject=temp.subType+' - '+string.ValueOf(date.today().format());  //Event Sub-Type_Meeting_Date
        e.startDateTime=System.today();
        //System.debug(temp.startDateTime);
        e.endDateTime=System.today();
        e.IsallDayEvent=true;
        //e.Asset_Class__c=temp.assetClass;
        e.Description=temp.notes;
        e.Type__c='Meeting';
        e.Sub_Type__c=temp.subType;
        
        //Added for Nuveen
        e.Status__c = 'Open';
        e.WhatId = '0010v000007QSNwAAO';
        
        //e.location=temp.location;
        //if(temp.contactId!=null){
          //  e.whatId=temp.contactId;
        //}
        System.debug('temp.selectedContact'+temp.selectedContact);
        if(temp.selectedContact.size()>0)
            e.WhoId=temp.selectedContact[0].id;
        
        alias.Contact__c = e.WhoId;
        insert alias;
        
        e.Strategies_Funds__c=alias.id;
        insert e;
        system.debug('temp'+temp);
        EventRelation er= new EventRelation ();
        List<EventRelation> erList = new List<EventRelation>();
        for(cls_selectedContact  tempc : temp.selectedContact){
            if(tempc.id!=e.WhoId){
                er= new EventRelation ();
                er.RelationId=tempc.id;
                er.EventId=e.id;
                er.isParent=true;
                erList.add(er);
            }
        }
        System.debug(erList);
        insert erList;
        
        //insert Funds/Strategies
        Activity_Product_Relationship__c actPro = new Activity_Product_Relationship__c();
        List<Activity_Product_Relationship__c> apList = new List<Activity_Product_Relationship__c>();
        System.debug('yeyyer'+temp.selectedFund);
        System.debug('alias'+alias.id);
        for(cls_selectedFund  tempsf : temp.selectedFund){
                actPro= new Activity_Product_Relationship__c ();
                actPro.Activity_Alias__c=alias.id;
                if(Id.valueOf(tempsf.id).getSObjectType() == Schema.Product2.SObjectType)
                    actPro.fund__c=tempsf.id;
                else if(Id.valueOf(tempsf.id).getSObjectType() == Schema.Investment_Strategy__c.SObjectType)
                    actPro.Investment_Strategy__c=tempsf.id;
                apList.add(actPro);
        }
         System.debug('apList'+apList);
        insert apList;
        return e.id;
    }
}