public class KeyCovAndTeam_DTO_AMA {
    @auraEnabled
    public List<keyContacts> lstKeyCon = new List<keyContacts>();
    @auraEnabled
    public List<CoverageTeamMember> lstCovTeamMem  = new List<CoverageTeamMember>();
	
    public class keyContacts
    {
        @auraEnabled
        public String conName {get;set;}
        @auraEnabled
        public String conRole {get;set;}
        @auraEnabled
        public Integer recActivityCount {get;set;}
        
        public keyContacts(String name, String role, Integer count)
        {
            conName = name;
            conRole = role;
            recActivityCount = count; 
        }        
    }
    public class CoverageTeamMember
    {
        @auraEnabled
        public String covTeamMemName {get;set;}
        @auraEnabled
        public String covTeamMemRole {get;set;}
        @auraEnabled
        public String covTeamMemEmail {get;set;}
        @auraEnabled
        public Integer covTeamMemRecActCount {get;set;}
        
        public CoverageTeamMember(String name, String role, String email,Integer count)
        {
            covTeamMemName = name;
            covTeamMemRole = role;
            covTeamMemEmail = email;
            covTeamMemRecActCount = count; 
        }        
    }
}