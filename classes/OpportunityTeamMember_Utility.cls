public class OpportunityTeamMember_Utility {

    /*
     * Description : Create new instance / return existing instance
     * Param : 
     * Returns :  
    */
    private static OpportunityTeamMember_Utility instance = null; 
    
    public static OpportunityTeamMember_Utility getInstance() {
        if (instance == null) {
            instance = new OpportunityTeamMember_Utility();
        }
        return instance;
    }
    
    /*
     * Description  : method called on before insert trigger event from OpportunityTeamMember_Trigger
     * Param        : trigger.new
     * Returns      : none
    */ 
    public void beforeInsert(List<OpportunityTeamMember> newList) {
        
        string defaultPrimaryMemberRole = Configuration_Util__c.getInstance('Primary Opportunity Team Role') != null ? Configuration_Util__c.getInstance('Primary Opportunity Team Role').value__c : 'Senior Sales';
        string primaryTeamMemberError = Label.Primary_Opportunity_Team_Member_Exists !=null ? Label.Primary_Opportunity_Team_Member_Exists : 'Primary Opportunity Team Member already exists.';
        opptyTeamMemberInfo primaryTeamMemInfo = new opptyTeamMemberInfo();
        Set<id> opptyIdSet = new Set<id>();
        Map<id, List<OpportunityTeamMember>> oppIdOppTeamMemberListMap = new Map<id, List<OpportunityTeamMember>>();
        Map<id,OpportunityTeamMember> oppTeamMemberToUpdateMap = new Map<id,OpportunityTeamMember>();
        Opportunity opp;
        List<Opportunity> oppToBeUpdatedList = new List<Opportunity>();
        try{
            for(OpportunityTeamMember oppTeamMem : newList){
                opptyIdSet.add(oppTeamMem.opportunityId);
            }
            
            oppIdOppTeamMemberListMap = getRelatedOpportunityTeamMembers(opptyIdSet);
            
            for(OpportunityTeamMember oppTeamMember : newList){
                //get primary team member info if one exists
                primaryTeamMemInfo = checkPrimaryTeamMemberExists(oppTeamMember.opportunityId, oppIdOppTeamMemberListMap);
                
                //if primary = true and the Role is not default primary role, check if there is an existing primary team member
                if(oppTeamMember.primary__c && !oppTeamMember.TeamMemberRole.equalsIgnoreCase(defaultPrimaryMemberRole)){
                
                    //if primaryTeamMember != null, primary team member already exists - add error
                    if(primaryTeamMemInfo.primaryTeamMember != null){
                        oppTeamMember.addError(primaryTeamMemberError);
                    }
                }
                //if the new member role is default primary role
                else if(oppTeamMember.TeamMemberRole.equalsIgnoreCase(defaultPrimaryMemberRole)){
                    //if the existing primary member does not have the default primary role
                    if(primaryTeamMemInfo.primaryTeamMember !=null && !primaryTeamMemInfo.primaryTeamMember.TeamMemberRole.equalsIgnoreCase(defaultPrimaryMemberRole)){
                        //make this member as primary, and uncheck existing primary if : primary flag is checked OR (default to primary - if primary flag unchecked AND no non-primary team member with default primary role)
                        if(oppTeamMember.primary__c || (!oppTeamMember.primary__c && !primaryTeamMemInfo.defaultPrimaryRoleMemberExists)){
                            primaryTeamMemInfo.primaryTeamMember.primary__c = false;
                            oppTeamMemberToUpdateMap.put(primaryTeamMemInfo.primaryTeamMember.id, primaryTeamMemInfo.primaryTeamMember);
                            
                            oppTeamMember.primary__c = true;
                        }
                    }
                    //if existing primary primary member has default primary role, and primary flag is true for new member add error for new member with default primary role
                    else if(primaryTeamMemInfo.primaryTeamMember !=null && oppTeamMember.primary__c && primaryTeamMemInfo.primaryTeamMember.TeamMemberRole.equalsIgnoreCase(defaultPrimaryMemberRole)){
                        oppTeamMember.addError(primaryTeamMemberError);
                    }
                    //no existing primary member and new member has default primary role, set as primary
                    else if(primaryTeamMemInfo.primaryTeamMember == null){
                        oppTeamMember.primary__c = true;
                    }
                }
                //US30337 - START - populating primary_opportunity_team_member__c on related opportunity record
                if(oppTeamMember.primary__c){
                    opp = new Opportunity();
                    opp.id = oppTeamMember.opportunityId;
                    //opp.primary_opportunity_team_member__c = oppTeamMember.userId;
                    oppToBeUpdatedList.add(opp);
                }
                //US30337 - END - populating primary_opportunity_team_member__c on related opportunity record
            }
            
            //updating old primary Team Members
            if(!oppTeamMemberToUpdateMap.isEmpty()){
                update oppTeamMemberToUpdateMap.values();
            }
            
            //US30337 - START - Update primary_opportunity_team_member__c on opportunity if the primary member has changed 
            if(!oppToBeUpdatedList.isEmpty()){
                update oppToBeUpdatedList;
            }
            //US30337 - END - Update primary_opportunity_team_member__c on opportunity if the primary member has changed
        }
        catch(Exception ex){
            CommonUtilities.createExceptionLog(ex);
        }
    }
    
    /*
     * Description  : method called on before update trigger event from OpportunityTeamMember_Trigger
     * Param        : trigger.new
     * Returns      : none
    */ 
    public void beforeUpdate(List<OpportunityTeamMember> newList, Map<id, OpportunityTeamMember> oldMap) {
        
        Set<id> opptyIdSet = new Set<id>();
        Map<id, List<OpportunityTeamMember>> oppIdOppTeamMemberListMap = new Map<id, List<OpportunityTeamMember>>();
        string primaryTeamMemberError = Label.Primary_Opportunity_Team_Member_Exists !=null ? Label.Primary_Opportunity_Team_Member_Exists : 'Primary Opportunity Team Member already exists.';
        Opportunity opp;
        Map<id, Opportunity> oppToBeUpdatedWithTeamMemberMap = new Map<id, Opportunity>();
        Map<id, Opportunity> oppToBeUpdatedWithOwnerIdMap = new Map<id, Opportunity>();
        Map<id, Opportunity> oppDetailsMap = new Map<id, Opportunity>();
        Opportunity_Utility oppUtil = Opportunity_Utility.getInstance();
        
        try{
            for(OpportunityTeamMember oppTeamMem : newList){
                opptyIdSet.add(oppTeamMem.opportunityId);
            }
            oppDetailsMap = oppUtil.getOpportunities(opptyIdSet); //US30337 - get all details for all opportunities
            oppIdOppTeamMemberListMap = getRelatedOpportunityTeamMembers(opptyIdSet);
           
            for(OpportunityTeamMember oppTeamMember : newList){
                //if during update primary flag has changed from false to true, check if there is an existing primary team member           
                if(oppTeamMember.primary__c && !(oldMap.get(oppTeamMember.id).primary__c)){
                    //if the value returned is != null, primary team member already exists - add error
                    if(checkPrimaryTeamMemberExists(oppTeamMember.opportunityId, oppIdOppTeamMemberListMap).primaryTeamMember != null){
                        oppTeamMember.addError(primaryTeamMemberError);
                    }
                }
                //US30337 - START
                //if the flag has been switched from true to false, just populate the primary_opportunity_team_member__c as opportunity owner
                else if(!oppTeamMember.primary__c && oldMap.get(oppTeamMember.id).primary__c){
                    opp = new Opportunity();
                    opp.id = oppTeamMember.opportunityId;
                   // opp.primary_opportunity_team_member__c = oppDetailsMap.get(oppTeamMember.opportunityId).ownerId;
                    oppToBeUpdatedWithOwnerIdMap.put(opp.id, opp);
                } 
                //populating primary_opportunity_team_member__c on related opportunity record
                if(oppTeamMember.primary__c){
                    opp = new Opportunity();
                    opp.id = oppTeamMember.opportunityId;
                   // opp.primary_opportunity_team_member__c = oppTeamMember.userId;
                    oppToBeUpdatedWithTeamMemberMap.put(opp.id, opp);
                }
            }
            //merging both maps to update opportunities - if primary team member has not been set on oppty (i.e. opportunity is not present in oppToBeUpdatedWithTeamMemberMap), get the opportunity from oppToBeUpdatedWithOwnerIdMap
            for(Id oppId : oppToBeUpdatedWithOwnerIdMap.keySet()){
                if(oppToBeUpdatedWithTeamMemberMap.isEmpty() || (!oppToBeUpdatedWithTeamMemberMap.isEmpty() && !oppToBeUpdatedWithTeamMemberMap.containsKey(oppId))){
                    oppToBeUpdatedWithTeamMemberMap.put(oppId, oppToBeUpdatedWithOwnerIdMap.get(oppId));
                }
            }
            
            //Update primary_opportunity_team_member__c on opportunity 
            if(!oppToBeUpdatedWithTeamMemberMap.isEmpty()){
                update oppToBeUpdatedWithTeamMemberMap.values();
            }
            //US30337 - END
        }
        catch(Exception ex){
            CommonUtilities.createExceptionLog(ex);
        }
    }
    
    
    /*
     * Description  : method called on before delete trigger event from OpportunityTeamMember_Trigger
     * Param        : trigger.old
     * Returns      : none
    */ 
    public void beforeDelete(List<OpportunityTeamMember> oldList) {
        
        List<Opportunity> oppToBeUpdatedList = new List<Opportunity>();
        Opportunity opp;
        Map<id, Opportunity> oppDetailsMap = new Map<id, Opportunity>();
        Opportunity_Utility oppUtil = Opportunity_Utility.getInstance();
        Set<id> opptyIdSet = new Set<id>();
        
        try{
            for(OpportunityTeamMember oppTeamMember : oldList){
                opptyIdSet.add(oppTeamMember.opportunityId);
            }
            //getting details for all opportunities
            oppDetailsMap = oppUtil.getOpportunities(opptyIdSet);
            
            for(OpportunityTeamMember oppTeamMem : oldList){
                //if the opportunity team member being deleted is primary, populate primary_opportunity_team_member__c on opportunity with oppty owner
                if(oppTeamMem.primary__c){
                    opp = new Opportunity();
                    opp.id = oppTeamMem.opportunityId;
                  //  opp.primary_opportunity_team_member__c = oppDetailsMap.get(oppTeamMem.opportunityId).ownerId;
                    oppToBeUpdatedList.add(opp);
                }
            }
            
            //update related opportunities
            if(!oppToBeUpdatedList.isEmpty()){
                update oppToBeUpdatedList;
            }
        }
        catch(Exception ex){
            CommonUtilities.createExceptionLog(ex);
        }
    }
    
    
    /*
     * Description  : method to check if a primary team member already exists for an opportunity and if a non-primary member exists with default primary role
     * Param        : opportunity Id, Map of opportunity id and list of related OpportunityTeamMember records
     * Returns      : wrapper instance Primary Team Member and boolean to indicate if a non-primary memberexists with a default primary role
    */
    public opptyTeamMemberInfo checkPrimaryTeamMemberExists(Id opptyId, Map<id, List<OpportunityTeamMember>> oppIdOppTeamMemberListMap){
        
        List<OpportunityTeamMember> existingOppTeamMemberList = new List<OpportunityTeamMember>();
        opptyTeamMemberInfo primaryTeamMemInfo = new opptyTeamMemberInfo();
        string defaultPrimaryMemberRole = Configuration_Util__c.getInstance('Primary Opportunity Team Role') != null ? Configuration_Util__c.getInstance('Primary Opportunity Team Role').value__c : 'Senior Sales';
        
        if(!oppIdOppTeamMemberListMap.isEmpty() && oppIdOppTeamMemberListMap.containsKey(opptyId)){
            existingOppTeamMemberList = oppIdOppTeamMemberListMap.get(opptyId);
        }
        
        //iterate over existing team members to check if an existing member is primary
        for(OpportunityTeamMember oppTeamMember : existingOppTeamMemberList){
            //get primary team member details
            if(oppTeamMember.primary__c){
                primaryTeamMemInfo.primaryTeamMember = oppTeamMember;
            }
            //check if a default primary role member is non-primary
            else if(!oppTeamMember.primary__c && oppTeamMember.TeamMemberRole.equalsIgnoreCase(defaultPrimaryMemberRole)){
                primaryTeamMemInfo.defaultPrimaryRoleMemberExists = true;
            }
        }
        
        return primaryTeamMemInfo;
    }
    
    
    /*
     * Description  : method to get get related OpportunityTeamMember records for an opportunity
     * Param        : set of opportunity IDs
     * Returns      : Map of opportunity id and list of related OpportunityTeamMember records
    */
    public Map<id, List<OpportunityTeamMember>> getRelatedOpportunityTeamMembers(set<Id> opptyIdSet){
        
        Map<id, List<OpportunityTeamMember>> oppIdOppTeamMemberListMap = new Map<id, List<OpportunityTeamMember>>();
        List<OpportunityTeamMember> oppTeamMemberList = new List<OpportunityTeamMember>();
        
        oppTeamMemberList = [select id, primary__c, TeamMemberRole, opportunityId from OpportunityTeamMember where opportunityId in: opptyIdSet];
        
        for(OpportunityTeamMember oppTeamMember : oppTeamMemberList){
            //if map contains the key, add to the list of team members
            if (!oppIdOppTeamMemberListMap.isEmpty() && oppIdOppTeamMemberListMap.containsKey(oppTeamMember.opportunityId)){
                oppIdOppTeamMemberListMap.get(oppTeamMember.opportunityId).add(oppTeamMember);
            }
            //map not empty but the oppty Id has not yet been encountered, add key and value
            else if(!oppIdOppTeamMemberListMap.isEmpty() && !oppIdOppTeamMemberListMap.containsKey(oppTeamMember.opportunityId)){
                oppIdOppTeamMemberListMap.put(oppTeamMember.opportunityId, new List<OpportunityTeamMember>{oppTeamMember});
            }
            //map empty, add key and value
            else{
                oppIdOppTeamMemberListMap.put(oppTeamMember.opportunityId, new List<OpportunityTeamMember>{oppTeamMember});
            }
        }
        
        return oppIdOppTeamMemberListMap;
    }
    
    /*
     * Description  : class to get capture primary team member and existing default primary role member information 
     */
    public class opptyTeamMemberInfo{
        OpportunityTeamMember primaryTeamMember;
        boolean defaultPrimaryRoleMemberExists;
        
        public opptyTeamMemberInfo(){
            defaultPrimaryRoleMemberExists = false;
        }
    }
}