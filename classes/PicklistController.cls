public class PicklistController {
    @AuraEnabled        
    public static List<String> getPicklistValuesIntoList(String objectType, String selectedField){
        List<String> picklistValuesList = new List<String>();
        Schema.SObjectType convertToObj = Schema.getGlobalDescribe().get(objectType);
        Schema.DescribeSObjectResult res = convertToObj.getDescribe();
        Schema.DescribeFieldResult fieldResult = res.fields.getMap().get(selectedField).getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            picklistValuesList.add(pickListVal.getLabel());
        }     
        return picklistValuesList;
    }
}