/*----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            clientFinancialsCtrl.cls
   Description:     Controller for the ClientFinancials lightning component
                    
   Date             Author                             Summary of Changes                  
   -----------      -----------------                  -------------------------------------------------------------------------------------
   Oct 2016         Andy Wallis                        Initial Release 
   Jan 2017         Andee Weir                         Replace GetFinancials procedure with GetUsersProfile.  Logic of which profile has access to which 
                                                       financials is now done in the component. (US41217)
------------------------------------------------------------------------------------------------------------------------------------------------------------*/
public with sharing class clientFinancialsCtrl {
    
    @AuraEnabled
    public static boolean IsNdam() {

        //If the user is flagged as a FIG (or NDAM), or is a Sys Admin, return true
        User runByUser = [select Is_FIG__c, profileId from user where id = :UserInfo.getUserId() limit 1];
        Profile userProfile = [select Name from profile where Id = :runByUser.ProfileId];
        If (userProfile.Name == 'System Administrator' || runByUser.Is_FIG__c) {
        	return true;  
        } else {
            return false;
        }
  
    }

    @AuraEnabled
    public static String GetUsersProfile() {

        //If the user's profile is on the list to see the Financials, or is Sys Admin, return true
        User runByUser = [select profileId from user where id = :UserInfo.getUserId() limit 1];
        Profile userProfile = [select Name from profile where Id = :runByUser.ProfileId limit 1];
        return userProfile.Name;
  
    }
}