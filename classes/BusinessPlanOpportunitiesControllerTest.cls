@isTest
public class BusinessPlanOpportunitiesControllerTest {
    
    public static testmethod void testBusinessPlanOpportunities(){
        Profile p1 = [select Id from Profile where Name like '%Retail%'];
        User u = CommonTestUtils.createTestUser(true);
        u.Business_Line__c = 'Retail';        
        u.ProfileId =p1.id;
        update u;
        Id standardPB = Test.getStandardPricebookId();
        Test.startTest();
        
        System.runAs(u){
            Product2 product = new Product2();
            product.name='Test Product';
            insert product;
            PriceBook2 priceBook = new PriceBook2(Name='Test PriceBook',IsActive= true);
            insert pricebook;
            PriceBook2 priceBook1 = new PriceBook2(Name='Test PriceBook1',IsActive= true);
            insert pricebook1;
            //   PriceBookEntry pb = new PriceBookEntry(PriceBook2Id = standardPB.Id, Product2Id=product.Id, UnitPrice=1000,IsActive=true, UseStandardPrice=false );
            // insert pb;
            PriceBookEntry pbe = new PriceBookEntry(PriceBook2Id = standardPB, Product2Id=product.Id );
            pbe.UseStandardPrice=false;
            pbe.UnitPrice=1000;
            pbe.IsActive=true;
            insert pbe;
            PriceBookEntry pbe1 = new PriceBookEntry(PriceBook2Id = priceBook1.Id, Product2Id=product.Id );
            pbe1.UseStandardPrice=false;
            pbe1.UnitPrice=1000;
            pbe1.IsActive=true;
            insert pbe1;
            Account newAccount1 = CommonTestUtils.CreateTestClient('Test User 1');
            Account newAccount2 = CommonTestUtils.CreateTestClient('Test User 2');
            insert(newAccount1);
            insert(newAccount2);
            
            Opportunity opp1 = CommonTestUtils.CreateTestOpportunityMandate('OppTest1', newAccount1.id,Date.today()+100,'6-Closed');
            Opportunity opp2 = CommonTestUtils.CreateTestOpportunityMandate('OppTest1', newAccount2.id,Date.today()+150,'7-Won');
            Opportunity opp3 = CommonTestUtils.CreateTestOpportunityMandate('OppTest1', newAccount1.id,Date.today()-10,'6-Closed');
            
            Business_Plan__c bp = BusinessPlanOpportunitiesControllerTest.createTestBusinessPlan('Business Plan 1');
            insert opp1;
            insert opp2;
            opp3.Product__c = product.Id;
            insert opp3;
            insert bp;
            OpportunityLineItem opl = new OpportunityLineItem();
            opl.OpportunityId = opp3.id;
            opl.PricebookEntryId= pbe.Id;
            opl.UnitPrice=1000;
            
            insert opl;
            OpportunityLineItem opl1 = new OpportunityLineItem();
            opl1.OpportunityId = opp1.id;
            opl1.PricebookEntryId= pbe1.Id;
            opl1.UnitPrice=1.00;
            
            insert opl1;
            // Currently no opportunities are linked to the businessPlan
            BusinessPlanOpportunitiesController.getRelatedOpportunities(bp.id);
            
            Business_Plan_Opportunity__c bpo1 = BusinessPlanOpportunitiesControllerTest.createTestBusinessPlanOpportunity(bp, opp1);
            Business_Plan_Opportunity__c bpo2 = BusinessPlanOpportunitiesControllerTest.createTestBusinessPlanOpportunity(bp, opp2);
            insert bpo1;
            insert bpo2;
            BusinessPlanOpportunitiesController.getRelatedOpportunities(bp.id);
            List<ID> oppIds = new List<ID>();    
            List<ID> delOppIds = new List<ID>();
            oppIds.add(opp3.ID);
            BusinessPlanOpportunitiesController.getOpportunity(oppIds);
            BusinessPlanOpportunitiesController.createBusinessPlanOpportunities(String.valueOf(bp.id),oppIds, new List<ID>());
            oppIds = new List<ID>();
            delOppIds = new List<ID>();
            oppIds.add(null);
            delOppIds.add(null);            
            BusinessPlanOpportunitiesController.createBusinessPlanOpportunities(String.valueOf(bp.id),oppIds, delOppIds);
            oppIds = new List<ID>();
            delOppIds = new List<ID>();
            oppIds.add(opp3.ID);
            delOppIds.add(opp1.ID);            
            BusinessPlanOpportunitiesController.createBusinessPlanOpportunities(String.valueOf(bp.id),oppIds, delOppIds);
        }
        Test.stopTest();        
    }
    
    public static Business_Plan__c createTestBusinessPlan(String businessPlanName){
        Business_Plan__c businessPlan = new Business_Plan__c();
        businessPlan.name = businessPlanName;
        businessPlan.RecordTypeId = Business_Plan__c.sObjectType.getDescribe().getRecordTypeInfosByName().get('Open').getRecordTypeId();
        return businessPlan;
    }
    public static Business_Plan_Opportunity__c createTestBusinessPlanOpportunity(Business_Plan__c businessPlan, Opportunity opp){
        Business_Plan_Opportunity__c bpo = new Business_Plan_Opportunity__c();
        bpo.Business_Plan__c = businessPlan.Id;
        bpo.Opportunity__c = opp.Id;
        return bpo;
    }           
    
}