public class AssignWarmAdvisor {
    
    public Id owner {get; set;}
    public Id taskId { get; set; }
    public Task taskRecord { get; set; }
    
    public AssignWarmAdvisor(ApexPages.StandardController controller) {
        taskRecord =  (Task) controller.getRecord();
        taskId = taskRecord.Id;
        owner = taskRecord.OwnerId;
    }
	
    public PageReference markWarmAdvisor(){
        
        //get all the contact associated with task
        if(taskId != null){
            //select Id, RelationId, TaskId from TaskRelation limit 10
            list<TaskRelation> taskRelations = [select Id, RelationId, TaskId from TaskRelation where TaskId = :taskId limit 10000];
            
            if(taskRelations != null && taskRelations.size() > 0){
            
                set<String> contactIds = new set<String>();
                
                for(TaskRelation t : taskRelations){
                    contactIds.add(t.RelationId);
                }
                
                if(contactIds != null && contactIds.size() > 0){
                    list<Contact> contactList = [select Id, Warm_Advisor__c from Contact where Id in :contactIds limit 10000];
                    
                    if(contactList != null && contactList.size() > 0){
                        for(Contact con : contactList){
                            con.Warm_Advisor__c = true;
                        }
                        
                        update contactList;
                    }
                }
            }
        }
        
        //PageReference pg = new 
        PageReference pageRef = new PageReference('/one/one.app#/sObject/'+ taskId + '/view');
        pageRef.setRedirect(true);
       	return pageRef; 
        
    }
}