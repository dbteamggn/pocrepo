@isTest
public class ContactTriggerHandler_Test {

    static void createCS(){
        Busines_Line_Mapping__c testCS = new Busines_Line_Mapping__c(Name = 'Retail', Field_Name__c = 'UK_Retail__c');
        insert testCS;
    }
    
    static testMethod void RetailUKTest(){
        User testUser = CommonTestUtils.createTestUser();
        testUser.Business_Line__c = 'Retail';
        update testUser;
        Id runningUserId = UserInfo.getUserId();
        Test.startTest();
            system.runAs(testUser){
                createCS();
                Object_Method_ByPass__c obByPass = new Object_Method_ByPass__c(Name = 'test', Object_Name__c = 'Client_Territory__c', User_Profile_ID__c = testUser.Id, Full_Bypass__c = true);
                insert obByPass;
                Region__c newRegion = new Region__c(Name = '61D', Contact_Owner__c = runningUserId, Business_Line__c = System.Label.RetailLabel);
                insert newRegion;
                Account acc = new Account(Name = 'New Account', Billing_PostalCode__c = '5005050', Status__c = 'Active', Region__c = 'EMEA', Business_Line__c = 'Retail', Hierarchy_Level__c = 'L0');
                insert acc;
                Client_Territory__c newCT = new Client_Territory__c(Client_Third_Party__c = acc.Id, Territory__c = newRegion.Id);
                insert newCT;
                
                Contact cnt = new Contact(LastName = 'Test Contact', AccountId = acc.Id,status__c='Active');
                insert cnt;
                
                List<Contact> results = [select OwnerId from Contact where Id = :cnt.Id];                    
                
                System.assertEquals(runningUserId, results[0].OwnerId);
                
                Account acc1 = new Account(Name = 'New Account1', Region__c = 'EMEA', Status__c = 'Active', Billing_PostalCode__c = '5105050', Business_Line__c = 'Retail', Hierarchy_Level__c = 'L0');
                insert acc1;
                AccountContactRelation acr = new AccountContactRelation(AccountId = acc1.id, ContactId = cnt.Id, Previous_Employee__c = true);
                insert acr;
                cnt.AccountId = acc1.Id;
                update cnt;
                
                delete cnt;
                undelete cnt;
            }
        Test.stopTest();
    }
}