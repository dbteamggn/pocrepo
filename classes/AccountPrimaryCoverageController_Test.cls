@isTest
public class AccountPrimaryCoverageController_Test{

 //Still in progress
 
 static testMethod void CreateAccount(){
        User user = CommonTestUtils.createTestUser();    
        List<Account> listOfAccount = new List<Account>();
        Account acc = CommonTestUtils.CreateTestClient('Client');
        acc.Primary_coverage_Member__c = user.Id;
        insert acc;
        
        List<AccountTeamMember> atList = new List<AccountTeamMember>();
        List<AccountShare> asList = new List<AccountShare>();
        
        AccountTeamMember accTM = new AccountTeamMember();
        accTM.UserId = user.Id;
        accTM.AccountAccessLevel = 'Edit';
        accTM.AccountId = acc.Id;
        if(acc.RecordTypeId ==  Account.sObjectType.getDescribe().getRecordTypeInfosByName().get('Client').getRecordTypeId()){
             accTM.TeamMemberRole = 'Senior Sales';
        }
        else if(acc.RecordTypeId ==  Account.sObjectType.getDescribe().getRecordTypeInfosByName().get('Third Party').getRecordTypeId()){ 
            accTM.TeamMemberRole = 'Consultant Relations';
        }
        atList.add(accTM);
                
        AccountShare accs = new AccountShare();
        accs.AccountId = acc.Id; 
        accs.AccountAccessLevel = 'Edit';
        accs.OpportunityAccessLevel='None';
        accs.CaseAccessLevel='None';
        accs.UserOrGroupId=user.Id;
        asList.add(accs);
            
        if(!atList.isEmpty()) insert atList;
        if(!asList.isEmpty()) insert asList;
        
        //Check the account team members are retrieved for the input account id
        List<AccountPrimaryCoverageController.AccountTMResult> AccTMs = AccountPrimaryCoverageController.findAll(String.valueOf(acc.Id));
        system.assert(AccTMs.size()>0);
        
        //Check the account is updated with the primary coverage member
        String errMsg = '';
        String stat = AccountPrimaryCoverageController.updatePrimary(acc.Id, user.Id, errMsg);
        system.assert(stat=='Success');
        
        errMsg = 'FIELD_CUSTOM_VALIDATION_EXCEPTION, validation error occurred:';
        stat = AccountPrimaryCoverageController.updatePrimary(acc.Id, user.Id, errMsg);
        system.assert(stat=='validation error occurred');
        
        errMsg = 'INSUFFICIENT_ACCESS_OR_READONLY, insufficient access:';
        stat = AccountPrimaryCoverageController.updatePrimary(acc.Id, user.Id, errMsg);
        system.assert(stat=='insufficient access');
        
        errMsg = 'UNABLE_TO_LOCK_ROW, Unable to lock row:';
        stat = AccountPrimaryCoverageController.updatePrimary(acc.Id, user.Id, errMsg);
        system.assert(stat=='Unable to lock row');
        
        errMsg = 'other errors';
        stat = AccountPrimaryCoverageController.updatePrimary(acc.Id, user.Id, errMsg);
        system.assert(stat=='other errors');
        
 }
}