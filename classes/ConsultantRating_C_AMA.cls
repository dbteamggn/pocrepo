public with sharing class ConsultantRating_C_AMA {
    
   
   
    /*
     * Description : RatedStrategyMap = Map with key of third party id + ':' + strategy id & value of they current rating.
     *               Only strategies/third parties that the user is interested in are returned
    */  
    @AuraEnabled  
    public  map<string, string> RatedStrategyMap { get;set;}
    @AuraEnabled  
    public  List<account> thirdParties { get;set;}
    @AuraEnabled  
    public  List<Investment_Strategy__c> strategies { get;set;} 
    /*
     * Description : Constructor
    */  
    public ConsultantRating_C_AMA(){
    }
    
    
    /*
     * Description : Get the third parties of the Client
     * Param : 
     * Returns :  list of Accounts.
    */ 
    @AuraEnabled    
    public static list<Account> GetThirdParties(String accId){
        return null;
    }
    
    
    /*
     * Description : Get the third parties that the user is interested in
     * Param : 
     * Returns :  list of Products.
    */ 
    @AuraEnabled    
    public static ConsultantRating_C_AMA getConRatingData(String accId){
        System.debug(accId);
        System.debug('accId');
       	ConsultantRating_C_AMA objController = new ConsultantRating_C_AMA(); 
        Set<ID> setTPId = new Set<ID>();
        Set<ID> setIS = new Set<ID>();
        for(Client_Third_Party_Relationship__c  objTP:[SELECT  Third_Party_Name__c FROM Client_Third_Party_Relationship__c where client_name__c = :accId] )
        {
            setTPId.add(objTP.Third_Party_Name__c);
        }
        objController.thirdParties = [select id, name from account where id in :setTPId order by name];
        
         for(Rated_Strategy__c  objRS:[select  Investment_Strategy__c FROM Rated_Strategy__c where Third_Party__c in :setTPId] )
        {
            setIS.add(objRS.Investment_Strategy__c);
        }
        objController.strategies =  [select id, name from Investment_Strategy__c where id in : setIS order by name];
		
		 if (objController.RatedStrategyMap == null){
                objController.RatedStrategyMap = new map<string, string>();
                for(rated_strategy__c ratedStrategy : [select id, Investment_strategy__c, third_party__c, current_Rating__c from rated_strategy__c 
                                                       where Investment_strategy__c In :setIS AND third_party__c In :setTPId]){
                    objController.RatedStrategyMap.put(ratedStrategy.third_party__c + ':' + ratedStrategy.Investment_strategy__c, ratedStrategy.current_Rating__c);                                                           
                }
            }
             return objController;
        } 
      
    
    /*
     * Description : Get map of third party + ':' + strategy & their current rating.
     * Param : 
     * Returns :  Map
    */ 
    @AuraEnabled
    public static Map<String,String> GetStrategyRating(){  
        System.debug('RatedStrategyMap');
       
        return null;
    }
    
    
    
    
}