public class CampaignTeamTriggerHandler extends TriggerHandler {
    protected override void beforeInsert(){
         if((triggerParams.objByPassMap.containsKey('CampaignTeam_Utility.postCampaignUpdates') && !triggerParams.objByPassMap.get(' CampaignTeam_Utility.postCampaignUpdates')) || 
                !triggerParams.objByPassMap.containsKey('CampaignTeam_Utility.postCampaignUpdates')){
         CampaignTeam_Utility.postCampaignUpdates(triggerParams);        
                }
    }
     protected override void beforeUpdate(){
          if((triggerParams.objByPassMap.containsKey('CampaignTeam_Utility.postCampaignUpdates') && !triggerParams.objByPassMap.get(' CampaignTeam_Utility.postCampaignUpdates')) || 
                !triggerParams.objByPassMap.containsKey('CampaignTeam_Utility.postCampaignUpdates')){
         CampaignTeam_Utility.postCampaignUpdates(triggerParams);        
                }
    }
}