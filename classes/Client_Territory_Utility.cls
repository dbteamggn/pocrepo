public class Client_Territory_Utility {
    
    /*
     * Description : Checks if the assigned Territory is Retail and updates in the client details
     * Param : Trigger Parameter List
     * Returns :  
     */
    public static void addClientTerritory(TriggerHandler.TriggerParameter tParam) {
        List < Client_Territory__c > clientTerritory = new List < Client_Territory__c > ();
        clientTerritory = tParam.newList;
        Set < Id > TerritoryIDSet = new Set < Id > ();
        Set < Id > ClientIDSet = new Set < Id > ();
        Map < Id, Account > modClients = new Map < Id, Account > ();
        Map < Id, Set < Id >> clientTerritoryMap = new Map < Id, Set < Id >> ();
        Map < id, Region__c > territoryMap;
        Account acc;
        Region__c territoryRec;
        DateTime territoryLastModified;

        //creating map of client id and set of related territoty ids
        for (Client_Territory__c cltTer: clientTerritory) {
            TerritoryIDSet.add(cltTer.Territory__c);
            ClientIDSet.add(cltTer.Client_Third_Party__c);
        }

        List < Client_Territory__c > tempClientTerr = [select id, Territory__c, Client_Third_Party__c from Client_Territory__c where Client_Third_Party__c in: ClientIDSet];

        for (Client_Territory__c cltTer: tempClientTerr) {
            TerritoryIDSet.add(cltTer.Territory__c);
            if (!clientTerritoryMap.isEmpty() && clientTerritoryMap.containsKey(cltTer.Client_Third_Party__c)) {
                clientTerritoryMap.get(cltTer.Client_Third_Party__c).add(cltTer.Territory__c);
            } else {
                clientTerritoryMap.put(cltTer.Client_Third_Party__c, new Set < id > {
                    cltTer.Territory__c
                });
            }
        }


        // Looping through Territories and assigning the corresponding name and id to client details. In case of multiple territories, selecting the latest one.
        territoryMap = new Map < id, Region__c > ([Select Id, Name, Business_Line__c, LastModifiedDate from Region__c where Id IN: TerritoryIDSet]);
        for (Id accId: clientTerritoryMap.keySet()) {
            acc = new Account();
            territoryLastModified = null;
            for (Id territoryId: clientTerritoryMap.get(accId)) {
                territoryRec = territoryMap.get(territoryId);
                if ((territoryRec.Business_Line__c == Label.RetailLabel && territoryLastModified == null) ||
                    (territoryRec.Business_Line__c == Label.RetailLabel && territoryLastModified != null && territoryRec.LastModifiedDate > territoryLastModified)) {
                    acc.id = accId;
                    acc.Territory__c = territoryRec.Id;
                    acc.Territory_Code__c = territoryRec.Name;
                    territoryLastModified = territoryRec.LastModifiedDate;
                    modClients.put(acc.Id, acc);
                }
                /* Yash- Part of APAC release */
                if (Label.APAC_Terr_Business_Line.contains(territoryRec.Business_Line__c)) {
                    if (Label.APAC_Terr_Region_Code.contains(territoryRec.Name)) {
                    
                        if(modClients.containsKey(accId)){
                            acc = modclients.get(accId);
                      }else{
                            acc.id = accId;
                        }
                        
                        if (acc.APAC_Territory_Code__c != null)
                            acc.APAC_Territory_Code__c = territoryRec.Name + ' ; ' + acc.APAC_Territory_Code__c;
                        else
                            acc.APAC_Territory_Code__c = territoryRec.Name + ' ; ';
                         
                        acc.APAC_Territory_Code__c = acc.APAC_Territory_Code__c.removeEnd(' ').removeEnd(';');
                        modClients.put(acc.Id, acc);
                    }
                }
                /* Joshna - 17 Nov.. commented out this section as we shouldn't set these values to NULL everytime we find a non Retail territory
                   Also updating modclients to map from list to avoid duplicate items in list issue
                 else{
                    acc.Territory__c = NULL;
                    acc.Territory_Code__c = NULL;
                }*/
            }
        }
        try{
            if(!modClients.isEmpty()){        
                update modClients.values();
            }
        } catch(Exception ex){ 
                    CommonUtilities.createExceptionLog(ex);
                    String stat = CommonUtilities.getUserFriendlyMessage(ex);
                    if (stat != ex.getMessage()){
                        for (Client_Territory__c cltTer: clientTerritory) {
                            cltTer.addError(stat);
                        }
                    }           
        }
    }   
    
    
    /* Description : Checks the assigned Territory, if it is changed from Retail, updates in the client details
     * Param : Trigger Parameter List
     * Returns :  
     */
    public static void removeClientTerritory(TriggerHandler.TriggerParameter tParam) {
        List < Client_Territory__c > clientTerritory = new List < Client_Territory__c > ();
        clientTerritory = tParam.oldList;
        List < Id > clientID = new List < Id > ();
        List < Id > TerritoryID = new List < Id > ();
        List < Id > ClientTerritoryID = new List < Id > ();
        Map < Id, Account > modClients = new Map < Id, Account > ();
        Map < ID, Set < ID >> territoryClientMap = new Map < ID, Set < ID >> ();
        Map < Id, Set < string >> clientTerritoryMap = new Map < Id, Set < string >> ();
        for (Client_Territory__c cltTer: clientTerritory) {
            clientID.add(cltTer.Client_Third_Party__c);
            TerritoryID.add(cltTer.Territory__c);
            ClientTerritoryID.add(cltTer.id);
            if (territoryClientMap.containsKey(cltTer.Territory__c)) {
                territoryClientMap.get(cltTer.Territory__c).add(cltTer.Client_Third_Party__c);
            } else {
                territoryClientMap.put(cltTer.Territory__c, new Set < Id > {
                    cltTer.Client_Third_Party__c
                });
            }
        }

        List < Client_Territory__c > territoryClientList = [select id, Client_Third_Party__c, Territory__c, Territory__r.Business_Line__c, Territory__r.Name,
                                                                Client_Third_Party__r.APAC_Territory_Code__c from Client_Territory__c
                                                                where Client_Third_Party__c in: clientID AND Id NOT IN: ClientTerritoryID
                                                            ];
       
        Account accToBeUpdated;
        for (Client_Territory__c territoryRec: territoryClientList) {
            if (Label.APAC_Terr_Business_Line.contains(territoryRec.Territory__r.Business_Line__c)&&(Label.APAC_Terr_Region_Code.contains(territoryRec.Territory__r.Name))) {                    
                    if (modClients.containsKey(territoryRec.Client_Third_Party__c)) {
                        accToBeUpdated = modClients.get(territoryRec.Client_Third_Party__c);
                    } else {
                        accToBeUpdated = new Account(Id = territoryRec.Client_Third_Party__c);
                    }
                    if (accToBeUpdated.APAC_Territory_Code__c != null)
                        accToBeUpdated.APAC_Territory_Code__c = territoryRec.Territory__r.Name + ' ; ' + accToBeUpdated.APAC_Territory_Code__c;
                    else
                        accToBeUpdated.APAC_Territory_Code__c = territoryRec.Territory__r.Name + ' ; ';
                    accToBeUpdated.APAC_Territory_Code__c = accToBeUpdated.APAC_Territory_Code__c.removeEnd(' ').removeEnd(';');
                    modClients.put(accToBeUpdated.Id, accToBeUpdated);
            }else{
                if (modClients.containsKey(territoryRec.Client_Third_Party__c)) {
                    accToBeUpdated = modClients.get(territoryRec.Client_Third_Party__c);
                } else {
                    accToBeUpdated = new Account(Id = territoryRec.Client_Third_Party__c);
                }
                accToBeUpdated.APAC_Territory_Code__c = '';
                modClients.put(accToBeUpdated.Id, accToBeUpdated);
            }
        }
        
        if(territoryClientList.isEmpty()){
             territoryClientList = [select id, Client_Third_Party__c, Territory__c, Territory__r.Business_Line__c, Territory__r.Name,
                                                                Client_Third_Party__r.APAC_Territory_Code__c from Client_Territory__c
                                                                where Id IN: ClientTerritoryID
                                                            ]; 
             for (Client_Territory__c territoryRec: territoryClientList) {
                if (Label.APAC_Terr_Business_Line.contains(territoryRec.Territory__r.Business_Line__c)) {
                    if (Label.APAC_Terr_Region_Code.contains(territoryRec.Territory__r.Name)) {
                        if (modClients.containsKey(territoryRec.Client_Third_Party__c)) {
                            accToBeUpdated = modClients.get(territoryRec.Client_Third_Party__c);
                        } else {
                            accToBeUpdated = new Account(Id = territoryRec.Client_Third_Party__c);
                        }
                        accToBeUpdated.APAC_Territory_Code__c = '';    
                        modClients.put(accToBeUpdated.Id, accToBeUpdated);
                    }
                }
            }                                                 
        }                                                    
        
        //Fetch Details of the Corresponding Client and Territory
        Map < ID, Account > retClient = new Map < ID, Account > ([Select Id, Territory__c, Territory_Code__c from Account where Id IN: clientID]);
        // Looping through Territories belonging to Retail and removing it from client details after junction object record being deleted
        for (Region__c reg: [Select Id, Name from Region__c where Id IN: TerritoryID and Business_Line__c =: Label.RetailLabel]) {
            if (territoryClientMap.containsKey(reg.Id)) {
                for (Id each: territoryClientMap.get(reg.Id)) {
                    if (modClients.containsKey(each)) {
                        accToBeUpdated = modClients.get(each);
                    } else {
                        accToBeUpdated = new Account(Id = each);
                    }
                    accToBeUpdated.Territory__c = NULL;
                    accToBeUpdated.Territory_Code__c = NULL;
                    modClients.put(accToBeUpdated.Id, accToBeUpdated);
                }
            }
        }
        try{
            if (!modClients.isEmpty()) {
                update modClients.values();
            }
        } catch(Exception ex){ 
                    CommonUtilities.createExceptionLog(ex);
                    String stat = CommonUtilities.getUserFriendlyMessage(ex);
                    if (stat != ex.getMessage()){
                        for (Client_Territory__c cltTer: clientTerritory) {
                            cltTer.addError(stat);
                        }
                    }           
        }
    }

    /* (DE7696)
     * Check that a maximum of 1 Retail territories are assigned per client
     */
    public static void checkExistingUKTerritory(TriggerHandler.TriggerParameter tParam) {
        // Get all UK regions
        map < id, region__c > regionMap = new map < id, region__c > ([select id from region__c where business_line__c = 'Retail']);

        // Loop thru the trigger newList building a map of clientId, list<clientTerritories> that were created/updated
        List < sObject > clientTerritories = tParam.newList;
        map < id, list < Client_Territory__c >> clientToClientTerritoriesMap = new map < id, list < Client_Territory__c >> (); // Map = clientId, list<Client_Territory__c>   UK ONLY
        for (sObject sObj: clientTerritories) {
            Client_Territory__c cltTer = (Client_Territory__c) sObj;
            if (regionMap.containsKey(cltTer.Territory__c)) {
                list < Client_Territory__c > tempList;
                // If more than one UK region is assigned to the client in the newList, throw an error
                if (clientToClientTerritoriesMap.containsKey(cltTer.Client_Third_Party__c)) {
                    sObj.addError('A UK territory has already been added for this client.');
                } else {
                    tempList = new list < Client_Territory__c > ();
                    tempList.add(cltTer);
                    clientToClientTerritoriesMap.put(cltTer.Client_Third_Party__c, tempList);
                }

            }
        }

        // Now get all other client territories that are currently assigned to the clients that were in the NewList & have UK regions
        map < id, list < Client_Territory__c >> allRelatedClientTerritoryMap = new map < id, list < Client_Territory__c >> ();
        list < Client_Territory__c > allRelatedClientTerritories = [select id, Client_Third_Party__c, Territory__c from Client_Territory__c
            where Client_Third_Party__c =: clientToClientTerritoriesMap.keySet()
            and territory__r.business_line__c = 'Retail'
        ];

        // Build a map of those per client
        for (Client_Territory__c ct: allRelatedClientTerritories) {
            list < Client_Territory__c > tempList;
            if (allRelatedClientTerritoryMap.containsKey(ct.Client_Third_Party__c)) {
                tempList = allRelatedClientTerritoryMap.get(ct.Client_Third_Party__c);
            } else {
                tempList = new list < Client_Territory__c > ();
            }
            tempList.add(ct);
            allRelatedClientTerritoryMap.put(ct.Client_Third_Party__c, tempList);
        }

        // Loop thru the original newList & check that not more than one UK Region is assigned per client
        for (sObject sObj: clientTerritories) {
            Client_Territory__c cltTer = (Client_Territory__c) sObj;
            if (allRelatedClientTerritoryMap.containsKey(cltTer.Client_Third_Party__c)) {
                list < Client_Territory__c > tempList = allRelatedClientTerritoryMap.get(cltTer.Client_Third_Party__c);
                try {
                    if ((tParam.isUpdate && tempList[0].id != cltTer.id) || tParam.isInsert) {
                        sObj.addError('A UK territory has already been added for this client.');
                    }
                } catch (Exception ex) { // Should never happen as the SOQL is only selecting those client territories with UK regions.
                    CommonUtilities.createExceptionLog(ex);
                }
            }
        }
    }
        
    public static void checkExistingTerritory(TriggerHandler.TriggerParameter tParam) {
        List <Client_Territory__c> clientTerritories = (List<Client_Territory__c>) tParam.newList;
        Set<Id> clientIdSet = new Set<Id>();
        for(Client_Territory__c each: clientTerritories){
            clientIdSet.add(each.Client_Third_Party__c);
        }
        Map<Id, Set<Id>> clientTerrMap = new Map<Id, Set<Id>>();
        
        for(Client_Territory__c each: [select Id, Territory__c, Client_Third_Party__c from Client_Territory__c where Client_Third_Party__c IN :clientIdSet]){
            if(clientTerrMap.containsKey(each.Client_Third_Party__c)){
                clientTerrMap.get(each.Client_Third_Party__c).add(each.Territory__c);
            } else{
                clientTerrMap.put(each.Client_Third_Party__c, new Set<Id>{each.Territory__c});
            }
        }
        for(Client_Territory__c each: clientTerritories){
            if(clientTerrMap.containsKey(each.Client_Third_Party__c) && clientTerrMap.get(each.Client_Third_Party__c).contains(each.Territory__c)){
                each.addError('This client is already associated with the Territory. Please choose another territory');
            }
        }
    }
}