@isTest
public class editEventController_Test {
    /*
     * 
     * Description : edit an Event Record .
     * Param : event id
     * Returns : 
    */
    static testMethod void editEventTest(){
         User user = CommonTestUtils.createTestUser();        
        
        // Create a client that the contacts will belong to
        Account acc = CommonTestUtils.CreateTestClient('Contact Tests');        
        insert acc;
        
        // Now create contacts for a client with last viewed Date
        List<contact> contacts = new List<contact>();       
        for(integer i = 0; i<10; i++ ){
            Contact c = CommonTestUtils.CreateTestContact('Contact ' + i);
            c.accountid = acc.id;
            contacts.add(c);                    
        }
        insert contacts;
        
        
        event e=new event();
        e.Whoid=contacts[0].id;
        e.startDateTime=system.now();
        e.endDateTime=system.now().addHours(1);
        insert e;
        
        eventRelation er=new eventRelation();
        er.RelationId=contacts[1].id;
        er.EventId=e.id;
        insert er;
        
        System.runAs(user){
            Test.startTest();
            editEventController.getEventDetails(e.id);
            Test.stopTest();            
        }
    }
    /*
     * 
     * Description : edit an Event Record .
     * Param : event id
     * Returns : 
    */
    static testMethod void editEventFandSTest(){
         User user = CommonTestUtils.createTestUser();        
        
        // Create a client that the contacts will belong to
        Account acc = CommonTestUtils.CreateTestClient('Contact Tests');        
        insert acc;
        
        // Now create contacts for a client with last viewed Date
        List<contact> contacts = new List<contact>();       
        for(integer i = 0; i<10; i++ ){
            Contact c = CommonTestUtils.CreateTestContact('Contact ' + i);
            c.accountid = acc.id;
            contacts.add(c);                    
        }
        insert contacts;
        
        Activity_alias__c aa=new Activity_alias__c();
        insert aa;
        
        Activity_alias__c aa1=new Activity_alias__c();
        insert aa1;
        
        Product2 p=new Product2();
        p.Name='test product';
        insert p;
        Investment_Strategy__c is=new Investment_Strategy__c();
        is.Name='Test Investment';
        insert is;
        
        Activity_Product_Relationship__c apr=new Activity_Product_Relationship__c();
        apr.Activity_alias__c=aa.id;
        apr.Fund__c=p.id;
        apr.Investment_Strategy__c=is.id;
        insert apr;
        
        Activity_Product_Relationship__c apr1=new Activity_Product_Relationship__c();
        apr1.Activity_alias__c=aa1.id;
        apr1.Investment_Strategy__c=is.id;
        insert apr1;
        
        event e=new event();
        e.Whoid=contacts[0].id;
        e.startDateTime=system.now();
        e.Strategies_Funds__c=aa.id;
        e.endDateTime=system.now().addHours(1);
        insert e;
        
        event e1=new event();
        e1.Whoid=contacts[0].id;
        e1.startDateTime=system.now();
        e1.Strategies_Funds__c=aa1.id;
        e1.endDateTime=system.now().addHours(1);
        insert e1;
        
        eventRelation er=new eventRelation();
        er.RelationId=contacts[1].id;
        er.EventId=e.id;
        insert er;
        
        System.runAs(user){
            Test.startTest();
            editEventController.getEventDetailsFundsAndStrategies(e.id);
            editEventController.getEventDetailsFundsAndStrategies(e1.id);
            Test.stopTest();            
        }
    }
    static testMethod void updateEvent(){
         User user = CommonTestUtils.createTestUser();        
        
        // Create a client that the contacts will belong to
        Account acc = CommonTestUtils.CreateTestClient('Contact Tests');        
        insert acc;
        
        // Now create contacts for a client with last viewed Date
        List<contact> contacts = new List<contact>();       
        for(integer i = 0; i<10; i++ ){
            Contact c = CommonTestUtils.CreateTestContact('Contact ' + i);
            c.accountid = acc.id;
            contacts.add(c);                    
        }
        insert contacts;
        
        List<id> contactIds=new List<id>();
        for(contact temp:contacts){
            contactIds.add(temp.id);
        }
        
        Activity_alias__c aa=new Activity_alias__c();
        insert aa;
        
        Product2 p=new Product2();
        p.Name='test product';
        insert p;
        Investment_Strategy__c is=new Investment_Strategy__c();
        is.Name='Test Investment';
        insert is;
        
        Activity_Product_Relationship__c apr=new Activity_Product_Relationship__c();
        apr.Activity_alias__c=aa.id;
        apr.Fund__c=p.id;
        apr.Investment_Strategy__c=is.id;
        insert apr;
        
        
        
        event e=new event();
        e.Whoid=contacts[0].id;
        e.startDateTime=system.now();
        e.Strategies_Funds__c=aa.id;
        e.endDateTime=system.now().addHours(1);
        insert e;
        
        eventRelation er=new eventRelation();
        er.RelationId=contacts[1].id;
        er.EventId=e.id;
        insert er;
        
        System.runAs(user){
            Test.startTest();
            editEventController.updateEvent(contactIds,e.id);
            Test.stopTest();            
        }
    }
    static testMethod void updateEventFandS(){
         User user = CommonTestUtils.createTestUser();        
        
        // Create a client that the contacts will belong to
        Account acc = CommonTestUtils.CreateTestClient('Contact Tests');        
        insert acc;
        
        // Now create contacts for a client with last viewed Date
        List<contact> contacts = new List<contact>();       
        for(integer i = 0; i<10; i++ ){
            Contact c = CommonTestUtils.CreateTestContact('Contact ' + i);
            c.accountid = acc.id;
            contacts.add(c);                    
        }
        insert contacts;
        
        Activity_alias__c aa=new Activity_alias__c();
        insert aa;
        
        Product2 p=new Product2();
        p.Name='test product';
        insert p;
        Investment_Strategy__c is=new Investment_Strategy__c();
        is.Name='Test Investment';
        insert is;
        
        List<id> fs=new List<id>();
        fs.add(p.id);
        fs.add(is.id);
        
        Activity_Product_Relationship__c apr=new Activity_Product_Relationship__c();
        apr.Activity_alias__c=aa.id;
        apr.Fund__c=p.id;
        apr.Investment_Strategy__c=is.id;
        insert apr;
        
        
        
        event e=new event();
        e.Whoid=contacts[0].id;
        e.startDateTime=system.now();
        e.Strategies_Funds__c=aa.id;
        e.endDateTime=system.now().addHours(1);
        insert e;
        
        event e1=new event();
        e1.Whoid=contacts[0].id;
        e1.startDateTime=system.now();
        e1.endDateTime=system.now().addHours(1);
        insert e1;
        
        eventRelation er=new eventRelation();
        er.RelationId=contacts[1].id;
        er.EventId=e.id;
        insert er;
        
        System.runAs(user){
            Test.startTest();
            editEventController.updateEventFundsAndStrategies(fs,e.id);
            editEventController.updateEventFundsAndStrategies(fs,e1.id);
            Test.stopTest();            
        }
    }
}