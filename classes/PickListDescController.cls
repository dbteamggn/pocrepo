/*
Copyright (c) 2012 tgerm.com
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, 
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
    Visualforce controller for the page that renders the XML output for the picklist options
    @author Abhinav 
*/
public class PickListDescController {
    public Sobject sobj {get;set;}
    public String pickListFieldName {get;set;}   
    public String ParpickListFieldName {get;set;}
    public String ParpickListFieldValue {get;set;}      
    
    public PickListDescController() {
        Map<String, String> reqParams = ApexPages.currentPage().getParameters();
        String sobjId = reqParams.get('id');
        String recordTypeId = reqParams.get('recordTypeId');
        String recordTypeName = reqParams.get('recordTypeName');
        String sobjectTypeName = reqParams.get('sobjectType'); 
        this.pickListFieldName = reqParams.get('picklistFieldName'); 
        this.ParpickListFieldName = reqParams.get('ParpickListFieldName');
        this.ParpickListFieldValue = reqParams.get('ParpickListFieldValue');
        
        Schema.SobjectType sobjectType = null;
        
        if (sobjectTypeName != null && sobjectTypeName.trim().length() > 0) {
            // find the so
            sobjectType = Schema.getGlobalDescribe().get(sobjectTypeName);
            // following not working with input:field
            //  sobj = sobjectType.newSobject(sobjId);
          
            sobj = sobjectType.newSobject();
            if(ParpickListFieldName != null){
                sobj.put(ParpickListFieldName, ParpickListFieldValue);
            }    
            if (recordTypeName != null && recordTypeName.trim().length() > 0) {
            // if no recordTypeId passed explicitly by user, try loading one from the RecordType table
             RecordType recType = [Select Id from RecordType Where SobjectType =:sobjectTypeName 
                                            AND DeveloperName like :recordTypeName];
             sobj.put('RecordTypeId', recType.id);                                           
            }
       } 
       else if (sobjId != null && sobjId.trim().length() > 0) {
             // find the so
            sobjectType = Id.valueOf(sobjId).getSObjectType();
        
            sobj = sobjectType.newSobject();
            if(ParpickListFieldName != null){
                sobj.put(ParpickListFieldName, ParpickListFieldValue);
            }
            if (recordTypeName != null && recordTypeName.trim().length() > 0) {
            // if no recordTypeId passed explicitly by user, try loading one from the RecordType table
             RecordType recType = [Select Id from RecordType Where SobjectType =:String.valueof(sobjectType)
                                            AND DeveloperName like :recordTypeName];
             sobj.put('RecordTypeId', recType.id);                                           
            }
        }   
            
    }
   
}