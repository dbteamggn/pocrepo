@isTest
public class BP_UKRetailFlowCtrl_Test{
    static testMethod void myUnitTestMethod(){
        List<String> allVals = BP_UKRetailFlowCtrl.getPicklistValues('business_plan__c', 'business_plan_duration__c');
        System.assertEquals(4, allVals.size());
        
        Region__c newRegion = new Region__c(Name = '61');
        insert newRegion;
        
        Business_Plan__c bpRec = new Business_Plan__c(BP_Territory__c = newRegion.Id, OwnerId = UserInfo.getUserId(), Current__c = TRUE);
        insert bpRec;
        Account acc = new Account(Name = 'L0 Client', Billing_PostalCode__c = '50505', Hierarchy_Level__c = 'L0', Region__c = 'EMEA', Business_Line__c = 'Retail', Status__c = 'Active');
        insert acc;
        Contact cnt = new Contact(AccountId = acc.Id, LastName = 'Test Contact',Status__c='Active');
        insert cnt;
        Business_Plan_Client_Relation__c bpc = new Business_Plan_Client_Relation__c(Business_Plan__c = bpRec.Id, Client__c = acc.Id);
        insert bpc;
        
        BP_UKRetailFlowCtrl.createNewPlan();
        
        Business_Plan__c bpRec1 = BP_UKRetailFlowCtrl.getBPRecord(bpRec.id, NULL, true);
        System.assertEquals(bpRec.Id, bpRec1.Id);
        bpRec1 = BP_UKRetailFlowCtrl.getBPRecord(NULL,'61', true);
        System.assertEquals(bpRec.Id, bpRec1.Id);
        bpRec1 = BP_UKRetailFlowCtrl.getBPRecord(NULL, NULL, true);
        System.assertEquals(bpRec.Id, bpRec1.Id);
        bpRec1 = BP_UKRetailFlowCtrl.getBPRecord(NULL, NULL, false);
        
        BP_UKRetailFlowCtrl.getRegions();
        BP_UKRetailFlowCtrl.updateBP(bpRec.Id, 'Test', '1 year');
        BP_UKRetailFlowCtrl.updateBP(NULL, 'Test', '1 year');
        
        BP_UKRetailFlowCtrl.getCurrentBatchStatus(bpRec.Id);
        BP_UKRetailFlowCtrl.getUsers();
        BP_UKRetailFlowCtrl.getLookupValues(bpRec.Id);
        BP_UKRetailFlowCtrl.isUKRetailUser();
        BP_UKRetailFlowCtrl.getWhereClause();
        
        User testUser = CommonTestUtils.createTestUser();
        testUser.FirstName = 'NamewithLetterA';
        testUser.Business_Line__c = System.Label.RetailLabel;
        update testUser;
        
        system.runAs(testUser){
            BP_UKRetailFlowCtrl.isUKRetailUser();
            BP_UKRetailFlowCtrl.findAll();
        }
        
        BP_UKRetailFlowCtrl.findByName('A');
        BP_UKRetailFlowCtrl.findByListId(new List<Id>{testUser.Id});
        BP_UKRetailFlowCtrl.findAll();
        
        List<BP_UKRetailFlowCtrl.FundWrapper> fundList = new List<BP_UKRetailFlowCtrl.FundWrapper>();
        Product2 product = new Product2(Name = 'Test Product');
        insert product;
        Investment_Strategy__c strategy = new Investment_Strategy__c(Name = 'Test Strategy');
        insert strategy;
        BP_UKRetailFlowCtrl.FundWrapper fundOne = new BP_UKRetailFlowCtrl.FundWrapper();
        fundOne.Id = product.Id;
        fundOne.Name = product.Name;
        fundList.add(fundOne);
        fundList.add(new BP_UKRetailFlowCtrl.FundWrapper(strategy.Id, strategy.Name));
        String actionStr = JSON.serialize(new Business_Plan_Actions__c(Name = 'Test Action', Business_Plan__c = bpRec.Id, Business_Plan_Client__c = bpc.Id));
       
        BP_UKRetailFlowCtrl.createEvent(CommonTestUtils.EventJsonData(new List<Contact>{cnt}));
        
        String fundStr = JSON.serialize(fundList);
        Id actionId = BP_UKRetailFlowCtrl.createBPAction(actionStr, fundStr);
        LightningCommonUtilities.getLoggedInUserId();
        System.assertEquals(BP_UKRetailFlowCtrl.getActionName(actionId), 'Test Action');
    }
}