public class editEventController {
    @AuraEnabled
    public static List<Results> getEventDetails(id eventid){
        List<eventrelation> er=[select id ,relationId  from eventrelation where eventid=: eventid];
        List<Results> Contacts=new List<Results>();
        for(eventrelation temp : er){
            Results cr=new Results();
            cr.id=temp.relationId;
            Contacts.add(cr);
        }      
        System.debug('Contacts'+Contacts);
        return Contacts;
    }
    @AuraEnabled
    public static List<Results> getEventDetailsFundsAndStrategies(id eventid){
        List<Event> ev=[select id , Strategies_Funds__c from Event where id =: eventid];
        Id AliasId;
        if(ev.size()>0){
            AliasId=ev[0].Strategies_Funds__c;
        }         
        List<Activity_Product_Relationship__c> Funds=[select id ,Activity_Alias__c,fund__c,Investment_Strategy__c
                                                   from Activity_Product_Relationship__c 
                                                   where Activity_Alias__c=: AliasId];
        List<Results> FundStrat=new List<Results>();
        for(Activity_Product_Relationship__c temp : Funds){
            Results fs=new Results();
            if(temp.fund__c!=null)
                fs.id=temp.fund__c;
            else if(temp.Investment_Strategy__c!=null)
                fs.id=temp.Investment_Strategy__c;
            FundStrat.add(fs);
        }      
        System.debug('FundStrat'+FundStrat);
        return FundStrat;
    }
    @AuraEnabled
    public static id updateEvent(List<id> contactIds,id eventId){
        Set<id> tempContactIds = new Set<id>();
        tempContactIds.addAll(contactIds);
        List<eventrelation> er=[select id , relationId  from eventrelation where eventid=: eventid];   
        List<eventrelation> toDelete=new List<eventrelation>();
        List<eventrelation> toAdd=new List<eventrelation>();
        EventRelation erTemp;
        for(eventrelation temp : er){
            if(!tempContactIds.contains(temp.relationId)){
                toDelete.add(temp);
            }else{
                tempContactIds.remove(temp.relationId);
            }
        }
        for(id temp : tempContactIds){
            erTemp= new EventRelation();
            erTemp.RelationId=temp;
            erTemp.EventId=eventId;
            erTemp.isParent=true;
            toAdd.add(erTemp);
        }
        insert toAdd;
        delete toDelete;
        return eventId;
    }
    @AuraEnabled
    public static id updateEventFundsAndStrategies(List<id> FundStratIds,id eventId){
        List<Event> ev=[select id , Strategies_Funds__c,whoId from Event where id =: eventid];
        Id AliasId;
        if(ev.size()>0){
            AliasId=ev[0].Strategies_Funds__c;
            if(AliasId==null){
                Activity_alias__c alias = new Activity_alias__c(Activity_ID__c = ev[0].Id);
                alias.Contact__c=ev[0].whoid;
                insert alias;
                ev[0].Strategies_Funds__c=alias.id;
                update ev;
                AliasId=alias.id;
                System.debug('AliasId'+AliasId);
            }
        } 
        System.debug('AliasId'+AliasId);

        Set<id> tempFundIds = new Set<id>();
        Set<id> tempStratIds = new Set<id>();
        List<Activity_Product_Relationship__c> apr=[select id ,Activity_Alias__c,fund__c,Investment_Strategy__c
                                                    from Activity_Product_Relationship__c 
                                                    where Activity_Alias__c=: AliasId];   
        List<Activity_Product_Relationship__c> toDelete=new List<Activity_Product_Relationship__c>();
        List<Activity_Product_Relationship__c> toAdd=new List<Activity_Product_Relationship__c>();
        for(id tempid : FundStratIds){
            if(Id.valueOf(tempid ).getSObjectType() == Schema.Product2.SObjectType)
                tempFundIds.add(tempid);
            else if(Id.valueOf(tempid).getSObjectType() == Schema.Investment_Strategy__c.SObjectType)
                tempStratIds.add(tempid);
        }
               
        Activity_Product_Relationship__c aprTemp;
        for(Activity_Product_Relationship__c temp : apr){
            if(temp.fund__c!= null){
                if(!tempFundIds.contains(temp.fund__c)){
                    toDelete.add(temp);
                }else{
                    tempFundIds.remove(temp.fund__c);
                }
            }
            if(temp.Investment_Strategy__c!= null){
                if(!tempStratIds.contains(temp.Investment_Strategy__c)){
                    toDelete.add(temp);
                }else{
                    tempStratIds.remove(temp.Investment_Strategy__c);
                }
            }
        }
        
        System.debug('AliasId'+AliasId);
        for(id temp : tempFundIds){
            aprTemp= new Activity_Product_Relationship__c();
            aprTemp.fund__c=temp;
            aprTemp.Activity_Alias__c=AliasId;
            toAdd.add(aprTemp);
        }
         for(id temp : tempStratIds){
            aprTemp= new Activity_Product_Relationship__c();
            aprTemp.Investment_Strategy__c=temp;
            aprTemp.Activity_Alias__c=AliasId;
            toAdd.add(aprTemp);
        }
        insert toAdd;
        delete toDelete;
        return eventId;
    }
    public class Results {
        @AuraEnabled
        public id id;
        
        @AuraEnabled
        public string name;
    }
}