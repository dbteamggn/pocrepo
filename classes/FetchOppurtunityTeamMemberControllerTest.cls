/*----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            FetchOpportunityTeamMemberControllerTest
   Description:     Test Methods for FetchOpportunityTeamMemberController Class
                    
      Date               Author                         Summary of Changes                  
   -----------      -----------------                  -------------------------------------------------------------------------------------
   06 OCT 2016          Dilipkumar                        Initial Release
------------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
public class FetchOppurtunityTeamMemberControllerTest {
    /*
     * Description : Create new Clients, Associate Opportunity
     * Param : 
     * Returns :  Opportunity
    */
    public static Opportunity createData(){
        //client
        Account testClient= CommonTestUtils.CreateTestClient('ABC Corp');
        
        //users
        list<user> userList = new list<user>();
        userList.add(CommonTestUtils.createTestUser(false));
        userList.add(CommonTestUtils.createTestUser(false));
        userList[1].username = 'user2@Deloitte.test.com';
        insert userList;
        
        //opportunity
        Opportunity testOpportunity = CommonTestUtils.CreateTestOpportunityMandate('templateTest',testClient.Id,null,'Buy');        
        insert testOpportunity;
        
        //create opportunity team members
        String teamRole = 'Consultant Relations';
        List<OpportunityTeamMember> oppTeamMembers = new List<OpportunityTeamMember>(); 
        oppTeamMembers.add(new OpportunityTeamMember(TeamMemberRole = teamRole, OpportunityId = testOpportunity.Id, UserId = userList[0].Id));
        oppTeamMembers.add(new OpportunityTeamMember(TeamMemberRole = teamRole, OpportunityId = testOpportunity.Id, UserId = userList[1].Id));       
        insert oppTeamMembers;
        
        //custom setting
        list<Configuration_Util__c> customSettingList = new list<Configuration_Util__c>();
        customSettingList.add(new Configuration_Util__c(Name='ET Team Roles Opp Won NF & Finals', Value__c='Sales Team Member,Consultant Relations,Investment Team'));
        customSettingList.add(new Configuration_Util__c(Name='Email Template Team Roles', Value__c='Senior Sales,Consultant Relations,Product Manager'));       
        insert customSettingList;
        
        //Return the opportunity whose ID would be used
        return testOpportunity;
    }
   
   /*
     * Description : Test method for "ET Team Roles Opp Won NF & Finals" calling template
     * Param : 
     * Returns :  
    */
    public static testMethod void OppStageWonNFFinalsTest(){
        Opportunity testOpportunity = createData();
        FetchOpportunityTeamMemberController testControl = new FetchOpportunityTeamMemberController();
        String templateName = 'Opportunity_Stage_Won_NF_Finals';
         
        Test.startTest();
            testControl.callingTemplateName = templateName;
            testControl.setoppId(testOpportunity.Id);
            testControl.getOppTeamRoleMembersMap();
        Test.stopTest();
        System.assertEquals(testControl.getoppId(), testOpportunity.Id);
    }
    
    /*
     * Description : Test method for "Email Template Team Roles" calling template
     * Param : 
     * Returns :  
    */
    public static testMethod void OppStageWonTest(){
        Opportunity testOpportunity = createData();
        FetchOpportunityTeamMemberController testControl = new FetchOpportunityTeamMemberController();
        String templateName = 'Opportunity_Won_Notification';
         
        Test.startTest();
            testControl.callingTemplateName = templateName;
            testControl.setoppId(testOpportunity.Id);
            testControl.getOppTeamRoleMembersMap();
        Test.stopTest();
        System.assertEquals(testControl.getoppId(), testOpportunity.Id);
    }
}