public with sharing class ConsultantRatingsCtl {
    
    /*
     * Description : UserSetting = the existing user's configuration that has been set up for 
     *                             Strategies & Third Parties
    */  
    public static list<User_Setting__c> UserSettings {
        get {
            if (UserSettings == null){
                UserSettings = [select setting__c, recordtype.developerName from User_Setting__c
                                            where user__c = :UserInfo.getUserId()
                                           and (recordtype.developerName = 'Followed_Strategies'
                                                or recordtype.developerName = 'Followed_Third_Parties')];
            }
            return UserSettings;
        }
        private set;
    }
    
    /*
     * Description : ThirdPartyIdList = List of third party IDs that the user is interested in
    */  
    public static list<id> ThirdPartyIdList {
        get {
            if (ThirdPartyIdList == null) {
                ThirdPartyIdList = new list<id>();
                for (User_Setting__c userSetting : UserSettings){
                    if(userSetting.recordtype.developerName == 'Followed_Third_Parties'){
                        ThirdPartyIdList.add(Id.valueOf(userSetting.setting__c));    
                    }
                }
            }
            //US33170- Default Consultant Ratings Table Layout - START
            String thirdPartyType = 'Third_Party';
            Map<String, List<Consultant_Ratings_Default_Config__c>> defaultsMap = getConsultantRatingDefaults();
            List<User_Setting__c> usList = new List<User_Setting__c>();
            
            //if there are no records in the user setting for this user, get the default data from custom setting
            if((ThirdPartyIdList == null || ThirdPartyIdList.isEmpty()) && UserSettings.isEmpty() && !RecurssiveTriggerController.defaultConsultantRatingsRetrigger){
                Id userSettThirdPartyRecTypeId = Schema.SObjectType.User_Setting__c.getRecordTypeInfosByName().get('Followed Third Parties').getRecordTypeId() ;
                if(!defaultsMap.isEmpty() && defaultsMap.containsKey(thirdPartyType)){
                    for(Consultant_Ratings_Default_Config__c each : defaultsMap.get(thirdPartyType)){
                        ThirdPartyIdList.add(each.name);
                        usList.add(new User_Setting__c(user__c=UserInfo.getUserId(), setting__c=each.name, recordTypeId=userSettThirdPartyRecTypeId));
                    }
                    if(!usList.isEmpty()){
                        insert usList;
                    }
                }
            }           
            //US33170- Default Consultant Ratings Table Layout - END
            return ThirdPartyIdList;
        }
        private set;
    }
        
    
    /*
     * Description : StrategyIdList = List of strategy IDs that the user is interested in
    */  
    public static list<id> StrategyIdList {
        get {
            if (StrategyIdList == null) {
                StrategyIdList = new list<id>();
                for (User_Setting__c userSetting : UserSettings){
                    if(userSetting.recordtype.developerName == 'Followed_Strategies'){
                        StrategyIdList.add(Id.valueOf(userSetting.setting__c));    
                    }
                }
            }
            //US33170- Default Consultant Ratings Table Layout - START
            String invStrategyType = 'Investment_Strategy';
            Map<String, List<Consultant_Ratings_Default_Config__c>> defaultsMap = getConsultantRatingDefaults();
            List<User_Setting__c> usList = new List<User_Setting__c>();
            
            //if there are no records in the user setting for this user, get the default data from custom setting
            if((StrategyIdList == null || StrategyIdList.isEmpty()) && UserSettings.isEmpty() && !RecurssiveTriggerController.defaultConsultantRatingsRetrigger){
                Id userSettStrategyRecTypeId = Schema.SObjectType.User_Setting__c.getRecordTypeInfosByName().get('Followed Strategies').getRecordTypeId() ;
                if(!defaultsMap.isEmpty() && defaultsMap.containsKey(invStrategyType)){
                    for(Consultant_Ratings_Default_Config__c each : defaultsMap.get(invStrategyType)){
                        StrategyIdList.add(each.name);
                        usList.add(new User_Setting__c(user__c=UserInfo.getUserId(), setting__c=each.name, recordTypeId=userSettStrategyRecTypeId));
                    }
                    if(!usList.isEmpty()){
                        insert usList;
                    }
                }
            }
            //US33170- Default Consultant Ratings Table Layout - END
            return StrategyIdList;
        }
        private set;
    }
    
    /*
     * Description : ThirdPartyList = List of third parties that the user is interested in
    */  
    public static list<account> ThirdPartyList {
        get {
            if (ThirdPartyList == null){
                ThirdPartyList = [select id, name from account where id = :ThirdPartyIdList and recordtype.developerName='Third_Party' order by name];
            } 
            return ThirdPartyList;
        } 
        private set;
    }
    
    /*
     * Description : StrategyList = List of strategies that the user is interested in
    */  
    public static list<Investment_Strategy__c> StrategyList {
        get {
            if (StrategyList == null){
                StrategyList = [select id, name from Investment_Strategy__c where id = :StrategyIdList order by name];
            }
            return StrategyList;
        } 
        private set;
    }
    
    /*
     * Description : RatedStrategyMap = Map with key of third party id + ':' + strategy id & value of they current rating.
     *               Only strategies/third parties that the user is interested in are returned
    */  
    public static map<string, string> RatedStrategyMap {
        get {
            if (RatedStrategyMap == null){
                RatedStrategyMap = new map<string, string>();
                for(rated_strategy__c ratedStrategy : [select id, Investment_strategy__c, third_party__c, current_Rating__c from rated_strategy__c 
                                                       where Investment_strategy__c = :StrategyIdList or third_party__c = :ThirdPartyList]){
                    RatedStrategyMap.put(ratedStrategy.third_party__c + ':' + ratedStrategy.Investment_strategy__c, ratedStrategy.current_Rating__c);                                                           
                }
            }
            return RatedStrategyMap;
        } 
        private set;
    }    
    
    
    /*
     * Description : ThirdPartyWrapperList = List of third parties that the user is interested in combined with a checkbox
    */  
    public static list<ThirdPartyWrapper> ThirdPartyWrapperList {
        get {
            ThirdPartyWrapperList = new list<ThirdPartyWrapper>();
            for (Account ThirdPartyListTemp : [select id, name from account where id = :ThirdPartyIdList and recordtype.developerName='Third_Party' order by name]){
                ThirdPartyWrapper temp = new ThirdPartyWrapper();
                temp.checked = true;
                temp.acc = ThirdPartyListTemp;
                ThirdPartyWrapperList.add(temp);
            }           
            return ThirdPartyWrapperList;
        } 
        private set;
    }     
    
    
    /*
     * Description : ThirdPartyWrapperList = List of strategies that the user is interested in combined with a checkbox
    */  
    public static list<StrategyWrapper> StrategyWrapperList {
        get {
            StrategyWrapperList = new list<StrategyWrapper>();
            for (Investment_Strategy__c strategyListTemp : [select id, name from Investment_Strategy__c where id = :StrategyIdList order by name]){
                StrategyWrapper temp = new StrategyWrapper();
                temp.checked = true;
                temp.investmentStrategy = strategyListTemp;
                StrategyWrapperList.add(temp);
            }            
            return StrategyWrapperList;
        } 
        private set;
    } 
    
    
    /*
     * Description : Constructor
    */  
    public ConsultantRatingsCtl(){
    }
    
    
    /*
     * Description : Get the third parties that the user is interested in
     * Param : 
     * Returns :  list of Accounts.
    */ 
    @AuraEnabled    
    public static list<Account> GetThirdParties(){
        return ConsultantRatingsCtl.ThirdPartyList;
    }
    
    
    /*
     * Description : Get the third parties that the user is interested in
     * Param : 
     * Returns :  list of Products.
    */ 
    @AuraEnabled    
    public static list<Investment_Strategy__c> GetStrategies(){
        return ConsultantRatingsCtl.StrategyList;
    }
    
    /*
     * Description : Get map of third party + ':' + strategy & their current rating.
     * Param : 
     * Returns :  Map
    */ 
    @AuraEnabled
    public static Map<String,String> GetStrategyRating(){       
        return ConsultantRatingsCtl.RatedStrategyMap;
    }
    
    // Get a list of the current user's chosen third parties
    @AuraEnabled
    public static list<ThirdPartyWrapper> GetThirdPartyWrappers(){       
        return ConsultantRatingsCtl.ThirdPartyWrapperList; 
    }
    
    // Get a list of the current user's chosen strategies
    @AuraEnabled
    public static list<StrategyWrapper> GetStrategyWrappers(){       
        return ConsultantRatingsCtl.StrategyWrapperList; 
    }
    
    // Delete the current user's references to third parties based upon a ':' separated list to provide the list of third party ids
    @AuraEnabled
    public static list<ThirdPartyWrapper> DeleteThirdPartyRefs(string thirdPartyRefsToDelete){
        list<id> thirdPartyRefsToDeleteIds = new list<id>();
        String[] thirdPartyRefsToDeleteArray = thirdPartyRefsToDelete.split(':');
        for (String tprs : thirdPartyRefsToDeleteArray){
           thirdPartyRefsToDeleteIds.add(Id.valueOf(tprs)); 
        }
        
        list<User_Setting__c> userSettingsToDelete = [select id from User_Setting__c
                                              where user__c = :UserInfo.getUserId()
                                              and recordtype.developerName = 'Followed_Third_Parties'
                                              and setting__c in :thirdPartyRefsToDeleteIds];
        
        if (userSettingsToDelete.size()> 0){
            
            delete userSettingsToDelete;
            ConsultantRatingsCtl.UserSettings = null;
            ConsultantRatingsCtl.ThirdPartyIdList = null;
            ConsultantRatingsCtl.ThirdPartyList = null;
            ConsultantRatingsCtl.ThirdPartyWrapperList = null;
            //US33170 - set static flag
            RecurssiveTriggerController.defaultConsultantRatingsRetrigger = true;            
        }
        return ConsultantRatingsCtl.ThirdPartyWrapperList; 
        
    }
    
    // Delete the current user's references to strategies based upon a ':' separated list to provide the list of strategy ids
    @AuraEnabled
    public static list<StrategyWrapper> DeleteStrategyRefs(string strategyRefsToDelete){
        list<id> strategyRefsToDeleteIds = new list<id>();
        String[] strategyRefsToDeleteArray = strategyRefsToDelete.split(':');
        for (String tprs : strategyRefsToDeleteArray){
           strategyRefsToDeleteIds.add(Id.valueOf(tprs)); 
        }
        
        list<User_Setting__c> userSettingsToDelete = [select id from User_Setting__c
                                              where user__c = :UserInfo.getUserId()
                                              and recordtype.developerName = 'Followed_Strategies'
                                              and setting__c in :strategyRefsToDeleteIds];
        
        if (userSettingsToDelete.size()> 0){            
            delete userSettingsToDelete;
            ConsultantRatingsCtl.UserSettings = null;
            ConsultantRatingsCtl.StrategyIdList = null;
            ConsultantRatingsCtl.StrategyList = null;
            ConsultantRatingsCtl.StrategyWrapperList = null;
            //US33170 - set static flag
            RecurssiveTriggerController.defaultConsultantRatingsRetrigger = true;            
        }
        return ConsultantRatingsCtl.StrategyWrapperList; 
        
    }

    // Search strategies based on the start of its name & return a list (max - 100)
    @AuraEnabled
    public static list<Investment_Strategy__c> GetStrategySearchResults(string StrategySearchName) {
        String searchName = '%' + StrategySearchName + '%';
        list<Investment_Strategy__c> StrategySearchResults = [select id, name 
                                    from Investment_Strategy__c 
                                    where name like :searchName 
                                    and id != :StrategyIdList
                                    order by name limit 100];         
        return StrategySearchResults;
    } 

    
    // Search third parties based on the start of its name & return a list (max - 100)
    @AuraEnabled
    public static list<Account> GetThirdPartySearchResults(string ThirdPartySearchName) {
        String searchName = '%' + ThirdPartySearchName + '%';
        list<Account> ThirdPartySearchResults = [select id, name, Billing_City__c, Billing_PostalCode__c 
                                    from account 
                                    where name like :searchName 
                                    and recordType.developerName='Third_Party'
                                    and id != :ThirdPartyIdList
                                    order by name limit 100];         
        return ThirdPartySearchResults;
    } 
    
    
    // Add a third party to the current user's user settings
    @AuraEnabled
    public static list<ThirdPartyWrapper> AddThirdPartyRef(String thirdPartyId) {
        list<recordType> recordTypes = [select id from RecordType where sObjectType = 'User_Setting__c' and developerName = 'Followed_Third_Parties' limit 1];
        
        if (recordTypes != null && recordTypes.size() > 0){
        
            User_Setting__c userSetting = new User_Setting__c();
            userSetting.user__c = UserInfo.getUserId();
            userSetting.recordtypeId = recordTypes[0].id;
            userSetting.Setting__c = thirdPartyId;
            insert userSetting;
        }

        
        ConsultantRatingsCtl.UserSettings = null;
        ConsultantRatingsCtl.ThirdPartyIdList = null;
        ConsultantRatingsCtl.ThirdPartyList = null;
        ConsultantRatingsCtl.ThirdPartyWrapperList = null;

        return ConsultantRatingsCtl.ThirdPartyWrapperList; 
    }     
    
    // Add a strategy to the current user's user settings
    @AuraEnabled
    public static list<StrategyWrapper> AddStrategyRef(String strategyId) {
        list<recordType> recordTypes = [select id from RecordType where sObjectType = 'User_Setting__c' and developerName = 'Followed_Strategies' limit 1];
        
        if (recordTypes != null && recordTypes.size() > 0){
        
            User_Setting__c userSetting = new User_Setting__c();
            userSetting.user__c = UserInfo.getUserId();
            userSetting.recordtypeId = recordTypes[0].id;
            userSetting.Setting__c = StrategyId;
            insert userSetting;
        }

        ConsultantRatingsCtl.UserSettings = null;
        ConsultantRatingsCtl.StrategyIdList = null;
        ConsultantRatingsCtl.StrategyList = null;
        ConsultantRatingsCtl.StrategyWrapperList = null;

        return ConsultantRatingsCtl.StrategyWrapperList; 
    }

    /*
     * Description : Method to get the the consultant rating default values from the custom setting. US33170
     */
    public static Map<String, List<Consultant_Ratings_Default_Config__c>> getConsultantRatingDefaults(){
        Map<String, List<Consultant_Ratings_Default_Config__c>> consultantRatingDefaults = new Map<String, List<Consultant_Ratings_Default_Config__c>>();
        //get records from custom setting against type
        if(Consultant_Ratings_Default_Config__c.getAll() !=  null){
            for(Consultant_Ratings_Default_Config__c each : Consultant_Ratings_Default_Config__c.getAll().values()){
                if(consultantRatingDefaults.containsKey(each.type__c)){
                    consultantRatingDefaults.get(each.type__c).add(each);
                }
                else{
                    consultantRatingDefaults.put(each.type__c, new List<Consultant_Ratings_Default_Config__c>{each});
                }
            }
        }
        return consultantRatingDefaults;
    }
    
    public class EventWrapper{
        @AuraEnabled
        public Date StartDate;
        @AuraEnabled
        public String participants;
        @AuraEnabled
        public Event EventRec;
        @AuraEnabled
        public String Contacts;
        @AuraEnabled
        public String URL;
        public EventWrapper(Event evRec, List<String> participants){
            StartDate = Date.valueOf(evRec.StartDateTime);
            Contacts = evRec.Who.Name;
            EventRec = evRec;
            if(!String.isBlank(evRec.Description)){
                EventRec.Description = evRec.Description.left(100);
            }
            this.participants = String.join(participants, ', ');
            URL = '/' + EventRec.Id;
        }
    }
    
    // Inner classes
    public class ThirdPartyWrapper{
        @AuraEnabled
        public boolean checked;
         
        @AuraEnabled
        public Account acc;
     
    }
    
    public class StrategyWrapper{
        @AuraEnabled
        public boolean checked;
         
        @AuraEnabled
        public Investment_Strategy__c investmentStrategy;
     
    }
    
    @AuraEnabled
    public static List<EventWrapper> getEvents(Id thirdPartyId, Id strategyId){
        List<EventWrapper> wrapperList = new List<EventWrapper>();
        Map<Id, Event> allEventsMap = new Map<Id, Event>();
        Set<Id> filteredEventIds = new Set<Id>();
        List<Event> allEvents = [select Id, StartDateTime, Subject, WhoId, Who.Name, Strategies_Funds__c, Type__c, Sub_Type__c, Description, (select Id, RelationId from EventRelations) from Event where AccountId = :thirdPartyId OR WhatId = :thirdPartyId ORDER BY EndDateTime DESC];
        Set<Id> contactIds = new Set<Id>();
        for(Event each: allEvents){
            allEventsMap.put(each.Strategies_Funds__c, each);
            for(EventRelation eachER: each.EventRelations){
                contactIds.add(eachER.RelationId);
            }
        }
        for(Activity_Product_Relationship__c each: [select Id, Activity_alias__c from Activity_Product_Relationship__c where Activity_alias__c IN :allEventsMap.keySet() AND Investment_Strategy__c = :strategyId]){
            filteredEventIds.add(each.Activity_alias__c);
        }
        Map<Id, Contact> contactsMap = new Map<Id, Contact>([select Id, Name from Contact where Id IN :contactIds AND (Internal_Employee__c = TRUE OR Account.Name LIKE '%Deloitte%')]);
        for(Id each: filteredEventIds){
            List<String> participantNames = new List<String>();
            if(allEventsMap.containsKey(each)){
                for(EventRelation eachER: allEventsMap.get(each).EventRelations){
                    if(contactsMap.containsKey(eachER.RelationId)){
                        participantNames.add(contactsMap.get(eachER.RelationId).Name);
                    }
                }
            }
            wrapperList.add(new EventWrapper(allEventsMap.get(each), participantNames));
        }
        return wrapperList;
    }
}