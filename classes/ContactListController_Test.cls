@isTest
public class ContactListController_Test {
    /*
     * 
     * Description : Find all Contacts which are last Viewed if no param is passed.
     * Param : 
     * Returns :  Contacts as a wrapper class
    */
    static testMethod void TestContactFindAll(){
        
        User user = CommonTestUtils.createTestUser();        
        
        // Create a client that the contacts will belong to
        Account acc = CommonTestUtils.CreateTestClient('Contact Tests');        
        insert acc;
        
        // Create a third party that the contacts will belong to
        Account tp = CommonTestUtils.CreateTestThirdParty('TP Tests');        
        insert tp;
        
        // Now create contacts for a client with last viewed Date
        List<contact> contacts = new List<contact>();       
        for(integer i = 0; i<10; i++ ){
            Contact c = CommonTestUtils.CreateTestContact('Contact ' + i);
            c.accountid = acc.id;
            contacts.add(c);                    
        }
        insert contacts;
        
        // Now create contacts for a tp with last viewed Date
        List<contact> contactstp = new List<contact>();       
        for(integer i = 0; i<10; i++ ){
            Contact c = CommonTestUtils.CreateTestContact('Contact ' + i);
            c.accountid = tp.id;
            contacts.add(c);                    
        }
        insert contactstp;
        
        event e=new event();
        e.Whoid=contacts[0].id;
        e.startDateTime=system.now();
        e.endDateTime=system.now().addHours(1);
        insert e;
        
        eventRelation er=new eventRelation();
        er.RelationId=contacts[1].id;
        er.EventId=e.id;
        insert er;
        
        task t=new task();
        t.whoid=contacts[0].id;
        insert t;
        
        taskRelation tr=new taskRelation();
        tr.RelationId=contacts[1].id;
        tr.TaskId=t.id;
        insert tr;
        
        Client_Third_Party_Relationship__c ctpr=new Client_Third_Party_Relationship__c();
        ctpr.Client_Name__c=acc.id;
        ctpr.Third_Party_Name__c=tp.id;
        insert ctpr;
        
        System.runAs(user){
            Test.startTest();
            List<Contact> allContacts = [select Id from Contact FOR VIEW];
            List<ContactListController.contactResult> ViewedContact =  ContactListController.findAll('',acc.id); 
            List<ContactListController.contactResult> ViewedContact0 =  ContactListController.findAll('test',acc.id);  
            List<ContactListController.contactResult> ViewedContact1 =  ContactListController.findAll('',contacts[0].id);  
            List<ContactListController.contactResult> ViewedContact2 =  ContactListController.findAll('',e.id);  
            List<ContactListController.contactResult> ViewedContact3 =  ContactListController.findAll('',t.id);  
            Test.stopTest();            
        }
    }
    /*
     * 
     * Description : Find all Contacts which are returned when name is passed as param.
     * Param : Name as String
     * Returns :  Contacts as a wrapper class
    */
    static testMethod void TestContactFindByName(){
        
        User user = CommonTestUtils.createTestUser();        
        
        // Create a client that the contacts will belong to
        Account acc = CommonTestUtils.CreateTestClient('Contact Tests');        
        insert acc;
        
         // Create a third party that the contacts will belong to
        Account tp = CommonTestUtils.CreateTestThirdParty('TP Tests');        
        insert tp;
        
        // Now create contacts for a client with last viewed Date
        List<contact> contacts = new List<contact>();       
        for(integer i = 0; i<10; i++ ){
            Contact c = CommonTestUtils.CreateTestContact('search' + i);
            c.accountid = acc.id;
            contacts.add(c);                    
        }
        insert contacts;
        
        event e=new event();
        e.Whoid=contacts[0].id;
        e.startDateTime=system.now();
        e.endDateTime=system.now().addHours(1);
        insert e;
        
        eventRelation er=new eventRelation();
        er.RelationId=contacts[1].id;
        er.EventId=e.id;
        insert er;
        
        task t=new task();
        t.whoid=contacts[0].id;
        insert t;
        
        taskRelation tr=new taskRelation();
        tr.RelationId=contacts[1].id;
        tr.TaskId=t.id;
        insert tr;
        
        Client_Third_Party_Relationship__c ctpr=new Client_Third_Party_Relationship__c();
        ctpr.Client_Name__c=acc.id;
        ctpr.Third_Party_Name__c=tp.id;
        insert ctpr;
        
        System.runAs(user){
            Test.startTest();
            List<ContactListController.contactResult> ViewedContact =  ContactListController.findByName('search','',acc.id); 
            List<ContactListController.contactResult> ViewedContact0 =  ContactListController.findByName('search','test',acc.id); 
            List<ContactListController.contactResult> ViewedContact1 =  ContactListController.findByName('search','',contacts[0].id); 
            List<ContactListController.contactResult> ViewedContact2 =  ContactListController.findByName('search','',e.id); 
            List<ContactListController.contactResult> ViewedContact3 =  ContactListController.findByName('search','',t.id); 
            System.debug(ViewedContact.size()); 
            System.assert(ViewedContact.size()==10);        
            Test.stopTest();
            
        }
    }
    /*
     * 
     * Description : Find all Contacts which are returned if contact ids are passed as param.
     * Param : List of Contact Ids
     * Returns :  Contacts as a wrapper class
    */
    static testMethod void TestContactFindByIds(){
        
        User user = CommonTestUtils.createTestUser();        
        
        // Create a client that the contacts will belong to
        Account acc = CommonTestUtils.CreateTestClient('Contact Tests');        
        insert acc;
        
        // Now create contacts for a client with last viewed Date
        List<contact> contacts = new List<contact>();       
        for(integer i = 0; i<10; i++ ){
            Contact c = CommonTestUtils.CreateTestContact('Contact ' + i);
            c.accountid = acc.id;
            contacts.add(c);                    
        }
        insert contacts;
        
        List<id> contactId=new List<id>();
        for(integer i = 0; i<5 ; i++){
            contactId.add(contacts[i].id);
        }
        
        System.runAs(user){
            Test.startTest();
            List<ContactListController.contactResult> ViewedContact =  ContactListController.findByListId(contactId,'test','');  
            System.assert(ViewedContact.size()==5); 
            Test.stopTest();
        }
    }
}