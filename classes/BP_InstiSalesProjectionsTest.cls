@isTest

public class BP_InstiSalesProjectionsTest{

public static testmethod void testGetSalesProjections(){

        Profile p1 = [select Id,name from Profile where Name like '%Insti%' limit 1];
        User u = CommonTestUtils.createTestUser(true);       
        u.ProfileId = p1.id;
        update u;
    Test.startTest();
        System.runAs(u){
        System.debug('user ---> '+ p1.name);
            Account newAccount1 = CommonTestUtils.CreateTestClient('Test User 1');
            Account newAccount2 = CommonTestUtils.CreateTestClient('Test User 2');
            insert(newAccount1);
            insert(newAccount2);
            Investment_Strategy__c strat1 =  new Investment_Strategy__c();
            insert strat1;
        
            Opportunity opp1 = CommonTestUtils.CreateTestOpportunityMandate('OppTest1', newAccount1.id,Date.today()+100,'Won-Funded');
            Opportunity opp2 = CommonTestUtils.CreateTestOpportunityMandate('OppTest2', newAccount2.id,Date.today()-390,'Won-Funded');
            Opportunity opp3 = CommonTestUtils.CreateTestOpportunityMandate('OppTest3', newAccount1.id,Date.today()-10,'Won');
                
            opp1.Amount = 23545.23;
            opp1.Investment_Strategy__c = strat1.id;
            opp2.Amount = 123456654.23;
            opp2.Investment_Strategy__c = strat1.id;            
            insert opp1;
            insert opp2;
            Opportunity_Funds__c op1 = new Opportunity_Funds__c();
            op1.Actual_Funding_Amount__c = 1234.22;
            op1.Opportunity__c = opp1.id;
            insert op1;        
            
            opp3.Amount = 432321.33;
            opp3.Investment_Strategy__c = strat1.id;
            opp3.Fee_Rate_bps__c = 1;
            insert opp3;
            
            Opportunity_Funds__c op2 = new Opportunity_Funds__c();
            op2.Actual_Funding_Amount__c = 123423.22;
            op2.Opportunity__c = opp2.id;
            insert op2;
            
            Business_Plan__c bp1 = BusinessPlanOpportunitiesControllerTest.createTestBusinessPlan('Business Plan 1');
            Business_Plan__c bp2 = BusinessPlanOpportunitiesControllerTest.createTestBusinessPlan('Business Plan 2');
            Business_Plan__c bp3 = BusinessPlanOpportunitiesControllerTest.createTestBusinessPlan('Business Plan 3');
            bp1.Starting_Fiscal_Year__c = Date.newInstance(2015,11,11);            
            
            bp1.RecordTypeId = Business_Plan__c.sObjectType.getDescribe().getRecordTypeInfosByName().get('Open').getRecordTypeId();
            bp2.Starting_Fiscal_Year__c = Date.newInstance(2016,11,11);
            bp2.RecordTypeId = Business_Plan__c.sObjectType.getDescribe().getRecordTypeInfosByName().get('Closed').getRecordTypeId();                       
            insert bp1;            
            insert bp2;
            //Getting projections for open business plan
            BP_InstiSalesProjectionController.SalesProjections projections = BP_InstiSalesProjectionController.getProjections(bp1.id);
            System.assertEquals(null,projections.isComplete);
            //Sending business plan id as NuLL and verifying the exception handling           
            BP_InstiSalesProjectionController.SalesProjections projections1 = BP_InstiSalesProjectionController.getProjections(null);
                    
            //Updating the business plan with the projections.
            Boolean isUpdated = BP_InstiSalesProjectionController.updateBusinessPlan(bp1.id, Json.serialize(projections));
            System.assertEquals(true, isUpdated);
            //Updating the business plan with empty projections.
            BP_InstiSalesProjectionController.updateBusinessPlan(bp1.id, '');
            BP_InstiSalesProjectionController.SalesProjections pro = new BP_InstiSalesProjectionController.SalesProjections();
        }
        Test.stopTest();

}
}