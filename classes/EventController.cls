public with sharing class EventController {

    public List<Event> events { 
        get {
            return [SELECT   Id, Subject, ActivityDateTime, Location, Who.Name,Who.Id, EndDateTime
                    FROM     Event 
                    WHERE    ActivityDateTime = TODAY AND
                             CreatedById = :UserInfo.getUserId()
                    ORDER BY ActivityDateTime ASC
                    LIMIT    5];
        }
    }
    
    public EventController(ApexPages.StandardController controller) {}
}