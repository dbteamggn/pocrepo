/*----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            clientFinancialsCtrl_Test.cls
   Description:     Test class for the clientFinancialsCtrl Controller
                    
   Date             Author                             Summary of Changes                  
   -----------      -----------------                  -------------------------------------------------------------------------------------
   Oct 2016         Andy Wallis                        Initial Release  
   Jan 2017         Andee Weir                         Replace GetFinancials procedure with GetUsersProfile in controller

------------------------------------------------------------------------------------------------------------------------------------------------------------*/

@isTest
public class clientFinancialsCtrl_Test {
    
    //Test that the is_FIG flag is found
    static testMethod void returnFIGUser() {
        User runAs = CommonTestUtils.createTestUser();
        Profile standardProfile = [select Id from Profile where name = 'Standard User' limit 1];
        runAs.Is_FIG__c = true;
        runAs.ProfileId = standardProfile.Id;
        update runAs;
        System.runAs(runAs){
            Test.startTest();
            Boolean isFig = clientFinancialsCtrl.IsNdam();
            Test.stopTest();
            System.assertEquals(true, isFig);
        }
    }
    
    //Test that the default non FIG user returns false
    //And also that a standard user will not have access to financial charts
    static testMethod void returnNonFIGUser() {
        User runAs = CommonTestUtils.createTestUser();
        Profile standardProfile = [select Id from Profile where name = 'Standard User' limit 1];
        runAs.ProfileId = standardProfile.Id;
        update runAs;
        System.runAs(runAs){
            Test.startTest();
            Boolean isFig = clientFinancialsCtrl.IsNdam();
            String userProfile = clientFinancialsCtrl.GetUsersProfile();
            Test.stopTest();
            System.assertEquals(false, isFig);
            System.assertEquals('Standard User', userProfile);
        }
    }
    
    //Test that the System Admin has all access
    static testMethod void returnSysAdminUser() {
        User runAs = CommonTestUtils.createTestUser();
        Profile sysAdminProfile = [select Id from Profile where name = 'System Administrator' limit 1];
        runAs.ProfileId = sysAdminProfile.Id;
        update runAs;
        System.runAs(runAs){
            Test.startTest();
            Boolean isFig = clientFinancialsCtrl.IsNdam();
            String userProfile = clientFinancialsCtrl.GetUsersProfile();
            Test.stopTest();
            System.assertEquals(true, isFig);
            System.assertEquals('System Administrator', userProfile);
        }
    }
    
    //Test that a Retail user has all access
    static testMethod void returnRetailUser() {
        User runAs = CommonTestUtils.createTestUser();
        Profile RetailProfile = [select Id from Profile where name = 'Retail Sales User' limit 1];
        runAs.ProfileId = RetailProfile.Id;
        update runAs;
        System.runAs(runAs){
            Test.startTest();
            String userProfile = clientFinancialsCtrl.GetUsersProfile();
            Test.stopTest();
            System.assertEquals('Retail Sales User', userProfile);
        }
    }
}