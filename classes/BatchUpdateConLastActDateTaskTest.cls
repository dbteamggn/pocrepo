@isTest
public class BatchUpdateConLastActDateTaskTest {
    
    /*
     * 
     * Description : Check that the workflow rule correctly sets the Date_Closed__c date when a
     *               task is closed.  Required as other tests are manually setting the date_Closed__c date.
     * Param : 
     * Returns :  
    */
    static testMethod void CheckDateClosedWorkflowRule(){
        User user = CommonTestUtils.createTestUser();        
        
        System.runAs(user){  
            
            Contact con = CommonTestUtils.CreateTestContact('a');
            insert con;
            
            Task task = CommonTestUtils.CreateTestTask('Test Subject', user.id, 'Normal', 'Open', con.id);
            insert task;   
            
            system.assertEquals(null, task.Closed_Date__c);
            
            test.startTest();
            task.Status = 'Completed';
            update task; 
            test.stopTest();
            
            datetime now = system.now();
            date nowDate = date.newInstance(now.year(), now.month(), now.day());
            
            list<task> resultTasks = [select id, Closed_Date__c from task where id = :task.id];
            
            system.assertEquals(1, resultTasks.size()); 
            system.assertEquals(nowDate, resultTasks[0].Closed_Date__c);
        }
    }
    
    /*
     * 
     * Description : Check that a past Task (End date earlier than today but later than the last batch run) 
     *               updates the contact that is associated with it.
     * Param : 
     * Returns :  
    */
    static testMethod void CheckPastDate(){
        User user = CommonTestUtils.createTestUser();        
        
        System.runAs(user){  
            
            Contact con = CommonTestUtils.CreateTestContact('a');
            insert con;
            
            Task task = CommonTestUtils.CreateTestTask('Test Subject', user.id, 'Normal', 'Completed', con.id);
            insert task;  
            
            // Overwrite the workflow rule generated Closed Date
            datetime yesterday = system.now() - 1;
            date yesterdayDate = date.newInstance(yesterday.year(), yesterday.month(), yesterday.day());

            task.Closed_Date__c = yesterdayDate;
            update task;
            
            list<task> preCheckTasks = [select id, Closed_Date__c from task where id = :task.id];
            
            system.assertEquals(1, preCheckTasks.size()); 
            system.assertEquals(yesterdayDate, preCheckTasks[0].Closed_Date__c);          
                        
            // start the test by triggering the batch job to run that does the update
            Test.startTest();
            Database.executeBatch( new BatchUpdateConLastActDateTask());
            Test.stopTest();
            
            // Get the contacts after the batch process has completed
            list<Contact> resultContacts = [select id, name, last_activity_date__c from contact];
            
            // Should be 1 contact
            system.assertEquals(1, resultContacts.size());
            
            // Check that the last activity date is yesterday
            system.assertEquals(yesterdayDate, resultContacts[0].last_activity_date__c);
        }
    }

    /*
     * 
     * Description : Check that a future Task (End date later than today) does not update the contact that is associated with it.
     * Param : 
     * Returns :  
    */    
    static testMethod void CheckFutureEvent(){
        User user = CommonTestUtils.createTestUser();        
        
        System.runAs(user){  
            
            Contact con = CommonTestUtils.CreateTestContact('a');
            insert con;
            
            Task task = CommonTestUtils.CreateTestTask('Test Subject', user.id, 'Normal', 'Completed', con.id);
            insert task;   
            
            // Overwrite the workflow rule generated Closed Date            
            datetime tomorrow = system.now() + 1;
            date tomorrowDate = date.newInstance(tomorrow.year(), tomorrow.month(), tomorrow.day());
            task.Closed_Date__c = tomorrowDate;
            update task;
            
            // start the test by triggering the batch job to run that does the update
            Test.startTest();
            Database.executeBatch( new BatchUpdateConLastActDateTask());
            Test.stopTest();
            
            // Get the contacts after the batch process has completed
            list<Contact> resultContacts = [select id, name, last_activity_date__c from contact];
            
            // Should be 1 contact
            system.assertEquals(1, resultContacts.size());
            
            // Check that the last activity date is null
            system.assertEquals(null, resultContacts[0].last_activity_date__c);
        }
    }
    
    

    /*
     * 
     * Description : Check that multiple past & future Tasks result in the most recent past task to update the Last Activity Date on contact.
     *               Also check that only the relevant contact is updated.
     * Param : 
     * Returns :  
    */ 
    static testMethod void CheckMultipleTasks(){
        User user = CommonTestUtils.createTestUser();        
        
        System.runAs(user){  
            
            list<contact> contacts = new list<contact>();
            Contact con1 = CommonTestUtils.CreateTestContact('a');
            contacts.add(con1);
            Contact con2 = CommonTestUtils.CreateTestContact('b');
            contacts.add(con2);
            insert contacts;
            
            datetime now = system.now();
            date todayDate = date.newInstance(now.year(), now.month(), now.day());            
            
            
            list<task> tasks = new list<task>();
            Task task1 = CommonTestUtils.CreateTestTask('Test Subject 1', user.id, 'Normal', 'Completed', con1.id);
            tasks.add(task1); 
            Task task2 = CommonTestUtils.CreateTestTask('Test Subject 2', user.id, 'Normal', 'Completed', con1.id);
            tasks.add(task2); 
            Task task3 = CommonTestUtils.CreateTestTask('Test Subject 3', user.id, 'Normal', 'Completed', con1.id);
            tasks.add(task3); 
            Task task4 = CommonTestUtils.CreateTestTask('Test Subject 4', user.id, 'Normal', 'Completed', con1.id);
            tasks.add(task4); 
            Task task5 = CommonTestUtils.CreateTestTask('Test Subject 5', user.id, 'Normal', 'Completed', con1.id);
            tasks.add(task5); 
            Task task6 = CommonTestUtils.CreateTestTask('Test Subject 6', user.id, 'Normal', 'Completed', con1.id);
            tasks.add(task6); 
            insert tasks; 
            
            // Now set the Closed_Date__c to the required test values
            task1.Closed_Date__c = todayDate - 3;
            task2.Closed_Date__c = todayDate - 2;
            task3.Closed_Date__c = todayDate - 1;
            task4.Closed_Date__c = todayDate;
            task5.Closed_Date__c = todayDate + 1;
            task6.Closed_Date__c = todayDate + 2;
            update tasks;
            
            // start the test by triggering the batch job to run that does the update
            Test.startTest();
            Database.executeBatch( new BatchUpdateConLastActDateTask());
            Test.stopTest();
            
            // Get the contacts after the batch process has completed
            list<Contact> resultContacts = [select id, name, last_activity_date__c from contact];
            
            // Should be 2 contact
            system.assertEquals(2, resultContacts.size());
            
            for (contact resultContact : resultContacts){
                if (resultContact.id == con1.id){
                    // Check that the last activity date is null
                    system.assertEquals(todayDate - 1, resultContact.last_activity_date__c);
                } else {
                    // Check that the last activity date is null
                    system.assertEquals(null, resultContact.last_activity_date__c);
                }
            }

        }
    }
    
    

    /*
     * 
     * Description : Check that all contacts referred to in a Task are updated
     * Param : 
     * Returns :  
    */ 
    static testMethod void CheckMultipleContactsOnTask(){
        User user = CommonTestUtils.createTestUser();        
        
        System.runAs(user){  
            
            list<contact> contacts = new list<contact>();
            Contact con1 = CommonTestUtils.CreateTestContact('a');
            contacts.add(con1);
            Contact con2 = CommonTestUtils.CreateTestContact('b');
            contacts.add(con2);
            Contact con3 = CommonTestUtils.CreateTestContact('c');
            contacts.add(con3);
            insert contacts;
            
            datetime now = system.now();
            date todayDate = date.newInstance(now.year(), now.month(), now.day());            
            
            
            list<task> tasks = new list<task>();
            Task task1 = CommonTestUtils.CreateTestTask('Test Subject 1', user.id, 'Normal', 'Completed', con1.id);
            tasks.add(task1);
            insert tasks; 
            
            // Now set the Closed_Date__c to the required test value
            task1.Closed_Date__c = todayDate - 3;
            update tasks;
            
            // Add other contacts to the task
            list<TaskRelation> taskRelations = new list<taskRelation>();
            TaskRelation tr1 = new TaskRelation(relationId = con2.id, taskId = task1.id);
            taskRelations.add(tr1);
            TaskRelation tr2 = new TaskRelation(relationId = con3.id, taskId = task1.id);
            taskRelations.add(tr2);
            insert taskRelations;
            
            // start the test by triggering the batch job to run that does the update
            Test.startTest();
            Database.executeBatch( new BatchUpdateConLastActDateTask());
            Test.stopTest();
            
            // Get the contacts after the batch process has completed
            list<Contact> resultContacts = [select id, name, last_activity_date__c from contact];
            
            // Should be 3 contacts
            system.assertEquals(3, resultContacts.size());
            
            for (contact resultContact : resultContacts){
                system.assertEquals(todayDate - 3, resultContact.last_activity_date__c);
            }

        }
    }

}