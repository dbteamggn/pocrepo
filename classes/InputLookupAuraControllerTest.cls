@isTest
private class InputLookupAuraControllerTest {
    
    /*
     * 
     * Description : Standard test set up
     *               Creates 1 client that has 4 contacts assigned to it
    */
    @testSetup
    private static void setup(){
        
        account account1 = CommonTestUtils.CreateTestClient('Test Account');
        insert account1;
        
        list<contact> contacts = new list<contact>();
        Contact con1 = CommonTestUtils.CreateTestContact('Lasta');
        con1.firstName = 'First1';
        con1.AccountId = account1.id;
        contacts.add(con1);
        Contact con2 = CommonTestUtils.CreateTestContact('Lastc');
        con2.firstName = 'First2';
        con2.AccountId = account1.id;
        contacts.add(con2);
        Contact con3 = CommonTestUtils.CreateTestContact('Lastb');
        con3.firstName = 'First3';
        con3.AccountId = account1.id;
        contacts.add(con3);
        Contact con4 = CommonTestUtils.CreateTestContact('Latter');
        con4.firstName = 'Finn';
        con4.AccountId = account1.id;
        contacts.add(con4);
        insert contacts;                                 
    }
    
    /*
     * 
     * Description : Simple seach on contact name
     *               
    */
    private static testmethod void testSimpleNameSearch(){
        Test.startTest();
        list<Id> excludeIds = new list<Id>();
        String jsonResult = InputLookupAuraController.getResults('contact', //objectName
                                                           'id',            //idFieldName
                                                           'name',          //searchFields
                                                           'name',          //sortFields
                                                           'Fir',           //searchValue
                                                           '',              //additionalWhereClause
                                                           '20',            //limitValue
                                                           '{name}',        //returnFormatStringDisplay
                                                           '{',             //fieldStartToken
                                                           '}',             //fieldEndToken
                                                           '{name}',        //returnFormatStringMatches
														   'id',            //excludeIdFieldName
                                                           excludeIds);
        Test.stopTest();
        
        List<InputLookupAuraController.SearchResult> sResList = (List<InputLookupAuraController.SearchResult>)JSON.deserialize(jsonResult, 
                                                                                                                               List<InputLookupAuraController.SearchResult>.class);
        System.assertEquals(3, sResList.size());
        System.assertEquals('First1 Lasta', sResList[0].matchName);
        System.assertEquals('First2 Lastc', sResList[1].matchName);
        System.assertEquals('First3 Lastb', sResList[2].matchName);
    }
    
    
    /*
     * 
     * Description : Search on contacts' last name but this time have an additonal where
     *               cause that always excludes first name First3.
    */
    private static testmethod void testWhereClauseAndFormat(){
        
        Test.startTest();
        list<Id> excludeIds = new list<Id>();
        String jsonResult = InputLookupAuraController.getResults('contact', //objectName
                                                           'id',          //idFieldName
                                                           'lastname',      //searchFields
                                                           'firstname',     //sortFields
                                                           'La',            //searchValue
                                                           'firstName!=\'First3\'',     //additionalWhereClause
                                                           '20',            //limitValue
                                                           '{lastname}:{FirstName}',        //returnFormatStringDisplay
                                                           '{',             //fieldStartToken
                                                           '}',             //fieldEndToken
                                                           '{lastname}:{FirstName}',        //returnFormatStringMatches
														   'id',            //excludeIdFieldName
                                                           excludeIds);    //excludeIds
        Test.stopTest();
        
        List<InputLookupAuraController.SearchResult> sResList = (List<InputLookupAuraController.SearchResult>)JSON.deserialize(jsonResult, 
                                                                                                                               List<InputLookupAuraController.SearchResult>.class);
        System.assertEquals(3, sResList.size());
        System.assertEquals('Latter:Finn', sResList[0].matchName);
        System.assertEquals('Lasta:First1', sResList[1].matchName);
        System.assertEquals('Lastc:First2', sResList[2].matchName);
    }
    
    
    /*
     * 
     * Description : Search contacts on last name but exclude the first contact.
     *               This would normally be used to exclude objects that are already appearing
     *               in the selection table & therefore shouldn't be included in the results.
    */
    private static testmethod void testExclude(){
        List<Contact> contacts = [Select Id, LastName, FirstName, Name From Contact order by lastName];
        system.assertEquals(4, contacts.size());
        
        
        Test.startTest();
        list<Id> excludeIds = new list<Id>();
        excludeIds.add(contacts[0].id);  // Exclude the first contact ('Lasta')
        String jsonResult = InputLookupAuraController.getResults('contact', //objectName
                                                           'id',          //idFieldName
                                                           'lastname',      //searchFields
                                                           'firstname',     //sortFields
                                                           'La',            //searchValue
                                                           '',     //additionalWhereClause
                                                           '20',            //limitValue
                                                           '{FirstName}:{lastname}',            //returnFormatStringDisplay
                                                           '{',             //fieldStartToken
                                                           '}',             //fieldEndToken
                                                           '{FirstName}:{lastname}',        //returnFormatStringMatches
														   'id',            //excludeIdFieldName
                                                           excludeIds);    //excludeIds
        Test.stopTest();
        
        List<InputLookupAuraController.SearchResult> sResList = (List<InputLookupAuraController.SearchResult>)JSON.deserialize(jsonResult, 
                                                                                                                               List<InputLookupAuraController.SearchResult>.class);
        System.assertEquals(3, sResList.size());
        System.assertEquals('Finn:Latter', sResList[0].matchName);
        System.assertEquals('First2:Lastc', sResList[1].matchName);
        System.assertEquals('First3:Lastb', sResList[2].matchName);
    }
    
    
    /*
     * 
     * Description : Search for a value across more than one field
     *               Note the addition of a new contact!
     * 
    */
    private static testmethod void testMultiFieldSearch(){
        
        Contact con1 = CommonTestUtils.CreateTestContact('Fire');
        con1.firstName = 'Zzzz';
        insert con1; 
        
        List<Contact> contacts = [Select Id, LastName, FirstName, Name From Contact order by lastName];
        system.assertEquals(5, contacts.size());
        
        
        Test.startTest();
        list<Id> excludeIds = new list<Id>();
        String jsonResult = InputLookupAuraController.getResults('contact', //objectName
                                                           'id',          //idFieldName
                                                           'lastname,firstname',      //searchFields
                                                           'firstname',     //sortFields
                                                           'Fir',            //searchValue
                                                           '',     //additionalWhereClause
                                                           '20',            //limitValue
                                                           'Results={FirstName} {lastname}',        //returnFormatStringDisplay
                                                           '{',             //fieldStartToken
                                                           '}',             //fieldEndToken
                                                           'Results={FirstName} {lastname}',        //returnFormatStringMatches
														   'id',            //excludeIdFieldName
                                                           excludeIds);    //excludeIds
        Test.stopTest();
        
        List<InputLookupAuraController.SearchResult> sResList = (List<InputLookupAuraController.SearchResult>)JSON.deserialize(jsonResult, 
                                                                                                                               List<InputLookupAuraController.SearchResult>.class);
        System.assertEquals(4, sResList.size());
        System.assertEquals('Results=First1 Lasta', sResList[0].matchName);
        System.assertEquals('Results=First2 Lastc', sResList[1].matchName);
        System.assertEquals('Results=First3 Lastb', sResList[2].matchName);
        System.assertEquals('Results=Zzzz Fire', sResList[3].matchName);
    }
    
    
    /*
     * 
     * Description : Check that related fields can appear in returned values
     * 
    */
    private static testmethod void testIncludeRelatedFieldsDisplay(){      
        
        Test.startTest();
        list<Id> excludeIds = new list<Id>();
        String jsonResult = InputLookupAuraController.getResults('contact', //objectName
                                                           'id',          //idFieldName
                                                           'lastname,firstname',      //searchFields
                                                           'firstname',     //sortFields
                                                           'Fir',            //searchValue
                                                           '',     //additionalWhereClause
                                                           '20',            //limitValue
                                                           '{FirstName} {lastname})',        //returnFormatStringDisplay
                                                           '{',             //fieldStartToken
                                                           '}',             //fieldEndToken
                                                           '{FirstName} {lastname} ({account.name})',        //returnFormatStringMatches
														   '',            //excludeIdFieldName
                                                           excludeIds);    //excludeIds
        Test.stopTest();
        
        List<InputLookupAuraController.SearchResult> sResList = (List<InputLookupAuraController.SearchResult>)JSON.deserialize(jsonResult, 
                                                                                                                               List<InputLookupAuraController.SearchResult>.class);
        System.assertEquals(3, sResList.size());
        System.assertEquals('First1 Lasta (Test Account)', sResList[0].matchName);
        System.assertEquals('First2 Lastc (Test Account)', sResList[1].matchName);
        System.assertEquals('First3 Lastb (Test Account)', sResList[2].matchName);
    }
    
    
    /*
     * 
     * Description : Check that the description for an object is correctly brought back
     * 
    */
    private static testmethod void testGetObjectSearchLabel(){      
        
        Test.startTest();
        String result1 = InputLookupAuraController.getObjectSearchLabel('account');
        String result2 = InputLookupAuraController.getObjectSearchLabel('contact');
        Test.stopTest();
        
        System.assertEquals('Client/Third-Party', result1);
        System.assertEquals('Contact', result2);
    }
    
    private static testmethod void test_get_name(){
        List<Contact> contacts = [Select Id, LastName, FirstName, Name From Contact];
        
        Test.startTest();
        
        String ret = InputLookupAuraController.getCurrentValue(null, null);
        System.assert(String.isBlank(ret), 'Should return null string ['+ret+']');
        
        ret = InputLookupAuraController.getCurrentValue('INVALID_OBJECT', 'INVALID_ID');
        System.assert(String.isBlank(ret), 'Should return null string ['+ret+']');
        
        ret = InputLookupAuraController.getCurrentValue('INVALID_OBJECT', '000000000000000');
        System.assert(String.isBlank(ret), 'Should return null string ['+ret+']');
        
        ret = InputLookupAuraController.getCurrentValue('Contact', '000000000000000');
        System.assert(String.isBlank(ret), 'Should return null string ['+ret+']');
        
        ret = InputLookupAuraController.getCurrentValue('Contact', contacts[0].Id);
        System.assert(ret == contacts[0].Name, 'Should return '+contacts[0].Name+ ' ['+ret+']');
            
        Test.stopTest();
    }
}