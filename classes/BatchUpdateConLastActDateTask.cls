global class BatchUpdateConLastActDateTask implements Database.Batchable<sObject> { 
   

    /*
     * 
     * Description : start method of the batch class to query the records to be processed
     * Param :   Database.BatchableContext
     * Returns : Database.QueryLocator  
    */
    global Database.QueryLocator start(Database.BatchableContext BC) { 
        //Fetch the last batch run time to process delta records
    	BatchRunTime__c lastRun = BatchRunTime__c.getValues('BatchUpdateConLastActDate'); // Handles both Event & Task batch jobs
        DateTime lastRunDateTime;
        if (lastRun == null){
            lastRunDateTime = system.now() - (365 * 5); // Default to 5 years ago
        } else {
            lastRunDateTime = lastRun.Last_Run__c;
        }
        Date lastRunDate = date.newInstance(lastRunDateTime.year(), lastRunDateTime.month(), lastRunDateTime.day());
        
        return Database.getQueryLocator([select ID, RelationId, Task.Closed_Date__c 
                                         from TaskRelation where RelationId != null 
                                         AND Task.Closed_Date__c >= :lastRunDate
                                         AND Task.Closed_Date__c < TODAY
                                         order by Task.Closed_Date__c desc
                                        ]);

    }

    /*
     * 
     * Description : execute method of the batch class to process the queried records
     * Param :   Database.BatchableContext
     * Returns : Scope - list of eventRelations  
    */
    global void execute(Database.BatchableContext BC, List<Sobject> scope) {
        Map<Id, date> contactIdToLastActivityDate = new Map<Id, date>();  // <ContactId, Date>
        
        // Loop through the TaskRelations
        for(Sobject s : Scope){
            TaskRelation e = (TaskRelation) s;
            // to check if task is associated with contact
            if(String.valueOf(e.RelationId).subString(0,3) == '003' ){
                
                // Add the task closed date to a map (keyed by contactId) if it is not there or later than the existing date in the map
                Date taskClosedDate = date.newInstance(e.Task.Closed_Date__c.year(), e.Task.Closed_Date__c.month(), e.Task.Closed_Date__c.day()); 
                if (contactIdToLastActivityDate.containsKey(e.RelationId)){
                    if (taskClosedDate > contactIdToLastActivityDate.get(e.RelationId) ){
                        contactIdToLastActivityDate.put(e.RelationId, taskClosedDate);
                    }   
                } else {
                    contactIdToLastActivityDate.put(e.RelationId, taskClosedDate);
                }
            }
        }
        
        // Get the related contacts, check the existing Last Activity Date & if the one in the map is later, the contact's Last Activity Date needs to be updated.
        list<contact> updateContacts = new list<contact>();
        for(contact contact : [select id, last_Activity_Date__c from contact where id in :contactIdToLastActivityDate.keySet()]){
            if (contact.last_Activity_Date__c == null || contactIdToLastActivityDate.get(contact.id) > contact.last_Activity_Date__c){
                contact.last_Activity_Date__c = contactIdToLastActivityDate.get(contact.id);
                updateContacts.add(contact);
            }
        }
        
        // Perform the update
        if (updateContacts.size() > 0){
            update updateContacts;
        }
         
            
    }
    
    /*
     * 
     * Description : finish method of the batch class, executed once the batch is completely processed
     *               1. If there are errors, sends out email in case of error
     *               2. otherwise, update the last batch run time
     * Param :   Database.BatchableContext
     * Returns :   
    */
    global void finish(Database.BatchableContext BC) {
        
        AsyncApexJob asynch=[Select Id,JobItemsProcessed,ExtendedStatus,CompletedDate,NumberOfErrors,TotalJobItems,Status,CreatedBy.Email 
                                 FROM   AsyncApexJob 
                                 WHERE  Id =: BC.getJobId()];
        String Subject = 'Scheduled Batch Job Failure in updating Contact last activity date - Tasks';
        String[] addr=new String[]{asynch.CreatedBy.Email};
            String PlainTextBody = 'Scheduled Batch job to update contacts\' Last Activity Date from Tasks completed at '
            + asynch.CompletedDate+' throws following error : \n Processed Job Itmes :'+ asynch.JobItemsProcessed
            +'\n No.of Job Failures : '+asynch.NumberofErrors+'\n Errors :'+asynch.ExtendedStatus;
        
        if(asynch.NumberOfErrors > 0){
            CommonUtilities.SendEmail(Subject,PlainTextBody,addr);
        } else {

            // update last batch run time
            BatchRunTime__c lastRun = BatchRunTime__c.getValues('BatchUpdateConLastActDate');
            if (lastRun == null){
                lastRun = new BatchRunTime__c();
                lastRun.name = 'BatchUpdateConLastActDate';
            }
            lastRun.Last_Run__c = system.now();
            upsert lastRun;
        }
    
    }

}