/*----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            Gift_Benefit_Utility
   Description:     Methods called by Gift_Benefit triggers
                    
   Date             Author                             Summary of Changes                  
   -----------      -----------------                  -------------------------------------------------------------------------------------
    22 Sep 2016      Sridhar Gurram                     Initial Release
------------------------------------------------------------------------------------------------------------------------------------------------------------*/
public class Gift_Benefit_Utility {

    /*
     * Description : Gift and Benefits cannot be deleted
     * Param : 
     * Returns :  
    */

    public static void validateDelete(TriggerHandler.TriggerParameter tParam){
        List<Gift_Benefit__c> oldGnBs = (List<Gift_Benefit__c>) tParam.oldList;
        for(Gift_Benefit__c each: oldGnBs){
           each.addError(System.Label.CantDeleteGift);
        }
    }
}