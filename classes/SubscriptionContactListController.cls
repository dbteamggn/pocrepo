public class SubscriptionContactListController {
    
    @AuraEnabled
    public static ContactPagination getAllContacts(string queryParam,string recordId, Decimal pageNumber) {
        
        System.debug('queryParam'+queryParam);
        System.debug('recordId'+recordId);
        List<contactResult> contactResultList = new List<contactResult>();
        ContactPagination paginationResult ;
        
        Integer pageSize = 10;
        Integer page = (Integer)pageNumber;
        Integer offset = (page-1) * pageSize;
        Integer contactCount = 0;
        List<Contact_Subscription_Link__c> contactSubscriptionList = new List<Contact_Subscription_Link__c>();
        List<Contact> contactList = new List<Contact>();
        if(recordId!=null && recordId != ''){
            if(Id.valueOf(recordId).getSObjectType() == Schema.Subscription__c.SObjectType){
                contactSubscriptionList = [select id, Contact__r.id,Contact__r.Name, Contact__r.Account.Name from Contact_Subscription_Link__c where Subscription__r.id=:recordId ORDER BY Contact__r.name LIMIT 10 OFFSET :offset ];
                if(contactSubscriptionList.size()>0){
                    contactCount = [select count() from Contact_Subscription_Link__c where Subscription__r.id =:recordId ];
                    System.debug('list size is ---> '+ contactCount);
                }
            }
            for(Contact_Subscription_Link__c temp : contactSubscriptionList){
                contactResult tempCon = new contactResult();
                tempCon.contactId = temp.Contact__r.id;
                tempCon.contactName = temp.Contact__r.name;
                tempCon.accountName = temp.Contact__r.Account.Name;
                tempCon.value = true;                
                contactResultList.add(tempCon);           
            }
            paginationResult = createContactResponse(contactResultList,page,pageSize,contactCount);
        }
        System.debug('contactResultList'+paginationResult);
        return paginationResult;
    }
    
    @AuraEnabled
    public static ContactPagination getContactsByName(String searchKey,string queryParam,string recordId, Decimal pageNumber) {
        System.debug('queryParam'+queryParam);
        System.debug('recordId'+recordId);
        List<contactResult> contactResultList = new List<contactResult>();
        ContactPagination paginationResult ;
        String name = '%' + searchKey + '%';
        Integer pageSize = 10;
        Integer page = (Integer)pageNumber;
        Integer offset = (page-1) * pageSize;
        Integer contactCount ;
        List<Contact> contactList = new List<Contact>();
        id AccId;
        if(recordId!=null && recordId != ''){
            if(Id.valueOf(recordId).getSObjectType() == Schema.Subscription__c.SObjectType){
                contactList = [select id, name ,Account.Name, (select id from  Contact.Contact_Subscription_Link__r where Subscription__r.id=:recordId) from Contact WHERE name LIKE :name ORDER BY name LIMIT 10 OFFSET :offset ];
                if(contactList.size()>0){
                    contactCount = [select count() from Contact where name LIKE :name];
                    System.debug('list size is ---> '+ contactCount);
                }
            }
            for(contact temp : contactList){
                contactResult tempCon = new contactResult();
                tempCon.contactId = temp.id;
                tempCon.contactName = temp.name;
                tempCon.accountName = temp.Account.Name;
                if(!temp.Contact_Subscription_Link__r.isEmpty())
                    tempCon.value = true;
                else
                    tempCon.value = false;
                contactResultList.add(tempCon);           
            }
            paginationResult = createContactResponse(contactResultList,page,pageSize,contactCount);
        }
        System.debug('contactResultList'+paginationResult);
        return paginationResult;
    }
    
    @AuraEnabled
    public static String updateContactSubscription(List<id> toAdd,List<id> toDelete,id recordId){
        String subscribed = 'false';
        Set<id> toAddSet=new Set<id>();
        toAddSet.addAll(toAdd);
        Set<id> toDeleteSet=new Set<id>();
        toDeleteSet.addAll(toDelete);
        List<contact_subscription_link__c> cs=[select id ,contact__c from contact_subscription_link__c where subscription__c =: recordId];   
        List<contact_subscription_link__c> toDeleteList=new List<contact_subscription_link__c>();
        List<contact_subscription_link__c> toAddList=new List<contact_subscription_link__c>();
        contact_subscription_link__c csTemp;
        system.debug(toDelete);
        for(contact_subscription_link__c temp : cs){
            if(toDeleteSet.contains(temp.contact__c)){
                toDeleteList.add(temp);
            }
            if(toAddSet.contains(temp.contact__c)){
                toAddSet.remove(temp.contact__c);
            }
        }
        for(id temp : toAddSet){
            csTemp= new contact_subscription_link__c();
            csTemp.contact__c=temp;
            csTemp.subscription__c=recordId;
            toAddList.add(csTemp);
        }
        
        System.debug('List to be unsubscribed --> '+toDeleteList);
       Database.SaveResult[] insertResult =Database.insert( toAddList, false);
        for(Database.SaveResult result : insertResult){
            if( result.isSuccess()){
                subscribed = 'true';
            }
        }
       Database.DeleteResult[] deleteResult = Database.delete( toDeleteList, false);
         for(Database.DeleteResult result : deleteResult){
            if( result.isSuccess()){
                subscribed = 'true';
            }
         }
        return subscribed;
    }
    
    public static ContactPagination createContactResponse(List<contactResult> contactResultList, Integer page, Integer pageSize, Integer contactCount){
        ContactPagination  paginationResult = new ContactPagination();
        paginationResult.contactResultList= contactResultList;
        paginationResult.page=page;
        paginationResult.pageSize = pageSize;
        if(contactCount != null)
        paginationResult.total= contactCount;
        else
        paginationResult.total= 0;    
        return paginationResult;
    }
    
    public class ContactResult {
        @AuraEnabled
        public id contactId;
        
        @AuraEnabled
        public string contactName;
        
        @AuraEnabled
        public string accountName;
        
        @AuraEnabled
        public boolean value;
    }
    
    public class ContactPagination {
        
        @AuraEnabled
        public Integer pageSize; 
        
        @AuraEnabled
        public Integer page;
        
        @AuraEnabled
        public Integer total;   
        
        @AuraEnabled
        public String searchKey;
        
        @AuraEnabled
        public List<ContactResult> contactResultList;
    }
    
}