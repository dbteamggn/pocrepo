public class AccountsController {
  @AuraEnabled
  public static List<Event> getAccounts() {
    return [select subject,type,owner.name,ActivityDate from event limit 5];
  }
}