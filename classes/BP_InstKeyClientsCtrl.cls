public class BP_InstKeyClientsCtrl {
    

    @AuraEnabled
    public static void addClient(String bpId, 
                                 List<Id> clientId) 
    {
        List<Business_Plan_Client_Relation__c> allClients = new List<Business_Plan_Client_Relation__c>();
        for(Id each: clientId){
            Business_Plan_Client_Relation__c bpcr = new Business_Plan_Client_Relation__c();
            bpcr.business_plan__c = id.valueOf(bpId);
            bpcr.Client__c = each;
            bpcr.key_client__c = true;
            allClients.add(bpcr);
        }
        insert allClients;
    }
    
    @AuraEnabled
    public static void removeClients(list<String> bpcrIds) 
    {
        list<Business_Plan_Client_Relation__c> bpcrs = [select id from Business_Plan_Client_Relation__c where id = :bpcrIds];
        if (bpcrs.size()>0){
            delete bpcrs;
        }
    }
    
     /*
     * 
     * 
     * Get the list of key clients that currently exist on the business plan.
     * Required by the main component to then pass into the serach component to exclude these ids fro the search results
     * 
     */
    @AuraEnabled
    public static list<id> getClientIdList(String bpId) 
    {
        list<id> returnIds = new list<id>();
        list<Business_Plan_Client_Relation__c> bpcrs = null;
        bpcrs = [select client__r.id from Business_Plan_Client_Relation__c where business_plan__r.id = :bpId and key_client__c = true];
        for(Business_Plan_Client_Relation__c bpcr : bpcrs){
            returnIds.add(bpcr.client__r.id);
        }
        return returnIds;
    }
    
    /*
     * 
     * 
     * Get some of of the main business plan settings that influence the client search criteria
     * 
     */
    @AuraEnabled
    public static Business_Plan__c getBPSettings(id bpId) 
    {
        list<Business_Plan__c> bps = [select country__c, BP_Territory__c from Business_Plan__c where id = :bpId limit 1];
        if (bps.size()>0){
            return bps[0];    
        } else {    
            return null;
        }
    }
    
    /*
     * 
     * 
     * Set the key_client__c to true for the specified client on the specified business plan
     * 
     */
    @AuraEnabled
    public static void setKeyClient(String bpId, 
                                         List<Id> clientId) 
    {
        list<Business_Plan_Client_Relation__c> bpcrs = [select id, key_client__c from Business_Plan_Client_Relation__c where business_plan__c = :bpId and client__c IN :clientId];
        for (Business_Plan_Client_Relation__c bpcr : bpcrs){
            bpcr.key_client__c = true;
        }
        update bpcrs;
    }
    
    /*
     * 
     * 
     * Uncheck key_client__c on the specified business plan client relation rows
     * 
     */
    @AuraEnabled
    public static void removeKeyClient(list<String> bpcrIds) 
    {
        list<Business_Plan_Client_Relation__c> bpcrs = [select id, key_client__c from Business_Plan_Client_Relation__c where id = :bpcrIds];
        for (Business_Plan_Client_Relation__c bpcr : bpcrs){
            bpcr.key_client__c = false;
        }
        update bpcrs;
    }
}