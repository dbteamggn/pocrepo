public class CampaignTeam_Utility {
    /* The postCampaignUpdates method posts the campaign notifications to the campaingn and contact chatter. */
    
    public static void postCampaignUpdates(TriggerHandler.TriggerParameter tParam){
        Set<String> emailIDs = new Set<String>();
        List<Campaign_Team__c> newCampaignTeamList = (List<Campaign_Team__c>)tParam.newList;
   
        Map<ID, Campaign_Team__c> oldMap = (Map<ID,Campaign_Team__c>) tParam.oldMap;
        
        Map<ID, ID> campaignTeamContactMap = new Map<ID,ID>();
        Map<ID,ID> campaignTeamCampaignMap = new Map<ID,ID>();
        Map<ID, Campaign> campaignsMap = new Map<ID, Campaign>();
        Map<ID, Contact> contactsMap = new Map<ID, Contact>();
        Map<ID, Campaign_Team__c> campaignTeamMap = new Map<ID,Campaign_Team__c>();
        Map<ID,String> campaignIDEmail = new Map<ID,String>();
        Map<Id,Contact> newContactMap;
        Map<ID,Campaign> newCampaignMap;
//Build the campaignTeamContactMap, campaignTeamCampaignMap and campaignTeamMap from the Trigger.new.
        try{
        if(!newCampaignTeamList.isEmpty() ){
            
            for(Campaign_Team__c ct : newCampaignTeamList){
                campaignTeamContactMap.put(ct.id,  ct.Team_Member__c);
                campaignTeamCampaignMap.put(ct.id, ct.Campaign__c);
                campaignTeamMap.put(ct.id, ct);
                
            }
        }
        
        //Query campaigns and contacts using the id's from campaignTeamCampaign and campaignTeamContact maps.
        if(campaignTeamCampaignMap.values() != null)
        	newCampaignMap = new Map<ID, Campaign>( [select id, name from Campaign where id IN : campaignTeamCampaignMap.values()]);
        if(campaignTeamContactMap.values() != null)
       	  	newContactMap = new Map<ID, Contact>( [select id, email, lastname, User__r.id, user__r.email,user__r.isActive from Contact where id IN : campaignTeamContactMap.values()]);
        
        for(ID campaignTeamID : campaignTeamMap.keySet()){
            Campaign_Team__c campaignTeam = campaignTeamMap.get(campaignTeamID);
            Contact contact = newContactMap.get(campaignTeamContactMap.get(campaignTeamID));
            Campaign campaign = newCampaignMap.get(campaignTeamCampaignMap.get(campaignTeamID));
            FeedResponse response = new FeedResponse();
            if(oldMap != null){
                if(oldMap.get(campaignTeamID) != null){
                    // Verify if the user has updated to allow notifications on the feed or 
                    // whether the contact has been changed for a particular team.
                    if((campaignTeam.Notification__c == true && !oldMap.get(campaignTeamID).Notification__c ) 
                       || ( campaignTeam.Team_Member__c != oldMap.get(campaignTeamID).Team_Member__c ) ){
                           response = postCampaignTeamFeed(contact,campaign);
                       }
                }
            }else{
                response = postCampaignTeamFeed(contact,campaign);
            }
            //Seperate the contacts with no user linked to send email updates instead of chatter posts
            if(response.feedPosted == false && response.isUser==false){
                emailIDs.add(contact.email);
                campaignIDEmail.put(campaignTeamID, contact.email);
            }
        }
        
        Profile p = [select id from Profile where name ='Chatter Free User'];
        List<User> users = new List<User>();
        users = [select id, email from User where email In :emailIDs and profileId =:p.id ];
        
       //Link the contact and chatter free user with similar email 
       //to send emails when the contact is added as a campaign team member
        if(users.size()> 0){
            for(ID campaignTeamID : campaignIDEmail.keySet()){
                for (User u : users){
                    if( campaignIDEmail.get(campaignTeamID) == u.email){
                        Campaign_Team__c campaignTeam = campaignTeamMap.get(campaignTeamID);
                        campaignTeam.Chatter_User_Email__c = u.email;
                    }
                }
            }
        }
        }catch(Exception ex ){
            System.debug('Exception details in CampainTeam_Utility ---> '+ ex);
            CommonUtilities.createExceptionLog(ex);
        }
    }
    //This method constructs feed message and posts it on the campaign record page.
    public static FeedResponse postCampaignTeamFeed(Contact contact, Campaign campaign){
        // Initializing the class to post a feed.
        ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
        ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
        ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
        ConnectApi.FeedElement feedElement;    
        ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
        messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
        FeedResponse response = new FeedResponse();
        try{
        if(contact != null && Campaign != null){
        //If a user is linked to a contact post the feed updates in the campaign record page.
        if(contact.User__r == null ){
            response.isUser = false;
            response.feedPosted = false;
        }else if(contact.User__r.isActive){
            if(contact.email != null){
                textSegmentInput.text=Label.Campaign_Team_Chatter_Text +' '+ contact.email;
            }else{
                textSegmentInput.text=Label.Campaign_Team_Chatter_Text;
            }
            feedItemInput.subjectId =  campaign.id;
            
            messageBodyInput.messageSegments.add(textSegmentInput);
           
            feedItemInput.body = messageBodyInput;
            feedItemInput.feedElementType= ConnectApi.FeedElementType.FeedItem;
            /* Post to the feed by calling postFeedElement method */                    
            feedElement = ConnectApi.ChatterFeeds.postFeedElement(null,feedItemInput);
            response.feedPosted = true;
        }
            }
        }catch(Exception ex){
            System.debug('Exception details from the trigger CampaignTeam_Trigger --> '+ ex);
            CommonUtilities.createExceptionLog(ex);
        }
        return response;
    }
    Public class FeedResponse{
        public boolean isUser;
        public boolean feedPosted;
    }
}