public without sharing class UtilsDate {
    
    public static string getPreviousYearName() {
    
        date dt = system.today().addYears(-1);
        
        return string.valueof(dt.year()); 
    
    }
    
    public static string getPreviousPreviousYearName() {
    
        date dt = system.today().addYears(-2);
        
        return string.valueof(dt.year());
    
    }

}