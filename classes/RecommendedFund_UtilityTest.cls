/*----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            RecommendedFund_UtilityTest
   Description:     Test Methods for RecommendedFund__c triggers
                    
      Date               Author                             Summary of Changes                  
   -----------      -----------------                  -------------------------------------------------------------------------------------
   06 OCT 2016         Dilipkumar                            Initial Release
------------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
public class RecommendedFund_UtilityTest {
   /*
     * Description : Create new Clients and assigns the hierarchy TC4->TC3->TC2->TC1->NULL
     * Param : Number of Clients to create
     * Returns :  List of Clients
    */
    public static Account createClients(Integer count, String clientName){
        System.debug('Enter client creation '+clientName);
        Account acc0 = new Account(Name = 'Test Account', Billing_PostalCode__c = '505050', Hierarchy_Level__c = 'L0');
        
        List<Account> testClients = new List<Account>();
        Account testClient = new Account();
        for(Integer iter=0;iter<count;iter++){
            testClient = CommonTestUtils.CreateTestClient('Test Client '+clientName+(iter+1));
            testClients.add(testClient);
        }
        Database.SaveResult[] resClients = Database.insert(testClients);
        List<Id> insID = new List<Id>();
        for(Database.SaveResult srIter : resClients){
            insID.add(srIter.getID());
        }
        List<Account> insClients = [Select id, Name, parentId from Account where id IN:insID];
        System.debug('INS Clients =====>> '+ insClients);
        //Creating Hierarchy TC4->TC3->TC2->TC1->NULL
        for(Integer iter = 0; iter< insClients.size() - 1; iter++){
            
            insClients.get(iter).ParentId = acc0.Id;
        }
        System.debug('The new list of 3 --> ' + insClients[3].parentId);
        Database.update( insClients);
        return insClients[3];
    }
    
    /*
     * Description : Create new Price book entry and add a prodcut to it
     * Param : Product name
     * Returns :  Product
    */
    public static Product2 createCustomPB(String prdName){
        // Pricebook2 custPB = new Pricebook2(Name=prdName,isActive=True,isStandard=True);
        //insert custPB;
        System.debug('PB creation '+prdName);
        Product2 prod=new Product2(Name=prdName,IsActive=True);
        insert prod;
        Id priceBookId = Test.getStandardPricebookId();
        //Creating a price book entry for standard price book and created product
        PricebookEntry sPrice = new PricebookEntry(IsActive = True,Pricebook2ID = priceBookId,Product2Id = prod.Id,UnitPrice=100.0);
        insert sPrice;
        return prod;
        
    }
    
    /*
     * Description : Create new Client Fund association
     * Param : Number of clients to create, Product name
     * Returns :  Client Fund association list
    */
  public static List<Client_Fund__c> createClientFunds(Integer noClients, String clientName, String prdName){
    System.debug('Client fund creation');
        Account testClients = createClients(noClients,clientName);
    Product2 testProduct = createCustomPB(prdName);
        //Create association between funds and clients
        List<Client_Fund__c> cFunds = new List<Client_Fund__c>();
    Client_Fund__c cFund = new Client_Fund__c();
            cFund.Client__c = testClients.ID;
      cFund.Product__c = testProduct.Id;
      
      cFunds.add(cFund);
        
    return cFunds;
  }
    
   /*
     * Description : Test method to create new Client Fund association and insert it
     * Param : 
     * Returns :  
    */
    public static testMethod void relateFundClient(){
    System.debug('Client fund insert test');
        Test.startTest();
        List<Client_Fund__c> cFunds1 = createClientFunds(4,'clientList1','Test PRD1');
        insert cFunds1;
        Test.stopTest();
        delete cFunds1;
    }
    
   /*
     * Description : Test method to create new Client Fund association and delete it
     * Param : 
     * Returns :  
    */
    public static testMethod void deleteFundClient(){
    System.debug('Client fund delete test');
        Test.startTest();
        List<Client_Fund__c> cFunds2 = createClientFunds(4,'clientList2','Test PRD2');
        System.debug('Account 2 -------> '+cFunds2);
        insert cFunds2;    
        delete cFunds2;
        Test.stopTest();
         
  }
}