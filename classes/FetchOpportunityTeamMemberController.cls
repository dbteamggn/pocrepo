public class FetchOpportunityTeamMemberController {
    
    private Map<String,String> oppTeamRoleMembersMap;
    private Id oppId;
    public string callingTemplateName{get;set;}
    
    public id getoppId(){
         return oppId; 
    } 

    public void setoppId(id oppRecID){
        if (oppRecID!= null){
          oppId = oppRecID;
        }
   }
       
    public FetchOpportunityTeamMemberController() {
    
    }

    public Map<String,String> getOppTeamRoleMembersMap() {
    
        List<String> teamRoleList = new List<String>();
        
        //get team roles from the custom setting depending on the calling email template - US30259 : START
        if(callingTemplateName.equalsIgnoreCase('Opportunity_Stage_Won_NF_Finals')){
            teamRoleList = Configuration_Util__c.getInstance('ET Team Roles Opp Won NF & Finals')!= null ? Configuration_Util__c.getInstance('ET Team Roles Opp Won NF & Finals').value__c.split(',') : 'Sales Team Member,Consultant Relations,Investment Team'.split(',');
        }
        else if(callingTemplateName.equalsIgnoreCase('Opportunity_Won_Notification')){
            teamRoleList = Configuration_Util__c.getInstance('Email Template Team Roles')!= null ? Configuration_Util__c.getInstance('Email Template Team Roles').value__c.split(',') : 'Senior Sales,Consultant Relations,Product Manager'.split(',');
        }
        //get team roles from the custom setting depending on the calling email template - US30259 : END
        
        oppTeamRoleMembersMap = new Map<String,String>();
        
        //get Team Members related to the opportunity based on the team roles in the custom setting
        for(OpportunityTeamMember oppTeamMemberRec : [Select id,TeamMemberRole,Name from OpportunityTeamMember where OpportunityId =:oppID and TeamMemberRole IN: teamRoleList]){
            
            if(oppTeamRoleMembersMap != null){
                if(oppTeamRoleMembersMap.containsKey(oppTeamMemberRec.TeamMemberRole) && oppTeamRoleMembersMap.get(oppTeamMemberRec.TeamMemberRole) != null){                    
                    oppTeamRoleMembersMap.put(oppTeamMemberRec.TeamMemberRole,oppTeamRoleMembersMap.get(oppTeamMemberRec.TeamMemberRole) + ', ' + oppTeamMemberRec.Name);
                }else if(!oppTeamRoleMembersMap.containsKey(oppTeamMemberRec.TeamMemberRole)){
                    oppTeamRoleMembersMap.put(oppTeamMemberRec.TeamMemberRole,oppTeamMemberRec.Name);
                }
            }
        }
        return oppTeamRoleMembersMap;        
    }
}