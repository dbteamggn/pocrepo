public class ContactSubscriptionListController {
    
    /* The annotation @AuraEnabled is used to access the method in lightning.
The method getAllSubscriptions is used to fetch all the subscriptions linked to a contact(recordID). 
This method is called on page load.
*/           
    @AuraEnabled
    public static SubscriptionResponse getAllSubscriptions(string recordId, Decimal pageNumber) {
        
        System.debug('recordId'+recordId);
        List<SubscriptionResult> subscriptionResultList = new List<SubscriptionResult>();
        SubscriptionResponse paginationResult ;
        Integer pageSize = 10;
        Integer page = (Integer)pageNumber;
        Integer offset = (page-1) * pageSize;
        Integer subscriptionCount = 0;
        try{
            List<Contact_Subscription_Link__c> contactSubscriptionList = new List<Contact_Subscription_Link__c>();
            List<Contact> contactList = new List<Contact>();
            List<Subscription__c> subscriptionList = new List<Subscription__c>();
            if(recordId!=null && recordId != ''){
                if(Id.valueOf(recordId).getSObjectType() == Schema.Contact.SObjectType){
                    contactSubscriptionList = [select id, Subscription__r.id, Subscription__r.Content__c, Subscription__r.Mailing_Name__c, Subscription__r.Subscription_Name__c,  Subscription__r.Subscription_Live_Start_Date__c, 
                        Subscription__r.Limited_by_Countries__c , Subscription__r.Subscription_Name_ISM__c, Subscription__r.Name, Subscription__r.Subscription_Live_End_Date__c
                        from Contact_Subscription_Link__c where Contact__r.id=:recordId ORDER BY Subscription__r.name LIMIT :pageSize OFFSET :offset ];
                    if(contactSubscriptionList.size()>0){
                        subscriptionCount = [select count() from Contact_Subscription_Link__c where Contact__r.id =:recordId ];
                        System.debug('list size is ---> '+ subscriptionCount);
                        for(Contact_Subscription_Link__c temp : contactSubscriptionList){
                            SubscriptionResult tempCon = new SubscriptionResult();
                            tempCon.subscriptionId = temp.Subscription__r.id;
                            tempCon.subscriptionNumber = temp.Subscription__r.name;
                            tempCon.subscriptioncountries = temp.Subscription__r.Limited_by_Countries__c;
                            tempCon.subscriptionName = temp.Subscription__r.Subscription_Name__c;
                            tempCon.subscriptionNameISM = temp.Subscription__r.Subscription_Name_ISM__c;
                            tempCon.subscriptionType = '';
                            tempCon.subscriptionMN = temp.Subscription__r.Mailing_Name__c;
                            tempCon.subscriptionLiveStartDate = temp.Subscription__r.Subscription_Live_Start_Date__c;
                            tempCon.subscriptionLiveEndDate = temp.Subscription__r.Subscription_Live_End_Date__c;
                            tempCon.content = temp.Subscription__r.Content__c;
                            tempCon.value = true;                
                            subscriptionResultList.add(tempCon);           
                        }
                        
                    }
                }
                paginationResult = createContactResponse(subscriptionResultList,page,pageSize,subscriptionCount);
                
            }
            /* Added for 100% code coverage, runs only during test */
            /*if(Test.isRunningTest()){
                throw new TestException();
            }*/
            
        }catch(Exception e){
            System.debug('Exception Details are --> '+e);
            CommonUtilities.createExceptionLog(e);
        }
        System.debug('subscriptionResultList'+paginationResult);
        return paginationResult;
    }
    /* The getSubscriptionsByName method is used to query subscriptions based on the subscription name searched by the user
searchKey : Subscription Name
recordId  : Contact ID
pageNumber: Used to for pagination.
*/    
    @AuraEnabled
    public static SubscriptionResponse getSubscriptionsByName(String searchKey,string recordId, Decimal pageNumber) {
        System.debug('recordId'+recordId);
        List<SubscriptionResult> subscriptionResultList = new List<SubscriptionResult>();
        SubscriptionResponse paginationResult ;
        String name = '%' + searchKey + '%';
        Integer pageSize = 10;
        Integer page = (Integer)pageNumber;
        Integer offset = (page-1) * pageSize;
        Integer subscriptionCount ;
        List<Contact> contactList = new List<Contact>();
        List<Subscription__c> subscriptionList = new List<Subscription__c>();
        id AccId;
        try{
            if(recordId!=null && recordId != ''){
                if(Id.valueOf(recordId).getSObjectType() == Schema.Contact.SObjectType){
                    subscriptionList = [select id, name , Subscription_Name__c, Mailing_Name__c, Content__c, 
                                            Limited_by_Countries__c, (select id from  Subscription__c.Contact_Subscription_Link__r where Contact__r.id=:recordId) from Subscription__c WHERE 
                                            (name LIKE :name OR Subscription_Name_ISM__r.Name LIKE :Name OR Subscription_Name__c LIKE :Name OR Content__c LIKE :Name OR Mailing_Name__c LIKE :Name)
                                             ORDER BY name LIMIT :pageSize OFFSET :offset ];
                    if(subscriptionList.size()>0){
                        subscriptionCount = [select count() from Subscription__c where name LIKE :name OR Subscription_Name_ISM__r.Name LIKE :Name OR Subscription_Name__c LIKE :Name OR Content__c LIKE :Name OR Mailing_Name__c LIKE :Name];
                        System.debug('list size is ---> '+ subscriptionCount);
                        for(Subscription__c temp : subscriptionList){
                            subscriptionResult tempCon = new subscriptionResult();
                            tempCon.subscriptionId = temp.id;
                            tempCon.subscriptionNumber = temp.name;
                            tempCon.subscriptionCountries = temp.Limited_by_Countries__c;
                            tempCon.subscriptionName = temp.Subscription_Name__c;
                            tempCon.subscriptionType = '';
                            tempCon.subscriptionMn = temp.Mailing_Name__c;
                            tempCon.content = temp.Content__c;
                            if(!temp.Contact_Subscription_Link__r.isEmpty())
                                tempCon.value = true;
                            else
                                tempCon.value = false;
                            subscriptionResultList.add(tempCon);           
                        }
                    }
                }
                
                paginationResult = createContactResponse(subscriptionResultList,page,pageSize,subscriptionCount);
                 /* Added for 100% code coverage, runs only during test */
                /*if(Test.isRunningTest()){
                    throw new TestException();
                }*/
            }
        }catch(Exception e){
            System.debug('Exception Details from getContactsByName function are ---> '+e);
            CommonUtilities.createExceptionLog(e);
        }
        System.debug('subscriptionResultList'+paginationResult);
        return paginationResult;
    }
    /* The updateContactSubscription method updates links a new subscription or removes the link from a contact */    
    @AuraEnabled
    public static string updateContactSubscription(List<id> toAdd,List<id> toDelete,id recordId){
        String subscribed = 'false';
        
        // By using Save Point we can rollback to a stable state in case of an exception
        SavePoint sp;
        Set<id> toAddSet=new Set<id>();
        Set<id> toDeleteSet=new Set<id>();
        List<contact_subscription_link__c> cs=[select id ,Subscription__c from contact_subscription_link__c where contact__r.id =: recordId];   
        List<contact_subscription_link__c> toDeleteList=new List<contact_subscription_link__c>();
        List<contact_subscription_link__c> toAddList=new List<contact_subscription_link__c>();
        contact_subscription_link__c csTemp;
        SubscriptionResponse paginationResult;
        SubscriptionStatus subscriptionStatus = new SubscriptionStatus();
        List<SubscriptionResult> subscriptionResultList = new List<SubscriptionResult>();
        
        if(toAdd != null && toAdd.size()> 0){
            
            toAddSet.addAll(toAdd);
        }
        
        if(toDelete != null && toDelete.size()> 0){
            toDeleteSet.addAll(toDelete);
        }
        
        for(contact_subscription_link__c temp : cs){
            if(toDeleteSet.contains(temp.Subscription__c)){
                toDeleteList.add(temp);
            }
            if(toAddSet.contains(temp.Subscription__c)){
                toAddSet.remove(temp.Subscription__c);
            }
        }
        for(id temp : toAddSet){
            csTemp= new contact_subscription_link__c();
            csTemp.Subscription__c=temp;
            csTemp.Contact__c=recordId;
            toAddList.add(csTemp);
        }
        
        System.debug('List to be unsubscribed --> '+toDeleteList);
        try{
            System.debug('Before Setting save point');
            sp = Database.setSavepoint();
            if(!toAddList.isEmpty()){
                /*if(Test.isRunningTest()){
                    contact_subscription_link__c a = new contact_subscription_link__c();
                    toAddList.add(a);
                }*/
                Database.SaveResult[] insertResult =Database.insert( toAddList, true);
                
                for(Database.SaveResult result : insertResult){
                    if( result.isSuccess()){
                        subscribed = 'true';
                    }else{
                        subscribed = 'false';
                    }
                }
            }else{
                subscribed = 'true';
            }
            if(!toDeleteList.isEmpty()){
                Database.DeleteResult[] deleteResult = Database.delete( toDeleteList, true);
                for(Database.DeleteResult result : deleteResult){
                    if( result.isSuccess()){
                        subscribed = 'true';
                    }else{
                        subscribed = 'false';
                    }
                }
            }else{
                subscribed = 'true';
            }
        }catch(DMLException dmlEx){
            System.debug('Exception details are --> '+ dmlEx);
            subscribed ='false';
            
          // In case of exception roll back to the save point.
            Database.rollback(sp);
            CommonUtilities.createExceptionLog(dmlEx);            
           
        }
        return subscribed;
    }
    
    /* The verifyCountries method is used to check if the contact mailing is in sSubscription  limited by countries */
    
    @AuraEnabled 
    public static CheckCountries verifyCountries(String subscriptionId, String recordId, String limitedCountries){
        System.debug('*******Subscription Id is ----> '+subscriptionId);
        System.debug('*******contact rec Id is ----> '+recordId);
        System.debug('*******countries  is ----> '+limitedCountries);
        
        Id rId;
        Id sId;
        if(subscriptionId != null)
            sId = (Id)subscriptionId;
        if(recordId !=null)
            rId = (Id)recordId;
        String countriesMatched = 'true';
        CheckCountries countries = new CheckCountries();
        Contact contact;
        List<Subscription__c> subscriptionObject;
        try{
            if(recordId != null)
                System.debug('******record id again: '+recordID);
            contact = [select id, name,Primary_Country__c from Contact where id =:rId limit 1];
            System.debug('*******contact queried  is ----> '+contact);
            System.debug('******countries matched before: '+countriesMatched);
            
            if(!String.isBlank(contact.Primary_Country__c)){
                System.debug('*******Primary_Country__c is blank---->');
                if(!String.isBlank(limitedCountries)){
                    System.debug('*******limitedCountries is blank---->');
                    if(!limitedCountries.contains(contact.Primary_Country__c)){
                        countriesMatched = 'false';                         
                    }
                }
            }
            else {
                if(!String.isBlank(limitedCountries)){
                    countriesMatched ='false';
                }
            } 
            System.debug('******countries matched: '+countriesMatched);
            countries.countriesMatched = countriesMatched;
            countries.subscriptionId = sId;
        }catch(Exception e){
            CommonUtilities.createExceptionLog(e);    
        }
        
        return countries;
    }
    
    /* The createContactResponse method is used to build a response object sent to the Lightning */
    
    public static SubscriptionResponse createContactResponse(List<SubscriptionResult> subscriptionResultList, Integer page, Integer pageSize, Integer contactCount){
        SubscriptionResponse  paginationResult = new SubscriptionResponse();
        
        paginationResult.subscriptionResultList= subscriptionResultList;
        paginationResult.page=page;
        paginationResult.pageSize = pageSize;
        if(contactCount != null)
        paginationResult.total= contactCount;
        else
         paginationResult.total= 0;
        return paginationResult;
    }
    
    
    /* The below inner classes are used to build response objects */    
    
    public class SubscriptionResult {
        @AuraEnabled
        public id subscriptionId;
        
        @AuraEnabled
        public String subscriptionNumber;
        
        @AuraEnabled
        public String subscriptionMN;
        
        @AuraEnabled
        public String content;
        
        @AuraEnabled
        public String subscriptionName;
        
        @AuraEnabled
        public String subscriptionNameISM;
        
        @AuraEnabled
        public String subscriptioncountries;
        
        @AuraEnabled
        public String subscriptionLiveStartDate;
        
        @AuraEnabled
        public String subscriptionLiveEndDate;
        
        @AuraEnabled
        public string subscriptionType;
        
        @AuraEnabled
        public string accountName;
        
        @AuraEnabled
        public boolean value;
    }
    
    
    public class SubscriptionResponse {
        
        @AuraEnabled
        public Integer pageSize; 
        
        @AuraEnabled
        public Integer page;
        
        @AuraEnabled
        public Integer total;   
        
        @AuraEnabled
        public String searchKey;
        
        @AuraEnabled
        public List<SubscriptionResult> subscriptionResultList;
    }
    
    public class SubscriptionStatus{
        
        @AuraEnabled
        public List<SubscriptionResult> subscriptionResultList;
        
        @AuraEnabled
        public string subscribed;
    }
    public class CheckCountries{
        @AuraEnabled
        public String countriesMatched;
        
        @AuraEnabled
        public Id subscriptionId;
    }
    
}