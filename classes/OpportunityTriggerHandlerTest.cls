@isTest
public class OpportunityTriggerHandlerTest {
    
    /*
     * 
     * Description : Test various scenarios that may or may not create/update Rated Strategies when opportunities are created
     * Param : 
     * Returns :  
    */
    private static testMethod void testRatedStrategyAfterOppInsert() {
      
        User user = CommonTestUtils.createTestUser();        
        
        System.runAs(user){
            
            // Create a Third Party & a Client
            list<account> accounts = new list<account>();
            account client = CommonTestUtils.CreateTestClient('Client');
            accounts.add(client);
            account thirdParty = CommonTestUtils.CreateTestThirdParty('Third Party');
            accounts.add(thirdParty);
            insert accounts;
            
            // Create 3 Investment Strategies
            list<Investment_Strategy__c> investmentStrategies = new list<Investment_Strategy__c>();
            for (integer i=0; i < 3; i++){
                investmentStrategies.add(CommonTestUtils.CreateTestInvestmentStrategy('Inv.Strategy ' + (i<10?'00'+ i:(i<100)?'0'+i: '' + i)));
            }
            insert investmentStrategies; 
            
            // Create Rated Strategies for the first investment Strategies.
            // Set the Current_Rating__c to be 'Buy'
            list<Rated_Strategy__c> ratedStrategies = new list<Rated_Strategy__c>();
            for (integer i=0; i < 1; i++){
                ratedStrategies.add(CommonTestUtils.CreateTestRatedStrategy(thirdParty.id, investmentStrategies[i].id, 'Buy'));
            }
            insert ratedStrategies;
            
            // Create some opportunities.  These will create/update strategies if all of the following are all true:-
            // a) the record type is 'New Strategy Rating'
            // b) Stage is Rated or Non-Rated
            // c) it has an investment strategy
            // d) it is for a third party (rather than a client)
            list<opportunity> opportunities = new list<opportunity>();
            // Create the opportunities that will NOT create Rated Strategies
            opportunity opp1 = CommonTestUtils.CreateTestOpportunityMandate('Mandate', thirdParty.id, null, 'Rated'); // Should not create anything as Mandate
            opp1.Investment_Strategy__c = investmentStrategies[1].id;
            opportunities.add(opp1);
            opportunity opp2 = CommonTestUtils.CreateTestOpportunityRatedStrat('RS 01', thirdParty.id, null, 'In Progress', null); // Should not create anything as not Rated nor Not Rated
            opp2.Investment_Strategy__c = investmentStrategies[1].id;
            opportunities.add(opp2);
            // Removed as validation rule now handles this scenario
            //opportunity opp3 = CommonTestUtils.CreateTestOpportunityRatedStrat('RS 02', thirdParty.id, null, 'Rated', null); // Should not create anything as no Investment Strategy
            //opportunities.add(opp3);
            opportunity opp4 = CommonTestUtils.CreateTestOpportunityRatedStrat('RS 03', client.id, null, 'In Progress', null); // Should not create anything as for Client
            opp4.Investment_Strategy__c = investmentStrategies[1].id;
            opportunities.add(opp4);
            
            // Create an opportuntity that should work, where no Rated Strategy currently exists 
            opportunity opp5 = CommonTestUtils.CreateTestOpportunityRatedStrat('RS 04', thirdParty.id, null, 'In Progress', 'Hold'); 
            opp5.Investment_Strategy__c = investmentStrategies[2].id;
            opportunities.add(opp5);
            // Create an opportuntity that should work, where Rated Strategy does currently exist
            opportunity opp6 = CommonTestUtils.CreateTestOpportunityRatedStrat('RS 05', thirdParty.id, null, 'In Progress', 'Sell'); 
            opp6.Investment_Strategy__c = investmentStrategies[0].id;
            opportunities.add(opp6);
            
            list<Rated_Strategy__c> ratedStrategyPreCheck = [select id, third_party__c, investment_strategy__c, current_rating__c from rated_strategy__c];
            system.assertEquals(1, ratedStrategyPreCheck.size());
            
            //Test the Scheduler Class
            Test.StartTest();
            insert opportunities;           
            Test.StopTest(); 
            
            list<Rated_Strategy__c> ratedStrategyResults1 = [select id, third_party__r.id, current_rating__c, 
                                                             Third_Party__r.name, investment_strategy__r.name 
                                                             from rated_strategy__c 
                                                             order by Third_Party__r.name, investment_strategy__r.name];
            
            // check that only 1 rows are returned
            system.assertEquals(1, ratedStrategyResults1.size());
            
            // Loop through results & check values
            for (integer i = 0; i<ratedStrategyResults1.size(); i++ ){
                Rated_Strategy__c rs = ratedStrategyResults1[i];
                if (rs.investment_strategy__r.name == 'Inv.Strategy 000'){
                  system.assertEquals(thirdParty.id, rs.third_party__r.id);
                    system.assertEquals('Buy', rs.current_rating__c);
                } else if (rs.investment_strategy__r.name == 'Inv.Strategy 002'){
                  system.assertEquals(thirdParty.id, rs.third_party__r.id);
                    system.assertEquals('Hold', rs.current_rating__c);
                } 
            }
        }
    }
    
    /*
     * 
     * Description : Test various scenarios that may or may not create/update Rated Strategies when opportunities are edited
     * Param : 
     * Returns :  
    */
    private static testMethod void testRatedStrategyAfterOppUpdate() {
      
        User user = CommonTestUtils.createTestUser();        
        
        System.runAs(user){
            
            // Create a Third Party & a Client
            list<account> accounts = new list<account>();
            account client = CommonTestUtils.CreateTestClient('Client');
            accounts.add(client);
            account thirdParty = CommonTestUtils.CreateTestThirdParty('Third Party');
            accounts.add(thirdParty);
            insert accounts;
            
            // Create 3 Investment Strategies
            list<Investment_Strategy__c> investmentStrategies = new list<Investment_Strategy__c>();
            for (integer i=0; i < 3; i++){
                investmentStrategies.add(CommonTestUtils.CreateTestInvestmentStrategy('Inv.Strategy ' + (i<10?'00'+ i:(i<100)?'0'+i: '' + i)));
            }
            insert investmentStrategies; 
            
          
            // Create some opportunities.  These will create/update strategies if all of the following are all true:-
            // a) the record type is 'New Strategy Rating'
            // b) Stage is Rated or Non-Rated
            // c) it has an investment strategy
            // d) it is for a third party (rather than a client)
            list<opportunity> opportunities = new list<opportunity>();
           
            // Create opportunities to edit as part of the test
            opportunity opp1 = CommonTestUtils.CreateTestOpportunityRatedStrat('RS 01', thirdParty.id, null, 'In Progress', 'Hold'); 
            opp1.Investment_Strategy__c = investmentStrategies[0].id;
            opportunities.add(opp1);

            opportunity opp2 = CommonTestUtils.CreateTestOpportunityRatedStrat('RS 02', thirdParty.id, null, 'In Progress', 'Sell'); 
            opp2.Investment_Strategy__c = investmentStrategies[1].id;
            opportunities.add(opp2);

            opportunity opp3 = CommonTestUtils.CreateTestOpportunityRatedStrat('RS 03', thirdParty.id, null, 'In Progress', 'Buy'); 
            opp3.Investment_Strategy__c = investmentStrategies[2].id;
            opportunities.add(opp3);
            
            insert opportunities;
            
            Opportunity_Utility testInst = new Opportunity_Utility();
            testInst.getOpportunities(new Set<Id>{opportunities[0].Id, opportunities[1].Id});
            
            list<Rated_Strategy__c> ratedStrategyPreCheck = [select id, third_party__r.id, investment_strategy__r.name, current_rating__c from rated_strategy__c];
            system.assertEquals(0, ratedStrategyPreCheck.size());
            // Loop through pre-check results & check values
            for (integer i = 0; i<ratedStrategyPreCheck.size(); i++ ){
                Rated_Strategy__c rs = ratedStrategyPreCheck[i];
                if (rs.investment_strategy__r.name == 'Inv.Strategy 000'){
                  system.assertEquals(thirdParty.id, rs.third_party__r.id);
                    system.assertEquals('Hold', rs.current_rating__c);
                } else if (rs.investment_strategy__r.name == 'Inv.Strategy 001'){
                  system.assertEquals(thirdParty.id, rs.third_party__r.id);
                    system.assertEquals('Sell', rs.current_rating__c);
                } 
            }
            
            opp1.Target_Rating__c = 'Buy+';  // Should update Rated Strategy
            opp2.StageName = 'In Progress'; // Should not update
            opp3.Target_Rating__c = 'Sell';
            opp3.StageName = 'In Progress'; // Should update
            
            //Test the Scheduler Class
            Test.StartTest();
            update opportunities;           
            Test.StopTest(); 
            
            list<Rated_Strategy__c> ratedStrategyResults1 = [select id, third_party__r.id, current_rating__c, 
                                                             Third_Party__r.name, investment_strategy__r.name 
                                                             from rated_strategy__c 
                                                             order by Third_Party__r.name, investment_strategy__r.name];
            
            system.assertEquals(0, ratedStrategyResults1.size());
            
            // Loop through results & check values
            for (integer i = 0; i<ratedStrategyResults1.size(); i++ ){
                Rated_Strategy__c rs = ratedStrategyResults1[i];
                if (rs.investment_strategy__r.name == 'Inv.Strategy 000'){
                  system.assertEquals(thirdParty.id, rs.third_party__r.id);
                    system.assertEquals('Buy+', rs.current_rating__c);
                } else if (rs.investment_strategy__r.name == 'Inv.Strategy 001'){
                  system.assertEquals(thirdParty.id, rs.third_party__r.id);
                    system.assertEquals('Sell', rs.current_rating__c);
                } else if (rs.investment_strategy__r.name == 'Inv.Strategy 002'){
                  system.assertEquals(thirdParty.id, rs.third_party__r.id);
                    system.assertEquals('Sell-', rs.current_rating__c);
                } 
            }
        }
    }
}