public class OpportunityTriggerHandler extends TriggerHandler{
    protected override void beforeInsert(){
        Opportunity_Utility util = Opportunity_Utility.getInstance();
        util.beforeInsert(triggerParams.newList);
    }
    
    protected override void afterInsert(){
        // Create/Update Rated_Strategy records
        createRatedStrategies(triggerParams.newMap);
    }
    
    protected override void afterUpdate(){
        // Create/Update Rated_Strategy records
        createRatedStrategies(triggerParams.newMap);
    }
    
    public void createRatedStrategies(Map<Id, sObject> newMap){
                
        // Loop through the trigger new map & build up a set of the account ids
        // that we need to check for existing rated strategies.
        // Only interested in ones that are Rated/Not Rated, have an investment strategy & are 'New Strategy Rating'
        
        set<id> oppAccountIds = new set<id>();
        set<opportunity> oppsToProcess = new set<opportunity>();
        for (id id : newMap.keySet()){
            Opportunity tempOpp = (Opportunity)newMap.get(id);
            if ((tempOpp.StageName == 'Rated' || tempOpp.StageName == 'Not Rated') 
                && tempOpp.Investment_Strategy__c != null 
                && tempOpp.recordTypeId == Schema.SObjectType.opportunity.getRecordTypeInfosByName().get('Strategy Rating').getRecordTypeId()){
                
                if(!String.isBlank(tempOpp.AccountId)){
                    oppAccountIds.add(tempOpp.accountId);
                }
                oppsToProcess.add(tempOpp);
            }
        }
        
        // Get those accounts & their current Rated Strategies    
        // & build a map of <accountId, map<Investment_Strategy.id, Rated_Strategy__c>> for those that are third Parties    
        map<account, map<id, Rated_Strategy__c>> accountRatedStrategyMap = new map<account, map<id, Rated_Strategy__c>>();
        map<id, account> accounts = new map<id, account>([select id, recordType.developerName, (select id, investment_strategy__r.id, current_rating__c from Rated_Strategies__r ) 
                                                            from account 
                                                            where id = :oppAccountIds
                                                         ]);
        
        for (id accountId : accounts.keySet())
        {
            account account = accounts.get(accountId);
            map<id, Rated_Strategy__c> ratedStrategyMap = new map<id, Rated_Strategy__c>();
            for (Rated_Strategy__c  ratedStrategy : account.Rated_Strategies__r){
                ratedStrategyMap.put(ratedStrategy.investment_strategy__r.id, ratedStrategy);
            }
            accountRatedStrategyMap.put(account, RatedStrategyMap);             
        }
        
        // Now loop through the trigger map & check to see if the Rated Strategy already exists
        list<Rated_Strategy__c> insertRatedStrategies = new list<Rated_Strategy__c>();
        list<Rated_Strategy__c> updateRatedStrategies = new list<Rated_Strategy__c>();
        for (Opportunity tempOpp : oppsToProcess){
            
            if (accounts.containsKey(tempOpp.accountId))  {  
                // Get the account record from Accounts
                if(String.isBlank(tempOpp.AccountId)){
                    continue;
                }
                account tempAccount = accounts.get(tempOpp.accountId);
                
                // Only process if a Third Party
                if (tempAccount != null && tempAccount.recordType.developerName == 'Third_Party'){
                
                    // Get the Rated Strategies currently assigned to the Third Party
                    if (accountRatedStrategyMap.containsKey(tempAccount)){
                        map<id, Rated_Strategy__c> tempRatedStrategyMap = accountRatedStrategyMap.get(tempAccount);
                        if(tempRatedStrategyMap.containsKey(tempOpp.Investment_Strategy__c)){
                            // Rated Strategy already exists for this third party so check if different
                            Rated_Strategy__c tempRatedStrategy = tempRatedStrategyMap.get(tempOpp.Investment_Strategy__c);
                            if (tempRatedStrategy.current_rating__c != tempOpp.Target_Rating__c){
                                // If different, update.
                                tempRatedStrategy.current_rating__c = tempOpp.Target_Rating__c;
                                updateRatedStrategies.add(tempRatedStrategy);
                            }
                        } else {
                            // Rated Strategy does not currently exist for this third party so create
                            Rated_Strategy__c tempRatedStrategy = new Rated_Strategy__c();
                            tempRatedStrategy.Third_Party__c = tempAccount.id;
                            tempRatedStrategy.Investment_Strategy__c = tempOpp.Investment_Strategy__c;
                            tempRatedStrategy.Current_Rating__c = tempOpp.Target_Rating__c;
                            tempRatedStrategy.Comments__c = 'Created automatically from opportunity ' + tempOpp.name;
                            insertRatedStrategies.add(tempRatedStrategy);
                        }
                    }
                }
            } // Else account didn't meet the earlier record criteria (Rated/Not Rated etc) so ignore.
        } 
        
        // Perform DML
        try {
            if (insertRatedStrategies.size()>0){
                insert insertRatedStrategies;

            }
            
            if (updateRatedStrategies.size()>0){
                update updateRatedStrategies;
            }
        }catch(Exception e){
            CommonUtilities.createExceptionLog(e);
        }
        
        
        
        
    }
}