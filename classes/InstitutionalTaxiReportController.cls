public class InstitutionalTaxiReportController { 
    public InstitutionalTaxiReportController(ApexPages.StandardController controller) {
        
    }
    
    
    public Account accountDetails{
        get{     
            return [select ID, Name, Equities__c, Fixed_Income__c, Real_Assets__c, Total_AUM__c, Client_Type__c, Client_Sub_Type__c, Primary_Coverage_Member__r.Name from Account where Id =: accountId];
        } set;
    }
    public Integer totalNumberOfCalls{get{
        List<Event> totalCallEvents = [select Id, Type__c, activitydate from Event where type__c='Call' and accountId =: accountId and accountId != NULL and type__c != null];        
        Integer count = 0;
        Date today = Date.Today();
        if(totalCallEvents.size() > 0){
            for(Event eve : totalCallEvents){
                if(0 <= eve.activitydate.monthsBetween(today) && eve.activitydate.monthsBetween(today) <= 12){
                    count++;
                }
            }
        }
        return count;
    } set;}
    
    public Integer totalNumberOfMeetings{get{
        List<Event> totalMeetingEvents = [select Id, Type__c, activitydate from Event where type__c='Meeting' and accountId =:accountId and accountId != NULL and type__c != null];
        
        Integer count = 0;
        Date today = Date.Today();
        if(totalMeetingEvents.size() > 0 ){
            for(Event eve : totalMeetingEvents){
                if(0 <= eve.activitydate.monthsBetween(today) && eve.activitydate.monthsBetween(today) <= 12){
                    count++;
                }
            }
        }
        return count;
    } set;}
    
    public List<Activity_Product_Relationship__c> strategiesOfIntreast{get{
        List<Event> events = [select id, Strategies_Funds__c, activitydate from Event where type__c != null and accountId =: accountId and accountId != NULL order by activitydate desc limit 5];
        Set<ID> aliasIds = new Set<ID>();
        for(Event e : events){
            aliasIds.add(e.Strategies_Funds__c);
        }
        strategiesOfIntreast = [select Investment_Strategy__r.Name from Activity_Product_Relationship__c where Investment_Strategy__c != NULL and Activity_alias__c IN :aliasIds ORDER BY Investment_Strategy__r.Name ASC];
        return strategiesOfIntreast;
    } set;} 
    
    public List<Asset> getCurrentMandates(){       
        List<Asset> assetValues = new List<Asset>();
        assetValues = [SELECT Investment_Strategy__r.Name, Vehicle__c, Quantity, Price, Estimated_Revenue__c,Stability_Ranking__c FROM Asset where AccountId =: accountId limit 10];           
        return assetValues;
        
    }
    
    public List<AccountContactRelation> keyContacts{
        get{
            List<AccountContactRelation> acc = new List<AccountContactRelation>();
            
            acc = [select Id, Contact.Name, Type__c, Contact_Sub_Type__c, Asset_Class_Focus__c from AccountContactRelation where Key_Contact__c = true and accountId =: accountId LIMIT 10];
            return acc;
        }
    }
    public List<AccountTeamMember> accountTeam{
        get{
            return [select User.Name from AccountTeamMember where AccountId=:accountId ORDER BY User.Name ASC LIMIT 10 ];
        }
        set;
    }
    
    public List<Opportunity> opportunities{
        get{
            return [SELECT Type, Investment_Strategy__r.Name , StageName,Probability__c, Amount,Fee_Rate_bps__c, Estimated_Revenue__c,CloseDate FROM Opportunity where AccountId=: accountId LIMIT 10];
        }
        set;
    }
    
    public List<ActivityHistory1> activities{
        get{
            Map<ID,Event> activityStrategyMap = new Map<ID,Event>();
            Map<ID,List<String>> strategyName = new Map<ID,List<String>>();
            
            Set<String> alreadyAdded = new Set<String>();
            List<String> employee ;
            List<String> contacts ;
            ActivityHistory1 ah;
            List<ActivityHistory1> activityList = new List<ActivityHistory1>();
            List<Event> eve = [select Id, ActivityDate, Contact_Name__c,whoID, Strategies_Funds__c, Subject,Type__c from Event where (Type__c='Call' OR Type__c='Meeting') and accountId =: accountId and accountId != NULL limit 10];
            Map<ID, Event> eveMap = new Map<ID, Event>(eve);
            Set<Id> relationIds = new Set<ID>();
            Map<ID, List<ID>> eventContactMap = new Map<ID, List<ID>>();
            for(EventRelation er : [select relationId,eventId from EventRelation where eventId IN :eveMap.keySet() and isWhat=false]){
                List<Id> temp = new List<ID>();
                if(er.relationId != null){
                    relationIds .add(er.relationId);
                    if(eventContactMap.isEmpty() || !eventContactMap.containsKey(er.eventId)){
                        temp.add(er.relationId);
                        eventContactMap.put(er.eventId, temp);
                    }else if(eventContactMap.containsKey(er.eventId)){
                        eventContactMap.get(er.eventId).add(er.relationId);
                    }
                }
            }
            Map<ID, contact> contactsMap = new Map<ID,contact>([select Id, name, account.name from contact where ID IN :relationIds]);
            for(Event evt : eve){               
                activityStrategyMap.put(evt.Strategies_Funds__c, evt);
            }
            List<Activity_Product_Relationship__c> strategiesOfIntreast = [select  Activity_alias__r.id, Investment_Strategy__r.Name from Activity_Product_Relationship__c where Investment_Strategy__c != NULL and Activity_alias__c IN : activityStrategyMap.keySet() ];             
            
            for(Activity_Product_Relationship__c a : strategiesOfIntreast){
                List<String> temp = new List<String>();
                if(strategyName.isEmpty() || !strategyName.containsKey(a.Activity_alias__r.id)){
                    temp.add(a.Investment_Strategy__r.Name);
                    strategyName.put(a.Activity_alias__r.id, temp);
                }else if (strategyName.containsKey(a.Activity_alias__r.id)){
                    strategyName.get(a.Activity_alias__r.id).add(a.Investment_Strategy__r.Name);
                }
            }
            
            for(Event e : eve ){                                
                
                ah = new ActivityHistory1();
                employee = new List<String>();
                contacts = new List<String>();
                
                if(strategyName.get(e.Strategies_Funds__c) != NULL){
                    List<String> strat;
                    for(string strategyId : strategyName.get(e.Strategies_Funds__c)){
                        strat = new List<String>();
                        strat.add(strategyId);
                    }
                    if(strat != NULL)
                        ah.strategyName = strat;
                }
                
                ah.ActivityDate = e.ActivityDate;
                ah.subject = e.subject;
                ah.type= e.type__c;
                if( eventContactMap.get(e.Id) != NULL){
                    for(ID contactId : eventContactMap.get(e.Id)){
                        if(contactsMap.get(contactId) != NULL){
                            Contact c = contactsMap.get(contactId);
                            if(c.accountId != null){
                                if(c.account.name.containsIgnoreCase('Deloitte')){
                                    employee.add(c.name);
                                }else{
                                    contacts.add(c.name);
                                }                       
                            }
                        }
                    }    
                }
                ah.employee =employee;
                ah.contacts = contacts;
                activityList.add(ah);
                
            }                             
            return activityList;
        }
        set;
    }       
    
    public List<ActivityHistory1> futureEvents{
        get{
            Date today = Date.Today();
            Map<ID,Event> events =new Map<ID,Event>( [select Id, activitydate,ActivityDateTime, EndDateTime, subject,type__c from event where type__c='Meeting' and accountId =:accountId and activitydate >= today order by activitydate asc limit 10]);
            Map<ID,List<ID>> eventContactMap = new Map<ID,List<ID>>();
            Set<ID> contactIds = new Set<ID>();
            ActivityHistory1 ah;
            List<ActivityHistory1> activityList = new List<ActivityHistory1>();
            List<String>  participents = new List<String>() ;
            for(EventRelation er : [select relationId, eventId from EventRelation where eventId IN :events.keySet()]){
                List<Id> temp = new List<Id>();
                contactIds.add(er.relationId);
                if(eventContactMap.isEmpty() || !eventContactMap.containsKey(er.eventId)){
                    temp.add(er.relationId);
                    eventContactMap.put(er.eventId,temp);
                }else{
                    eventContactMap.get(er.eventId).add(er.relationId);
                }
            }
            Map<ID, Contact> contactMap = new Map<ID,Contact>([select Id, name from contact where Id IN :contactIds]);
            for(Event e : events.values() ){
                ah= new ActivityHistory1();
                ah.ActivityDate = e.ActivityDate ;               
                ah.subject = e.subject;
                for(Id contactId : eventContactMap.get(e.Id)){
                    if( contactMap.get(contactId) != NULL){
                        Contact c = contactMap.get(contactId);
                        
                        participents.add(c.name);                        
                    }                    
                    ah.contacts = participents;
                }
                String starttime = e.ActivityDateTime.hour() < 12 ?'am':'pm';
                
                ah.duration = e.ActivityDateTime.hour()+':'+e.ActivityDateTime.minute()+e.ActivityDateTime.second()+ ' '+starttime +' - '+e.EndDateTime.hour()+':'+e.EndDateTime.minute()+e.EndDateTime.second()+ ' '+starttime ;
                
                ah.contacts = participents;
                activityList.add(ah);
            }
            
            return activityList;
        }
        set;
    }
    
    public List<Campaign> getCampaigns{
        get{
            Map<ID,Contact> contactsMap = new Map<ID,Contact>([select Id from Contact where AccountId =: accountId]);
            Set<ID> campaignIds = new Set<ID>();
            
            for(Campaign_Team__c ct : [select Id,Campaign__c,Team_Member__c from Campaign_Team__c where Team_Member__c IN : contactsMap.keySet()]){
                campaignIds.add(ct.Campaign__c);
            }
            List<Campaign> campaigns = [select Id, Name, Type, StartDate, Objective__c  from Campaign where Id IN :campaignIds LIMIT 10];
            return campaigns;
        }
        set;        
    }
    
    public List<Client_Third_Party_Relationship__c> getConsultants{
        get{
            return [SELECT Third_Party_Name__r.Name FROM Client_Third_Party_Relationship__c where Client_Name__c= :accountId  and Relationship_Type__c='Consultant' limit 10];
        }
        set;
    }
    
    public static string accountId;
    public void setAccountId() {
        accountId= ApexPages.currentPage().getParameters().get('accountId');
    }
    public class ActivityHistory1{
        public List<string> strategyName{set;get;}
        public Date ActivityDate{set;get;}    
        public String duration{set;get;}
        public string thoughtLeadership {set;get;}
        public string subject{set;get;}
        public string type{set;get;}
        public List<String> contacts{set;get;}
        public List<String> employee{set;get;}
    }
}