public class Client_Territory_TriggerHandler extends TriggerHandler{
    protected override void beforeInsert(){
        // Check that no existing UK Territory has already been created for this client
        if((triggerParams.objByPassMap.containsKey('Client_Territory_Utility.checkExistingUKTerritory') && !triggerParams.objByPassMap.get('Client_Territory_Utility.checkExistingUKTerritory')) || 
           !triggerParams.objByPassMap.containsKey('Client_Territory_Utility.checkExistingUKTerritory')){
               Client_Territory_Utility.checkExistingUKTerritory(triggerParams);  
        }
        
        if((triggerParams.objByPassMap.containsKey('Client_Territory_Utility.checkExistingTerritory') && !triggerParams.objByPassMap.get('Client_Territory_Utility.checkExistingTerritory')) || 
           !triggerParams.objByPassMap.containsKey('Client_Territory_Utility.checkExistingTerritory')){
               Client_Territory_Utility.checkExistingTerritory(triggerParams);  
        }
    }

    protected override void afterInsert(){
        if((triggerParams.objByPassMap.containsKey('Client_Territory_Utility.addClientTerritory') && !triggerParams.objByPassMap.get('Client_Territory_Utility.addClientTerritory')) || 
           !triggerParams.objByPassMap.containsKey('Client_Territory_Utility.addClientTerritory')){
               Client_Territory_Utility.addClientTerritory(triggerParams); 
               
           }
    }
    
    protected override void beforeDelete(){
        if((triggerParams.objByPassMap.containsKey('Client_Territory_Utility.removeClientTerritory') && !triggerParams.objByPassMap.get('Client_Territory_Utility.removeClientTerritory')) || 
           !triggerParams.objByPassMap.containsKey('Client_Territory_Utility.removeClientTerritory')){
               Client_Territory_Utility.removeClientTerritory(triggerParams);     
               
           }
    }
}