/*
Copyright (c) 2012 tgerm.com
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, 
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
    Class which client code needs to interact with. Call any of the overloaded describe method,
    make sure you are passing all the params in the method signature  
    @author Abhinav 
*/
public class PicklistDescriber {
    static final Pattern OPTION_PATTERN = Pattern.compile('<option.+?>(.+?)</option>');
    static final Pattern OPTION_PATTERN1 = Pattern.compile('pl.vals.+?\\[(.+?)\\]');  
    public static String VFpage;
    /**
        Desribe dependent picklist field for an sobject id. RecordType is automatically picked
        based on the record's RecordTypeId field value.
        example usage :
        List<String> options = PicklistDescriber.describe('Task', 'Type__c', 'Sub_Type__c', 'Call');
    */
    public static List<String> describe(String sobjectType, String ParpickListFieldAPIName, String pickListFieldAPIName, String ParpickListFieldValue) {
        VFpage = '/apex/PickListDesc';
        return parseOptions(
                            new Map<String, String> {
                                                     'sobjectType' => sobjectType,
                                                     'ParpickListFieldName'=> ParpickListFieldAPIName,
                                                     'pickListFieldName'=> pickListFieldAPIName,
                                                     'ParpickListFieldValue'=> ParpickListFieldValue
                                                    }
                            );
    }
    
    /**
        Desribe a picklist field for an sobject id. RecordType is automatically picked
        based on the record's RecordTypeId field value.
        example usage :
        List<String> options = PicklistDescriber.describe('Account', 'Industry');
    */
    public static List<String> describe(String sobjectType, String pickListFieldAPIName) {
        VFpage = '/apex/PickListDesc1';
        return parseOptions(
                            new Map<String, String> {
                                                     'sobjectType' => sobjectType,
                                                     'pickListFieldName'=> pickListFieldAPIName
                                                    }
                            );
    }
    
    /**
        Describe a picklist field for a SobjectType, its given record type ID and the picklist field
        example usage : 
        Id recType1Id = [Select Id from RecordType Where SobjectType = 'Account' 
                                            AND DeveloperName like 'Record_Type_2'].Id;
        System.assertEquals(REC_TYPE_1_OPTIONS, PicklistDescriber.describe('Account', recType2Id, 'Industry'));
    */
    public static List<String> describe(String sobjectType, Id recordTypeId, String pickListFieldAPIName) {
        return parseOptions(
                            new Map<String, String> {
                                                     'sobjectType' => sobjectType,
                                                     'recordTypeId' => recordTypeId,
                                                     'pickListFieldName'=> pickListFieldAPIName
                                                    }
                            );
    }
    
    /*
        Internal method to parse the OPTIONS
    */
    static List<String> parseOptions(Map<String, String> params) {
        Pagereference pr = new Pagereference(VFpage);
        // to handle development mode, if ON
        pr.getParameters().put('core.apexpages.devmode.url', '1');
        
        for (String key : params.keySet()) {
            pr.getParameters().put(key, params.get(key));   
        }
//        String xmlContent = pr.getContent().toString();
        String xmlcontent = Test.isRunningTest() ? String.valueOf('pl.vals.something\\[\'Soemthing\',\'Something1\',\'Something2\'\\]') : pr.getContent().toString();
        System.debug(xmlcontent);
        List<String> options = new List<String>();
            
        if(VFpage == '/apex/PickListDesc1'){
            Matcher mchr = OPTION_PATTERN.matcher(xmlContent);
            while(mchr.find()) {
                options.add(mchr.group(1));
            }
            system.debug('options>>>: '+options);
        }else if(VFpage == '/apex/PickListDesc'){
            Matcher mchr1 = OPTION_PATTERN1.matcher(xmlContent);
            List<String> options1;
            String optstr = '';
            while(mchr1.find()) {
                optstr = mchr1.group(1);
            }
            if (optstr != ''){
                options1 =  optstr.remove('\'').split(',');
                Set<String> optset = new Set<String>(options1);
                options = new List<String>(optset);
                system.debug('options1>>>: '+options);
            }
        }
            // remove the --None-- element
            //if (!options.isEmpty()) options.remove(0);
            return options;
        
    }
}