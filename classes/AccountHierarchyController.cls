public with sharing class AccountHierarchyController {
     /*
     * Description : Create new instance / return existing instance
     * Param : Account Id 
     * Param : Boolean to identify if third party information has to be fetched
     * Returns : Instance of wrapper class Account Result 
    */    
    public static string Thirdparty='Thirdparty';
    public static string Networks='Networks';
    public static string SchemesLabel='Schemes';
    
    @AuraEnabled
    public static AccountResult getAccountInContext(Id accountId,string value){

        AccountResult result = new AccountResult();
        /* Load the account currently being viewed */
        
        List<account> acc=  [SELECT Id, Name, 
                                     RecordType.Name,
                                     CurrencyISOCode,
                                     Primary_Coverage_Member__r.Name,
                                     Total_AUM__c,
                                     Total_AUM_in_Hierarchy__c,
                                     Billing_City__c,Billing_Country__c,
                                     Hierarchy_Level__c,
                                     Investment_Strategies_Amount__c,
                                     Target_Sales__c,
                                     (SELECT Id,
                                          Third_Party_Name__c,
                                          Third_Party_Name__r.Name,
                                          Third_Party_Name__r.RecordType.Name,
                                          Third_Party_Name__r.Third_Party_Type__c,
                                          Lead_Third_Party_Contact__c,
                                          Lead_Third_Party_Contact__r.Name,
                                          Consultant_Function__c,
                                          Relationship_Type__c
                                     FROM   Client_Third_Party_Relationships1__r ),
                                     (SELECT id, 
                                            Department__c,
                                            Department__r.Name,
                                            Company_Name__c
                                     FROM Department_Client_Relationships__r)
                                     FROM Account WHERE Id = :accountId];
        for(account temp :  acc){
            result.account = temp;
        	result.hierachyLevel = temp.Hierarchy_Level__c;
        }
            
        List<Pools_of_Capital__c> schemes=[select id,Scheme_Name__c,Client_Name__c,Client_Name__r.Name , Scheme_Type__c ,Client_Name__r.Total_AUM__c,Client_Name__r.Total_AUM_in_Hierarchy__c,
                                         Client_Name__r.Primary_Coverage_Member__r.Name from Pools_of_Capital__c where Client_Name__c = : accountId];
        /*Value to check if Third Party Accounts are shown*/
        if(value==Thirdparty){
            result.thirdPartyaccounts= result.account.Client_Third_Party_Relationships1__r;
        }else if(value==SchemesLabel){
            result.schemes=schemes;
            result.departmentClients=result.account.Department_Client_Relationships__r;
        }
        
        /* Immediately load the child accounts underneath the main account */
        result.childAccounts = getAccounts( accountId,value);
        result.childCount = result.childAccounts.size();
        
        System.debug('result'+result);
        
        return result;
    }
    
    /*Gets the ultimate parent Id for the Account */
    @AuraEnabled
    public static id getAccountUltimateParent(Id accountId){
        id ParentId; 
        list<account> acc = [SELECT Id, CurrencyISOCode, ultimate_parent_id__c FROM Account WHERE Id = :accountId];
        for(account temp : acc)
            ParentId = temp.ultimate_parent_id__c;
        return ParentId;
    }

    /*Gets the child Record on clicking of + button */
    @AuraEnabled
    public static List<AccountResult> getAccounts(Id parentAccountId,string value){                           
        List<AccountResult> results = new List<AccountResult>();
        List<account> childRecord=  [SELECT 
                                            Id, 
                                             Name, 
                                             RecordType.Name,
                                             CurrencyISOCode,
                                             Primary_Coverage_Member__c,
                                             Primary_Coverage_Member__r.Name,
                                             Total_AUM__c,
                                             Total_AUM_in_Hierarchy__c,
                                             Billing_City__c,Billing_Country__c,
                                             Hierarchy_Level__c,
                                             Investment_Strategies_Amount__c,
                                             Target_Sales__c,
                                             (SELECT Id
                                              FROM ChildAccounts),
                                            (SELECT Id,
                                                    Third_Party_Name__c,
                                                    Third_Party_Name__r.Name,
                                                    Third_Party_Name__r.RecordType.Name,
                                                    Third_Party_Name__r.Third_Party_Type__c,
                                                      Lead_Third_Party_Contact__c,
                                                      Lead_Third_Party_Contact__r.Name,
                                                      Consultant_Function__c,
                                                      Relationship_Type__c
                                             FROM   Client_Third_Party_Relationships1__r),
                                             
                                            (SELECT id, 
                                                Department__c,
                                                Department__r.Name,
                                                Company_Name__c
                                             FROM Department_Client_Relationships__r)
                                        FROM 
                                            Account 
                                        WHERE 
                                            ParentId = :parentAccountId];
        List<Pools_of_Capital__c> schemes=[select id,Scheme_Name__c,Client_Name__c,
                                            Client_Name__r.Name , Scheme_Type__c ,Client_Name__r.Total_AUM__c,Client_Name__r.Total_AUM_in_Hierarchy__c,
                                            Client_Name__r.Primary_Coverage_Member__r.Name 
                                            from Pools_of_Capital__c where Client_Name__r.ParentId = : parentAccountId];
        Map<id,list<Pools_of_Capital__c>> schemeMap = new Map<id,list<Pools_of_Capital__c>>();
        for(Pools_of_Capital__c temp : schemes){
            if(!schemeMap.containsKey(temp.Client_Name__c)){
                list<Pools_of_Capital__c> tempList = new list<Pools_of_Capital__c>();
                templist.add(temp);
                schemeMap.put(temp.Client_Name__c,templist);
            }else{
                schemeMap.get(temp.Client_Name__c).add(temp);
            }
        }
        for(Account account : childRecord){
            AccountResult child = new AccountResult();
            child.account = account;
            child.childCount = 0;
            child.hierachyLevel = account.Hierarchy_Level__c;
            if(!account.ChildAccounts.isEmpty()){
                child.childCount = account.ChildAccounts.size();
            }
            if(value==Thirdparty){
                child.thirdPartyaccounts= account.Client_Third_Party_Relationships1__r;
            }else if (value==SchemesLabel){
                child.Schemes= schemeMap.get(account.Id);
                child.departmentClients =account.Department_Client_Relationships__r;
            }
            results.add(child);
        }
        return results;
    }

    /* Wrapper to hold data that is displayed on the page */
    public class AccountResult {
        @AuraEnabled
        public Account account;
        
        @AuraEnabled
        public String hierachyLevel;
        
        @AuraEnabled
        public Integer childCount;
        
        @AuraEnabled
        public List<Client_Third_Party_Relationship__c> thirdPartyaccounts;
       
        @AuraEnabled
        public List<Department_Client_Relationship__c> departmentClients;
        
        @AuraEnabled
        public List<Pools_of_Capital__c> Schemes;

        @AuraEnabled
        public List<AccountResult> childAccounts;
    }

}