@isTest
public class createMeetingController_Test {
    /*
     * 
     * Description : Create an Event Record .
     * Param : json Data
     * Returns :  Id
    */
    static testMethod void CreateEventTest(){
        User user = CommonTestUtils.createTestUser();  
        
        // Create a client that the contacts will belong to
        Account acc = CommonTestUtils.CreateTestClient('Contact Tests');        
        insert acc;
        
        // Now create contacts for a client with last viewed Date
        List<contact> contacts = new List<contact>();       
        for(integer i = 0; i<2; i++ ){
            Contact c = CommonTestUtils.CreateTestContact('Contact ' + i);
            c.accountid = acc.id;
            contacts.add(c);                    
        }
        insert contacts;
        System.runAs(user){
            Test.startTest();
            string data=CommonTestUtils.EventJsonData(contacts);
            Id eventId=createMeetingController.createMeeting(data);
            createMeetingController.GetPckLstValues('event','Type__c');
            createMeetingController.GetDpndntPckLstValues('event','Type__c','Sub_Type__c','Call');
            System.assert(eventId != null); 
        }
    }
}