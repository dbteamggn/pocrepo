public with sharing class DealVaultController {
	@AuraEnabled
    public static Opportunity updateDealWithVaultId(Id dealId, Id vaultId) {
        Opportunity deal = [SELECT Id, Vault__c FROM Opportunity WHERE Id=: dealId];
        deal.Vault__c = vaultId;
        update deal;
        return deal;
    }
}