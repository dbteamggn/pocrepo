/* Class Name  : BatchGiftBenefitRollUpOnCon
* Description  : Batch class to roll up amount from Gifts and  Benefits to its associated account
* Created By   : Deloitte
* Created Date : 10-10-2016
* Modification Log:  
* --------------------------------------------------------------------------------------------------------------------------------------
* Developer                Date                 Modification ID        Description 
* ---------------------------------------------------------------------------------------------------------------------------------------
* Mohit Raj                10-10-2016           1001                   Initial Version
*/
global class BatchGiftBenefitRollUpOnCon implements Database.Batchable<sObject>,Database.Stateful{

      

   /* Description:This method returns the query for all the records that needs to be rolledup
    *  Param:None
    *  Returns:queryLocator
    */
    global database.querylocator start(Database.BatchableContext BC){
        //Fetch the last batch run time to process delta records
        BatchRunTime__c lastRun = BatchRunTime__c.getValues('BatchUpdateConGBAmount'); 
        DateTime lastRunDateTime;
        if (lastRun == null || (system.today().day() == 1 && system.today().month() == 1)){
            lastRunDateTime = system.now() - (365 * 5); // Default to 5 years ago
        } else {
            lastRunDateTime = lastRun.Last_Run__c;
        }
        //Query to select all the contacts whose values needs to be rolleded up
        return Database.getqueryLocator([Select id, Related_Contact__r.currencyIsoCode, Related_Contact__c from Gift_Benefit__c where lastmodifiedDate >=:lastRunDateTime order by Related_Contact__c]);
    }

    /* Description:This method roll up amount from Gifts and  Benefits to its associated account
    *  Param: List of gifts and benefits
    *  Returns:None
    *
    */
    global void execute(Database.BatchableContext BC, List<Gift_Benefit__c> scope){
        Set<id> setOfContactId = new set<id>(); 
        Map<id, String> mapOfconIdAndIsoCode = new Map<id, String>();
        Map<id, contact> mapOfIdContact = new Map<id, contact>();
        
        for(Gift_Benefit__c each:scope){
            setOfContactId.add(each.Related_Contact__c);
            mapOfconIdAndIsoCode.put(each.Related_Contact__c, each.Related_Contact__r.currencyIsoCode );            
            Contact obj = new Contact();
            obj.id = each.Related_Contact__c;
            obj.Given_Amount__c = 0;
            obj.Received_Amount__c = 0;
            mapOfIdContact.put(obj.id, obj); 
        }
        
        list<AggregateResult> listofAr = [SELECT   SUM(Amount__c) amt,Given_Received__c,
                                                   Related_Contact__c con
                                          FROM     Gift_Benefit__c 
                                          WHERE    Related_Contact__c IN:setOfContactId 
                                          AND      Calendar_Year(Date__c)  =: System.today().year()
                                          GROUP BY Related_Contact__c, Given_Received__c
                                         ];
        if(listofAr != null && listOfAr.size()>0){
            Map<String , Decimal> mapOfCurrAndVal = CommonUtilities.getCurrencyInfo();
            for(AggregateResult each:listOfAr){
                Contact obj = mapOfIdContact.get((Id) each.get('con'));
                decimal conversionRate = mapOfconIdAndIsoCode.get((Id) each.get('con')) != null ? mapOfCurrAndVal.get(mapOfconIdAndIsoCode.get((Id) each.get('con'))) : 1;
                if('Given'== each.get('Given_Received__c')){
                    obj.Given_amount__c = ((Decimal) each.get('amt') != null) ?  (Decimal) each.get('amt') * conversionRate :0;
                }else{
                    obj.Received_Amount__c =((Decimal) each.get('amt') != null) ?  (Decimal) each.get('amt') * conversionRate :0;
                }
                mapOfIdContact.put(obj.id,obj);
            }
            try{
              update mapOfIdContact.values();
            }catch(Exception ex){
              CommonUtilities.createExceptionLog(ex);
            }
        }
    } 
    
    /* Description: Finish method, this method creates a record in Integration Job
     *              stating the status of the merge process
     * Param: Batch Context
     * Returns:None
     *
     */
    global void finish(Database.BatchableContext BC){
        AsyncApexJob asynch=[Select Id,JobItemsProcessed,ExtendedStatus,CompletedDate,NumberOfErrors,TotalJobItems,Status,CreatedBy.Email 
                             FROM   AsyncApexJob 
                             WHERE  Id =: BC.getJobId()];
        String Subject = 'Scheduled Batch Job Failure in updating Contact amount fields from Gifts and Benefits';
        String[] addr = new String[]{asynch.CreatedBy.Email};
        String PlainTextBody = 'Scheduled Batch job to update contacts\' amount from Gifts and Benefits completed at '
                               + asynch.CompletedDate+' throws following error : \n Processed Job Items :'+ asynch.JobItemsProcessed
                               +'\n No.of Job Failures : '+asynch.NumberofErrors+'\n Errors :'+asynch.ExtendedStatus;
        
        if(asynch.NumberOfErrors > 0){
            CommonUtilities.SendEmail(Subject,PlainTextBody,addr);
        }else{
            // update last batch run time
            BatchRunTime__c lastRun = BatchRunTime__c.getValues('BatchUpdateConGBAmount');
            if (lastRun == null){
                lastRun = new BatchRunTime__c();
                lastRun.name = 'BatchUpdateConGBAmount';
            }
            lastRun.Last_Run__c = system.now();
            upsert lastRun;
        }
    }

}