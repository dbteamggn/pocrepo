public with sharing class BP_UKRetailFlowCtrl{
    public static Set<String> regionsSet;
    
    @AuraEnabled
    public static Id createNewPlan(){
        Business_Plan__c newRec = new Business_Plan__c(Business_Line__c = 'Retail', OwnerId = UserInfo.getUserId(), Business_Plan_Duration__c = '1 Year', 
                                                        Name = 'Business Plan ' + String.valueOf(Date.Today()));
        insert newRec;
        return newRec.Id;
    }
    
    @AuraEnabled
    public static Boolean getCurrentBatchStatus(Id bpId){
        Boolean isRunning = false;
        for(Business_Plan__c each: [select Id, Open_Batch_Job__c from Business_Plan__c where Id = :bpId]){
            isRunning = each.Open_Batch_Job__c ;
        }
        return isRunning;
    }
    
    public class FundWrapper {
        @AuraEnabled
        public String id;   
        @AuraEnabled
        public String name; 
        public FundWrapper(){
            
        }
        public FundWrapper(Id recId, String name){
            this.id = recId;
            this.name = name;
        }
    }
    
    @AuraEnabled
    public static List<FundWrapper> getUsers(){
        List<FundWrapper> allVals = new List<FundWrapper>();
        for(User each: [select Id, Name from User where Profile.Name = 'Retail User' ORDER BY Name]){
            allVals.add(new FundWrapper(each.Id, each.Name));
        }
        return allVals;
    }
    
    @AuraEnabled
    public static List<FundWrapper> getLookupValues(Id bpRecId){
        List<FundWrapper> allVals = new List<FundWrapper>();
        for(Business_Plan_Client_Relation__c each: [select Id, Name, Client__r.Name from Business_Plan_Client_Relation__c where Business_Plan__c = :bpRecId]){
            allVals.add(new FundWrapper(each.Id, each.Client__r.Name));
        }
        return allVals;
    }
        
    @AuraEnabled
    public static Id createBPAction(String jsonStr, String fundStr){
        Business_Plan_Actions__c bpActRec = (Business_Plan_Actions__c) System.JSON.deserialize(jsonStr, Business_Plan_Actions__c.class);
        insert bpActRec;
        
        if(!String.isBlank(fundStr)){
            Schema.DescribeSObjectResult bpProdRes = Schema.SObjectType.Business_Plan_Product_Relation__c;
            Map<String, Schema.RecordTypeInfo> rtMapByNameBPProd = bpProdRes.getRecordTypeInfosByName();
            
            List<FundWrapper> selectedFunds = (List<FundWrapper>) System.JSON.deserialize(fundStr, List<FundWrapper>.class);
            
            List<Business_Plan_product_Relation__c> actionProds = new List<Business_Plan_product_Relation__c>();
            for(FundWrapper  each : selectedFunds){
                Business_Plan_product_Relation__c eachProd = new Business_Plan_product_Relation__c ();
                eachProd.Business_Plan__c = bpActRec.Business_Plan__c;
                eachProd.Business_Plan_Action__c = bpActRec.Id;
                if(Id.valueOf(each.id).getSObjectType() == Schema.Product2.SObjectType){
                    eachProd.Funds__c = each.id;
                    if(rtMapByNameBPProd.containsKey(System.Label.FundRecTypeName)){
                        eachProd.RecordTypeId = rtMapByNameBPProd.get(System.Label.FundRecTypeName).getRecordTypeId();
                    }
                } else if(Id.valueOf(each.id).getSObjectType() == Schema.Investment_Strategy__c.SObjectType){
                    eachProd.Investment_Strategy__c = each.id;
                    if(rtMapByNameBPProd.containsKey(System.Label.StrategyRecTypeName)){
                        eachProd.RecordTypeId = rtMapByNameBPProd.get(System.Label.StrategyRecTypeName).getRecordTypeId();
                    }
                }
                actionProds.add(eachProd);
            }
            if(!actionProds.isEmpty()){
                insert actionProds;
            }
        }
        return bpActRec.Id;
    }
    
    @AuraEnabled
    public static Boolean isUKRetailUser(){
        for(User each: [select Id, Business_Line__c from User where Id = :UserInfo.getUserId()]){
            if(!String.isBlank(each.Business_Line__c) && each.Business_Line__c.equalsIgnoreCase(System.Label.RetailLabel)){
                return true;
            }
        }
        return false;
    }
    
    @AuraEnabled
    public static void createEvent(String stringInst){
        createEventController.createEvent(stringInst);
    }
    
    @AuraEnabled
    public static String getActionName(Id actionId){
        String actionName;
        for(Business_Plan_Actions__c each: [select Name from Business_Plan_Actions__c where Id = :actionId]){
            actionName = each.Name;
        }
        return actionName;
    }
    
    @AuraEnabled
    public static String getWhereClause(){
        String clause = ' where OwnerId = \'' + UserInfo.getUserId() + '\'';
        return clause;
    }
    
    @AuraEnabled
    public static List<String> getPicklistValues(String objName, String fieldName){
        List<String> options = new List<String>();       
        Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(objName).getDescribe().fields.getMap();
        List<Schema.PicklistEntry> ple = objectFields.get(fieldName.toLowerCase()).getDescribe().getPicklistValues();
        for( Schema.PicklistEntry f : ple){
            options.add(f.getValue());
        }
        return options;
    }
  
    @AuraEnabled
    public static Id updateBP(Id recordId, String overview, String duration){
        Business_Plan__c bpRec;
        if(recordId != NULL){
            bpRec = new Business_Plan__c(Id = recordId, Business_Plan_Summary__c = overview, Business_Plan_Duration__c = duration);
        } else{
            bpRec = new Business_Plan__c(Business_Plan_Summary__c = overview, Business_Plan_Duration__c = duration);
        }
        upsert bpRec;
        return bpRec.Id;
    }
    
    @AuraEnabled
    public static Business_Plan__c getBPRecord(String recId, String region, Boolean isUKRetail){
        Business_Plan__c bpRec;
        if(!String.isBlank(region)){
            for(Business_Plan__c each: [select Id, is_FIG__c, Is_Open__c, Business_Plan_Duration__c, BP_Territory__r.Name, Business_Plan_Summary__c from Business_Plan__c where OwnerId = :UserInfo.getUserId() AND Current__c = TRUE AND BP_Territory__r.Name = :region]){
                bpRec = each;
            }
        } else if (!String.isBlank(recId)){
            for(Business_Plan__c each: [select Id, is_FIG__c, Is_Open__c, Business_Plan_Duration__c, BP_Territory__r.Name, Business_Plan_Summary__c from Business_Plan__c where Id = :recId]){
                bpRec = each;
            }
        } else{
            if(isUKRetail){
                for(Business_Plan__c each: [select Id, is_FIG__c, Is_Open__c, Business_Plan_Duration__c, BP_Territory__r.Name, Business_Plan_Summary__c from Business_Plan__c where OwnerId = :UserInfo.getUserId() AND Current__c = TRUE]){
                    if(bpRec == NULL){
                        bpRec = each;
                    }
                }
            } else{
                bpRec = new Business_Plan__c(is_FIG__c = false);
            }
        }
        return bpRec;
    }
    
    @AuraEnabled
    public static List<String> getRegions(){
        regionsSet = new Set<String>();
        for(Business_Plan__c each: [select Id, is_FIG__c, Business_Plan_Duration__c, BP_Territory__r.Name, Business_Plan_Summary__c from Business_Plan__c where OwnerId = :UserInfo.getUserId() AND Current__c = TRUE]){
            regionsSet.add(each.BP_Territory__r.Name);
        }
        List<String> regionsList = new List<String>();
        regionsList.addAll(regionsSet);
        return regionsList;
    }
    
    @AuraEnabled
    public static List<UserResult> findByName(String searchKey){
        String searchVal = '%' + searchKey + '%';
        List<UserResult> results = new List<UserResult>();
        for(User each: [select Id, Name, Title from User where Name LIKE :searchVal]){
            results.add(new UserResult(each.Id, each.Name, each.Title, false));
        }
        return results;
    }
    
    @AuraEnabled
    public static List<UserResult> findByListId(List<Id> userIds){
        List<UserResult> results = new List<UserResult>();
        for(User each: [select Id, Name, Title from User where Id IN :userIds]){
            results.add(new UserResult(each.Id, each.Name, each.Title, true));
        }
        return results;
    }
    
    @AuraEnabled
    public static List<UserResult> findAll(){
        Set<Id> userIds = new Set<Id>();
        List<UserResult> results = new List<UserResult>();
        for(RecentlyViewed recentItem : [select Id, Name FROM RecentlyViewed WHERE Type = 'User' ORDER BY LastViewedDate DESC LIMIT 10]){
            userIds.add(recentItem.Id);
        }
        if(!userIds.isEmpty()){
            for(User each: [select Id, Name, Title from User where Id IN :userIds ORDER BY Name]){
                results.add(new UserResult(each.Id, each.Name, each.Title, false));
            }
        } else{
            for(User each: [select Id, Name, Title from User where isActive = TRUE LIMIT 10]){
                results.add(new UserResult(each.Id, each.Name, each.Title, false));
            }
        }
        return results;
    }
    
    public class UserResult {
        @AuraEnabled
        public Id userId;
        @AuraEnabled
        public String userName;
        @AuraEnabled
        public String userTitle;
        @AuraEnabled
        public Boolean value;
        
        public UserResult(Id userId, String userName, String userTitle, Boolean value){
            this.userId = userId;
            this.userName = userName;
            this.userTitle = userTitle;
            this.value = value;
        }
    }
}