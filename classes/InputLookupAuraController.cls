public with sharing class InputLookupAuraController {
    @AuraEnabled
    public static String getObjectSearchLabel(String objectName) 
    {
        list<schema.describeSObjectResult> dsors = Schema.describeSObjects(new List<String> {objectName});
        if (dsors==null || dsors.size()==0){
            return objectName;
        } 
        return dsors[0].getLabel();
    }

    @AuraEnabled
    public static String getResults(String objectName,
                                       String idFieldName, 
                                       String searchFields, 
                                       String sortFields, 
                                       String searchValue,
                                       String additionalWhereClause,
                                       String limitValue,
                                       String returnFormatStringDisplay,
                                       String fieldStartToken,
                                       String fieldEndToken,
                                       String returnFormatStringMatches,
                                       String excludeIdFieldName,
                                   	   list<Id> excludeIds
                                   	   ) 
    {
		if (idFieldName == null || idFieldName == ''){
            idFieldName = 'Id';
        }
        
        if (searchFields == null || searchFields == ''){
            searchFields = 'Name';
        }
        
        if (limitValue == null || limitValue == ''){
            limitValue = '50';
        }
        
        if (fieldStartToken == null || fieldStartToken == ''){
            fieldStartToken = '{';
        }
        
        if (fieldEndToken == null || fieldEndToken == ''){
            fieldEndToken = '}';
        }
        
        if (returnFormatStringDisplay == null || returnFormatStringDisplay == ''){
            returnFormatStringDisplay = fieldStartToken + 'Name' + fieldEndToken;
        }
        
        if (returnFormatStringMatches == null || returnFormatStringMatches == ''){
            returnFormatStringMatches = returnFormatStringDisplay;
        }
        
        if (excludeIdFieldName == null || excludeIdFieldName == ''){
            excludeIdFieldName = 'id';
        }
        
        // soqlFields contains a set of fields that are required to be returned by the soql        
        set<string> soqlFields = new set<string>(); 
        soqlFields.add(idFieldName.toLowerCase());  // Id is always returned
                                           
        // Split the search fields by comma & add to the list of required fields.
        // Also build up the search clause
        string[] searchFieldsArr = searchFields.split(',');
        String searchFieldsString = '';
        for(string str : searchFieldsArr){
            searchFieldsString = searchFieldsString + (searchFieldsString!=''?' or ':'') + str + ' like \'' + searchValue + '%\'';
            soqlFields.add(str.trim().toLowerCase());
        }
        
        // Deconstruct returnFormatStringMatches, looking for fields that need to be replaced by their values
        //Set<String> requiredFields = getRequiredFields(returnFormatStringMatches, fieldStartToken, fieldEndToken);
        Set<String> requiredFields = new Set<String>();
        integer posStart = returnFormatStringMatches.indexOf(fieldStartToken);
        integer posEnd = -1;
        while (posStart > -1){
            posEnd = returnFormatStringMatches.indexOf(fieldEndToken, posStart+1);
            if (posEnd > -1){
                requiredFields.add(returnFormatStringMatches.substring(posStart+1,posEnd).toLowerCase());
                returnFormatStringMatches = (posStart > 0?returnFormatStringMatches.substring(0,posStart):'') + returnFormatStringMatches.substring(posStart,posEnd+1).toLowerCase() + (posEnd<returnFormatStringMatches.length()?returnFormatStringMatches.substring(posEnd+1):'');
                posStart = returnFormatStringMatches.indexOf(fieldStartToken, posEnd+1);
            } else {
                posStart = -1;
            }
        }
        
        posStart = returnFormatStringDisplay.indexOf(fieldStartToken);
        posEnd = -1;
        while (posStart > -1){
            posEnd = returnFormatStringDisplay.indexOf(fieldEndToken, posStart+1);
            if (posEnd > -1){
                requiredFields.add(returnFormatStringDisplay.substring(posStart+1,posEnd).toLowerCase());
                returnFormatStringDisplay = (posStart > 0?returnFormatStringDisplay.substring(0,posStart):'') + returnFormatStringDisplay.substring(posStart,posEnd+1).toLowerCase() + (posEnd<returnFormatStringDisplay.length()?returnFormatStringDisplay.substring(posEnd+1):'');
                posStart = returnFormatStringDisplay.indexOf(fieldStartToken, posEnd+1);
            } else {
                posStart = -1;
            }
        }
        
        soqlFields.addAll(requiredFields);
                               
                                           
        // Build up the SOQL
        String soql = '';
        for(String str : soqlFields) {
        	if (str != ''){
        		soql = (soql==''?'select ' + str:soql + ', ' + str); 
            }                                       
        }                                  
        soql = soql + ' from ' + objectName + ' where (' + searchFieldsString + ')';
        if (additionalWhereClause != null && additionalWhereClause != ''){
        	 soql = soql + ' AND (' + additionalWhereClause + ')';                                 
        }
        if (sortFields != null && sortFields != '') {                                 
        	soql = soql + ' order by ' + sortFields;
        }
    	soql = soql + ' limit ' + limitValue;

        // Return tne SOQL results
        list<sObject> results = Database.query(soql);
        
        // Remove any Ids that we specifically want to exclude from results
        if (excludeIds != null && excludeIds.size()>0){
            map<id, id> excludeIdMap = new map<id,id>();
            for(id excludeId : excludeIds){
                excludeIdMap.put(excludeId, excludeId);
            }
            
            list<sObject> alteredResults = new list<sObject>();
            for(sObject sobj : results){
                if(!excludeIdMap.containsKey((Id)InputLookupAuraController.returnFieldValue(sobj, excludeIdFieldName.toLowerCase()))){
                    alteredResults.add(sObj);
                }
            }
            results = alteredResults;
        }

        
        List<SearchResult> output = new List<SearchResult>();
               
        for(SObject sobj : results){
            SearchResult sr = new SearchResult();
            sr.id = InputLookupAuraController.returnFieldValue(sobj, idFieldName.toLowerCase());
            sr.matchName = returnFormatStringMatches;
            for (String field : requiredFields){
                sr.matchName=sr.matchName.replace(fieldStartToken + field.toLowerCase() + fieldEndToken, InputLookupAuraController.returnFieldValue(sobj, field));
            }
            
            sr.chosenName = returnFormatStringDisplay;
            for (String field : requiredFields){
                sr.chosenName=sr.chosenName.replace(fieldStartToken + field.toLowerCase() + fieldEndToken, InputLookupAuraController.returnFieldValue(sobj, field));
            }
            output.add(sr);
                                               
        }
        
        return JSON.serialize(output);   
        //return output;                                

    }
    
    /*
     * Return the field value from sObj
     * This would normally be done with a .get e.g. sObj.get(field) but is complicated
     * by NOT being able to refer to related objects using that notation e.g. sObj.get('account.name') doesn't work.
     * Need to drill down through objects first.
    */
    private static string returnFieldValue(sObject sObj, string field){
        string returnValue = '';
        string[] subfieldsArr = field.split('\\.');
        sObject tempObj = sObj;
        integer count = 0;               
        
        for(string str : subfieldsArr){
            if (tempObj != null){
                count++;
                if (count == subfieldsArr.size()){
                    if(tempObj.get(str) != NULL){
                        returnValue=(String) tempObj.get(str);
                    } 
                    
                } else {
                    if(tempObj.getSobject(str) != NULL){
                        tempObj = tempObj.getSobject(str);
                    } else {
                        tempObj = null;
                    }
                }
            }
            
        }
        return returnValue;
    }
    
    @AuraEnabled
    public static String getCurrentValue(String type, String value){
        if(String.isBlank(type)){
            return null;
        }
        
        ID lookupId = null;
        try{   
            lookupId = (ID)value;
        }catch(Exception e){
            return null;
        }
        
        if(String.isBlank(lookupId)) return null;
        
        SObjectType objType = Schema.getGlobalDescribe().get(type);
        if(objType == null) return null;

        String nameField = getSobjectNameField(objType);
        String query ='Select Id, '+nameField+' From '+type+' Where Id = \''+lookupId+'\'';
       
        List<SObject> oList = Database.query(query);
        if(oList.size()==0) return null;
        
        return (String) oList[0].get(nameField);
    }
    
    /*
     * Returns the "Name" field for a given SObject (e.g. Case has CaseNumber, Account has Name)
    */
    private static String getSobjectNameField(SobjectType sobjType){
        
        //describes lookup obj and gets its name field
        String nameField = 'Name';
        Schema.DescribeSObjectResult dfrLkp = sobjType.getDescribe();
        for(schema.SObjectField sotype : dfrLkp.fields.getMap().values()){
            Schema.DescribeFieldResult fieldDescObj = sotype.getDescribe();
            if(fieldDescObj.isNameField() ){
                nameField = fieldDescObj.getName();
                break;
            }
        }
        return nameField;
    }
    
    /*
     * Utility class for search results
    */
    public class SearchResult{
        public String matchName{get;Set;}
        public String chosenName{get;Set;}
        public String id{get;set;}
    }
}