/********************************************************************************
 * Name        : BP_InstitutionalCtrl
 * Release     : R2
 * Phase       : P1
 * Description : Used for BP Insti Flow lightning components. Has common methods
 * Author      : Joshna
 * Reviewed By : Andy Wallis November 2016
 *********************************************************************************/
public with sharing class BP_InstitutionalFlowCtrl{
    //Creates a new institutional business plan. Name is always set to Business Plan followed by created date in 
    //current user's locale
    @AuraEnabled
    public static Id createNewInstiPlan(){
        Business_Plan__c newRec = new Business_Plan__c(Business_Line__c = System.Label.InstiBLLabel, OwnerId = UserInfo.getUserId(), Business_Plan_Duration__c = '1 Year', 
                                                        Starting_Fiscal_Year__c = Date.newInstance(Date.Today().year() + 1, 1, 1));
        newRec.Name = 'Business Plan ' + Date.Today().format();
        insert newRec;
        return newRec.Id;
    }
    
    //Update methods used to save strategic objectives + duration + fy of the plan
    @AuraEnabled
    public static Id updateBP(Id recordId, String overview, String duration, String durationOther,String yearVal){
        Business_Plan__c bpRec;
        if(recordId != NULL){
            bpRec = new Business_Plan__c(Id = recordId, Business_Plan_Summary__c = overview, Business_Plan_Duration__c = duration,Duration_Other__c = durationOther);
            if(!String.isBlank(yearVal)){
                bpRec.Starting_Fiscal_Year__c  = Date.valueOf(yearVal);
            }
        } else{
            bpRec = new Business_Plan__c(Business_Plan_Summary__c = overview, Business_Plan_Duration__c = duration,Duration_Other__c = durationOther);
            if(!String.isBlank(yearVal)){
                bpRec.Starting_Fiscal_Year__c  = Date.valueOf(yearVal);
            }
        }
        upsert bpRec;
        return bpRec.Id;
    }
    
    //Used by strategic overview screen to fetch the BP details
    @AuraEnabled
    public static Business_Plan__c getBPRecord(String recId){
        Business_Plan__c bpRec;
        if(!String.isBlank(recId)){
            for(Business_Plan__c each: [select Id, Starting_Fiscal_Year__c,Duration_Other__c, Business_Plan_Duration__c, Business_Plan_Summary__c from Business_Plan__c where Id = :recId]){
                bpRec = each;
            }
        }
        return bpRec;
    }
    
    //Common method to get picklist values for insti screens
    @AuraEnabled
    public static List<String> getPicklistValues(String objName, String fieldName){
        return BP_UKRetailFlowCtrl.getPicklistValues(objName, fieldName);
    }
    
    @AuraEnabled
    public static List<BPRecWrapper> getClientRecs(Id bpRecId){
        List<Business_Plan_Client_Relation__c> bpcRecs = new List<Business_Plan_Client_Relation__c>();
        List<BPRecWrapper> returnList = new List<BPRecWrapper>();
        Set<Id> clientIds = new Set<Id>();
        Integer year = Date.Today().year();
        Boolean getMeetingsCount = false;
        for(Business_Plan_Client_Relation__c each: [select Id, PY_Meetings__c, Client__r.Name, Expected_Meetings__c, Resource_Needs__c, Notes__c, Client__c, Business_Plan__r.Starting_Fiscal_Year__c,
                     (select Investment_Strategy__c, Investment_Strategy__r.Name from Business_Plan_Products__r) from Business_Plan_Client_Relation__c where Business_Plan__c = :bpRecId]){
            clientIds.add(each.Client__c);
            bpcRecs.add(each);
            if(each.Business_Plan__r.Starting_Fiscal_Year__c != NULL){
                year = each.Business_Plan__r.Starting_Fiscal_Year__c.year() - 1;
            }
            if(each.PY_Meetings__c == NULL){
                getMeetingsCount = true;
            }
        }
        DateTime startDate = DateTime.newInstance(year, 1, 1, 0, 0, 0);
        DateTime endDate = DateTime.newInstance(year, 12, 31, 23, 59, 59);
        Map<Id, Integer> activityCount = new Map<Id, Integer>();
        if(getMeetingsCount && !clientIds.isEmpty()){
            List<AggregateResult> eventARList = [select count(Id) cnt, AccountId from Event where AccountId IN :clientIds AND StartDateTime > :startDate AND EndDateTime <: endDate GROUP BY AccountId];
            for(AggregateResult each: eventARList){
                activityCount.put((Id) each.get('AccountId'), (Integer) each.get('cnt'));
            }
        }
        for(Business_Plan_Client_Relation__c each: bpcRecs){
            if(activityCount.containsKey(each.Client__c)){
                each.PY_Meetings__c = activityCount.get(each.Client__c);
            }
            List<Id> selectedIds = new List<Id>();
            List<String> selectedNames = new List<String>();
            if(!each.Business_Plan_Products__r.isEmpty()){
                for(Business_Plan_product_Relation__c eachProd: each.Business_Plan_Products__r){
                    selectedIds.add(eachProd.Investment_Strategy__c);
                    selectedNames.add(eachProd.Investment_Strategy__r.Name);
                }
            }
            //this is to avoid the JSON exception that lightning causes during parsing
            Business_Plan_Client_Relation__c newRec = new Business_Plan_Client_Relation__c(Id = each.Id, PY_Meetings__c = each.PY_Meetings__c, Expected_Meetings__c = each.Expected_Meetings__c,
                    Notes__c = each.Notes__c, Resource_Needs__c = each.Resource_Needs__c);
            returnList.add(new BPRecWrapper(newRec, selectedIds, selectedNames, each.Client__r.Name));
        }
        return returnList;
    }
    
    public class BPRecWrapper{
        @AuraEnabled
        public List<Id> selectedIds;
        @AuraEnabled
        public String clientName;
        @AuraEnabled
        public List<String> selectedNames;
        @AuraEnabled
        public Business_Plan_Client_Relation__c bpcRec;
        @AuraEnabled
        public Integer ExpectedMeetings;
        @AuraEnabled
        public String Notes;
        @AuraEnabled
        public String strategiesString;
        public BPRecWrapper(Business_Plan_Client_Relation__c busPlanClient, List<Id> selectedIds, List<String> selectedNames, String clientName){
            bpcRec = busPlanClient;
            this.clientName = clientName;
            this.selectedIds = selectedIds;
            this.selectedNames = selectedNames;
            strategiesString = String.join(selectedNames, ',');
            if(bpcRec.Expected_Meetings__c != NULL){
                ExpectedMeetings = Integer.valueOf(bpcRec.Expected_Meetings__c);
            }
            Notes = bpcRec.Notes__c;
        }
    }
    
    @AuraEnabled
    public static List<String> getStrategyNames(List<Id> selectedIds){
        List<String> strategyNames = new List<String>();
        for(Investment_Strategy__c each: [select Id, Name from Investment_Strategy__c where Id IN :selectedIds]){
            strategyNames.add(each.Name);
        }
        return strategyNames;
    }
    
    @AuraEnabled
    public static void updateBPC(String recordVals, Id recordId, List<String> resourceVals, List<Decimal> meetingVals, List<String> noteVals){
        if(!String.isBlank(recordVals)){
            Set<Id> bpcIds = new Set<Id>();
            List<Business_Plan_product_Relation__c> newProdRecs = new List<Business_Plan_product_Relation__c>();
            Id recTypeId = Schema.SObjectType.Business_Plan_Product_Relation__c.getRecordTypeInfosByName().get(System.Label.StrategyRecTypeName).getRecordTypeId();
            Map<Id, Map<Id, Business_Plan_product_Relation__c>> bpProdMap = new Map<Id, Map<Id, Business_Plan_product_Relation__c>>();
            List<BPRecWrapper> inputRecs = (List<BPRecWrapper>) System.JSON.deserialize(recordVals, List<BPRecWrapper>.class);
            List<Business_Plan_Client_Relation__c> toBeUpdated = new List<Business_Plan_Client_Relation__c>();
            List<Business_Plan_product_Relation__c> deleteProdRecs = new List<Business_Plan_product_Relation__c>();
            
            Integer i = 0;
            for(BPRecWrapper each: inputRecs){
                each.bpcRec.Resource_Needs__c = resourceVals.get(i);
                if(meetingVals.get(i) != NULL){
                    each.bpcRec.Expected_Meetings__c = meetingVals.get(i);
                }
                each.bpcRec.Notes__c = noteVals.get(i++);
                toBeUpdated.add(each.bpcRec);
                bpcIds.add(each.bpcRec.Id);
            }
            for(Business_Plan_product_Relation__c each: [select Investment_Strategy__c, Id, Business_Plan_Client__c from Business_Plan_product_Relation__c where Business_Plan_Client__c IN :bpcIds]){
                if(bpProdMap.containsKey(each.Business_Plan_Client__c)){
                    bpProdMap.get(each.Business_Plan_Client__c).put(each.Investment_Strategy__c, each);
                } else{
                    bpProdMap.put(each.Business_Plan_Client__c, new Map<Id, Business_Plan_product_Relation__c>{each.Investment_Strategy__c => each});
                }
            }
            for(BPRecWrapper each: inputRecs){
                Map<Id, Business_Plan_product_Relation__c> existingIds = new Map<Id, Business_Plan_product_Relation__c>();
                if(bpProdMap.containsKey(each.bpcRec.Id)){
                    existingIds = bpProdMap.get(each.bpcRec.Id);
                }
                for(Id eachId: each.selectedIds){
                    if(!existingIds.containsKey(eachId)){
                        newProdRecs.add(new Business_Plan_product_Relation__c(Business_Plan_Client__c = each.bpcRec.Id, Business_Plan__c = recordId, RecordTypeId = recTypeId, Investment_Strategy__c = eachId));
                    }
                }
                Set<Id> selectedIdSet = new Set<Id>();
                selectedIdSet.addAll(each.selectedIds);
                for(Id eachId: existingIds.keySet()){
                    if(!selectedIdSet.contains(eachId)){
                        deleteProdRecs.add(existingIds.get(eachId));
                    }
                }
            }
            
            Database.DeleteResult[] dResult = Database.delete(deleteProdRecs, false);
            Database.SaveResult[] iResult = Database.insert(newProdRecs, false);
            Database.SaveResult[] sResult = Database.update(toBeUpdated, false);
            
            for(Database.SaveResult each: sResult){
                if(!each.isSuccess()){
                    system.debug(each.getErrors());
                }
            }
        }
    }
}