public class KeyConAndCovergeTeam_C_AMA {    
   @auraEnabled
    public static List<Event> getRecActs(String accId){
        return [select Id,ActivityDate,Type__c ,subject, who.name,whoid, owner.name from Event where whatID = :accId order by activityDate desc limit 10];
    }
     @auraEnabled
    public static ClientDetailWrapper getAccountInfo(String accId){
      List<ClientDetailWrapper> lstcdw = new List<ClientDetailWrapper>();  
     List<Account> acc=   [select ID,name,Website,phone,Client_Logo__c, Billing_City__c, Billing_Country__c, Billing_County__c, Billing_PostalCode__c, Billing_State__c, Billing_Street_Line_1__c from Account Where id=:accId];
     List<Attachment> attachedFiles = [select Id from Attachment where parentId =:accId and Name like '%Logo%' order By LastModifiedDate DESC limit 1];
	 ClientDetailWrapper obj = new ClientDetailWrapper()     ;
       	  obj.sName =  acc[0].name;  
          obj.sAddress =  acc[0].Billing_Street_Line_1__c + ', ' + acc[0].Billing_City__c + ', '+ acc[0].Billing_State__c +', '+ acc[0].Billing_Country__c +', '+ acc[0].Billing_PostalCode__c ;
          obj.sWebsite =  acc[0].website;
          obj.sPhone =  acc[0].phone;
        if(!attachedFiles.isEmpty()) obj.sLogoId = '/servlet/servlet.FileDownload?file='+ attachedFiles[0].id;
        lstcdw.add(obj);
        System.debug(obj) ;
        return  obj;
    }
    @auraEnabled
    public static KeyCovAndTeam_DTO_AMA getKeyConAndCovTeamData(String accId){
        KeyCovAndTeam_DTO_AMA objKeyConDTO ;
        KeyCovAndTeam_DTO_AMA.CoverageTeamMember objCTM ;
        KeyCovAndTeam_DTO_AMA.keyContacts objKeyCon;
        Map<ID, KeyCovAndTeam_DTO_AMA.keyContacts> mapKeyContacts = new Map<ID, KeyCovAndTeam_DTO_AMA.keyContacts> ();
        Map<ID, KeyCovAndTeam_DTO_AMA.CoverageTeamMember> mapCovTeam = new Map<ID, KeyCovAndTeam_DTO_AMA.CoverageTeamMember> ();
        Set<ID> setContactsId = new Set<ID>();
           if(String.isNotBlank(accId))
           {	
               objKeyConDTO = new KeyCovAndTeam_DTO_AMA();
                for( AccountContactRelation objconRel : [SELECT Id, Roles, ContactID,Contact.name, Key_Contact__c FROM AccountContactRelation where Key_Contact__c  = true and AccountID = :accId])           
                {   objKeyCon = new KeyCovAndTeam_DTO_AMA.keyContacts(objconRel.Contact.name,objconRel.Roles,0);
                    mapKeyContacts.put(objconRel.ContactID,objKeyCon);  
                }               
               for( AggregateResult objAggr : [SELECT count(Id) total, WhoID FROM Task where whoId in :mapKeyContacts.keySet() and WhatID =:accId group by WhoID])
               {
                   KeyCovAndTeam_DTO_AMA.keyContacts objkecon = mapKeyContacts.get((ID)objAggr.get('WhoID'));
                   objkeycon.recActivityCount = objkeycon.recActivityCount + (Integer)objAggr.get('total');
                   mapKeyContacts.put((ID)objAggr.get('WhoID'),objkeycon);                   
               }
                for( AggregateResult objAggr : [SELECT count(Id) total, WhoID FROM Event where whoId in :mapKeyContacts.keySet() and WhatID =:accId group by WhoID])
               {
                   KeyCovAndTeam_DTO_AMA.keyContacts objkecon = mapKeyContacts.get((ID)objAggr.get('WhoID'));
                   objkeycon.recActivityCount = objkeycon.recActivityCount + (Integer)objAggr.get('total');
                   mapKeyContacts.put((ID)objAggr.get('WhoID'),objkeycon); 
               }
                
               for( AccountTeamMember  objATM:   [SELECT Id, AccountId, UserId,User.name, user.email,TeamMemberRole FROM AccountTeamMember where AccountID = :accId])
               {
                   objCTM = new KeyCovAndTeam_DTO_AMA.CoverageTeamMember(objATM.user.name,objATM.TeamMemberRole,objATM.user.email,0);
                    mapCovTeam.put(objATM.UserId,objCTM);  
               }
               for( AggregateResult objAggr : [SELECT count(Id) total, ownerID FROM Task where ownerID in :mapCovTeam.keySet() and WhatID =:accId group by ownerID])
               {
                  KeyCovAndTeam_DTO_AMA.CoverageTeamMember objCovTeam = mapCovTeam.get((ID)objAggr.get('ownerID'));
                   objCovTeam.covTeamMemRecActCount = objCovTeam.covTeamMemRecActCount + (Integer)objAggr.get('total');
                   mapCovTeam.put((ID)objAggr.get('ownerID'),objCovTeam);                                    
               }
                for( AggregateResult objAggr : [SELECT count(Id) total, ownerID FROM Event where ownerID in :mapCovTeam.keySet() and WhatID =:accId group by ownerID])
               {
                 KeyCovAndTeam_DTO_AMA.CoverageTeamMember objCovTeam = mapCovTeam.get((ID)objAggr.get('ownerID'));
                   objCovTeam.covTeamMemRecActCount = objCovTeam.covTeamMemRecActCount + (Integer)objAggr.get('total');
                   mapCovTeam.put((ID)objAggr.get('ownerID'),objCovTeam); 
               }
               
                objKeyConDTO.lstCovTeamMem.addAll(mapCovTeam.values());
                objKeyConDTO.lstKeyCon.addAll(mapKeyContacts.values());
           }
        system.debug(objKeyConDTO);
        return objKeyConDTO;
     }
    @auraEnabled
    public static DataWrapper prepareChartData(){
        Decimal totalAUM = 0.0;
        map<String,decimal> investmentStrategyAUM = new map<String,decimal>();
        list<PieDataWrapper> piedataWrapperList = new list<PieDataWrapper>();
        for(AUM_by_Strategy__c tempVar : [select Id,Total_Inst_AUM__c,Investment_Strategy__r.Name from AUM_by_Strategy__c]){
            totalAUM += tempVar.Total_Inst_AUM__c;
            if(!investmentStrategyAUM.containsKey(tempVar.Investment_Strategy__r.Name))
                investmentStrategyAUM.put(tempVar.Investment_Strategy__r.Name,tempVar.Total_Inst_AUM__c);
           	else
                investmentStrategyAUM.put(tempVar.Investment_Strategy__r.Name,investmentStrategyAUM.get(tempVar.Investment_Strategy__r.Name)+tempVar.Total_Inst_AUM__c);
        }
        for(String strategyName : investmentStrategyAUM.keySet()){
            decimal stratPer = (investmentStrategyAUM.get(strategyName)/totalAUM)*100;
            stratPer = stratPer.setScale(2);
            PieDataWrapper temp = new PieDataWrapper();
            temp.name = strategyName;
            temp.y = stratPer;
            piedataWrapperList.add(temp);
        }
        DataWrapper dw = new DataWrapper();
        SeriesWrapper sw = new SeriesWrapper();
        PieWrapper pw = new PieWrapper();
        pw.chartTitle = 'Strategies';
        dw.pieChartDetails = pw;
        sw.name = 'Strategy';
        sw.data = new list<PieDataWrapper>();
        sw.data.addAll(piedataWrapperList);
        dw.series = new list<SeriesWrapper>();
        dw.series.add(sw);
        return dw;
    }
    
    public class DataWrapper{
        @AuraEnabled
        public list<SeriesWrapper> series{get;set;}
        @AuraEnabled
        public PieWrapper pieChartDetails{get;set;}
    }
    
    public Class PieWrapper{
        @AuraEnabled
        public string chartTitle{get;set;}
    }
    
    public class PieDataWrapper{
        @AuraEnabled
        public string name{get;set;}
        @AuraEnabled
        public Decimal y{get;set;}
        
    }
    
    public class SeriesWrapper{
        @AuraEnabled
        public string name{get;set;}
        @AuraEnabled
        public list<PieDataWrapper> data{get;set;}
        public SeriesWrapper(){
            
        }
    }
     
}