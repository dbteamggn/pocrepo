public class DBDealTeamMemberApexSharing {
    
    private static DBDealTeamMemberApexSharing instance = null;
    
    public static DBDealTeamMemberApexSharing getInstance() {
        
        if (instance == null) {
            
            instance = new DBDealTeamMemberApexSharing();
        }
        
        return instance;
    }
    
    public void createDealShares (List<DB_Deal_Team_Member__c> dtmList) {
        
        List<OpportunityShare> dealShrs  = new List<OpportunityShare>();
        
        OpportunityShare dealShare;
        
        for(DB_Deal_Team_Member__c dtm : dtmList){
            
            boolean createNewShare = true;
            
            List<OpportunityShare> existingShares = [SELECT Id, OpportunityAccessLevel
                                                     FROM OpportunityShare 
                                                     WHERE OpportunityId =: dtm.DB_Deal_Name__c
                                                     AND UserOrGroupId =: dtm.DB_User__c];
            
            for(OpportunityShare share : existingShares) {
                if(share.OpportunityAccessLevel == 'Edit'){
                    createNewShare = false;
                    break;
                }
                else if(share.OpportunityAccessLevel == 'Read' && dtm.DB_Access_Level__c == 'Read Only') {
                    createNewShare = false;
                    break;
                }
            }
            
            if(createNewShare) {
                dealShare = new OpportunityShare();
                
                dealShare.OpportunityId = dtm.DB_Deal_Name__c;
                
                dealShare.UserOrGroupId = dtm.DB_User__c;
                
                if (dtm.DB_Access_Level__c == 'Read Only') {
                    dealShare.OpportunityAccessLevel = 'Read';
                }
                if (dtm.DB_Access_Level__c == 'Read / Write') {
                    dealShare.OpportunityAccessLevel = 'Edit';
                }
                
                dealShrs.add(dealShare);
            }
        }
        
        Database.SaveResult[] lsr = Database.insert(dealShrs, false);
        
        Integer i=0;
        
        for(Database.SaveResult sr : lsr){
            if(!sr.isSuccess()){
                Database.Error err = sr.getErrors()[0];
                
                if(!(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  
                     &&  err.getMessage().contains('AccessLevel'))){
                         trigger.newMap.get(dealShrs[i].OpportunityId).
                             addError(
                                 'Unable to grant sharing access due to following exception: '
                                 + err.getMessage());
                     }
            }
            i++;
        }
    }
    
    public void modifyDealShares (List<DB_Deal_Team_Member__c> dtmDeletedList) {
        
        List<OpportunityShare> sharesToModify  = new List<OpportunityShare>();
        List<OpportunityShare> sharesToDelete  = new List<OpportunityShare>();
        
        for(DB_Deal_Team_Member__c dtm : dtmDeletedList) {
            
            boolean modifyShare = true;
            boolean deleteShare = true;
            
            List<DB_Deal_Team_Member__c> existingDTMs = [SELECT Id, DB_Access_Level__c
                                                         FROM DB_Deal_Team_Member__c
                                                         WHERE DB_Deal_Name__c =: dtm.DB_Deal_Name__c
                                                         AND DB_User__c =: dtm.DB_User__c
                                                         AND DB_Conflict_Clearing_Status__c = 'Approved'
            											 AND Id !=: dtm.Id];

            if(existingDTMs.size() > 0) {
                deleteShare = false;
                for(DB_Deal_Team_Member__c existingDTM : existingDTMs) {
                    if(existingDTM.DB_Access_Level__c == 'Read / Write'){
                        modifyShare = false;
                        break;
                    }
                }
            }
            else {
                modifyShare = false;
            }
            
            OpportunityShare existingShare = [SELECT Id, OpportunityAccessLevel
                                                     FROM OpportunityShare 
                                                     WHERE OpportunityId =: dtm.DB_Deal_Name__c
                                                     AND UserOrGroupId =: dtm.DB_User__c];
            
            if(deleteShare) {
                sharesToDelete.add(existingShare);
                continue;
            }
            if(modifyShare) {
                existingShare.OpportunityAccessLevel = 'Read';
                sharesToModify.add(existingShare);
                continue;
            }   
        }
        
        if(!sharesToModify.isEmpty()) {
            Database.update(sharesToModify, false);
        }
        if(!sharesToDelete.isEmpty()) {
            Database.delete(sharesToDelete, false);
        }
    }
}