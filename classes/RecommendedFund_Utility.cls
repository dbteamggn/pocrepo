public class RecommendedFund_Utility {
    
    public static void linkRelatedFundsWithClients(TriggerHandler.TriggerParameter tParam){
        List<Client_Fund__c> triggerCompanyFunds = (List<Client_Fund__c>) tParam.newList;
        Set<Id> accountIds = new Set<Id>();
        List<Client_Fund__c> childRecs = new List<Client_Fund__c>();
        Map<Id, List<Client_Fund__c>> clientToClientFunds = new Map<Id, List<Client_Fund__c>>();
        String emptyString = '';
        Set<String> isAlreadyAdded = new Set<String>();
        
        if(!RecurssiveTriggerController.recommendedFundRetrigger){
            for (Client_Fund__c triggerCompanyFund: triggerCompanyFunds){ 
                accountIds.add(triggerCompanyFund.Client__c);
                if(clientToClientFunds.containsKey(triggerCompanyFund.Client__c)){
                    clientToClientFunds.get(triggerCompanyFund.Client__c).add(triggerCompanyFund);
                } else{
                    clientToClientFunds.put(triggerCompanyFund.Client__c, new List<Client_Fund__c>{triggerCompanyFund});
                }
            }
            // Query the child accounts based on the parent account and its associated product.
            for(Account each: [SELECT id, Hierarchy_Level__c, parentId, parent.parentId, parent.parent.parentID, (select Product__c from
                                Client_Funds__r) from Account where 
                                parentId In :clientToClientFunds.keySet() or parent.parentId IN :clientToClientFunds.keySet() or 
                                parent.parent.parentId IN :clientToClientFunds.keySet()]){
                Id clientId;
                if(each.ParentId != NULL && clientToClientFunds.containsKey(each.ParentId)){
                    clientId = each.ParentId;
                } else if(each.Parent.ParentId != NULL && clientToClientFunds.containsKey(each.Parent.ParentId)){
                    clientId = each.Parent.ParentId;
                } else if(each.Parent.Parent.ParentId != NULL && clientToClientFunds.containsKey(each.Parent.Parent.ParentId)){
                    clientId = each.Parent.Parent.ParentId;
                }
                if(!each.Client_Funds__r.isEmpty()){
                    for(Client_Fund__c eachFund: each.Client_Funds__r){
                        isAlreadyAdded.add(emptyString + each.Id + eachFund.Product__c);
                    }
                }
                if(clientId != NULL && clientToClientFunds.get(clientId) != NULL){
                    for(Client_Fund__c eachFund: clientToClientFunds.get(clientId)){
                        if(!isAlreadyAdded.contains(emptyString + each.Id + eachFund.Product__c)){
                            childRecs.add(new Client_Fund__c(Fund_Status_at_Company__c = eachFund.Fund_Status_at_Company__c, Client__c = each.Id, 
                                Product__c = eachFund.Product__c, Is_Inherited_From_Hierarchy__c = true));
                            isAlreadyAdded.add(emptyString + each.Id + eachFund.Product__c);
                        }
                    }
                }
            }
            // Insert the child records by linking with the parents fund.
            RecurssiveTriggerController.recommendedFundRetrigger = TRUE;            
            Database.SaveResult[] sResult = Database.insert(childRecs, false);
            for(Database.SaveResult each: sResult){
                if(!each.isSuccess()){
                    system.debug('****An error occurred: ' + each.getErrors());
                }
            }
            RecurssiveTriggerController.recommendedFundRetrigger = FALSE;
        }
    }
    
    public static void deleteRecommendedFunds(TriggerHandler.TriggerParameter tParam){
        Set<ID> productIds = new Set<ID>();
        Set<ID> accountIDs = new Set<ID>();        

        if(!RecurssiveTriggerController.recommendedFundRetrigger){
            for(Client_Fund__c cf : (List<Client_Fund__c>)tParam.oldList){
                productIDs.add(cf.Product__c);
                accountIDs.add(cf.Client__c);            
            }
            List<Client_Fund__c> toDeleteList = new List<Client_Fund__c>();
            
            List<Account> childAccounts = [select Id, name from Account where parentID IN :accountIDs or 
                            parent.parentID IN :accountIDs or parent.parent.parentID IN :accountIDs];

            for(Client_Fund__c existingCF :[select id, client__c, Product__c from Client_Fund__c where Product__c IN :productIDs
                                        AND Client__c IN :childAccounts AND Is_Inherited_from_Hierarchy__c = TRUE]){
                toDeleteList.add(existingCF);
            }
            
            if(toDeleteList.size()>0){
                RecurssiveTriggerController.recommendedFundRetrigger = true;
                Database.DeleteResult[] dResult = Database.delete(toDeleteList, false);
                for(Database.DeleteResult each: dResult){
                    if(!each.isSuccess()){
                        system.debug('**** an error occurred: ' + each.getErrors());
                    }
                }
                RecurssiveTriggerController.recommendedFundRetrigger = false;
            }
        }
    }
}