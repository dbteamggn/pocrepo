/* Name: TableAuraController
   Description: Will be used to display tables in lightning components
   Current features: Accepts a mandatory object name and mandatory fields list and an optional where clause
*/
public with sharing class TableAuraController{
    @AuraEnabled
    public static List<ObjectResult> getRecords(String objectName, String fieldNames, String whereClause, 
                                                String orderBy, String limitValue, String searchStr, 
                                                String searchFieldNames, List<Id> excludeIds, String excludeIdFieldName,
                                                String idFieldName, List<id> selectedIds){
        List<ObjectResult> returnResults = new List<ObjectResult>();
        List<String> allFields = (List<String>) System.JSON.deserialize(fieldNames, List<String>.class);
        List<String> searchFields = new List<String>();
        if(!String.isBlank(searchFieldNames)){
            searchFields = (List<String>) System.JSON.deserialize(searchFieldNames, List<String>.class);
        }
        Boolean displayId = false;
        
        Set<String> allFieldsSet = new Set<String>();
        allFieldsSet.addAll(allFields);
        if(allFieldsSet.contains(idFieldName)){
            displayId = true;
        }
        allFieldsSet.add(idFieldName);
        List<String> uniqueFields = new List<String>();
        uniqueFields.addAll(allFieldsSet);
        String queryStr = 'SELECT ' + String.join(uniqueFields, ',') + ' FROM ' + objectName;
        if(!String.isBlank(whereClause)){
            queryStr += ' where ' + whereClause;
        }
        if(!selectedIds.isEmpty()){
            queryStr += ' where Id IN :selectedIds';
        }
        if(!String.isBlank(searchStr)){
            String likeStr = ' LIKE \'%' + searchStr + '%\'';
            if(!String.isBlank(whereClause)){
                queryStr += ' AND (';
            } else{
                queryStr += ' where (';
            }
            for(Integer i = 1; i <= searchFields.size(); i++){
                if(i == searchFields.size()){
                    queryStr += searchFields.get(i-1) + likeStr;
                } else{
                    queryStr += searchFields.get(i-1) + likeStr + ' OR ';
                }
            }
            queryStr += ')';
        }
        
        if (excludeIds != null && !excludeIds.isEmpty()){
            if(!String.isBlank(whereClause) || !String.isBlank(searchStr)){
                queryStr += ' AND ' + excludeIdFieldName + ' NOT IN :excludeIds';
            } else{
                queryStr += ' where ' + excludeIdFieldName + ' NOT IN :excludeIds';
            }
        }
                
        if(!String.isBlank(orderBy)){
            queryStr += ' order by ' + orderBy;
        }
        if(!String.isBlank(limitValue)){
            queryStr += ' LIMIT ' + limitValue;
        } else {
            queryStr += ' LIMIT 100';
        }
        for(sObject each: Database.query(queryStr)){
            List<String> allValues = new List<String>();
            Id idVal;
            for(String eachF: uniqueFields){
                String[] subfieldsArr = eachF.split('\\.');
                sObject tempObj = each;
                integer count = 0;
                
                String valueToAdd = '';
                for(string str : subfieldsArr){
                    if (tempObj != null){
                        count++;        
                        if (count == subfieldsArr.size()){
                             if(tempObj.get(str) != NULL){
                                valueToAdd = String.valueOf(tempObj.get(str));
                             }
                        } else {
                            if(tempObj.getSobject(str) != NULL){
                                tempObj = tempObj.getSobject(str);
                            } else {
                                tempObj = null;
                            }
                        }
                    }
                }
                if(eachF == idFieldName){
                    idVal = Id.valueOf(valueToAdd);
                    if(!displayId) continue;
                    else allValues.add(valueToAdd);
                } else{
                    allValues.add(valueToAdd);
                }
            }
            if(!selectedIds.isEmpty()){
                returnResults.add(new ObjectResult(allValues, idVal, true));
            } else{
                returnResults.add(new ObjectResult(allValues, idVal, false));
            }
        }
        return returnResults;
    }
    
    public class ObjectResult {
       @AuraEnabled
       public List<String> objValues;
       @AuraEnabled
       public Id objId;
       @AuraEnabled
       public Boolean isSelected;
       public ObjectResult(List<String> objValues, Id recId, Boolean isSelected){
           this.objValues = objValues;
           objId = recId;
           this.isSelected = isSelected;
       }
    }
}