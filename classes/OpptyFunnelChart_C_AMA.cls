public class OpptyFunnelChart_C_AMA {
   
     @auraEnabled
    public static List<Opportunity> getOptyTableData(String accId){
        return [SELECT Id, Name, StageName, Amount, CloseDate,Pools_of_Capital__r.Scheme_Type__c FROM Opportunity where isClosed = false AND AccountId = :accId];
    }
    
    @auraEnabled
    public static DataWrapper prepareFunnelData(String accId){
        
        list<funnelDataWrapper> funnelDataWrapperList = new list<funnelDataWrapper>();
        for(AggregateResult tempVar : [select Sum(Amount) sumAmt,StageName stage from opportunity where accountID = :accId AND StageName != 'Closed' group by StageName]){
            funnelDataWrapperList.add( new funnelDataWrapper( (Decimal)tempVar.get('sumAmt'),  (String)tempVar.get('stage')));
        }
        
        DataWrapper dw = new DataWrapper();
        SeriesWrapper sw = new SeriesWrapper();
        FunnelWrapper fw = new FunnelWrapper();
        fw.chartTitle = 'Open Opportunities';
        dw.funnelChartDetails = fw;
        sw.name = 'Opportunities';
        sw.data = funnelDataWrapperList;
        dw.series = new list<SeriesWrapper>();
        dw.series.add(sw);
        return dw;
    }
    
    public class DataWrapper{
        @AuraEnabled
        public list<SeriesWrapper> series{get;set;}
        @AuraEnabled
        public FunnelWrapper funnelChartDetails{get;set;}
    }
    
    public Class FunnelWrapper{
        @AuraEnabled
        public string chartTitle{get;set;}
    }
    
    public class funnelDataWrapper{
        @AuraEnabled
        public string name{get;set;}
        @AuraEnabled
        public Decimal sumAmt{get;set;}
        public funnelDataWrapper(Decimal amt, String sName)
        {
         name = sName;
         sumAmt = amt;
        }
        
    }
    
    public class SeriesWrapper{
        @AuraEnabled
        public string name{get;set;}
        @AuraEnabled
        public list<funnelDataWrapper> data{get;set;}
        public SeriesWrapper(){
            
        }
    }

}