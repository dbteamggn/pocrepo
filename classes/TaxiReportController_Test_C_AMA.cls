public  class TaxiReportController_Test_C_AMA {
    public  Account objTPClient {get;set;}
    public TaxiReportController_Test_C_AMA()
    {
        String thpId = ApexPages.currentPage().getParameters().get('id');
        if(!String.isBlank(thpId))
        {
            objTPClient = [select ID,Business_Line__c,name,Website,phone, Billing_City__c, Client_Logo__c,Billing_Country__c, Billing_County__c, Billing_PostalCode__c, Billing_State__c, Billing_Street_Line_1__c from Account Where id=:thpId];
            
        }
    }

}