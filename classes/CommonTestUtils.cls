@isTest
public class CommonTestUtils {
    
  
  /*
     * 
     * Description : Create a default Client
     * Param :   Client name
     * Returns : Account
     * 
    */
    public static Boolean BLMappingExists = false;
    
    public static Account CreateTestClient(string accountName){
    Account acc = new Account();
    acc.name = accountName;
    acc.Hierarchy_Level__c = 'L1';
    acc.status__c='Active';
        // Because Billing_PostalCode__c is mandatory we need to populate it.
        // Cannot hardcode as that causes Duplicate Rules to cause an exception
        // the account names are similar & the post code is the same.
        // Base the post code on the data after the last space (if there is one), or on the last 5 characters of the account name
        if (accountName.contains(' ')){
          acc.Billing_PostalCode__c = accountName.substringAfterLast(' ');
        } else {
            acc.Billing_PostalCode__c= accountName.right(Math.min(5, accountName.length()));
        }
    acc.Business_Line__c = 'Retail';
    acc.Region__c = 'EMEA';
    acc.Status__c = 'Active';
    acc.RecordTypeId = Account.sObjectType.getDescribe().getRecordTypeInfosByName().get('Client').getRecordTypeId();
    return acc;
  }
  
    public static void insertBLMapping(String nameVal, String fnVal){
        if(BLMappingExists) return;
        else{
            Busines_Line_Mapping__c newMapping = new Busines_Line_Mapping__c(Name = nameVal, Field_Name__c = fnVal);
            Database.insert(newMapping, false);
            BLMappingExists = true;
        }
    }
    
    /*
     * 
     * Description : Create a default Third Party
     * Param :   Client name
     * Returns : Account
    */
    public static Account CreateTestThirdParty(string accountName){
    Account acc = new Account();
    acc.name = accountName;
     acc.status__c='Active';
    acc.Hierarchy_Level__c = 'L1';
        // Because Billing_PostalCode__c is mandatory we need to populate it.
        // Cannot hardcode as that causes Duplicate Rules to cause an exception
        // the account names are similar & the post code is the same.
        // Base the post code on the data after the last space (if there is one), or on the last 5 characters of the account name
        if (accountName.contains(' ')){
          acc.Billing_PostalCode__c = accountName.substringAfterLast(' ');
        } else {
            acc.Billing_PostalCode__c = accountName.right(Math.min(5, accountName.length()));
        }
        acc.Business_Line__c = 'Retail';
        acc.Region__c = 'EMEA';
        acc.Status__c = 'Active';
        acc.RecordTypeId = Account.sObjectType.getDescribe().getRecordTypeInfosByName().get('Third Party').getRecordTypeId();    
    return acc;
  }
    
    /*
     * 
     * Description : Create a default Client and Third Party Association
     * Param :   Client Id
     * Param :   Third Party Id
     * Returns : Client_Third_Party_Relationship__c
    */
    public static Client_Third_Party_Relationship__c CreateTestClientThirdPartyAss(Id clientId, Id thirdPartyId){
        Client_Third_Party_Relationship__c obj = new Client_Third_Party_Relationship__c();
        obj.Client_Name__c = clientId;
        obj.Third_Party_Name__c = thirdPartyId;
        System.debug('obj.Client_Name__c '+obj.Client_Name__c);
        System.debug('obj.Third_Party_Name__c '+obj.Third_Party_Name__c);
        Contact thirdPartyCon = CreateTestContact('last');
        thirdPartyCon.Accountid = thirdPartyId;
        insert thirdPartyCon;
        obj.Lead_Third_Party_Contact__c = thirdPartyCon.id;
        
        return obj;
  }
  
  /*
     * 
     * Description : Create a default Contact
     * Param :   Contact name
     * Returns : Contact  
    */
    public static Contact CreateTestContact(string lastName){
    Contact c = new Contact();
    c.Status__c ='Active';
    c.LastName = lastName;
        
    //insert c;
    return c;
    
  }
  
  /*
     * 
     * Description : Create a default Product
     * Param :   Product name
     * Returns : Product  
    */
    public static Product2 CreateTestProduct(string name){
    Product2 p = new Product2();
    p.name = name;    
    return p;
    
  }
  
  /*
     * 
     * Description : Create a default Investment Strategy
     * Param :   Investment Strategy name
     * Returns : Investment_Strategy__c  
    */
    public static Investment_Strategy__c CreateTestInvestmentStrategy(string name){
    Investment_Strategy__c ins = new Investment_Strategy__c();
    ins.name = name;    
    return ins;
    
  }
  
    /*
     * Description : Create a default Campaign
     * Param :   Campaign name
     * Returns : Campaign
     */
    public static Campaign CreateTestCampaign(string name){
        return CommonTestUtils.CreateTestCampaign(name, true);
    }
    
    /*
     * Description : Create a default Campaign
     * Param :   Campaign name
     * Returns : Campaign
     */
    public static Campaign CreateTestCampaign(string name, boolean insertCampaign){
        Campaign c = new Campaign();
        c.name = name; 
        c.Campaign_Country__c = 'UK';
        if (insertCampaign){
            insert c;
        }
        return c;
    }

    /*
     * Description : Create a default Campaign Team
     * Param :   Campaign 
     * Param :   contact
     * Returns : Campaign Team
     */
    public static Campaign_Team__c createTestCampaignTeam(Campaign campaign, Contact contact){
        Campaign_Team__c c = new Campaign_Team__c();
        c.Campaign__c  = campaign.id;    
        c.Team_Member__c = contact.id;
        c.Notification__c = false;
        insert c;
        return c;
    }
    /*
     * Description : Create a default Scheme
     * Param :   client ID      
     * Returns : Scheme
     */
    public static Pools_of_Capital__c createTestScheme(Account client){
        Pools_of_Capital__c scheme = new Pools_of_Capital__c();
        scheme.Client_Name__c = client.id;
        scheme.Scheme_Type__c='White Label';
        insert scheme;
        return scheme;
    }
    /*
     * Description : Create a default Scheme Third Party Association
     * Param :   Scheme 
     * Param :   Client-Third Party Association
     * Returns : scheme third party association
     */
  
    public static Scheme_Third_Party_Relationship__c createTestSchemeThirdPartyAss(Pools_of_Capital__c scheme, Client_Third_Party_Relationship__c thirdParty){
    Scheme_Third_Party_Relationship__c stp = new Scheme_Third_Party_Relationship__c();
        stp.Scheme__c = scheme.id;
        stp.Third_Party__c = thirdParty.id;
        return stp;
    }
    /*
     * Description : Create a default Rated Strategy
     * Param :   thirdPartyId - The accountId of a Third Party
     *           investmentStrategyId - the id for the related investment_strategy__c
     *           currentRating - The current rating
     * Returns : Rated_Strategy__c
     */
    public static Rated_Strategy__c CreateTestRatedStrategy(id thirdPartyId, id investmentStrategyId, string currentRating){
        Rated_Strategy__c rs = new Rated_Strategy__c();
        rs.third_party__c = thirdPartyId;    
        rs.investment_strategy__c = investmentStrategyId;    
        rs.current_rating__c = currentRating;
        rs.Comments__c = 'Test Comment';    
        return rs;
    }
  
    /*
     * Description : Create a default Opportunity (Mandate)
     * Param :   name
     *           accountId
     *           closeDate - defaults to tomorrow if not entered
     *           stageName e.g. Buy
     * Returns : Opportunity
     */
    public static Opportunity CreateTestOpportunityMandate(string name, id accountId, date closeDate, string stageName){
        Opportunity opp = new Opportunity();
        opp.RecordTypeId = Opportunity.sObjectType.getDescribe().getRecordTypeInfosByName().get('New Institutional Opportunity').getRecordTypeId();
        opp.name = name;
        opp.AccountId = accountId;
        if (closeDate == null){
            opp.closeDate = system.Date.today() + 1;
        } else {
            opp.closeDate = closeDate;
        }
        opp.StageName = stageName;
    
        return opp;
    }
  
    /*
     * Description : Create a default Opportunity (New Rated Strategy)
     * Param :   name
     *           accountId
     *           closeDate - defaults to tomorrow if not entered
     *           stageName e.g. Rated
     *           Rating e.g. Sell (Defaults to Buy if not supplied & stageName is either Rated or Not Rated.)
     * Returns : Opportunity
     */
    public static Opportunity CreateTestOpportunityRatedStrat(string name, id accountId, date closeDate, string stageName, string rating){
        Opportunity opp = new Opportunity();        
        opp.RecordTypeId = Opportunity.sObjectType.getDescribe().getRecordTypeInfosByName().get('Strategy Rating').getRecordTypeId();
        opp.name = name;
        opp.AccountId = accountId;
        if (closeDate == null){
            opp.closeDate = system.Date.today() + 1;
        } else {
            opp.closeDate = closeDate;
        }
        opp.StageName = stageName;
        if (stageName == 'Rated' || stageName == 'Not Rated'){
            if (rating == null){
                opp.Target_Rating__c = 'Buy'; // Default to Buy if rating not supplied.  Mandatory value if Stage = Rated/Not Rated
            } else {
                opp.Target_Rating__c = rating;
            }
        }
    
        return opp;
    }
    
     /*
     * Description : Create a default Client Third Party Relationship 
     * Param :  Client Id
     * Param :  Third Party Id
     * Returns : Client_Third_Party_Relationship__c 
     */
    public static Client_Third_Party_Relationship__c CreateTestClientTPRel(string clientId, String thirdPartyId){
        Client_Third_Party_Relationship__c cp = new Client_Third_Party_Relationship__c();
        cp.Client_Name__c = clientId;
        cp.Third_Party_Name__c = thirdPartyId;
        return cp;
    }
    
    /*
     * Description : Create a default subscription
     * Param :  Subscription Name ISM
     * Param :  Subscription Type
     * Returns : Subscription
     */
    public static Subscription__c CreateTestSubscription(string name, String subscriptionType){
        Subscription__c s = new Subscription__c ();    
        s.Subscription_Name__c = name;
        s.Limited_by_Countries__c = 'Germany';
        insert s;
        return s;
    }
    
    /*
     * Description : Create a default contact, subscription
     * Param :  Contact
     * Param :  Subscription 
     * Returns : Contact_Subscription_Link__c 
     */
    
    public static Contact_Subscription_Link__c createTestContactSubscription(Contact c , Subscription__c s){
        Contact_Subscription_Link__c link = new Contact_Subscription_Link__c();
        link.Contact__c = c.id;
        link.Subscription__c = s.id;
        insert link;
        return link;
    }
    
    /*
     * Description : Create a gifts & benefits
     * Param :  Contact 
     * Returns : Gift_Benefit__c
     */
    
    public static Gift_Benefit__c  createTestGiftBenefit(Contact c, Account a){
        Gift_Benefit__c obj = new Gift_Benefit__c();
        obj.Amount__c = 1000;
        obj.Related_Contact__c = c.id;
        obj.Client__c = a.id;
        return obj;
    }
  
  /*
     * 
     * Description : Create a default Event
     * Param :   Subject description
     *       :   Event Start Date/Time
     *       :   Event End Date/Time
     *       :   Id of linked Contact
     * Returns : Event  
    */
    public static Event CreateTestEvent(string subject, datetime startDateTime, datetime endDateTime, id whoId, string eventType, string status){
    Event e = new Event();
    e.Subject = subject;
        e.StartDateTime = startDateTime;
        e.EndDateTime = endDateTime;
        e.WhoId = whoId;
        e.type = eventType;
        e.Type__c = eventType;
        e.Status__c = status;
    
    return e;
    
  }
  
  /*
     * 
     * Description : Create a default Task
     * Param :   Subject description
     *       :   Event Start Date/Time
     *       :   Event End Date/Time
     *       :   Id of linked Contact
     * Returns : Event  
    */
    public static Task CreateTestTask(string subject, id ownerId, String priority, String status, id whoId){
    Task t = new Task();
    t.Subject = subject;
        t.OwnerId = ownerId;
        t.priority = priority;
        t.Status = status;
        t.WhoId = whoId;
    
    return t;
    }
    
    /*
     * Description : Create a Financial Account
     * Param :  FA Status, Client/TP, FA Name 
     * Returns : Asset
     */
    
    public static Asset  createTestAsset(String Status, Account a, String Name){
        Asset obj = new Asset();
        obj.Name = Name;
        obj.AccountId = a.id;
        obj.Status = Status;
        return obj;
    }
    
    /*
     * 
     * Description : Return a default user for testing purposes based upon the user doing the requesting
     * Param :   Boolean indicating whether to create the user in this procedure or not
     * Returns : User  
    */
    public static User createTestUser(boolean createUser) {
        id userId = UserInfo.getUserId() ;
    user uss=[select LanguageLocaleKey,ProfileId,TimeZoneSidKey,LocaleSidKey,EmailEncodingKey from user where id = : userId ];
  
    //New user to run test
    user user1 = new user();
    user1.Username='user1@deloitte.test.com';
    user1.firstname = 'aaaaa';
    user1.LastName='bbbb';
    user1.email='user1@deloitte.com';
    user1.Alias='fred';
    user1.TimeZoneSidKey=uss.TimeZoneSidKey;
    user1.LocaleSidKey=uss.LocaleSidKey;
    user1.EmailEncodingKey=uss.EmailEncodingKey;
    user1.ProfileId=uss.ProfileId;
    user1.LanguageLocaleKey=uss.LanguageLocaleKey;
        if (createUser){
            insert user1;
        }
    return user1;
    }
    
    /*
     * 
     * Description : Stub to create a test user
     * Param :   
     * Returns : User  
    */
    public static User createTestUser() {
        return createTestUser(true);
    }
    
    /*
     * 
     * Description : Stub to create a EventJsonData
     * Param :   
     * Returns : User  
    */
    public static string EventJsonData(List<contact> contacts) {
        product2 p=new product2();
        p.Name='test product';
        insert p;
        Investment_Strategy__c is=new Investment_Strategy__c();
        is.Name='Test Investment';
        insert is;
        String dataJsonPart1='{"subject":"Test","whatId":"' + contacts[0].AccountId + '","startDateTime":"2020-08-04T07:30:00.000Z","endDateTime":"2020-08-08T08:30:00.000Z","allDayEvent":false,"ownerId":"'+UserInfo.getUserId()+'","selectedContact":[';
        String dataJsonPart2='],"location":"Hyderbad","selectedFund":[{"id":"'+p.id+'"},{"id":"'+is.id+'"}]}';
        string tempData='',data='';
        for(contact temp : contacts){
            tempData='{"id":"'+temp.id+'","name":"'+temp.LastName+'"},';
            data=data+tempData;
        }
        data=data.substring(0,data.length()-1);
        return dataJsonPart1+data+dataJsonPart2;
    }
    
    /*
     * 
     * Description : Stub to create a EventJsonData
     * Param :   
     * Returns : User  
    */
    public static string GnBJsonData(List<contact> contacts) {
        product2 p=new product2();
        p.Name='test product';
        insert p;
        Investment_Strategy__c is=new Investment_Strategy__c();
        is.Name='Test Investment';
        insert is;
        String dataJsonPart1='{"subject":"Test","gnb": true,"whatId":"' + contacts[0].AccountId + '","startDateTime":"2020-08-04T07:30:00.000Z","endDateTime":"2020-08-08T08:30:00.000Z","allDayEvent":false,"ownerId":"'+UserInfo.getUserId()+'","selectedContact":[';
        String dataJsonPart2='],"location":"Hyderbad","selectedFund":[{"id":"'+p.id+'"},{"id":"'+is.id+'"}]}';
        string tempData='',data='';
        for(contact temp : contacts){
            tempData='{"id":"'+temp.id+'","name":"'+temp.LastName+'"},';
            data=data+tempData;
        }
        data=data.substring(0,data.length()-1);
        return dataJsonPart1+data+dataJsonPart2;
    }
    
    /*
     * 
     * Description  : Create and return multiple users, without inserting
     * Param        : ProfileId - profile to be assigned to new users, Count - no. of records to be created
     * Returns      : User  
    */
    public static List<User> createMultipleUsers(id profileId, integer count) {
        id userId = UserInfo.getUserId() ;
        List<user> userList = new List<User>();
        user user1; 
        
        //get required field info from logged in user
        user uss = [select LanguageLocaleKey,TimeZoneSidKey,LocaleSidKey,EmailEncodingKey from user where id = : userId ];
        
        //Creating Users
        for(integer i=0; i<count; i++){
            user1 = new user();
            user1.Username = 'user1'+i+'@deloitte.test.com';
            user1.firstname = 'aaaaa';
            user1.LastName = 'bbbb';
            user1.email = 'user1'+i+'@deloitte.com';
            user1.Alias = 'fred';
            user1.TimeZoneSidKey = uss.TimeZoneSidKey;
            user1.LocaleSidKey = uss.LocaleSidKey;
            user1.EmailEncodingKey = uss.EmailEncodingKey;
            user1.ProfileId = profileId;
            user1.LanguageLocaleKey = uss.LanguageLocaleKey;
            user1.IsActive=true;
            userList.add(user1);
        }
        return userList;
    }
    
    public static Boolean runningInASandbox() {
      return [SELECT Id, IsSandbox FROM Organization LIMIT 1].IsSandbox;
    }
}