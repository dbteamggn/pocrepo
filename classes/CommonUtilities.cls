public class CommonUtilities {

    
    /*
     * Description : This function is used to send email.
     * Param : String Subject, String PlainTextBody,String[] addr 
     * Returns :  None.
    */
    public static void SendEmail(String Subject, String PlainTextBody, String[] addr){ 
    
        Messaging.SingleEmailMessage Mail = new Messaging.SingleEmailMessage();
        mail.settoAddresses(addr);
        mail.setSubject(Subject);
        mail.setPlainTextBody(PlainTextBody);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    
    }
    
    /*
     * Description : This function is used for inserting the exception occurred into the system.
     * Param - Exception Record ex 
     * Returns :  None.
    */

    public static void createExceptionLog(Exception ex){
        Exceptions_Log__c o_exception = new Exceptions_Log__c();
        o_exception.Exception_Description__c = ex.getMessage();
        o_exception.Exception_Stack_Trace__c = ex.getStackTraceString();
        o_exception.Type__c = ex.getTypeName();
        insert o_exception;  
    }

    
    /*
     * Description : This function is used for retrieving the user-friendly messages in the validation errors, etc.,.
     * Param - Exception Record ex 
     * Returns :  Substring which is actual error message
    */

    public static String getUserFriendlyMessage(Exception ex){
    	String stat = '';
        if (ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')){
           stat = ex.getMessage().substringAfterLast('FIELD_CUSTOM_VALIDATION_EXCEPTION, ').substringBefore(':');
        }else if (ex.getMessage().contains('INSUFFICIENT_ACCESS_OR_READONLY')){
           stat = ex.getMessage().substringAfterLast('INSUFFICIENT_ACCESS_OR_READONLY, ').substringBefore(':');
        }else if (ex.getMessage().contains('UNABLE_TO_LOCK_ROW')){
           stat = ex.getMessage().substringAfterLast('UNABLE_TO_LOCK_ROW, ').substringBefore(':');
        }else stat = ex.getMessage();  
        return stat;
    }     

    /*
     * Description : This function is used for correcting the picklist values retrieved by GetDependentOptions method
     * Param - 
     * Returns :  None.
     * Sourced from NA Retail(Original link - http://titancronus.com/blog/2014/05/01/salesforce-acquiring-dependent-picklists-in-apex/)
    */
    public class Bitset{
        public Map<String,Integer> AlphaNumCharCodes {get;set;}
        public Map<String, Integer> Base64CharCodes { get; set; }
        public Bitset(){
            LoadCharCodes();
        }
        //Method loads the char codes
        private void LoadCharCodes(){
            Base64CharCodes = new Map<String,Integer>{
                'A'=>0, 'B'=>1, 'C'=>2, 'D'=>3, 'E'=>4, 'F'=>5, 'G'=>6, 'H'=>7, 'I'=>8, 'J'=>9, 
                'K'=>10,    'L'=>11,    'M'=>12,    'N'=>13,    'O'=>14,    'P'=>15,    'Q'=>16,    
                'R'=>17,    'S'=>18,    'T'=>19,    'U'=>20,    'V'=>21,    'W'=>22,    'X'=>23,    
                'Y'=>24,    'Z'=>25,    'a'=>26,    'b'=>27,    'c'=>28,    'd'=>29,    'e'=>30,    
                'f'=>31,    'g'=>32,    'h'=>33,    'i'=>34,    'j'=>35,    'k'=>36,    'l'=>37,    
                'm'=>38,    'n'=>39,    'o'=>40,    'p'=>41,    'q'=>42,    'r'=>43,    's'=>44,    
                't'=>45,    'u'=>46,    'v'=>47,    'w'=>48,    'x'=>49,    'y'=>50,    'z'=>51,    
                '0'=>52,    '1'=>53,    '2'=>54,    '3'=>55,    '4'=>56,    '5'=>57,    '6'=>58,    
                '7'=>59,    '8'=>60,    '9'=>61,    '+'=>62,    '/'=>63
                
            };
            /*AlphaNumCharCodes = new Map<String,Integer>{
                'A'=>65,'B'=>66,'C'=>67,'D'=>68,'E'=>69,'F'=>70,'G'=>71,'H'=>72,'I'=>73,'J'=>74,
                'K'=>75,'L'=>76,'M'=>77,'N'=>78,'O'=>79,'P'=>80,'Q'=>81,'R'=>82,'S'=>83,'T'=>84,
                'U'=>85,'V'=> 86,'W'=>87,'X'=>88,'Y'=>89,'Z'=>90    
            };
            Base64CharCodes = new Map<String, Integer>();
            Set<String> pUpperCase = AlphaNumCharCodes.keySet();
            for(String pKey : pUpperCase){
                AlphaNumCharCodes.put(pKey.toLowerCase(),AlphaNumCharCodes.get(pKey)+32);
                Base64CharCodes.put(pKey,AlphaNumCharCodes.get(pKey) - 65);
                Base64CharCodes.put(pKey.toLowerCase(),AlphaNumCharCodes.get(pKey) - (65) + 26);
            }
            //numerics
            for (Integer i=0; i<=9; i++){
                AlphaNumCharCodes.put(string.valueOf(i),i+48);
                Base64CharCodes.put(string.valueOf(i), i + 52);
            }*/
        }
        public List<Integer> testBits(String pValidFor,List<Integer> nList){
            List<Integer> results = new List<Integer>();
            List<Integer> pBytes = new List<Integer>();
            Integer bytesBeingUsed = (pValidFor.length() * 6)/8;
            Integer pFullValue = 0;
            if (bytesBeingUsed <= 1)
                return results;
            for(Integer i=0;i<pValidFor.length();i++){
                pBytes.Add((Base64CharCodes.get((pValidFor.Substring(i, i+1)))));
            }   
            for (Integer i = 0; i < pBytes.size(); i++)
            {
                Integer pShiftAmount = (pBytes.size()-(i+1))*6;//used to shift by a factor 6 bits to get the value
                system.debug('pFullValue>>:'+pFullValue+' >>pBytes>>:'+pBytes[i]+'>>pShiftAmount>>:'+pShiftAmount);
                pFullValue = pFullValue + (pBytes[i] << (pShiftAmount));
            }
            Integer bit;
            Integer targetOctet;
            Integer shiftBits;
            Integer tBitVal;
            Integer n;
            Integer nListSize = nList.size();
            for(Integer i=0; i<nListSize; i++){
                n = nList[i];
                bit = 7 - (Math.mod(n,8)); 
                targetOctet = (bytesBeingUsed - 1) - (n >> bytesBeingUsed); 
                shiftBits = (targetOctet * 8) + bit;
                tBitVal = ((Integer)(2 << (shiftBits-1)) & pFullValue) >> shiftBits;
                if (tBitVal==1)
                    results.add(n);
            }
            return results;
        }
    }
    
    /*
     * Description : Method to get dependent picklist values
     * Param : Object name, Controlling Field name, dependent field name, controlling field value
     * Returns :  
    */ 
    public static Map<String,List<String>> GetDependentOptions(String pObjName, String pControllingFieldName, String pDependentFieldName){
        Map<String,List<String>> objResults = new Map<String,List<String>>();
        Map<String,Schema.SObjectType> objGlobalMap = Schema.getGlobalDescribe();
        if (!Schema.getGlobalDescribe().containsKey(pObjName))
            return objResults;
        Schema.SObjectType pType = Schema.getGlobalDescribe().get(pObjName);
        return GetDependentOptionsImpl(pType,pControllingFieldName,pDependentFieldName);        
    }
    
    public static Map<String,List<String>> GetDependentOptionsImpl(Schema.SObjectType pType, String pControllingFieldName, String pDependentFieldName){
        Map<String,List<String>> objResults = new Map<String,List<String>>();
        if (pType==null)
            return objResults;
        CommonUtilities.Bitset BitSetInstance = new CommonUtilities.Bitset();
        Map<String, Schema.SObjectField> objFieldMap = pType.getDescribe().fields.getMap();
        if (!objFieldMap.containsKey(pControllingFieldName) || !objFieldMap.containsKey(pDependentFieldName))
            return objResults;     
        List<Schema.PicklistEntry> ctrl_ple = objFieldMap.get(pControllingFieldName).getDescribe().getPicklistValues();
        List<Schema.PicklistEntry> dep_ple = objFieldMap.get(pDependentFieldName).getDescribe().getPicklistValues();
        objFieldMap = null;
        List<Integer> lstControllingIndexes = new List<Integer>();
        for(Integer pControllingIndex=0; pControllingIndex<ctrl_ple.size(); pControllingIndex++){           
            Schema.PicklistEntry ctrl_entry = ctrl_ple[pControllingIndex];
            String pControllingLabel = ctrl_entry.getLabel();
            objResults.put(pControllingLabel,new List<String>());
            lstControllingIndexes.add(pControllingIndex);
        }
        objResults.put('',new List<String>());
        objResults.put(null,new List<String>());
        List<Schema.PicklistEntry> objEntries = new List<Schema.PicklistEntry>();
        List<CommonUtilities.TPicklistEntry> objDS_Entries = new List<CommonUtilities.TPicklistEntry>();
        for(Integer pDependentIndex=0; pDependentIndex<dep_ple.size(); pDependentIndex++){          
            Schema.PicklistEntry dep_entry = dep_ple[pDependentIndex];
            objEntries.add(dep_entry);
            
        } 
        objDS_Entries = (List<CommonUtilities.TPicklistEntry>)JSON.deserialize(JSON.serialize(objEntries), List<CommonUtilities.TPicklistEntry>.class);
        System.debug('objDS_Entries>>>:'+objDS_Entries);
        List<Integer> validIndexes;
        for (CommonUtilities.TPicklistEntry objDepPLE : objDS_Entries){
            if (objDepPLE.validFor==null || objDepPLE.validFor==''){
                continue;
            }
            validIndexes = BitSetInstance.testBits(objDepPLE.validFor,lstControllingIndexes);
            for (Integer validIndex : validIndexes){                
                String pControllingLabel = ctrl_ple[validIndex].getLabel();
                objResults.get(pControllingLabel).add(objDepPLE.label);
            }
        }
        objEntries = null;
        objDS_Entries = null;
        return objResults;
    }
    
    
    /*
     * Description : Method to fetch the currency code and conversion rate in map
     * Param : None
     * Returns : Map<String, Decimal>
    */
    public static Map<String, Decimal> getCurrencyInfo(){
        Map<String, Decimal> mapOfCurrAndVal = new Map<String, Decimal>();
        list<CurrencyType> l_currencyTpe = [SELECT ISOCode, ConversionRate FROM CurrencyType WHERE IsActive=TRUE];
        if(l_currencyTpe != null && l_currencyTpe.size()>0){
            for(CurrencyType ct:l_currencyTpe){
                mapOfCurrAndVal.put(ct.ISOCode, ct.ConversionRate);
            }
        }
        return mapOfCurrAndVal;
    }
    
    /*
     * Description : Method to get the object name for the given record id
     * Param : Record id
     * Returns : Object name
    */
    public static String getObjectName(String recordId){
        String objectName = '';
        try{
            //Get prefix from record ID
            String s_idPrefix = String.valueOf(recordId).substring(0,3);
             
            //Get schema information
            Map<String, Schema.SObjectType> gd =  Schema.getGlobalDescribe(); 
             
            //Loop through all the sObject types returned by Schema
            for(Schema.SObjectType stype : gd.values()){
                Schema.DescribeSObjectResult r = stype.getDescribe();
                String prefix = r.getKeyPrefix();
                                
                //Check if the prefix matches with requested prefix
                if(prefix!=null && prefix.equals(s_idPrefix)){
                    objectName = r.getName();
                    break;
                }
            }
        }catch(Exception e){
            createExceptionLog(e);
        }
        return objectName;
    }

    
    /*
     * Description : Method to get picklist values for a field
     * Param : Object name, Picklist Field name
     * Returns :  
    */
    public Static List<String> GetFieldPicklistValues(String ObjectApi_name, String picklistField)
    {
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(ObjectApi_name);
        Sobject Object_name = targetType.newSObject();
        Schema.sObjectType sobject_type = Object_name.getSObjectType();
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe();
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap();
        List<Schema.PicklistEntry>  FieldValues = field_map.get(picklistField).getDescribe().getPickListValues();
        list<String> values =  new  list<String>();
        for (Schema.PicklistEntry pklVal : FieldValues){
            values.add(pklVal.getValue());  
        }
        return values;
    }
    
    public class TPicklistEntry{
        public string active {get;set;}
        public string defaultValue {get;set;}
        public string label {get;set;}
        public string value {get;set;}
        public string validFor {get;set;}
        public TPicklistEntry(){
            
        }
    }
    
    /*
     * Description : Method to return a list of RGB colours primarily for charts.  Defaults to no opacity e.g. solid
     * Param : required number of colours.  
     * Returns : list of RGB colours
    */
    public static list<String> GetBrandedColours(integer noOfColours){
        return CommonUtilities.GetBrandedColours(noOfColours, 1.0); 
    }
    
    /*
     * Description : Method to return a list of RGB colours primarily for charts
     * Param : required number of colours.  
     * Returns : list of RGB colours
    */
    public static list<String> GetBrandedColours(integer noOfColours, decimal opacity){
        list<string> returnedColours = new list<string>();
        string[] colours = new String[]{'rgba(0, 26, 123, ', // dark blue
                                        'rgba(0, 183, 96, ', //green
                                        'rgba(175, 35, 165, ', // fuschia
                                        'rgba(211, 5, 71, ', //red
                                        'rgba(0, 193, 181, ', // turquoise
                                        'rgba(249, 221, 22, ', //yellow
                                        'rgba(0, 155, 250, ', // light blue
                                        'rgba(255, 0, 145, ', // pink
                                        'rgba(86, 12, 112, ', // royal purple
                                        'rgba(140, 200, 0, ', // lime
                                        'rgba(20, 80, 210, ', // blue
                                        'rgba(250, 69, 38, ', // orange
                                        'rgba(0, 140, 130, ', // sherwood green
                                        'rgba(99, 35, 160, ' // magenta 
            }; 
        integer colourIndex = 0;
        for (integer i=0; i<noOfColours; i++){
            if (colourIndex >= colours.size()){
                colourIndex = 0;
            }
            returnedColours.add(colours[colourIndex] + opacity + ')');  
            colourIndex++;
        }
        return returnedColours; 
    }
}