public class LightningCommonUtilities{
    
    @AuraEnabled
    public static string getLoggedInUserId(){
        return UserInfo.getUserId();
    }
}