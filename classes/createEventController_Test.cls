@isTest
public class createEventController_Test {
    /*
     * 
     * Description : Create an Event Record .
     * Param : json Data
     * Returns :  Id
    */
    static testMethod void CreateEventTest(){
        User user = CommonTestUtils.createTestUser();  
        
        // Create a client that the contacts will belong to
        Account acc = CommonTestUtils.CreateTestClient('Contact Tests');        
        insert acc;
        
        // Now create contacts for a client with last viewed Date
        List<contact> contacts = new List<contact>();       
        for(integer i = 0; i<2; i++ ){
            Contact c = CommonTestUtils.CreateTestContact('Contact ' + i);
            c.accountid = acc.id;
            contacts.add(c);                    
        }
        insert contacts;
        System.runAs(user){
            Test.startTest();
            string data=CommonTestUtils.EventJsonData(contacts);
            Id eventId=createEventController.createEvent(data);
            System.assert(eventId != null); 
        }
    }
    
    static testMethod void createGnBTest(){
        User user = CommonTestUtils.createTestUser();  
        
        // Create a client that the contacts will belong to
        Account acc = CommonTestUtils.CreateTestClient('Contact Tests');
                acc.OwnerId = user.ID;        
        insert acc;
        System.debug('**********account :'+acc);        
        // Now create contacts for a client with last viewed Date
        List<contact> contacts = new List<contact>();       
        for(integer i = 0; i<2; i++ ){
            Contact c = CommonTestUtils.CreateTestContact('Contact ' + i);
            c.accountid = acc.id;
            c.OwnerId = acc.OwnerId;        
            contacts.add(c);                    
        }
        insert contacts;
        System.debug('**********contact1 owner id'+contacts[0].OwnerId);
        System.debug('**********contact2 owner id'+contacts[1].OwnerId);
        System.runAs(user){
            Test.startTest();
            string data=CommonTestUtils.GnBJsonData(contacts);
            Id eventId=createEventController.createEvent(data);
            System.assert(eventId != null); 
            CreateEventController.getPicklistValues('cost_type__c');
        }
    }
}