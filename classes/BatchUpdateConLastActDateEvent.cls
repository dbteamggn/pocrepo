global class BatchUpdateConLastActDateEvent implements Database.Batchable<sObject> { 
   

    /*
     * 
     * Description : start method of the batch class to query the records to be processed
     * Param :   Database.BatchableContext
     * Returns : Database.QueryLocator  
    */
    global Database.QueryLocator start(Database.BatchableContext BC) { 
        //Fetch the last batch run time to process delta records
    	BatchRunTime__c lastRun = BatchRunTime__c.getValues('BatchUpdateConLastActDate'); // Handles both Event & Task batch jobs
        DateTime lastRunDateTime;
        if (lastRun == null){
            lastRunDateTime = system.now() - (365 * 5); // Default to 5 years ago
        } else {
            lastRunDateTime = lastRun.Last_Run__c;
        }
        
        //AJW 02/08/16 - Amended to reference Completed Events only and also use custom field Type__c instead of std Type field
        return Database.getQueryLocator([select ID, RelationId, Event.EndDateTime, Event.Type__c
                                         from EventRelation where RelationId != null 
                                         AND Event.LastModifiedDate >= :lastRunDateTime
                                         AND Event.EndDateTime < TODAY
                                         AND Event.Status__c = 'Completed'
                                         order by Event.EndDateTime desc
                                        ]);
    }

    /*
     * 
     * Description : execute method of the batch class to process the queried records
     * Param :   Database.BatchableContext
     * Returns : Scope - list of eventRelations  
    */
    global void execute(Database.BatchableContext BC, List<Sobject> scope) {
        Map<Id, date> contactIdToLastActivityDate = new Map<Id, date>();  // <ContactId, Date>
        Map<Id, string> contactIdToLastActivityType = new Map<Id, string>();  // <ContactId, EventType>
        
        // Loop through the EventRelations
        for(Sobject s : Scope){
            EventRelation e = (EventRelation) s;
            // to check if event is associated with contact
            if(String.valueOf(e.RelationId).subString(0,3) == '003' ){
                
                // Add the event end date to a map (keyed by contactId) if it is not there or later than the existing date in the map
                Date eventEndDate = date.newInstance(e.Event.EndDateTime.year(), e.Event.EndDateTime.month(), e.Event.EndDateTime.day()); 
                if (contactIdToLastActivityDate.containsKey(e.RelationId)){
                    if (eventEndDate > contactIdToLastActivityDate.get(e.RelationId) ){
                        contactIdToLastActivityDate.put(e.RelationId, eventEndDate);
                        contactIdToLastActivityType.put(e.RelationId, e.Event.Type__c);
                    }   
                } else {
                    contactIdToLastActivityDate.put(e.RelationId, eventEndDate);
                    contactIdToLastActivityType.put(e.RelationId, e.Event.Type__c);
                }
            }
        }
        
        // Get the related contacts, check the existing Last Activity Date & if the one in the map is later, the contact's Last Activity Date needs to be updated.
        list<contact> updateContacts = new list<contact>();
        for(contact contact : [select id, last_Activity_Date__c, last_Activity_type__c from contact where id in :contactIdToLastActivityDate.keySet()]){
            if (contact.last_Activity_Date__c == null || contactIdToLastActivityDate.get(contact.id) > contact.last_Activity_Date__c){
                contact.last_Activity_Date__c = contactIdToLastActivityDate.get(contact.id);
                contact.last_Activity_Type__c = contactIdToLastActivityType.get(contact.id);
                updateContacts.add(contact);
            }
        }
        
        // Perform the update
        if (updateContacts.size() > 0){
            update updateContacts;
        }
         
            
    }
    
    /*
     * 
     * Description : finish method of the batch class, executed once the batch is completely processed
     *               1. If there are errors, sends out email in case of error
     *               2. otherwise, update the last batch run time
     * Param :   Database.BatchableContext
     * Returns :   
    */
    global void finish(Database.BatchableContext BC) {
        
        AsyncApexJob asynch=[Select Id,JobItemsProcessed,ExtendedStatus,CompletedDate,NumberOfErrors,TotalJobItems,Status,CreatedBy.Email 
                                 FROM   AsyncApexJob 
                                 WHERE  Id =: BC.getJobId()];
        String Subject = 'Scheduled Batch Job Failure in updating Contact last activity date';
        String[] addr=new String[]{asynch.CreatedBy.Email};
            String PlainTextBody = 'Scheduled Batch job to update contacts\' Last Activity Date from Events completed at '
            + asynch.CompletedDate+' throws following error : \n Processed Job Items :'+ asynch.JobItemsProcessed
            +'\n No.of Job Failures : '+asynch.NumberofErrors+'\n Errors :'+asynch.ExtendedStatus;
        
        if(asynch.NumberOfErrors > 0){
            CommonUtilities.SendEmail(Subject,PlainTextBody,addr);
        } else {

            // update last batch run time
            BatchRunTime__c lastRun = BatchRunTime__c.getValues('BatchUpdateConLastActDate');
            if (lastRun == null){
                lastRun = new BatchRunTime__c();
                lastRun.name = 'BatchUpdateConLastActDate';
            }
            lastRun.Last_Run__c = system.now();
            upsert lastRun;
        }

    
    	// now start the task batch process.  
    	// 
    	// Commented out after it was decided to ignore tasks.
    	// but left in code in case it changes back again!  If it does the Else section above that  
    	// upserts to BatchRunTime__c needs to be removed.
        //ID batchprocessid = Database.executeBatch(new BatchUpdateConLastActDateTask()); 
     
    }
}