/*----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            Task_Utility
   Description:     Methods called by Task triggers
                    
   Date             Author                             Summary of Changes                  
   -----------      -----------------                  -------------------------------------------------------------------------------------
    08 Jul 2016      Sridhar Gurram                     Initial Release 
------------------------------------------------------------------------------------------------------------------------------------------------------------*/
public class Task_Utility {

    /*
     * Description : Create new instance / return existing instance
     * Param : 
     * Returns :  
    */
    private static Task_Utility instance = null; 
    
    public static Task_Utility getInstance() {
        if (instance == null) {
            instance = new Task_Utility();
        }
        return instance;
    }
    
    /*
     * Description : Method to get Contact name and its Client name from Task.whoid
     * Param : Tasks from Trigger where whoid has changed
     * Returns :  
    */ 
    public void UpdateTaskCustomFields(List<Task> tskLst) {
        //Get Task contact ids in a Set
        Set<Id> conids = new Set<Id>();
        for (Task tsk : tskLst) {
            conids.add(tsk.whoid);
        } 
    
        //Get Contact Id and account relationship into a Map
        Map<Id, Contact> conMap = new Map<Id, Contact>([Select Id, Name, Account.Name from Contact where Id in :conIds]);
    
        //Iterate through the Task list and fetch/assign Contact name and its Client Name to the Task custom fields
        for (Task tsk : tskLst) {
            if (conMap.containskey(tsk.whoid)){
                tsk.Contact_Name__c = conMap.get(tsk.whoid).Name;
                tsk.Client_Name__c = conMap.get(tsk.whoid).Account.Name;
            }
        }
        update tskLst;
    }
    
    /*
     * Description : Method to get dependent picklist values
     * Param : Object name, Controlling Field name, dependent field name, controlling field value
     * Returns :  
    */
    @AuraEnabled
    public static List<String> GetDpndntPckLstValues(String ObjectName, String CtrlField, String DependField, String CtrlFieldValue) {
        /*Map<String, List<String>> picklistMap = CommonUtilities.GetDependentOptions(ObjectName, CtrlField, DependField);
        List<String> values = picklistMap.get(CtrlFieldValue);*/
        List<String> values = PicklistDescriber.describe(ObjectName, CtrlField, DependField, CtrlFieldValue);
        return values;
    }
    
    /*
     * Description : Method to get picklist values for a field
     * Param : Object name, Picklist Field name
     * Returns :  
    */
    @AuraEnabled
    public static List<String> GetPckLstValues (String ObjectApi_name, String picklistField){
        //List<String> values = CommonUtilities.GetFieldPicklistValues(ObjectApi_name, picklistField);
        List<String> values = PicklistDescriber.describe(ObjectApi_name, picklistField);        
        return values;
    }
    
    @AuraEnabled
    public static String TaskCreation(Task newTask, List<Id> contactIds,String recordId){
        String whoId;
        if(newTask.Subject == '')
        {
         newTask.Subject = 'Please enter subject...';
        } 
        if(recordId!=null && recordId != ''){
            if(Id.valueOf(recordId).getSObjectType() == Schema.Contact.SObjectType){
                whoid = recordId;
            }else if(Id.valueOf(recordId).getSObjectType() == Schema.Event.SObjectType){
                whoId=[select id, whoId from event where id=:recordId].whoId;
            }
            else if(Id.valueOf(recordId).getSObjectType() == Schema.Task.SObjectType){
                whoId=[select id, whoId from task where id=:recordId].whoId;
            }
        }
        newTask.whoId = whoId;
        insert newTask;
        String TaskId = newTask.Id;
        
        TaskRelation tr= new TaskRelation ();
        List<TaskRelation> trList = new List<TaskRelation>();
        for(Integer i=0;i < contactIds.size();i++){
            if(contactIds[i] != whoId){
                tr= new TaskRelation ();
                tr.RelationId=contactIds[i];
                tr.TaskId=newTask.id;
                tr.isWhat=false;
                trList.add(tr);
            }
        }
        System.debug(trList);
        insert trList;
        
        return TaskId;    
    }
    
    /*
     * Description  : method to create Activity Client Relationship records. Added for US28000. Invoked from Process Builder Flow 'Task On Create'
     * Param        : Task Ids
     * Returns      : none
    */
    @InvocableMethod
    public static void createActivityClientRelation(List<Id> taskIds){
        
        Map<id, Activity_Client_Relationship__c> newActClientRelMap = new Map<id, Activity_Client_Relationship__c>();
        Map<id, Activity_Client_Relationship__c> existingACRsByContact = new Map<id, Activity_Client_Relationship__c>();
        List<Activity_Client_Relationship__c> newActClientRelList = new List<Activity_Client_Relationship__c>();
        Set<Id> contactIds = new Set<Id>();
        Map<Id, Contact> contactMap;
        String contactPrefix = Schema.getGlobalDescribe().get('Contact').getDescribe().getKeyPrefix();
        List<Task> taskList = new List<Task>();
        Set<Id> filteredTaskIdSet = new Set<Id>();
        map<id, id> existingACRsByClient = new map<id, id>();
        
        try{
            //get existing Activity_Client_Relationship__c records related to the Task
            for(Activity_Client_Relationship__c acr : [select Activity__c, Contact__c, Client__c from Activity_Client_Relationship__c where Activity__c IN : taskIds]){
                existingACRsByClient.put(acr.Client__c, acr.Activity__c);
                if(acr.Contact__c != null){
                    existingACRsByContact.put(acr.Contact__c, acr);
                }
            }
            
            taskList = [select id, whatId, AccountId, subject from task where id IN : taskIds];
            for(Task each : taskList){
                //if whatId != null AND AccountId != null, AND record doesn't already exist, create ACR for task Id and AccountId
                if(each.whatId != null && each.AccountId != null){
                    if(existingACRsByClient.isEmpty() || (!existingACRsByClient.isEmpty() && !(existingACRsByClient.containsKey(each.AccountId) && existingACRsByClient.get(each.AccountId) == each.id))){
                        newActClientRelList.add(new Activity_Client_Relationship__c(Activity__c = each.id, Client__c = each.AccountId, Activity_Subject__c = each.subject));
                    }
                }
                //if whatId == null, get info from TaskRelation
                else if(each.whatId == null){
                    filteredTaskIdSet.add(each.id);
                }               
            }
        
            //get all TaskRelations and create Activity_Client_Relationship__c records based on the TaskRelation details
            for(TaskRelation taskRel: [select Id, TaskId, isWhat, RelationId, AccountId, Task.subject from TaskRelation where TaskId IN :filteredTaskIdSet AND isWhat=false]){
                if(taskRel.RelationId != null && String.valueOf(taskRel.RelationId).startsWith(contactPrefix)){
                    newActClientRelMap.put(taskRel.RelationId, new Activity_Client_Relationship__c(Activity__c = taskRel.TaskId, Contact__c = taskRel.RelationId, Activity_Subject__c = taskRel.Task.subject));
                    
                    //collect all contact ids from TaskRelation records
                    contactIds.add(taskRel.RelationId);
                }
            }          
            
            //get primary Account from collected contact records
            if(!contactIds.isEmpty()){
                contactMap = new Map<Id, Contact>([select Id, Name, AccountId from Contact where Id IN :contactIds]);
            }
            
            //update Client__c field with the associated contact's AccountId AND insert if a record does not already exist in Activity Client Relation
            for(Activity_Client_Relationship__c acr : newActClientRelMap.values()){
                if(!contactMap.isEmpty() && contactMap.containskey(acr.contact__c) && contactMap.get(acr.contact__c).AccountId != null){
                    acr.Client__c = contactMap.get(acr.contact__c).AccountId;
                    if(!existingACRsByContact.isEmpty() && !existingACRsByContact.containsKey(acr.contact__c)){
                        newActClientRelList.add(acr);
                    }
                    else if(existingACRsByContact.isEmpty()){
                        newActClientRelList.add(acr);
                    }
                }
            }
            
            //insert Activity Client Relation records for each TaskRelation
            if(!newActClientRelList.isEmpty()){
                insert newActClientRelList;
            }
        }
        catch(Exception e){
            CommonUtilities.createExceptionLog(e);
        }
    }
}