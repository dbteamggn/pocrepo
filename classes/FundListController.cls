public with sharing class FundListController {
    @AuraEnabled
    public static List<Result> findAll() {
        string currentProfileName = [select id , name from profile where id=: UserInfo.getProfileId()].Name;
        string profilesName;
        string type1;
        string type2;
        Configuration_Util__c profiles= Configuration_Util__c.getValues('FundProfile');
        if(profiles!=null){
            profilesName=profiles.value__c;
            if(profilesName.contains(currentProfileName)){
                type1='funds';
            }
        }
        profiles= Configuration_Util__c.getValues('InvestmentProfile');
        if(profiles!=null){
            profilesName=profiles.value__c;
            if(profilesName.contains(currentProfileName)){
                type2='Investment';
            }
        }
        List<Result> ResultList = new List<Result>();
        if(type1=='funds'){
            List<product2> product2List= [SELECT id, name FROM product2 ORDER BY Our_Product__c DESC, Name LIMIT 25];           
            for(product2 temp : product2List){
                Result tempfund = new Result();
                tempfund.Id = temp.id;
                tempfund.Name = temp.name;
                tempfund.type= 'Funds';
                tempfund.value = false;
                ResultList.add(tempfund);           
            }
        }
        if(type2=='Investment'){
            List<Investment_Strategy__c> InvestmentStrategyList= [SELECT id, name FROM Investment_Strategy__c   LIMIT 25];           
            for(Investment_Strategy__c temp : InvestmentStrategyList){
                Result tempStrat = new Result();
                tempStrat.Id = temp.id;
                tempStrat.Name = temp.name;
                tempStrat.type= 'Investment Strategy';
                tempStrat.value = false;
                ResultList.add(tempStrat);           
            }
        }
        return ResultList;
    }
    @AuraEnabled
    public static List<Result> findByName(string searchKey) {
        string currentProfileName = [select id , name from profile where id=: UserInfo.getProfileId()].Name;
        string profilesName;
        string type1;
        string type2;
        Configuration_Util__c profiles= Configuration_Util__c.getValues('FundProfile');
        if(profiles!=null){
            profilesName=profiles.value__c;
            if(profilesName.contains(currentProfileName)){
                type1='funds';
            }
        }
        profiles= Configuration_Util__c.getValues('InvestmentProfile');
        if(profiles!=null){
            profilesName=profiles.value__c;
            if(profilesName.contains(currentProfileName)){
                type2='Investment';
            }
        }
        List<Result> ResultList = new List<Result>();
        String searchStr = '';
        List<String> searchStrings = new List<String>();
        if(!String.isBlank(searchKey)){
            for(String each: searchKey.split(' ')){
                searchStrings.add('name LIKE \'%' + each + '%\'');
            }
        }
        searchStr = String.join(searchStrings, ' AND ');
        //String name = '%' + searchKey + '%';
        
        if(type1=='funds'){        
            String queryStr = 'SELECT id, name FROM product2 WHERE ' + searchStr + ' ORDER BY Our_Product__c DESC, name LIMIT 25';
            List<product2> product2List= Database.query(queryStr); //[SELECT id, name FROM product2 WHERE name LIKE :name ORDER BY name LIMIT 25];
            for(product2 temp : product2List){
                Result tempfund = new Result();
                tempfund.Id = temp.id;
                tempfund.Name = temp.name;
                tempfund.type= 'Funds';
                tempfund.value = false;
                ResultList.add(tempfund);           
            }
        }
        if(type2=='Investment'){
            String queryStr = 'SELECT id, name FROM Investment_Strategy__c WHERE ' + searchStr + ' ORDER BY name LIMIT 25';
            List<Investment_Strategy__c> InvestmentStrategyList= Database.query(queryStr); //[SELECT id, name FROM Investment_Strategy__c where name LIKE :name  ORDER BY name LIMIT 25];           
            for(Investment_Strategy__c temp : InvestmentStrategyList){
                Result tempStrat = new Result();
                tempStrat.Id = temp.id;
                tempStrat.Name = temp.name;
                tempStrat.type= 'Investment Strategy';
                tempStrat.value = false;
                ResultList.add(tempStrat);           
            }
        }
        return ResultList;
    }
    @AuraEnabled
    public static List<Result> findByListId(List<id> fundStratIds) {
        string currentProfileName = [select id , name from profile where id=: UserInfo.getProfileId()].Name;
        string profilesName;
        string type1;
        string type2;
        List<id> fundsIds=new list<id>();
        List<id> stratsIds=new list<id>();
        Configuration_Util__c profiles= Configuration_Util__c.getValues('FundProfile');
        if(profiles!=null){
            profilesName=profiles.value__c;
            if(profilesName.contains(currentProfileName)){
                type1='funds';
                for(id temp : fundStratIds){
                    if(Id.valueOf(temp).getSObjectType() == Schema.Product2.SObjectType){
                        fundsIds.add(temp);
                    }
                } 
            }
        }
        profiles= Configuration_Util__c.getValues('InvestmentProfile');
        if(profiles!=null){
            profilesName=profiles.value__c;
            if(profilesName.contains(currentProfileName)){
                type2='Investment';
                for(id temp : fundStratIds){
                    if(Id.valueOf(temp).getSObjectType() == Schema.Investment_Strategy__c.SObjectType){
                        stratsIds.add(temp);
                    }
                } 
            }
        }
        List<Result> ResultList = new List<Result>();
        if(type1=='funds'){        
            List<product2> product2List= [SELECT id, name FROM product2 WHERE Id in :fundsIds ORDER BY Our_Product__c DESC, name];
            for(product2 temp : product2List){
                Result tempfund = new Result();
                tempfund.Id = temp.id;
                tempfund.Name = temp.name;
                tempfund.type= 'Funds';
                tempfund.value = true;
                ResultList.add(tempfund);           
            }
        }
        if(type2=='Investment'){
            List<Investment_Strategy__c> InvestmentStrategyList= [SELECT id, name FROM Investment_Strategy__c WHERE Id in :stratsIds ORDER BY name];           
            for(Investment_Strategy__c temp : InvestmentStrategyList){
                Result tempStrat = new Result();
                tempStrat.Id = temp.id;
                tempStrat.Name = temp.name;
                tempStrat.type= 'Investment Strategy';
                tempStrat.value = true;
                ResultList.add(tempStrat);           
            }
        }
        return ResultList;
    }
    public class Result {
        @AuraEnabled
        public id id;
        
        @AuraEnabled
        public string name;
        
        @AuraEnabled
        public string type;
     
        @AuraEnabled
        public boolean value;
    }
}