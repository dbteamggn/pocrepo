public with sharing class UtilsDocuments {
  
  public static string getDocUrl(String docUniqueName) {
      string imageURL = null;
        if (docUniqueName != null){
          List< document > documentList=[select name from document where 
                                        Name=:docUniqueName];
      
        if(documentList.size()>0)
        {
          imageURL='/servlet/servlet.FileDownload?file=' + documentList[0].id;
        }
        }
        
        return imageURL;
    }
}