public class SendChatterNotification
{
    public void sendChatterNotification()
    {
        ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
        ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
        ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
        ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
        
        messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
        
        mentionSegmentInput.id = '0050v000000gEgfAAE';
        messageBodyInput.messageSegments.add(mentionSegmentInput);
        
        textSegmentInput.text = 'The Fed has increased interest rates by 20 basis points, affecting advisors invested in NHMAX fund';
        messageBodyInput.messageSegments.add(textSegmentInput);
        
        feedItemInput.body = messageBodyInput;
        feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
        feedItemInput.subjectId = '0010v000007OKbLAAW';
        
        ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);
        //ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput, null);
        //ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(null, '0050v000000gEgfAAE', ConnectApi.FeedElementType.FeedItem, 'The Fed has increased interest rates by 20 basis points, affecting advisors invested in NHMAX fund');

    }
}