/*----------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            ThirdPartySchemesController
Description:     Methods return the schemes related to third party

Date             Author                             Summary of Changes                  
-----------      -----------------                  -------------------------------------------------------------------------------------
15 Sep 2016      Sachin Seshabhattar                     Initial Release 
------------------------------------------------------------------------------------------------------------------------------------------------------------*/


public class ThirdPartySchemesController {
/*
* Description : Query the related schemes from Client ThirdParty Relation object based on the third party id.
* Param : Third Party record Id
* Returns :  Related Schemes
*/
    
    @AuraEnabled
    public static Response getRelatedSchemes(String recordId){
        List<Client_Third_Party_Relationship__c> clientThirdPartyList = new List<Client_Third_Party_Relationship__c>();
        List<Scheme_Third_Party_Relationship__c> schemeThirdPartyList = new List<Scheme_Third_Party_Relationship__c>();        
        List<SchemesRelated> schemesRelatedList = new List<SchemesRelated>();
        Response response = new Response();
        SchemesRelated sr;
        try{
        
            List<Account> thirdParty = [select id,RecordType.Name from Account where id=:recordId];
            if(thirdParty.Size() > 0){
            /* Verifying if the record is client or Third Party */
                if(thirdParty[0].RecordType.Name == 'Third Party'){
                    response.isThirdParty = true;
                
                /* Query the related schemes from Client ThirdParty Relation based on the Third Party Id */                                
                clientThirdPartyList = [select id, (select Scheme__r.id,Scheme__r.name, Scheme__r.Client_Name__r.Name, Scheme__r.Scheme_Type__c from Scheme_Third_Party_Relationship__r) from Client_Third_Party_Relationship__c where Third_Party_Name__r.id =: recordId];       
                if(!clientThirdPartyList.isEmpty()){
                    for( Client_Third_Party_Relationship__c c : clientThirdPartyList){
                        if( c.Scheme_Third_Party_Relationship__r !=null){
                            /* Iterating through the scheme third party relation to get the schemes */
                            for(Scheme_Third_Party_Relationship__c s : c.Scheme_Third_Party_Relationship__r ){
                                sr = new SchemesRelated();                                                     
                                sr.schemeId = s.Scheme__r.Id;
                                sr.clientName = s.Scheme__r.Client_Name__r.Name;
                                sr.schemeType = s.Scheme__r.Scheme_Type__c;
                                sr.schemeName=  sr.clientName + ' - ' +sr.schemeType;                        
                                schemesRelatedList.add(sr);
                            }
                        }
                    }
                    if(schemesRelatedList.size()>0){
                        response.schemesRelated = schemesRelatedList;
                        response.schemeCount = schemesRelatedList.size() ;
                    }else if (schemesRelatedList == null || schemesRelatedList.isEmpty()){
                        response.schemeCount = 0;
                        response.schemesRelated = new List<SchemesRelated>();
                    }
                }
            }else{
                response.isThirdParty = false;    
            }
        }
        }catch(Exception ex){
            CommonUtilities.createExceptionLog(ex);
        }
        return response;
    }
    
    public class Response {
        @AuraEnabled
        public Integer schemeCount;
        
        @AuraEnabled
        public boolean isThirdParty;
        
        @AuraEnabled
        public List<SchemesRelated> schemesRelated ;
    }
    public class SchemesRelated{
        
        @AuraEnabled
        public Id schemeId;
        @AuraEnabled
        public string schemeName;
        @AuraEnabled
        public string clientName;
        @AuraEnabled
        public string schemeType;
    } 
}