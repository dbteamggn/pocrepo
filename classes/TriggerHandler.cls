/**************************************************************************************
 Name        :    TriggerHandler
 Description :    Common trigger handler for all objects
 Created By  :    Deloitte
 Created On  :    02 August 2016
 --------------------------------------------------------------------------------------
 Modification Log
 -----------------
 Name                Date            Comments
 -----------------   -------------   --------------------------------------------------
 Joshna              08-02-2016      Created
 --------------------------------------------------------------------------------------
 Review Log
 ----------    
 Name                Date            Comments
 -----------------   -------------   --------------------------------------------------
 
***************************************************************************************/
public virtual class TriggerHandler {

    private Boolean bypassFullObject;
    private Boolean globalByPass;
    public TriggerParameter triggerParams;
    
    private STATIC FINAL Integer firstRec = 0;
    
    {
        globalByPass = false;
        bypassFullObject = false;
        triggerParams = new TriggerParameter();
        triggerParams.objectName = '';
    }    
    
    public class TriggerParameter{
        public Boolean isInsert;
        public Boolean isUpdate;
        public Boolean isDelete;
        public Boolean isUndelete;
        public Boolean isBefore;
        public Boolean isAfter;
        public String objectName;
        public List<sObject> newList;
        public List<sObject> oldList;
        public Map<Id, sObject> newMap;
        public Map<Id, sObject> oldMap;
        public Map<String, Boolean> objByPassMap;
    }
    
    public void run(){
        triggerParams.objByPassMap = new Map<String, Boolean>();
        /* Bypass can be turned on for the entire org or user or profile using hierarchy custom setting.
           Using Object bypass custom setting, a method or full object can be bypassed*/
        if(Trigger.new != NULL){
            triggerParams.objectName = Trigger.new.get(firstRec).getSObjectType().getDescribe().getName();
        } else if(Trigger.old != NULL){
            triggerParams.objectName = Trigger.old.get(firstRec).getSObjectType().getDescribe().getName();
        }

        getByPassDetails();
        if(globalByPass || bypassFullObject){
            return;
        }
        
        /* Assign the right context variable to the variable that will be used in trigger handlers.
           Object name will be required to identify full bypass logic*/
        triggerParams.isInsert = Trigger.isInsert;
        triggerParams.isUpdate = Trigger.isUpdate;
        triggerParams.isDelete = Trigger.isDelete;
        triggerParams.isUndelete = Trigger.isUndelete;
        triggerParams.isBefore = Trigger.isBefore;
        triggerParams.isAfter = Trigger.isAfter;
        triggerParams.newList = Trigger.new;
        triggerParams.oldList = Trigger.old;
        triggerParams.newMap = Trigger.newMap;
        triggerParams.oldMap = Trigger.oldMap;
        
        // Call the appropriate method based on the right context
        if(Trigger.isBefore){
            if(Trigger.isInsert){
                this.beforeInsert();
            } else if(Trigger.isUpdate){
                this.beforeUpdate();
            } else if(Trigger.isDelete){
                this.beforeDelete();
            }
        } else if(Trigger.isAfter){
            if(Trigger.isInsert){
                this.afterInsert();
            } else if(Trigger.isUpdate){
                this.afterUpdate();
            } else if(Trigger.isDelete){
                this.afterDelete();
            } else if(Trigger.isUndelete){
                this.afterUndelete();
            }
        }
    }

    protected virtual void beforeInsert(){}
    protected virtual void beforeUpdate(){}
    protected virtual void beforeDelete(){}
    protected virtual void afterInsert(){}
    protected virtual void afterUpdate(){}
    protected virtual void afterDelete(){}
    protected virtual void afterUndelete(){}
    
    public void getByPassDetails(){
        if(Profile_Exclusion__c.getValues(UserInfo.getUserId()) != NULL){
            globalByPass = Profile_Exclusion__c.getValues(UserInfo.getUserId()).Bypass_Triggers__c;
        } else if(Profile_Exclusion__c.getValues(UserInfo.getProfileId()) != NULL){
            globalByPass = Profile_Exclusion__c.getValues(UserInfo.getProfileId()).Bypass_Triggers__c;
        } else if(Profile_Exclusion__c.getValues(UserInfo.getOrganizationId()) != NULL){
            globalByPass = Profile_Exclusion__c.getValues(UserInfo.getOrganizationId()).Bypass_Triggers__c;
        }
        if(!Object_Method_ByPass__c.getAll().isEmpty()){
            for(Object_Method_ByPass__c each: Object_Method_ByPass__c.getAll().values()){
                if(each.Object_Name__c.toLowerCase() == triggerParams.objectName.toLowerCase() && (each.User_Profile_ID__c == UserInfo.getUserId() || each.User_Profile_ID__c == UserInfo.getProfileId())){
                    if(each.Full_Bypass__c){
                        bypassFullObject = true;
                    }
                    if(each.Method_Name__c != NULL){
                        triggerParams.objByPassMap.put(each.Method_Name__c, each.Method_ByPass__c);
                    }
                }
            }
        }
    }
}