/********************************************************************************
 * Name        : BP_InstExtendedPipelineCtrl
 * Release     : R2
 * Phase       : P1
 * Description : Used for BP Insti Flow Extended Pipeline screen.
 * Author      : Deloitte
 * Reviewed By : 
 *********************************************************************************/
public with sharing class BP_InstExtendedPipelineCtrl{
    
    //Method to check if the Business Plan already has related Business Plan Opportunities
    @AuraEnabled
    public static boolean hasBusinessPlanOpp(String recId){
        boolean alreadyInsertedBPOppExtended;
        if(!String.isBlank(recId)){
            for(Business_Plan__c each: [select Id, Business_Plan_Oppor_Exten_Inserted__c from Business_Plan__c where Id =: recId]){
                alreadyInsertedBPOppExtended = each.Business_Plan_Oppor_Exten_Inserted__c;
            }
        }
        return alreadyInsertedBPOppExtended;
    }
    
    //Method to get opportunities from existing Business Plan Opportunities
    @AuraEnabled
    public static List<RelatedOpportunity> getBusinessPlanOpps(String recId){
        system.debug('inside getBusinessPlanOpps-->');
        List<RelatedOpportunity> RelatedOpportunities = new List<RelatedOpportunity>();
        Set<id> oppIdSet = new Set<id>();
        Date startingFiscalYear = Date.newInstance(Date.Today().year() + 1, 1, 1);
        Map<id, string> oppIdSeniorSalesMap = new Map<id, string>();
        
        List<string> stageName=new List<string>{'Lead','Qualified','Finals','Won Not-Funded'};
        
        if(!String.isBlank(recId)){
            for(Business_Plan__c bpRec : [select Starting_Fiscal_Year__c  from Business_Plan__c where id =: recId]){
                if(bpRec.Starting_Fiscal_Year__c != null){
                    startingFiscalYear = bpRec.Starting_Fiscal_Year__c;
                }
            }
            for(Business_Plan_Opportunity__c each: [select Id, Opportunity__c from Business_Plan_Opportunity__c where Business_Plan__c =: recId]){
                oppIdSet.add(each.Opportunity__c);
            }
        }
        system.debug('oppIdSet-->'+oppIdSet);
        
        if(!oppIdSet.isEmpty()){
            //get the opportunity team members with role 'Senior Sales'
            oppIdSeniorSalesMap = getSeniorSalesMap(oppIdSet);
            
            //get opportunity info to be displayed and add to list
            for(Opportunity opp : [select id, name, account.name, Scheme_Type__c,account.Prospect__c, account.Status__c, Third_Party_Name__r.name, Investment_Strategy__r.name, Investment_Strategy__c , CloseDate, Vehicle__c, StageName, Probability, Amount, Fee_Rate_bps__c, Estimated_Revenue__c, Estimated_Funding_Date__c, Consultant_Relations__r.name, Product_Manager__r.name from opportunity where id IN: oppIdSet and CloseDate >: startingFiscalYear and StageName in : StageName ORDER BY StageName DESC,Amount DESC NULLS LAST]){
                //system.debug('opp-->'+opp);
                if(!oppIdSeniorSalesMap.isEmpty() && oppIdSeniorSalesMap.containsKey(opp.id)){
                    //system.debug('if-->');
                    RelatedOpportunities.add(new RelatedOpportunity(opp.id, opp, opp.account.name, opp.account.Prospect__c, opp.Third_Party_Name__r.name, opp.Investment_Strategy__r.name, opp.Consultant_Relations__r.name, opp.Product_Manager__r.name, oppIdSeniorSalesMap.get(opp.id)));
                }else{
                    //system.debug('else-->');
                    RelatedOpportunities.add(new RelatedOpportunity(opp.id, opp, opp.account.name, opp.account.Prospect__c, opp.Third_Party_Name__r.name, opp.Investment_Strategy__r.name, opp.Consultant_Relations__r.name, opp.Product_Manager__r.name, oppIdSeniorSalesMap.get(opp.id)));
                }
            }            
        }
        system.debug('RelatedOpportunities-->'+RelatedOpportunities);
        return RelatedOpportunities;
    }
    
    //Method to get opportunities other than the ones added as Business Plan Opportunities
    @AuraEnabled
    public static List<RelatedOpportunity> getNonBusinessPlanOpps(String recId, boolean alreadyInsertedBPOpp){
        system.debug('inside getNonBusinessPlanOpps-->');
        system.debug('recId-->'+recId);
        system.debug('alreadyInsertedBPOpp-->'+alreadyInsertedBPOpp);
        List<RelatedOpportunity> nonBPOpportunities = new List<RelatedOpportunity>();
        Date startingFiscalYear = Date.newInstance(Date.Today().year() + 1, 1, 1);
        Set<id> existingOppIdSet = new Set<id>();
        Set<id> oppIdSet = new Set<id>();
        Map<id, String> oppIdSeniorSalesMap = new Map<id, String>();
        
        List<string> stageName=new List<string>{'Lead','Qualified','Finals','Won Not-Funded'};
        
        if(!String.isBlank(recId)){
            //get Starting_Fiscal_Year__c from Buiness Plan
            for(Business_Plan__c bpRec : [select Starting_Fiscal_Year__c from Business_Plan__c where id =: recId]){
                if(bpRec.Starting_Fiscal_Year__c != null){
                    startingFiscalYear = bpRec.Starting_Fiscal_Year__c;
                }
            }
            system.debug('startingFiscalYear-->'+startingFiscalYear);
            //get opportunities already added to Business Plan as Business Plan Opportunities
            for(Business_Plan_Opportunity__c each: [select Id, Opportunity__c from Business_Plan_Opportunity__c where Business_Plan__c =: recId]){
                existingOppIdSet.add(each.Opportunity__c);
            }
            system.debug('existingOppIdSet.size(), existingOppIdSet-->'+existingOppIdSet.size()+' : '+existingOppIdSet);
            
            //get opportunities where decision date < Business Plan's Starting_Fiscal_Year__c
            for(Opportunity opp : [select id, name, account.name,Account.Status__c ,Scheme_Type__c, account.Prospect__c, Third_Party_Name__r.name, Investment_Strategy__r.name, Investment_Strategy__c , CloseDate, Vehicle__c, StageName, Probability, Amount, Fee_Rate_bps__c, Estimated_Revenue__c, Estimated_Funding_Date__c, Consultant_Relations__r.name, Product_Manager__r.name from opportunity where CloseDate >: startingFiscalYear and StageName in : StageName ORDER BY StageName DESC,Amount DESC NULLS LAST]){
                //system.debug('opp-->'+opp);
                oppIdSet.add(opp.id);               
                
                //for a new business plan the flag will be false, adding all the opportunities to this list
                if(!alreadyInsertedBPOpp){
                    system.debug('inside if - alreadyInsertedBPOpp-->'+alreadyInsertedBPOpp);
                    nonBPOpportunities.add(new RelatedOpportunity(opp.id, opp, opp.account.name, opp.account.Prospect__c, opp.Third_Party_Name__r.name, opp.Investment_Strategy__r.name, opp.Consultant_Relations__r.name, opp.Product_Manager__r.name, oppIdSeniorSalesMap.get(opp.id)));
                }
                //for an existing business plan the flag will be true, display only the ones which which are not added to the section for Already added opportunities
                else{
                    if(existingOppIdSet.isEmpty() || (!existingOppIdSet.isEmpty() && !existingOppIdSet.contains(opp.id))){
                        system.debug('inside else - if - existingOppIdSet -->');
                        nonBPOpportunities.add(new RelatedOpportunity(opp.id, opp, opp.account.name, opp.account.Prospect__c, opp.Third_Party_Name__r.name, opp.Investment_Strategy__r.name, opp.Consultant_Relations__r.name, opp.Product_Manager__r.name, oppIdSeniorSalesMap.get(opp.id)));
                    }
                }
            }
            system.debug('oppIdSet.size(), oppIdSet--> '+oppIdSet.size()+' : '+oppIdSet);
            
            //get the opportunity team members with role 'Senior Sales'
            if(!oppIdSet.isEmpty()){
                oppIdSeniorSalesMap = getSeniorSalesMap(oppIdSet);
            }
            
            //update the list with Senior Sales information
            if(oppIdSeniorSalesMap.isEmpty()){
                for(RelatedOpportunity each : nonBPOpportunities){
                    if(oppIdSeniorSalesMap.containsKey(each.oppId)){
                        each.seniorSales = oppIdSeniorSalesMap.get(each.oppId);
                    }
                }
            }
        }
        system.debug('nonBPOpportunities , size --> '+nonBPOpportunities.size()+'   '+ nonBPOpportunities);
        return nonBPOpportunities;
    }
    
    //Method to insert Business Plan Opportunities
    @AuraEnabled
    public static void insertBusinessPlanOpportunities(Id recId, String nonBPOpportunitiesJSONStr){     
        List<RelatedOpportunity> nonBPOpportunities =  (List<RelatedOpportunity>) System.JSON.deserialize(nonBPOpportunitiesJSONStr, List<RelatedOpportunity>.class);
        system.debug('inside insertBusinessPlanOpportunities : nonBPOpportunities-->' + nonBPOpportunities);
        List<Business_Plan_Opportunity__c> bpOppToInsertList = new List<Business_Plan_Opportunity__c>();
        Business_Plan_Opportunity__c bpOpp;
        
        for(RelatedOpportunity each : nonBPOpportunities){
            system.debug('abc-->');
            bpOpp = new Business_Plan_Opportunity__c();
            bpOpp.Opportunity__c = each.oppId;
            bpOpp.Business_Plan__c = recId;
            bpOppToInsertList.add(bpOpp);
        }
        if(!bpOppToInsertList.isEmpty()){
            List<Database.SaveResult> srList = Database.insert(bpOppToInsertList, false);
            for(Database.SaveResult sr : srList){
                if(!sr.isSuccess()){
                    system.debug('An error occured : '+ sr.getErrors()[0]);
                }
            }
        }
    }
    
    //Method to update Business Plan field Business_Plan_Opportunities_Inserted__c
    @AuraEnabled
    public static void updateBusinessPlan(Id recId){
        system.debug('inside updateBusinessPlan-->');
        Business_Plan__c bpRec = new Business_Plan__c();
        bpRec.id = recId;
        bpRec.Business_Plan_Oppor_Exten_Inserted__c = true;
        
        update bpRec;
    }
    
    //method to get the Team Members with the role 'Senior Sales'
    public static Map<id, string> getSeniorSalesMap(Set<id> oppIdSet){
        
        string tempStr;
        Map<id, string> oppIdSeniorSalesMap = new Map<id, string>();
        
        //get senior sales from opp team
        for(OpportunityTeamMember teamMember : [select opportunityId, name from OpportunityTeamMember where opportunityId IN: oppIdSet and TeamMemberRole='Senior Sales']){
            tempStr = '';
            if(!oppIdSeniorSalesMap.isEmpty() && oppIdSeniorSalesMap.containsKey(teamMember.opportunityId)){
                tempStr = oppIdSeniorSalesMap.get(teamMember.opportunityId);
                tempStr = tempStr +', '+ teamMember.name;
                oppIdSeniorSalesMap.put(teamMember.opportunityId, tempStr);
            }else{
                oppIdSeniorSalesMap.put(teamMember.opportunityId, teamMember.name);
            }
        }
        system.debug('oppIdSeniorSalesMap-->'+oppIdSeniorSalesMap);
        return oppIdSeniorSalesMap;
    }
    
    public class RelatedOpportunity{        
         @AuraEnabled
        public String oppId;
        @AuraEnabled
        public Opportunity oppRec;
        @AuraEnabled
        public String clientName;
        @AuraEnabled
        public Boolean prospect;
        @AuraEnabled
        public String prospectText;
        @AuraEnabled
        public String thirdPartyName;
        @AuraEnabled
        public String investmentStrategy;
        @AuraEnabled
        public String consultantRelations;
        @AuraEnabled
        public String productManager;
        @AuraEnabled
        public String seniorSales;
        
        public RelatedOpportunity(string oId, opportunity opp, string client, boolean prospect, string tp, string invSt, string conRel, string prodMgr, string ss){
            this.oppId = oId;
            this.oppRec = opp;
            this.clientName = client;
            this.prospect = prospect;
            this.prospectText = prospect? 'Prospect' : 'Client';
            this.thirdPartyName = tp;
            this.investmentStrategy = invSt;
            this.consultantRelations = conRel;
            this.productManager = prodMgr;
            this.seniorSales = ss;
        }
    }
}