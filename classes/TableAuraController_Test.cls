/* Test class for TableAuraController
   Author: Joshna
   Created on: Nov 9
*/
@isTest
public class TableAuraController_Test{
    static testMethod void myUnitTestMethod(){
        User testUser = CommonTestUtils.createTestUser();
        testUser.FirstName = 'NamewithLetterA';
        update testUser;
        
        List<TableAuraController.ObjectResult> testResults = TableAuraController.getRecords('User', '["Id","FirstName","LastName"]', 'isActive=true', 'FirstName', '10', 
                        'A', '["FirstName","LastName"]', new List<Id>{UserInfo.getUserId()}, 'Id', 'Id', new List<Id>());
        System.assert(testResults.size() > 0, 'Success');
        
        List<TableAuraController.ObjectResult> testResults1 = TableAuraController.getRecords('User', '["Id","FirstName","LastName"]', '', 'FirstName', '', 
                        'A', '["FirstName","LastName"]', new List<Id>{UserInfo.getUserId()}, 'Id', 'Id', new List<Id>());
        System.assert(testResults1.size() > 0, 'Success');
        
        List<TableAuraController.ObjectResult> testResults2 = TableAuraController.getRecords('User', '["Id","FirstName","LastName","Profile.Name","UserRole.Name"]', '', 'FirstName', '', 
                        '', '["FirstName","LastName"]', new List<Id>{UserInfo.getUserId()}, 'Id', 'Id', new List<Id>());
        System.assert(testResults2.size() > 0, 'Success');
        
        List<TableAuraController.ObjectResult> testResults3 = TableAuraController.getRecords('User', '["Id","FirstName","LastName","Profile.Name","UserRole.Name"]', '', 'FirstName', '', 
                        '', '', new List<Id>{}, '', 'Id', new List<Id>{UserInfo.getUserId()});
        System.assert(testResults3.size() > 0, 'Success');
    }
}