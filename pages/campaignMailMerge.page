<apex:page standardController="Campaign" extensions="campaignMailMergeController" contentType="application/msWord#{!docName}.doc">
<html>
        <head>
            <style type="text/css" media="print">
             body{
                font-family: Arial Unicode MS;
                font-size:11px;
             }
             td.header1 {
                font-family: Arial Unicode MS;
                font-size: 16px;
                font-weight: bolder;
                color: black;
                border-bottom:solid #230470;
                padding-left:6px;
                margin-bottom:2px;
             }
             td.valField{
                text-align:left;
                padding:2px;            
             }
             table.tabVal{
                border-color: black;
                border-style: solid;
               
                border-width: thin;
                border-spacing: 0px;
             
             }
             table.tabVal td{
                border-width: thin;
                padding: 0px;
                border-style: solid;
                border-color: black;
                background-color: white;
             }
             table.tabVal th{
                border-width: thin;
                padding: 0px;
                border-style: solid;
                border-color: black;
                background-color: white;
             
             }
             table.rPort {
                border-width: 1px;
                border-spacing: 0px;
                border-style: solid;
                border-color: #C0C0C0;
                border-collapse: separate;
                background-color: white;
            }
             table.rPort td {
                border-width: 1px;
                padding: 0px;
                border-style: solid;
                border-color: #BBBABB;
                background-color: white;
            }
            @media print
            {
                table {page-break-inside:auto }
                tr    { page-break-inside:avoid; page-break-after:auto }
                thead {display:table-header-group }
            }
          </style>
      </head>
      <body class="body">
        <table width="100%" border="0">
            <tr>
                <td colspan="4" align="center" style="color:black;font-weight:bolder;">
                    <h1>
                        <apex:image value="{!$Label.Mail_Merge_Url}" width="150" height="150"/>
                    </h1>
                </td>
            </tr>
            <tr>
                <td colspan="4" align="center" style="color:black;font-weight:bolder;"><h1><font size="4">Strategic Marketing Brief</font></h1></td>
            </tr>
        </table>
        <apex:outputPanel >
            <table width="100%" border="0" style="border-collapse: collapse">
                <tr>
                    <td><br/></td>
                </tr>
                <tr>
                    <td class="header1" colspan="4" valign="bottom">Report Details</td>
                </tr>
            </table>
            <br/>
            <table width="100%" border="1" style="border-collapse: collapse" class="rPort">
                <tr width="100%">
                    <td width="25%" style="padding:3px;font-weight:bolder;">Title of brief</td>
                    <td width="75%" colspan="3" align="left" style="background-color:#F3F3F3;color:#000000;text-align:left;padding:3px;">
                        <apex:outputField value="{!camp.Name}"/>
                    </td>                   
                </tr>
                <tr/>
                <tr width="100%">
                    <td width="25%" style="padding:3px;font-weight:bolder;">Campaign Reference Number</td>
                    <td width="25%" align="left" style="background-color:#F3F3F3;color:#000000;text-align:left;padding:3px;">
                        <apex:outputField value="{!camp.Campaign_reference_number__c}"/>
                    </td>
                    <td width="25%" style="padding:3px;font-weight:bolder;">Briefing date</td>
                    <td width="25%" align="left" style="background-color:#F3F3F3;color:#000000;text-align:left;padding:3px;">
                        <apex:outputField value="{!camp.Briefing_Date__c}"/>
                    </td>
                </tr>
                <tr/>
                <tr width="100%">                   
                    <td style="padding:3px;font-weight:bolder;">Marketing Owner</td>
                    <td align="left" style="background-color:#F3F3F3;color:#000000;text-align:left;padding:3px;">
                        <apex:outputField value="{!camp.Owner.Name}"/>
                    </td>
                    <td style="padding:3px;font-weight:bolder;">Business unit</td>
                    <td align="left" style="background-color:#F3F3F3;color:#000000;text-align:left;padding:3px;">
                        <apex:outputText value="{!businessLine}"/>
                    </td>
                </tr>
                <tr/>
                <tr width="100%">
                    <td style="padding:3px;font-weight:bolder;">Primary purpose of activity</td>
                    <td align="left" style="background-color:#F3F3F3;color:#000000;text-align:left;padding:3px;">
                           
                    </td>
                    <td style="padding:3px;font-weight:bolder;">Strategic context / rationale</td>
                    <td align="left" style="background-color:#F3F3F3;color:#000000;text-align:left;padding:3px;">
                        <apex:outputField value="{!camp.Strategic_Context_Rationale__c}"/>
                    </td>
                </tr>
                <tr width="100%">   
                    <td style="padding:3px;font-weight:bolder;">Target audience insight</td>
                    <td align="left" style="background-color:#F3F3F3;color:#000000;text-align:left;padding:3px;">
                        <apex:outputField value="{!camp.Target_Audience_Insight__c}"/>
                    </td>
                    <td style="padding:3px;font-weight:bolder;">Defined elements </td>
                    <td align="left" style="background-color:#F3F3F3;color:#000000;text-align:left;padding:3px;">
                       
                    </td>   
                </tr>
            </table>    
        </apex:outputPanel>
        <apex:outputPanel >
            <table width="100%" border="0" style="border-collapse: collapse">
                <tr>
                    <td><br/></td>
                </tr>
                <tr>
                    <td class="header1" colspan="4" valign="bottom">Background</td>
                </tr>
            </table>
            <apex:outputField value="{!camp.Background__c}"/><br/>
            <apex:outputField value="{!camp.Description}"/><br/>
        </apex:outputPanel>
        <apex:outputPanel >
            <table width="100%" border="0" style="border-collapse: collapse">
                <tr>
                    <td><br/></td>
                </tr>
                <tr>
                    <td class="header1" colspan="4" valign="bottom">Key delivery dates </td>
                </tr>
            </table>
            <table width="100%" border="1" style="border-collapse: collapse" class="rPort">
                <tr width="100%">
                    <td width="25%" style="padding:3px;font-weight:bolder;">Campaign Briefing Date</td>
                    <td width="75%" colspan="3" align="left" style="background-color:#F3F3F3;color:#000000;text-align:left;padding:3px;">
                        <apex:outputField value="{!camp.Briefing_Date__c}"/>
                    </td>                   
                </tr>
                <tr/>
                <tr width="100%">
                    <td width="25%" style="padding:3px;font-weight:bolder;">Campaign Start Date</td>
                    <td width="75%" colspan="3" align="left" style="background-color:#F3F3F3;color:#000000;text-align:left;padding:3px;">
                        <apex:outputField value="{!camp.StartDate}"/>
                    </td>                   
                </tr>
                <tr/>
                <tr width="100%">
                    <td width="25%" style="padding:3px;font-weight:bolder;">Campaign End Date</td>
                    <td width="75%" colspan="3" align="left" style="background-color:#F3F3F3;color:#000000;text-align:left;padding:3px;">
                        <apex:outputField value="{!camp.EndDate}"/>
                    </td>                   
                </tr>
            </table>
        </apex:outputPanel>
    </body>     
  </html>
</apex:page>