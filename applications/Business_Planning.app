<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <brand>
        <headerColor>#080808</headerColor>
        <logo>BP_Logo</logo>
        <logoVersion>1</logoVersion>
    </brand>
    <description>An app for the Sales users to see all the business planning related tools in one area for business planning activities</description>
    <formFactors>Large</formFactors>
    <label>Business Planning</label>
    <navType>Standard</navType>
    <tab>standard-Account</tab>
    <tab>standard-Opportunity</tab>
    <tab>Region__c</tab>
    <tab>Business_Plan__c</tab>
    <tab>standard-Dashboard</tab>
    <tab>standard-report</tab>
    <tab>standard-Campaign</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Product2</tab>
    <tab>standard-Task</tab>
    <tab>Vault__c</tab>
    <uiType>Lightning</uiType>
</CustomApplication>
